<?php
/**
 * @version		$Id: default.php 
 * @package		Joomla.Site
 * @subpackage	mod_vm2tags_cloud
 * @copyright	Copyright (C) 2010 - 2015 Nordmograph.com, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$min_font_size 	= $params->get( 'min_font', '10');
$max_font_size 	= $params->get( 'max_font', '25');
$limit 			= $params->get( 'limit', '50');

$cparams 					= JComponentHelper::getParams('com_vm2tags');
$mode	 					= $cparams->get('mode','1');
$vmvendor_filter 			= $cparams->get('vmvendor_filter',0);
$vm2tags_itemid 			= $cparams->get('vm2tags_itemid');
if(!$vm2tags_itemid)
{
	require_once JPATH_SITE.'/components/com_vm2tags/helpers/vm2tags.php';
	$vm2tags_itemid = Vm2tagsHelper::getVM2TItemid();
}	
if($vm2tags_itemid=='')
	$vm2tags_itemid = JFactory::getApplication()->input->getInt('Itemid');

$minimum_count = 0;
$maximum_count = 0;
$thelist ='';
if(count($list)>0)
{
	
	foreach($list as $set_tag)
	{
			$thelist .= trim($set_tag->metakey).',';

		
	}
	//var_dump($list);
	$thelist = mb_strtolower($thelist);
	
	$thelist = trim($thelist);
	//$thelist = str_replace(' ','',$thelist);
	
	$thelist = substr($thelist,0,-1);
	
	$expls = explode(",", $thelist);
	$x = array_count_values($expls);
	//var_dump($expls);
	foreach ($expls as $expl) //loop through the object and find the highest and lowest values
	{
		if(isset($x[$expl])){
			if ($minimum_count > $x[$expl]) 
				$minimum_count = $x[$expl];
			if ($maximum_count < $x[$expl])
				$maximum_count = $x[$expl];	
		}
	}
	$spread = $maximum_count - $minimum_count; //figure out the difference between the highest and lowest values
	if($spread == 0) 
		$spread = 1;
	$i=0;

	echo '<div style="text-align:center;" >';
	$key = key($x);
	$value = current($x);
	
	function myEach(&$arr) {  // each() being deprecated as for php 7.2
		$key = key($arr);
		$result = ($key === null) ? false : [$key, current($arr), 'key' => $key, 'value' => current($arr)];
		next($arr);
		return $result;
	}

	while (list( $key, $value) = myEach($x))
	{
		$i++;
		$size = $min_font_size + ( $value - $minimum_count) * ($max_font_size - $min_font_size) / $spread;
		if ($value>0 && $i <= $limit && $key!=''){
			//$key = json_decode('"'.$key.'"');
			$key = trim($key);
			$sef_tag = str_replace('-','--',$key );
			$sef_tag = str_replace(' ','-',$sef_tag );
			$sef_tag = mb_strtolower( $sef_tag );
			if($mode==1)
			{
				$tag_url = 'index.php?option=com_vm2tags&view=productslist&tag='.$sef_tag;
				if($vmvendor_filter)
					$tag_url .= '&vendorid='.$set_tag->virtuemart_vendor_id;
				$tag_url .= '&Itemid='.$vm2tags_itemid ;
				
			}
			else
			{
					$tag_url = 'index.php?searchword='.$sef_tag.'&ordering=newest&searchphrase=exact&option=com_search' ;
					$tag_url .= '&Itemid='.$vm2tags_itemid ;
			}
			//echo $tag_url ;
			$tag_url = JRoute::_($tag_url );
			echo ' <a class="btn btn-mini hasTooltip" href="'.$tag_url.'"  ><span style="font-size:'. floor($size) .'px">'.$key.'</span></a>';
		}
	}
	echo '</div>';
}
?>