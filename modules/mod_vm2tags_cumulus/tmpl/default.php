<?php
/**
 * @version		$Id: default.php 
 * @package		Joomla.Site
 * @subpackage	mod_vm2tags_cloud
 * @copyright	Copyright (C) 2005 - 2012 Nordmograph.com, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$juri 				= JURI::base();
$doc 				= JFactory::getDocument();
$doc->addScript($juri.'modules/mod_vm2tags_cumulus/js/jquery.tagcanvas.min.js');




$limit          = $params->get( 'limit', '50');
$width          = $params->get( 'width', '200');
$height         = $params->get( 'height', '200');
$speed          = $params->get( 'speed', '150');
$tcolor         = $params->get( 'tcolor','0077b3');
$tcolor         = str_replace('#','', $tcolor);

$bgcolor        = $params->get( 'bgcolor');
$bgcolor        = str_replace('#','', $bgcolor);

$outlinecolor   = $params->get( 'outlinecolor');
$outlinecolor   = str_replace('#','', $outlinecolor);
$cloudshapes    = $params->get( 'cloudshapes', 'sphere');
$lock           = $params->get( 'lock');
$offsetx        = $params->get( 'offsetx');
$offsety        = $params->get( 'offsety');
$zoom           = $params->get( 'zoom','1.0');
$minzoom        = $params->get( 'minzoom','0.3');
$maxzoom        = $params->get( 'maxzoom','3.0');
$zoomstep       = $params->get( 'zoomstep','0.05');
$maxspeed       = $params->get( 'maxspeed','0.05');
$outlinethickness  = $params->get( 'outlinethickness','1');

$cparams        = JComponentHelper::getParams('com_vm2tags');
$mode           = $cparams->get('mode','1');

$vmvendor_filter= $cparams->get('vmvendor_filter',0);
    
$vm2tags_itemid = $cparams->get('vm2tags_itemid');
if(!$vm2tags_itemid)
{
	require_once JPATH_SITE.'/components/com_vm2tags/helpers/vm2tags.php';
	$vm2tags_itemid = Vm2tagsHelper::getVM2TItemid();
}	
if($vm2tags_itemid=='')
	$vm2tags_itemid = JFactory::getApplication()->input->getInt('Itemid');

$minimum_count = 0;
$maximum_count = 0;
$array = array();
$thelist ='';
if(count($list)>0)
{
  foreach($list as $set_tag)
  {
    $thelist .= trim($set_tag->metakey).',';
  }
  $thelist = mb_strtolower($thelist);
  //$thelist = str_replace(' ','',$thelist);
  $thelist = substr($thelist,0,-1);
  $expls = explode(",", $thelist);
  $x = array_count_values($expls);

  foreach ($expls as $expl) //loop through the object and find the highest and lowest values
  {
    if(isset($x[$expl])){
      if ($minimum_count > $x[$expl]) 
        $minimum_count = $x[$expl];
      if ($maximum_count < $x[$expl])
        $maximum_count = $x[$expl]; 
    }
  }
  $spread = $maximum_count - $minimum_count; //figure out the difference between the highest and lowest values
  if($spread == 0) 
    $spread = 1;
  $i=0;

  $script ="jQuery(document).ready(function() {
   if( ! jQuery('#vm2tagsCanvas').tagcanvas({
    shape: '".$cloudshapes."', ";
  if($lock)
    $script .= "lock:'".$lock."',";
   $script .= "textHeight: 25,
    textColour: '#".$tcolor."', ";
    if($outlinecolor)
    $script .= "outlineColour: '#".$outlinecolor."',";
  if($bgcolor)
    $script .= "bgColour: '#".$bgcolor."',";

  if($zoom)
    $script .= "zoom: ".$zoom.",";
  if($minzoom)
    $script .= "minzoomMin: ".$minzoom.",";
  if($maxzoom)
    $script .= "zoomMax: ".$maxzoom.",";
  if($zoomstep)
    $script .= "zoomStep: ".$zoomstep.",";
  if($offsetx)
    $script .= "offsetX: ".$offsetx.",";
  if($offsetx)
    $script .= "offsetY: ".$offsety.",";
  if($maxspeed)
    $script .= "maxSpeed: ".$maxspeed.",";

// more options at http://www.goat1000.com/tagcanvas-options.php
    $script .="shadow:'#000000',
    reverse: true,
    shuffleTags:true,
    depth: 0.8,
     outlineThickness : ".$outlinethickness."
   })) {
     // TagCanvas failed to load
     jQuery('#vm2tagsCanvasContainer').hide();
   }
   // your other jQuery stuff here...
 });

function ResponsiveTCforIE(c) {
  var e = document.getElementById(c), rw;
  if(e && e.height && e.width) {
    rw = parseInt( document.defaultView.getComputedStyle( e ).getPropertyValue( 'width' ) );
    e.style.height = (rw * e.height / e.width) + 'px';
  }
}
 
if(document.all && document.addEventListener) {
  window.addEventListener('resize', function() {
    ResponsiveTCforIE('vm2tagsCanvas');
  }, false);
  window.addEventListener('load', function() {
    ResponsiveTCforIE('vm2tagsCanvas');
  }, false);
}";
  $doc->addScriptDeclaration($script);



  echo '<div id="vm2tagsCanvasContainer">
 <canvas width="'.$width.'" height="'.$height.'" id="vm2tagsCanvas">
  <p>Anything in here will be replaced on browsers that support the canvas element</p>
  <ul>';
  while (list($key, $value) = each($x))
  {
    $i++;
    $size = $min_font_size + ( $value - $minimum_count) * ($max_font_size - $min_font_size) / $spread;
    if ($value>0 && $i <= $limit && $key!=''){
      	$key = json_decode('"'.$key.'"');
	  	$sef_tag = str_replace('-','--',$key );
		$sef_tag = str_replace(' ','-',$sef_tag );
		$sef_tag = mb_strtolower( $sef_tag );
      if($mode==1)
      {
        $tag_url = 'index.php?option=com_vm2tags&view=productslist&tag='.$sef_tag;
        if($vmvendor_filter)
          $tag_url .= '&vendorid='.$set_tag->virtuemart_vendor_id;
        $tag_url .= '&Itemid='.$vm2tags_itemid ;
        
      }
      else
      {
          $tag_url = 'index.php?searchword='.$sef_tag.'&ordering=newest&searchphrase=exact&option=com_search' ;
          $tag_url .= '&Itemid='.$vm2tags_itemid ;
      }
      $tag_url = JRoute::_($tag_url );
      echo '<li><a href="'.$tag_url.'" >'.$key.'</a></li>';

    }
  }
  echo '</ul>
  </canvas>
  </div>';
}

?>