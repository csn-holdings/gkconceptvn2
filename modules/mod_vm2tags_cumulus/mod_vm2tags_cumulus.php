<?php
/**
 * @version		$Id:mod_vm2tags_cumulus.php 
 * @package		Joomla.Site
 * @subpackage	mod_vm2tags_cumulus
 * @copyright	Copyright (C) 2005 - 2012 Nordmograph.com, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined('_JEXEC') or die;
$app	= JFactory::getApplication();
$view	= $app->input->get('view');
if($view=='cart' || $view=='user')
	return false;
$cparams 			= JComponentHelper::getParams('com_vm2tags');

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';
$list = modVm2tagsCumulusHelper::getList($params);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
require JModuleHelper::getLayoutPath('mod_vm2tags_cumulus', $params->get('layout', 'default'));