<?php
	
	defined('_JEXEC') or die('Restricted access');
	JHtml::_('behavior.modal');
	
	$js = "
jQuery(document).ready(function () {
	jQuery('.orderlistcontainer').hover(
		function() { jQuery(this).find('.orderlist').stop().show()},
		function() { jQuery(this).find('.orderlist').stop().hide()}
	)

	// Click Button
	function display(view) {
		jQuery('.browse-view .row').removeClass('vm-list vm-grid').addClass(view);
		jQuery('.icon-list-grid .vm-view').removeClass('active');
		if(view == 'vm-list') {

			jQuery('.browse-view .product').addClass('col-lg-12 product-full');
			jQuery('.browse-view .product .product-left').addClass('col-md-4');
			jQuery('.browse-view .product .product-right').addClass('col-md-8');
			jQuery('.icon-list-grid .' + view).addClass('active');
		}else{
			jQuery('.browse-view .product').removeClass('col-lg-12 product-full');
			jQuery('.browse-view .product .product-left').removeClass('col-md-4');
			jQuery('.browse-view .product .product-right').removeClass('col-md-8');
			jQuery('.icon-list-grid .' + view).addClass('active');
		}
	}

	jQuery('.vm-view-list .vm-view').each(function() {
		var ua = navigator.userAgent,
		event = (ua.match(/iPad/i)) ? 'touchstart' : 'click';
		 jQuery('.vm-view-list .vm-view').bind(event, function() {
			jQuery(this).addClass(function() {
				if(jQuery(this).hasClass('active')) return '';
				return 'active';
			});
			jQuery(this).siblings('.vm-view').removeClass('active');
			catalog_mode = jQuery(this).data('view');
			display(catalog_mode);

		});

	});
});
";
	

	
	
 
	
	function html_cut($text, $max_length)
	{
		$tags = array();
		$result = "";
		
		$is_open = false;
		$grab_open = false;
		$is_close = false;
		$in_double_quotes = false;
		$in_single_quotes = false;
		$tag = "";
		
		$i = 0;
		$stripped = 0;
		
		$stripped_text = strip_tags($text);
		
		while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length) {
			$symbol = $text{$i};
			$result .= $symbol;
			
			switch ($symbol) {
				case '<':
					$is_open = true;
					$grab_open = true;
					break;
				
				case '"':
					if ($in_double_quotes)
						$in_double_quotes = false;
					else
						$in_double_quotes = true;
					
					break;
				
				case "'":
					if ($in_single_quotes)
						$in_single_quotes = false;
					else
						$in_single_quotes = true;
					
					break;
				
				case '/':
					if ($is_open && !$in_double_quotes && !$in_single_quotes) {
						$is_close = true;
						$is_open = false;
						$grab_open = false;
					}
					
					break;
				
				case ' ':
					if ($is_open)
						$grab_open = false;
					else
						$stripped++;
					
					break;
				
				case '>':
					if ($is_open) {
						$is_open = false;
						$grab_open = false;
						array_push($tags, $tag);
						$tag = "";
					} else if ($is_close) {
						$is_close = false;
						array_pop($tags);
						$tag = "";
					}
					
					break;
				
				default:
					if ($grab_open || $is_close)
						$tag .= $symbol;
					
					if (!$is_open && !$is_close)
						$stripped++;
			}
			
			$i++;
		}
		
		while ($tags)
			$result .= "</" . array_pop($tags) . ">";
		
		return $result;
	}
	
	vmJsApi::addJScript('vm.hover', $js);

	if (!function_exists('loadImg'))
	{
		function loadImg($path, $replacement = 'nophoto.jpg')
		{
			return (file_exists($path) || @getimagesize($path) !== false) ? $path : 'images/' . $replacement;
		}
	}
	
	if(isset($_GET['discount']) && $_GET['discount'])
	{
		if (VmConfig::get('showCategory', 1) and empty($this->keyword))
		{
			if (!empty($this->category->haschildren))
			{
				
				echo ShopFunctionsF::renderVmSubLayout('categories', array('categories' => $this->category->children, 'category_parent' => $this->category));
				
			}
		}
		?>
		<div class="row" style="margin: 30px;">
			<div class="col-sm-4 productdetails-chinhsach-img">
				<a href="https://gkconcept.vn/chinh-sach-hau-mai/chinh-sach-bao-hanh-va-dich-vu">
					<img src="images/stories/virtuemart/product/BANNER-02.jpg" alt="chinh sach">
				</a>
			</div>
			<div class="col-sm-4 productdetails-chinhsach-img">
				<a href="https://gkconcept.vn/chinh-sach-hau-mai/chinh-sach-van-chuyen-gkconcept" >
					<img src="images/stories/virtuemart/product/BANNER-03.jpg" alt="chinh sach">
				</a>
			</div>
			<div class="col-sm-4 productdetails-chinhsach-img">
				<a href="https://gkconcept.vn/chinh-sach-hau-mai/chinh-sach-giai-quyet-khieu-nai" >
					<img src="images/stories/virtuemart/product/BANNER-01.jpg" alt="chinh sach">
				</a>
			</div>
		</div>
		
			<div class="browse-view">
			<div class="col-md-9 my-4">
			<?php echo shopFunctionsF::renderVmSubLayout('products_categories_discount', array()); ?>
				<div class="orderby-displaynumber top">
					
					<div class="clearfix"></div>
				</div>
				</div>
				<div class="col-md-3 my-4">
					<?php
						$modules =& JModuleHelper::getModules('right_detail');
						foreach ($modules as $module) {
							echo JModuleHelper::renderModule($module);
						}
					?>
				</div>
				<style>
					img.featuredProductImage {
						width: 100px;
						float: left;
						margin: 0px 20px 20px 0px;
					}

					.vmproduct {
						background-color: #fff;
						/* border: black 1px solid; */
						/* margin: 0px 0px 2px 0px; */
						border-radius: 10px;
						padding: 10px;
					}

					.vmgroup {
						background-color: #fff;
						border: black 1px solid;
						/*border-radius: 10px;*/
					}
					.mobilemenuck-bar-button {
						position: relative !important;
						float: left !important;
						left: 10px !important;
						top: 15px !important;
					}
				</style>

				</div>
				
				<?php
	}
	else
	{
		if (empty($this->keyword) and !empty($this->category) and !empty($this->category->category_name))
		{
		
			$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $this->category->virtuemart_category_id);
			$status = $this->category->category_description ? true : false ;
			if($status == false)
			{
				$category_description = 'GKconcept- với mẫu mã và dịch vụ đa dạng, sáng tạo đứng đầu Việt Nam về sản phẩm nội thất, trang trí và gia dụng được làm hoàn toàn từ gỗ tự .
nhiên, cho bạn thỏa mãn đam mê kiến tạo không gian sống, làm viêc và tận hưởng, khẳng định gu thẩm mỹ cá nhân không thể nhầm lẫn';
			}
			else
			{
				$category_description = $this->category->category_description;
			}
		
			?>
			<a href="<?= $caturl ?>">
				<h1 class="cat_title"><?= $this->category->customtitle ?  $this->category->customtitle : $this->category->category_name ?></h1>
			</a>
			<div class="clearfix"></div>
			<div class="category_description_2" style="margin-top: 10px">
				<p>
					<?php echo $this->category->category_description_2; ?>
				</p>
			</div>
		<?php
		}
		if (VmConfig::get('showCategory', 1) and empty($this->keyword))
		{
			if (!empty($this->category->haschildren)) {
			
				echo ShopFunctionsF::renderVmSubLayout('categories', array('categories' => $this->category->children, 'category_parent' => $this->category));
			
			}
		}
		?>
		<div class="row" style="margin: 30px;">
			<div class="col-sm-4 productdetails-chinhsach-img">
				<a href="https://gkconcept.vn/chinh-sach-hau-mai/chinh-sach-bao-hanh-va-dich-vu">
					<img src="images/stories/virtuemart/product/BANNER-02.jpg" alt="chinh sach">
				</a>
			</div>
			<div class="col-sm-4 productdetails-chinhsach-img">
				<a href="https://gkconcept.vn/chinh-sach-hau-mai/chinh-sach-van-chuyen-gkconcept" >
					<img src="images/stories/virtuemart/product/BANNER-03.jpg" alt="chinh sach">
				</a>
			</div>
			<div class="col-sm-4 productdetails-chinhsach-img">
				<a href="https://gkconcept.vn/chinh-sach-hau-mai/chinh-sach-giai-quyet-khieu-nai" >
					<img src="images/stories/virtuemart/product/BANNER-01.jpg" alt="chinh sach">
				</a>
			</div>
		</div>

		<?php
	
		if ($this->showproducts)
		{
		
			?>

			<div class="browse-view">
			<?php
			$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $this->category->virtuemart_category_id);
			?>
			<div class="col-md-9 my-4">
<!--			<a href="--><?//= $caturl ?><!--">-->
<!--				<h1 class="cat_title">--><?php //echo $this->category->category_name; ?><!--</h1>-->
<!--			</a>-->
		
			<?php
			if (!empty($this->products) && empty($this->products['search_product'])) {
				$products = array();
				$products[0] = $this->products;
				echo shopFunctionsF::renderVmSubLayout('products_categories', array('products' => $products, 'currency' => $this->currency, 'products_per_row' => $this->perRow, 'showRating' => $this->showRating));
				?>
				<div class="orderby-displaynumber top">
					<div class="row">
						<div class="toolbar-center col-md-6 col-sm-10">
							<div class="display-number">
								<div class="title"><?php echo 'Hiển Thị:'; ?></div>
								<div class="display-number-list"><?php echo $this->vmPagination->getLimitBox($this->category->limit_list_step); ?></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="vm-pagination vm-pagination-top col-md-4 col-sm-12">
							<?php echo $this->vmPagination->getPagesLinks(); ?>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				</div>
				<div class="col-md-3 my-4">
					<div class="row">
						<div>
							<div class="vmproduct">
								<div class="product-img">
									<a href="https://gkconcept.vn/qua-tang-mieng-lot-dien-thoai" title="Logo 1">
										<img class="logo"  src="images/stories/virtuemart/product/4d523a8cd6af24f17dbe.jpg" alt="Logo 1">
									</a>
								</div>
								<div class="product-img">
									<a href="https://gkconcept.vn/qua-tang-ebook" title="Logo 1">
										<img class="logo"  src="images/stories/virtuemart/product/27705937b514474a1e05.jpg" alt="Logo 1">
									</a>
								</div>
								<div class="product-img">
									<a href="https://gkconcept.vn/trai-nghiem-san-pham" title="Logo 1">
										<img class="logo"  src="images/stories/virtuemart/product/3b53b1bc5a9fa8c1f18e.jpg" alt="Logo 1">
									</a>
								</div>
								<style>
									.logo{
										width: max-content;
										float: left;
										margin: 10px;
										height: revert;
									}
								</style>
							</div>
						</div>
					</div>
					<?php
						$modules =& JModuleHelper::getModules('right_detail');
						foreach ($modules as $module) {
							echo JModuleHelper::renderModule($module);
						}
					?>
				</div>
				<style>
					img.featuredProductImage {
						width: 100px;
						float: left;
						margin: 0px 20px 20px 0px;
					}

					.vmproduct {
						background-color: #fff;
						/* border: black 1px solid; */
						/* margin: 0px 0px 2px 0px; */
						border-radius: 10px;
						padding: 10px;
					}

					.vmgroup {
						background-color: #fff;
						border: black 1px solid;
						/*border-radius: 10px;*/
					}
					.mobilemenuck-bar-button {
						position: relative !important;
						float: left !important;
						left: 10px !important;
						top: 15px !important;
					}
				</style>

				</div>
			
				<?php
			}
			elseif (!empty($this->products['search_product']))
			{
				$products = array();
				$products[0] = $this->products['search_product'];
				echo shopFunctionsF::renderVmSubLayout('products_categories_search', array('products' => $products, 'currency' => $this->currency, 'products_per_row' => $this->perRow, 'showRating' => $this->showRating));
			
				?>
				<div class="orderby-displaynumber top">
					<div class="row">
						<div class="toolbar-center col-md-6 col-sm-10">
							<div class="display-number">
								<div class="title"><?php echo 'Hiển Thị:'; ?></div>
								<div class="display-number-list"><?php echo $this->vmPagination->getLimitBox($this->products->limit_list_step); ?></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="vm-pagination vm-pagination-top col-md-4 col-sm-12">
							<?php echo $this->vmPagination->getPagesLinks(); ?>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				</div>
				<div class="col-md-3 my-4">
					<?php
						$modules =& JModuleHelper::getModules('right_detail');
						foreach ($modules as $module) {
							echo JModuleHelper::renderModule($module);
						}
					?>
				</div>
				<style>
					img.featuredProductImage {
						width: 100px;
						float: left;
						margin: 0px 20px 20px 0px;
					}

					.vmproduct {
						background-color: #fff;
						/* border: black 1px solid; */
						/* margin: 0px 0px 2px 0px; */
						border-radius: 10px;
						padding: 10px;
					}

					.vmgroup {
						background-color: #fff;
						border: black 1px solid;
						border-radius: 10px;
					}
					.mobilemenuck-bar-button {
						position: relative !important;
						float: left !important;
						left: 10px !important;
						top: 15px !important;
					}
				</style>

				</div>
				<?php
			}
			elseif (!empty($this->product_filter) && empty($this->products['search_product']))
			{
				$products = array();
				$products = $this->product_filter;
			
			
				echo shopFunctionsF::renderVmSubLayout('products_categories_search', array('products' => $products, 'currency' => $this->currency, 'products_per_row' => $this->perRow, 'showRating' => $this->showRating));
			
				?>
				<div class="orderby-displaynumber top">
					<div class="row">
						<div class="toolbar-center col-md-6 col-sm-10">
							<div class="display-number">
								<div class="title"><?php echo 'Hiển Thị:'; ?></div>
								<div class="display-number-list"><?php echo $this->vmPagination->getLimitBox($this->products->limit_list_step); ?></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="vm-pagination vm-pagination-top col-md-4 col-sm-12">
							<?php echo $this->vmPagination->getPagesLinks(); ?>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-3 my-4">
					<?php
						$modules =& JModuleHelper::getModules('right_detail');
						foreach ($modules as $module) {
							echo JModuleHelper::renderModule($module);
						}
					?>
				</div>
				<style>
					img.featuredProductImage {
						width: 100px;
						float: left;
						margin: 0px 20px 20px 0px;
					}

					.vmproduct {
						background-color: #fff;
						/* border: black 1px solid; */
						/* margin: 0px 0px 2px 0px; */
						border-radius: 10px;
						padding: 10px;
					}

					.vmgroup {
						background-color: #fff;
						border: black 1px solid;
						border-radius: 10px;
					}
					.mobilemenuck-bar-button {
						position: relative !important;
						float: left !important;
						left: 10px !important;
						top: 15px !important;
					}
				</style>
				<?php
			}
			elseif (!empty($this->keyword)) {
				echo vmText::_('COM_VIRTUEMART_NO_RESULT') . ($this->keyword ? ' : (' . $this->keyword . ')' : '');
			}
			?>
	
		<?php }
		if($status)
		{
		?>
		<div class="clearfix"></div>
		<div class="category_description" style="margin-top: 10px">
			<p>
				<?php echo implode(' ', array_slice(explode(' ', $category_description), 0, 150)); ?>
				<span id="dots001">...</span>
			<div id="more001" style="display: none">
				<?php echo implode(' ', array_slice(explode(' ', $category_description), 150)); ?>
			</div>
			</p>

			<button onclick="myFunction001()" id="myBtn001">Xem Thêm</button>
			<div class="clearfix"></div>
			<script>
				function myFunction001() {
					var dots = document.getElementById( "dots001" );
					var moreText = document.getElementById( "more001" );
					var btnText = document.getElementById( "myBtn001" );

					if (dots.style.display === "none") {
						dots.style.display = "inline";
						btnText.innerHTML = "Xem Thêm";
						moreText.style.display = "none";
					} else {
						dots.style.display = "none";
						btnText.innerHTML = "Rút Gọn";
						moreText.style.display = "inline";
					}
				}
			</script>
			<div class="clearfix"></div>

		</div>
	<?php
		}
		else
		{
	?>
		<div class="category_description" style="margin-top: 10px">
			<p>
				<?php echo implode(' ', array_slice(explode(' ', $category_description), 0, 150)); ?>
			</p>
		</div>
		<div class="clearfix"></div>
	<?php
		}
	}
	?>

<div class="row">
	<div class="col-md-12" style="margin: 20px 0px 0px 0px;">
		<?php echo shopFunctionsF::renderVmSubLayout('related_categories_categories_v1', array('products' => $this->products,'category' => $this->category,'currency' => $this->currency, 'position' => 'related_products', 'class' => 'product-related-products', 'customTitle' => true)); ?>
	</div>
</div>

<!-- end browse-view -->


