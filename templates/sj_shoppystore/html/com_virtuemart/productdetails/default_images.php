<?php
	defined('_JEXEC') or die('Restricted access');
	
	$document = JFactory::getDocument();
	$app = JFactory::getApplication();
	$templateDir = JURI::base() . 'templates/' . $app->getTemplate();
?>
<div id="imagesdestop">
	<!-- GOOGLE WEB FONTS -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,400,500,700,900&amp;ver=1.0.0"
		  type="text/css" media="all"/>

	<!-- BOOTSTRAP 3.3.7 CSS -->
	<link rel="stylesheet" href="templates/sj_shoppystore/css/theme/bootstrap.min.css"/>

	<!-- OPEN LIBS CSS -->
	<link rel="stylesheet" href="templates/sj_shoppystore/css/theme/font-awesome.min.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/owl-carousel/owl.carousel.min.css"/>

	<link rel="stylesheet" href="templates/sj_shoppystore/css/js_composer/js_composer.min.css"/>

	<!-- YT CSS -->
	<link rel="stylesheet" href="templates/sj_shoppystore/css/colorbox/colorbox.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/sw_core/jquery.fancybox.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/sw_woocommerce/slider.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/woocommerce/woocommerce-layout.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/woocommerce/woocommerce-smallscreen.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/woocommerce/woocommerce.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/theme/wishtlist.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/theme/app-responsive.min.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/theme/flexslider.min.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/theme/custom-violet.min.css"/>
	<link rel="stylesheet" href="templates/sj_shoppystore/css/theme/home-style-14.min.css"/>
	<script type="text/javascript" src="templates/sj_shoppystore/js/sw_woocommerce/slick.min.js"></script>
	<script type="text/javascript" src="plugins/system/ytshortcodes/assets/js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript"
			src="templates/sj_shoppystore/js/prettyPhoto/jquery.prettyPhoto.init.min.js"></script>
	<style>
		.size-full {
			width: 100%;
			height: auto;
			margin: 0 0 30px 0;
		}
	</style>
	<?php
		if (!empty($this->product->images) and count($this->product->images) >= 1) { ?>
			<div class="portfolio-product-wrapper">
				<ul style="list-style: none;" id="container_sw_portfolio_product_01"
					class="portfolio-product-content portfolio-product-grid clearfix">
					<?php
						foreach ($this->product->images as $key => $image) {
							if ($key >= 6) {
								break;
							}
							
							$imageslarge = YTTemplateUtils::resize($image->file_url, '600', '600', 'fill', 'webp');
							$imagesradditional = YTTemplateUtils::resize($image->file_url, '1000', '1000', 'fill', 'webp');
							?>
							<li class="portfolio-product-item col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
								<div class="item-wrap" class="product-images" data-rtl="false" data-vertical="false">
									<div class="item-img products-thumb">
										<a href="#">
											<div class="item-img-slider">
												<div class="images">
													<a href="<?php echo JURI::root() . $image->file_url; ?>"
													   rel="prettyPhoto[product-gallery]" class="zoom">
														<img width="600" height="600"
															 class="attachment-shop_single size-full size-shop_single"
															 alt=""
															 src="<?php echo $image->file_url; ?>"
															 srcset="<?php echo $image->file_url; ?> 600w, <?php echo $image->file_url; ?> 260w, <?php echo $image->file_url; ?> 180w, <?php echo $image->file_url; ?> 300w"
															 sizes="(max-width: 600px) 100vw, 600px"
														/>
													</a>
												</div>
											</div>
										</a>
									</div>
								</div>
							</li>
							<?php
							if (count($this->product->images) == 1) {
								?>
							<li class="portfolio-product-item col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
								<div class="item-wrap" class="product-images" data-rtl="false"
									 data-vertical="false">
									<div class="item-img products-thumb">
										<a href="#">
											<div class="item-img-slider">
												<div class="images">
													<a href="<?php echo JURI::root() . $image->file_url; ?>"
													   rel="prettyPhoto[product-gallery]" class="zoom">
														<img width="600" height="600"
															 class="attachment-shop_single size-full size-shop_single"
															 alt=""
															 src="<?php echo $image->file_url; ?>"
															 srcset="<?php echo $image->file_url; ?> 600w, <?php echo $image->file_url; ?> 260w, <?php echo $image->file_url; ?> 180w, <?php echo $image->file_url; ?> 300w"
															 sizes="(max-width: 600px) 100vw, 600px"
														/>
													</a>
												</div>
											</div>
										</a>
									</div>
								</div>
							</li>
								<?php
							}
						}
					?>
				</ul>
			</div>
		
		<?php }
	?>
</div>
<div id="imagesmobile">
	<?php
		$document = JFactory::getDocument();
		$app = JFactory::getApplication();
		$templateDir = JURI::base() . 'templates/' . $app->getTemplate();
		if (!empty($this->product->images) and count($this->product->images) >= 1)
		{ ?>
            <div class="gkconcept">
                <ul id="lightSlidergkconcept">
	                <?php
		                foreach ($this->product->images as $key => $image) {
			                ?>
                            <li class="owl2-item" data-thumb="<?php echo JURI::base() . $image->file_url; ?>">
                                <a href="#" class="img thumbnail"
                                   data-image="<?php echo JURI::base() . $image->file_url; ?>"
                                   data-zoom-image="<?php echo JURI::base() . $image->file_url; ?>">
                                <img src="<?php echo JURI::base() . $image->file_url; ?>" />
                                </a>
                            </li>
			                <?php
		                }
	                ?>
                 
                </ul>
            </div>
			<?php
		}
	?>
	<script type="text/javascript" src="<?php echo $templateDir . '/js/jquery.elevateZoom-3.0.8.min.js' ?>"></script>
	<script type="text/javascript" src="<?php echo $templateDir . '/js/lightslider/lightslider.js' ?>"></script>
	<link rel="stylesheet" href="<?php echo $templateDir . '/js/lightslider/lightslider.css' ?>" type="text/css">
    <style>
        ul {
            list-style: none outside none;
        }
        .gkconcept {
            margin: 0px 0px 10px 0px;
        }
    </style>
	<script type="text/javascript">
		jQuery( document ).ready( function ($) {
            $('#lightSlidergkconcept').lightSlider({
                gallery: true,
                item: 1,
                loop:true,
                slideMargin: 0,
                thumbItem: 9
            });

   
            
		} );
	</script>
</div>



