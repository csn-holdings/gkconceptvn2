<?php
	defined('_JEXEC') or die('Restricted access');
	$currency = CurrencyDisplay::getInstance();
	if (empty($this->product)) {
		echo vmText::_('COM_VIRTUEMART_PRODUCT_NOT_FOUND');
		echo '<br /><br />  ' . $this->continue_link_html;
		
		return;
	}
	
	
	echo shopFunctionsF::renderVmSubLayout('askrecomjs', array('product' => $this->product));
	$this->product->product_desc_bigpigshop = $this->product->product_desc;
	$product_s_desc = $this->product->product_s_desc;
	if (vRequest::getInt('print', false))
	{
?>
<body onload="javascript:print();">
<?php
	}
?>
<script type="text/javascript" src="<?php echo JURI::base(true) . '/components/com_sppagebuilder/assets/js/engine.js'; ?>"></script>
<div class="productdetails-view productdetails">
	<div class="vm-product-container">
		<div class="col-sm-12 col-md-8 vm-product-images-container">
			<?php echo $this->loadTemplate('images'); ?>
			<div class="row mt-2 mb-2 productdetails-chinhsach">
				<div class="col-sm-4 productdetails-chinhsach-img">
					<a href="https://gkconcept.vn/chinh-sach-hau-mai/chinh-sach-bao-hanh-va-dich-vu">
						<img src="images/stories/virtuemart/product/BANNER-02.jpg" alt="chinh sach">
					</a>
				</div>
				<div class="col-sm-4 productdetails-chinhsach-img">
					<a href="https://gkconcept.vn/chinh-sach-hau-mai/chinh-sach-van-chuyen-gkconcept" >
						<img src="images/stories/virtuemart/product/BANNER-03.jpg" alt="chinh sach">
					</a>
				</div>
				<div class="col-sm-4 productdetails-chinhsach-img">
					<a href="https://gkconcept.vn/chinh-sach-hau-mai/chinh-sach-giai-quyet-khieu-nai" >
						<img src="images/stories/virtuemart/product/BANNER-01.jpg" alt="chinh sach">
					</a>
				</div>
			</div>
		</div>

		<div class="col-sm-12 col-md-4 vm-product-details-container">
			<h1 class="product_title" itemprop="name"><?php echo $this->product->product_name; ?></h1>
			<?php echo $this->product->event->afterDisplayTitle ?>
<!--			<div class="product-short-description">-->
<!--				<h3 style="color: #51A54C;font-size: 19px;">Mã Sản Phẩm: --><?php //echo $this->product->product_sku; ?><!--</h3>-->
<!--			</div>-->
			<?php echo shopFunctionsF::renderVmSubLayout('prices5', array('product' => $this->product, 'currency' => $this->currency)); ?>
			<span>(<?php echo $this->product->product_sku; ?>)</span>
			<div class="clearfix"></div>
			
			<div class="product-short-description">
				<?php
					// cân nặng sản phẩm
					//                $product_weight = $this->product->product_weight ? $this->product->product_weight : 0 ;
					//                $product_weight_uom = $this->product->product_weight_uom ? $this->product->product_weight_uom : '' ;
					//                if(!empty($product_weight) && $product_weight && !empty($product_weight_uom) && $product_weight_uom)
					//                {
					//	                $pro_weig =  round($product_weight,0) . ' x (' . $product_weight_uom . ') ';
					//                }
					// kích thước:
					$product_length = $this->product->product_length ? $this->product->product_length : 0;
					$product_width  = $this->product->product_width ? $this->product->product_width : 0;
					$product_height = $this->product->product_height ? $this->product->product_height : 0;
					$product_lwh_uom = $this->product->product_lwh_uom ? $this->product->product_lwh_uom : '';
		
					if(!empty($product_length) && $product_length && !empty($product_width) && $product_width &&
						!empty($product_height) && $product_height && !empty($product_lwh_uom) && $product_lwh_uom)
					{
						$len_wit_hei = round($product_length,0) . ' x ' . round($product_width,0) . ' x ' . round($product_height,0) . ' ('. strtolower($product_lwh_uom) .') ';
					}
				?>
				<table class="table">
					<caption>Chi Tiết Sản Phẩm</caption>
					<tbody>
					<tr>
						<th>Mã Sản Phẩm</th>
						<td><?php echo $this->product->product_sku; ?></td>
					</tr>
					<tr>
						<th >Chất Liệu</th>
						<td>Gỗ  mật hồng </td>
					</tr>
					<tr>
						<th>Kích Thước</th>
						<td><?php echo !empty($len_wit_hei) ? $len_wit_hei : 'Chưa Cập Nhật '; ?></td>
					</tr>
					<tr>
						<th>Màu Sắc</th>
						<td>Gỗ Tự Nhiên</td>
					</tr>
					<tr>
						<th>Bảo Hành</th>
						<td>
							<?php
							$warranty_forever = "12 tháng";
							foreach ($this->product->customfieldsSorted['addtocart'] as $warranty)
							{
								//warranty_forever
								if($warranty->custom_title === 'warranty_forever' && $warranty->custom_value)
								{
									$warranty_forever = $warranty->customfield_value;
								}
							}
							 echo $warranty_forever;
							?>
						</td>
					</tr>
					<tr>
						<th>Tình Trạng</th>
						<td>Còn Hàng</td>
					</tr>
					</tbody>
				</table>
			</div>
			<?php
				if (!empty($this->product->prices['product_price'])  && $this->product->prices['product_price'] - $this->product->prices['salesPrice'] > 0)
				{
					?>
					<div class="product-short-description">
						<h5 style="color: #BFBFC1;">Tiết Kiệm:
							<div class="savemoney">
								<?php
										echo '<div class="PricesalesPrice vm-display vm-price-value"><span class="PricesalesPrice percent"> - ';
										echo ceil((($this->product->prices['product_price'] - $this->product->prices['salesPrice']) / $this->product->prices['product_price']) * 100);
										echo ' % </span></div>';
										echo $currency->createPriceDiv('discountAmount', 'COM_VIRTUEMART_PRODUCT_DISCOUNT_AMOUNT', $this->product->prices);
								?>
							</div>
						</h5>
					</div>
					<div class="product-short-description">
						<h5 style="color: #BFBFC1;">Giá Thị Trường:<?= $currency->createPriceDiv('product_price', '', $this->product->prices, FALSE, FALSE, 1.0, TRUE); ?></h5>
					</div>
					<?php
				}
			?>
			<?php
				if (!empty($product_s_desc))
				{
					?>
					<div class="product-short-description">
						<?php echo nl2br($product_s_desc); ?>
					</div>
					<?php
				}
				else
				{
					?>
					<div class="product-short-description">
						<?php echo nl2br('  '); ?>
					</div>
					<?php
				}
			?>
			<?php
				echo shopFunctionsF::renderVmSubLayout('addtocart', array('product' => $this->product));
				
				//echo shopFunctionsF::renderVmSubLayout('productdetailservice', array());
				
				echo shopFunctionsF::renderVmSubLayout('call_api_boxme_fee', array('product' => $this->product, 'currency' => $this->currency));
			?>
   
			<?php
				$modules =&  JModuleHelper::getModules('productdetail-service-right');
				foreach ($modules as $module)
				{
					echo JModuleHelper::renderModule($module);
				}
			?>
			<div class="clearfix"></div>


			<div class="product-other-container">
				<div class="hidden-sm hidden-md hidden-xs">
					<?php echo shopFunctionsF::renderVmSubLayout('product_buy_together2', array('product' => $this->product, 'currency' => $this->currency)); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="vm-product-container">
		<div class="row">
			<div class="col-md-9 product-tabs">
				<?php
					echo shopFunctionsF::renderVmSubLayout('productdetailservicetabcsn', array('product' => $this->product, 'currency' => $this->currency));
				?>
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12 product-tabs">
				<div class="row">
					<div>
						<div class="vmproduct">
							<div class="product-img">
								<a href="https://gkconcept.vn/qua-tang-mieng-lot-dien-thoai" title="Logo 1">
									<img class="logo"  src="images/stories/virtuemart/product/4d523a8cd6af24f17dbe.jpg" alt="Logo 1">
								</a>
							</div>
							<div class="product-img">
								<a href="https://gkconcept.vn/qua-tang-ebook" title="Logo 1">
									<img class="logo"  src="images/stories/virtuemart/product/27705937b514474a1e05.jpg" alt="Logo 1">
								</a>
							</div>
							<div class="product-img">
								<a href="https://gkconcept.vn/trai-nghiem-san-pham" title="Logo 1">
									<img class="logo"  src="images/stories/virtuemart/product/3b53b1bc5a9fa8c1f18e.jpg" alt="Logo 1">
								</a>
							</div>
							<style>
								.logo{
									width: max-content;
									float: left;
									margin: 10px;
									height: revert;
								}
							</style>
						</div>
					</div>
				</div>
				<?php
					$modules =&  JModuleHelper::getModules('right_detail');
					foreach ($modules as $module)
					{
						echo JModuleHelper::renderModule($module);
					}
				?>
				<style>
					img.featuredProductImage {
						width: 100px;
						float: left;
						margin: 0px 20px 20px 0px;
					}
					.vmproduct {
						background-color: #fff;
						/* border: black 1px solid; */
						/* margin: 0px 0px 2px 0px; */
						border-radius: 10px;
						padding: 10px;
					}
					.vmgroup {
						background-color: #fff;
						border: black 1px solid;
						/*border-radius: 10px;*/
					}
					.module.sj_verticalmegamenu.category {
						margin: -171px 0px 0px 0px;
					}
					.mobilemenuck-bar-button {
						position: relative !important;
						float: left !important;
						left: 10px !important;
						top: 15px !important;
					}
					.productdetailsymbol {
						font-size: 20px;
					}
				</style>
			</div>
		</div>
	</div>

	<div class="vm-product-container">
		<div class="row">
			<div class="col-md-12 product-tabs">
				<div class="content-vm2tags"></div>
				<div class="spacer-buy-area">
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12 product-related">
		<?php echo shopFunctionsF::renderVmSubLayout('related_products_v3', array('product' => $this->product, 'currency' => $this->currency, 'position' => 'related_products', 'class' => 'product-related-products', 'customTitle' => true)); ?>
	</div>

	<div class="col-md-12 product-related">
		<?php echo shopFunctionsF::renderVmSubLayout('related_categories_v2', array('product' => $this->product, 'currency' => $this->currency, 'position' => 'related_products', 'class' => 'product-related-products', 'customTitle' => true)); ?>
	</div>

</div>
<?php echo vmJsApi::writeJS(); ?>