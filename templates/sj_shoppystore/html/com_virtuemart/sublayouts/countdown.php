<?php
	/**
	 *
	 * Show the countdown
	 * @author BigPig
	 */
	// Check to ensure this file is included in Joomla!
	defined('_JEXEC') or die('Restricted access');
	$product = $viewData['product'];
	$currency = $viewData['currency'];
	
	foreach ($product->customfieldsSorted['addtocart'] as $fied) {
		if ($fied->custom_title == 'countdown') {
			?>
			<div style="padding-top: 10px">
				<div class="iq-header-title">
					<h4 class="card-title">Thời gian còn lại :</h4>
				</div>
				<div class="btn" style="background-color: rgb(9, 33, 67, 1) !important;color: white;"
					 id="countdownproductdetail"></div>
			</div>

			<script>
				// Set the date we're counting down to
				var countDownDate = new Date("<?= $fied->customfield_value ?>").getTime();

				// Update the count down every 1 second
				var x = setInterval(function () {

					// Get today's date and time
					var now = new Date().getTime();

					// Find the distance between now and the count down date
					var distance = countDownDate - now;

					// Time calculations for days, hours, minutes and seconds
					var days = Math.floor(distance / (1000 * 60 * 60 * 24));
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);

					// Output the result in an element with id="countdownproductdetail"
					document.getElementById("countdownproductdetail").innerHTML = days + " Ngày " + hours + " Giờ "
						+ minutes + " Phút " + seconds + " Giây ";

					document.getElementById("countdownproductdetail").innerHTML = html;

					// If the count down is over, write some text
					if (distance < 0) {
						clearInterval(x);
						document.getElementById("countdownproductdetail").innerHTML = "EXPIRED";
					}
				}, 1000);
			</script>
			<?php
		}
	}
?>




