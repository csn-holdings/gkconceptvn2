<?php
	/**
	 *
	 * Show the product_buy_together
	 * @author BigPig
	 */
	// Check to ensure this file is included in Joomla!
	defined('_JEXEC') or die('Restricted access');
	$product = $viewData['product'];
	$currency = $viewData['currency'];
	
	$product_detail_introduce_category = "Chua Co Thong Tin";
	$product_detail_introduce_manufacturer = "Chua Co Thong Tin";
	$product_detail_introduce_produce = "Chua Co Thong Tin";
	$product_detail_introduce_country = "Chua Co Thong Tin";
	$product_detail_introduce_material = "Chua Co Thong Tin";
	$product_detail_infor = "Chua Co Thong Tin";
	$product_detail_box_delivery = "Chua Co Thong Tin";
	$product_detail_warranty_return = "Chua Co Thong Tin";
	$product_detail_use_storage = "Chua Co Thong Tin";
	$product_detail_tech_description = "Chua Co Thong Tin";
	$product_detail_video_tutorial = '<iframe width="560" height="315" src="https://www.youtube.com/embed/XOH3QYYlN2g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
	
	foreach ($product->customfieldsSorted['addtocart'] as $fied) {
		
		if($fied->custom_title == 'product_detail_introduce_category')
		{
			$product_detail_introduce_category =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_introduce_manufacturer')
		{
		    $product_detail_introduce_manufacturer = $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_introduce_produce')
		{
			$product_detail_introduce_produce =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_introduce_country')
		{
			$product_detail_introduce_country =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_introduce_material')
		{
			$product_detail_introduce_material =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_infor')
		{
			$product_detail_infor =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_box_delivery')
		{
			$product_detail_box_delivery =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_warranty_return')
		{
			$product_detail_warranty_return =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_use_storage')
		{
			$product_detail_use_storage =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_tech_description')
		{
			$product_detail_tech_description =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_video_tutorial')
		{
			$product_detail_video_tutorial =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
	}

?>
<ul class="yt-clearfix yt-accordion ul_5f505f5bac9502836776031599102811 pull-left border">
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading enable">
            <i class="icon_accordion fa fa-dropbox"></i>
            GIỚI THIỆU CHUNG VỀ SẢN PHẨM
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_introduce_category ?>
        </div>
    </li>
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading disable">
            <i class="icon_accordion fa fa-dropbox"></i>
            GIỚI THIỆU VỀ NHÀ SẢN XUẤT
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_introduce_manufacturer ?>
        </div>
    </li>
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading disable">
            <i class="icon_accordion fa fa-dropbox"></i>
            CHI TIẾT SẢN PHẨM
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_introduce_produce ?>
        </div>
    </li>
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading disable">
            <i class="icon_accordion fa fa-dropbox"></i>
            QUỐC GIA SẢN XUẤT
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_introduce_country ?>
        </div>
    </li>
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading disable">
            <i class="icon_accordion fa fa-dropbox"></i>
            THÔNG TIN VỀ CHẤT LIỆU SẢN PHẨM
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_introduce_material ?>
        </div>
    </li>
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading disable">
            <i class="icon_accordion fa fa-dropbox"></i>
            THÔNG TIN SẢN PHẨM
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_infor ?>
        </div>
    </li>
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading disable">
            <i class="icon_accordion fa fa-dropbox"></i>
            THÔNG TIN VỀ KÍCH THƯỚC SẢN PHẨM
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_box_delivery ?>
        </div>
    </li>
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading disable">
            <i class="icon_accordion fa fa-dropbox"></i>
            CHÍNH SÁCH BẢO HÀNH VÀ ĐỔI TRẢ
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_warranty_return ?>
        </div>
    </li>
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading disable">
            <i class="icon_accordion fa fa-dropbox"></i>
            HƯỚNG DẪN SỬ DỤNG VÀ BẢO QUẢN
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_use_storage ?>
        </div>
    </li>
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading disable">
            <i class="icon_accordion fa fa-dropbox"></i>
            MÔ TẢ CÔNG NGHỆ
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_tech_description ?>
        </div>
    </li>
    <li class="yt-accordion-group" >
        <h3 class="accordion-heading disable">
            <i class="icon_accordion fa fa-dropbox"></i>
            VIDEO MÔ TẢ SẢN PHẨM
        </h3>
        <div class="yt-accordion-inner" style="display: none;">
			<?= $product_detail_video_tutorial ?>
        </div>
    </li>
</ul>


