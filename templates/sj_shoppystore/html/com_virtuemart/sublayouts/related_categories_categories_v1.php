<?php
	defined('_JEXEC') or die('Restricted access');
	
	$product = $viewData['products'];
	$category = $viewData['category'];
	$currency = $viewData['currency'];
	$position = $viewData['position'];
	$customTitle = isset($viewData['customTitle']) ? $viewData['customTitle'] : false;;
	if (isset($viewData['class'])) {
		$class = $viewData['class'];
	} else {
		$class = 'product-fields';
	}
	
	$listCategory[] = $product->categoryItem;
	$category_related = array();
	$category_parent = array();
	$list_product = array();
	$list = array();
	
	if(empty($category->virtuemart_category_id))
    {
        return;
    }
	
	if(!empty($category))
    {
	    $db = JFactory::getDBO ();
	    $q = 'SELECT category_child_id,category_parent_id FROM #__virtuemart_category_categories as c WHERE c.category_child_id = ' . $category->virtuemart_category_id;
	    $db->setQuery($q);
	    $result = $db->loadObject();
	
	    if($result->category_parent_id == 0 && $result->category_child_id != 0)
	    {
		    $list[] = $result->category_child_id;
	    }
        elseif ($result->category_parent_id != 0 && $result->category_child_id != 0)
	    {
		    $db = JFactory::getDBO ();
		    $q2 = 'SELECT category_child_id,category_parent_id FROM #__virtuemart_category_categories as c WHERE c.category_parent_id = ' . $result->category_parent_id;
		    $db->setQuery($q2);
		    $result = $db->loadObjectList();
		
		    foreach ($result as $cat)
		    {
			    $list[] = $cat->category_child_id;
		    }
	    }
	    
	    if(!empty($list))
	    {
		    $q3  = 'SELECT `virtuemart_category_id`,`category_name`,`slug` FROM `#__virtuemart_categories_en_gb` WHERE virtuemart_category_id IN ( ' . implode(',', $list) .' )';
		    $db->setQuery($q3);
		    $listresult = $db->loadObjectList();
		
		    foreach ($listresult as $key => $cate)
		    {
			    $link_category = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $cate->virtuemart_category_id, FALSE);
			    $listresult[$key]->link_category = $link_category;
		    }
	    }
    }
	
	if(!empty($list))
	{
		switch ($category->virtuemart_category_id)
		{
			case 292:
				$listrelatedcategory = array(304,265,191,140,251,252,253,192,92,276,158,185);
				break;
			case 265:
				$listrelatedcategory = array(292,304,191,140,251,252,253,192,92,276,158,185);
				break;
			case 191:
				$listrelatedcategory = array(292,304,265,140,251,252,253,192,92,292,276,158);
				break;
			case 140:
				$listrelatedcategory = array(292,265,191,304,251,252,253,192,92,276,140,185);
				break;
			case 251:
				$listrelatedcategory = array(292,265,191,140,305,252,253,192,92,276,251,185);
				break;
			case 252:
				$listrelatedcategory = array(292,265,191,140,251,304,253,192,92,276,252,185);
				break;
			case 253:
				$listrelatedcategory = array(292,265,191,140,251,252,307,192,92,276,304,185);
				break;
			case 192:
				$listrelatedcategory = array(292,265,191,140,251,252,253,306,92,276,158,185);
				break;
			case 294:
				$listrelatedcategory = array(140,304,305,306,330,279,322,303,319,92,294,321);
				break;
			case 92:
				$listrelatedcategory = array(140,304,305,306,330,279,322,303,319,294,192,321);
				break;
			case 304:
				$listrelatedcategory = array(92,264,305,306,330,279,322,303,319,294,304,321);
				break;
			case 305:
				$listrelatedcategory = array(92,304,251,306,330,279,322,303,319,294,305,321);
				break;
			case 330:
				$listrelatedcategory = array(92,304,305,306,140,279,322,303,319,294,330,321);
				break;
			case 306:
				$listrelatedcategory = array(92,304,305,140,330,279,322,303,294,319,306,321);
				break;
			case 297:
				$listrelatedcategory = array(264,307,298,99,313,320,304,297,127,305,92,96);
				break;
			case 264:
				$listrelatedcategory = array(298,307,264,99,313,320,306,297,127,305,92,96);
				break;
			case 307:
				$listrelatedcategory = array(140,264,305,99,313,304,297,320,127,307,92,96);
				break;
			case 298:
				$listrelatedcategory = array(297,307,192,99,313,320,264,305,127,304,92,96);
				break;
			case 300:
				$listrelatedcategory = array(303,300,319,294,95,79,322,304,324,118,302,96);
				break;
			case 302:
				$listrelatedcategory = array(300,118,319,294,279,149,322,303,324,79,276,334);
				break;
			case 118:
				$listrelatedcategory = array(302,323,319,294,279,149,322,303,324,79,118,321);
				break;
			case 303:
				$listrelatedcategory = array(321,185,99,323,324,276,140,192,322,294,155,79);
				break;
			case 321:
				$listrelatedcategory = array(303,185,322,323,324,276,140,192,79,294,155,305);
				break;
			case 185:
				$listrelatedcategory = array(321,328,322,323,324,276,140,192,79,294,155,302);
				break;
			case 322:
				$listrelatedcategory = array(321,185,92,323,324,276,140,192,79,294,155,302);
				break;
			case 323:
				$listrelatedcategory = array(321,185,322,79,324,276,140,192,304,294,155,302);
				break;
			case 324:
				$listrelatedcategory = array(321,185,322,323,118,276,140,192,304,294,155,305);
				break;
			case 276:
				$listrelatedcategory = array(321,185,322,323,324,334,140,192,305,294,155,79,249);
				break;
			case 312:
				$listrelatedcategory = array(281,182,328,95,149,276,334,185,79,153,319,140);
				break;
			case 281:
				$listrelatedcategory = array(279,182,328,95,149,276,334,185,79,153,319,140);
				break;
			case 182:
				$listrelatedcategory = array(281,279,328,95,149,182,334,185,79,153,319,328);
				break;
			case 328:
				$listrelatedcategory = array(281,182,321,95,149,276,334,185,79,153,319,99);
				break;
			case 313:
				$listrelatedcategory = array(99,320,127,96,331,329,264,307,297,140,92,319);
				break;
			case 99:
				$listrelatedcategory = array(185,320,127,96,331,329,264,307,297,140,92,319);
				break;
			case 320:
				$listrelatedcategory = array(99,323,127,96,331,329,264,307,297,140,92,319);
				break;
			case 127:
				$listrelatedcategory = array(99,320,79,96,331,329,264,307,297,140,92,319);
				break;
			case 331:
				$listrelatedcategory = array(99,320,127,96,79,329,264,307,297,140,92,319);
				break;
			case 329:
				$listrelatedcategory = array(99,320,127,96,331,185,264,307,297,140,92,319);
				break;
			case 96:
				$listrelatedcategory = array(99,320,127,321,331,329,264,307,297,96,92,319);
				break;
			case 314:
				$listrelatedcategory = array(340,341,339,74,318,69,83,78,142,325,92,140);
				break;
			case 341:
				$listrelatedcategory = array(340,274,339,74,318,69,83,78,142,325,92,140);
				break;
			case 339:
				$listrelatedcategory = array(340,341,72,74,318,69,83,78,142,325,92,73);
				break;
			case 340:
				$listrelatedcategory = array(340,341,339,74,318,69,83,78,142,325,92,140);
				break;
			case 315:
				$listrelatedcategory = array(274,81,84,334,72,341,258,255,316,265,140,95);
				break;
			case 274:
				$listrelatedcategory = array(81,84,334,140,72,341,258,255,316,265,92,95);
				break;
			case 81:
				$listrelatedcategory = array(274,140,84,334,72,341,258,255,316,265,81,95);
				break;
			case 84:
				$listrelatedcategory = array(274,81,140,334,72,341,258,255,316,265,92,95);
				break;
			case 334:
				$listrelatedcategory = array(274,81,84,140,72,341,258,255,316,265,92,95);
				break;
			case 72:
				$listrelatedcategory = array(254,140,255,256,100,257,258,259,149,92,155,341);
				break;
			case 360:
				$listrelatedcategory = array(254,314,255,256,100,257,258,259,149,92,155,341);
				break;
			case 254:
				$listrelatedcategory = array(314,140,255,256,100,257,258,259,149,92,155,341);
				break;
			case 255:
				$listrelatedcategory = array(254,140,314,256,100,257,258,259,149,92,155,341);
				break;
			case 256:
				$listrelatedcategory = array(254,140,255,314,100,257,258,259,149,92,155,341);
				break;
			case 100:
				$listrelatedcategory = array(254,140,255,256,314,257,258,259,149,92,155,341);
				break;
			case 257:
				$listrelatedcategory = array(254,140,255,256,100,314,258,259,149,92,155,341);
				break;
			case 258:
				$listrelatedcategory = array(254,140,255,256,100,257,314,259,149,92,155,341);
				break;
			case 259:
				$listrelatedcategory = array(254,140,255,256,100,257,258,314,149,92,155,341);
				break;
			case 316:
				$listrelatedcategory = array(79,149,151,99,281,96,279,140,185,322,153,307);
				break;
			case 149:
				$listrelatedcategory = array(79,316,151,99,281,96,279,140,185,322,153,307);
				break;
			case 79:
				$listrelatedcategory = array(316,149,151,99,281,96,279,140,185,322,153,307);
				break;
			case 151:
				$listrelatedcategory = array(151,323,264,99,281,96,279,140,185,322,153,307);
				break;
			case 153:
				$listrelatedcategory = array(155,158,92,99,281,96,279,140,185,322,153,307);
				break;
			case 155:
				$listrelatedcategory = array(153,158,92,323,281,96,279,140,185,322,99,307);
				break;
			case 158:
				$listrelatedcategory = array(155,153,99,257,281,96,279,140,185,322,158,307);
				break;
			case 66:
				$listrelatedcategory = array(325,142,153,80,314,83,339,69,340,318,74,247);
				break;
			case 142:
				$listrelatedcategory = array(325,66,153,80,314,83,339,69,340,318,74,247);
				break;
			case 325:
				$listrelatedcategory = array(66,142,92,80,314,83,339,69,340,318,74,247);
				break;
			case 74:
				$listrelatedcategory = array(78,80,153,73,247,74,314,83,339,69,340,318);
				break;
			case 78:
				$listrelatedcategory = array(74,80,153,73,78,79,314,83,339,69,340,318);
				break;
			case 80:
				$listrelatedcategory = array(78,153,79,73,74,80,314,83,339,69,340,318);
				break;
			case 83:
				$listrelatedcategory = array(78,80,79,73,74,153,314,66,339,69,340,318);
				break;
			case 73:
				$listrelatedcategory = array(78,80,99,74,73,153,314,83,339,69,340,318);
				break;
			case 119:
				$listrelatedcategory = array(263,115,247,336,337,345,142,191,305,92,304,264);
				break;
			case 263:
				$listrelatedcategory = array(119,115,153,336,337,345,92,304,264,305,334,325);
				break;
			case 115:
				$listrelatedcategory = array(263,119,247,336,337,345,305,191,307,306,92,142);
				break;
			case 247:
				$listrelatedcategory = array(263,115,247,336,337,345,305,191,69,306,92,330);
				break;
			case 336:
				$listrelatedcategory = array(263,115,247,119,337,345,305,191,307,306,92,142);
				break;
			case 337:
				$listrelatedcategory = array(263,115,247,336,119,345,305,191,307,306,92,142);
				break;
			case 345:
				$listrelatedcategory = array(263,115,92,336,337,119,330,334,72,69,340,339);
				break;
			case 318:
				$listrelatedcategory = array(69,71,92,66,72,307,341,83,340,314,339,142);
				break;
			case 69:
				$listrelatedcategory = array(318,71,92,66,72,307,341,83,340,314,339,142);
				break;
			case 71:
				$listrelatedcategory = array(69,318,96,66,71,307,341,83,340,314,339,142);
				break;
			case 319:
				$listrelatedcategory = array(184,95,66,334,281,302,276,321,322,274,313,185);
				break;
			case 184:
				$listrelatedcategory = array(319,95,279,334,192,78,80,83,322,316,79,151);
				break;
			case 95:
				$listrelatedcategory = array(184,319,92,334,281,302,276,321,322,274,313,185);
				break;
			case 279:
				$listrelatedcategory = array(184,95,66,334,281,302,276,321,322,274,313,185);
				break;
			default:
				$listrelatedcategory = array(184,95,66,334,281,302,276,321,322,274,313,185);
		}
		
		
		if(!empty($listrelatedcategory))
		{
			$db = JFactory::getDBO ();
			$q3  = 'SELECT c.*,mm.*
                    FROM `#__virtuemart_categories_en_gb` as c
                    left join `#__virtuemart_category_medias` as m ON c.`virtuemart_category_id` = m.`virtuemart_category_id`
                    left join `#__virtuemart_medias` as mm ON mm.`virtuemart_media_id` = m.`virtuemart_media_id`
                    WHERE c.`virtuemart_category_id` IN ( ' . implode(',', $listrelatedcategory) .' )';
			$db->setQuery($q3);
			$listresult = $db->loadObjectList();
			
			foreach ($listresult as $key => $cate)
			{
				$link_category = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $cate->virtuemart_category_id, FALSE);
				$listresult[$key]->link_category = $link_category;
				$listresult[$key]->file_url =  !empty($cate->file_url) ? JURI::ROOT() . $cate->file_url : 'https://gkconcept.vn/images/stories/virtuemart/product/tu-de-do-tien-dung2.jpg';
				$category_related[] = $cate;
			}
		}
        elseif (!empty($list))
		{
			$db = JFactory::getDBO ();
			$q3  = 'SELECT c.*,mm.*
                    FROM `#__virtuemart_categories_en_gb` as c
                    left join `#__virtuemart_category_medias` as m ON c.`virtuemart_category_id` = m.`virtuemart_category_id`
                    left join `#__virtuemart_medias` as mm ON mm.`virtuemart_media_id` = m.`virtuemart_media_id`
                    WHERE c.`virtuemart_category_id` IN ( ' . implode(',', $list) .' )';
			$db->setQuery($q3);
			$listresult = $db->loadObjectList();
			
			foreach ($listresult as $key => $cate)
			{
				$link_category = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $cate->virtuemart_category_id, FALSE);
				$listresult[$key]->link_category = $link_category;
				$listresult[$key]->file_url = !empty($cate->file_url) ? JURI::ROOT() . $cate->file_url : 'https://gkconcept.vn/images/stories/virtuemart/product/tu-de-do-tien-dung2.jpg';
				$category_related[] = $cate;
			}
		}
	}
	
	if (!empty($category_related) && is_array($category_related) && count($category_related)>0)
	{
	
//		$category_related = array_slice($category_related,0,12);
		?>
        <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
        <div class="module <?php echo $class ?>">
            <h3 class="modtitle product-fields-title-wrapper">
				<span class="product-fields-title">
					<?php echo vmText::_('Danh Mục Liên Quan') ?>
				</span>
            </h3>

            <div class="portfolio-product-wrapper" style="background-color: #f6f6f6 !important;padding: 10px;" id="appproductdetail">
                <ul style="list-style: none;" class="portfolio-product-content portfolio-product-grid clearfix" >
                            <li class="portfolio-product-item col-lg-2 col-md-2 col-sm-6 col-xs-6 " v-for="cate in category">
                                <div class="item-wrap">
                                    <div class="item-img products-thumb text-center">
                                        <div class="item-img-slider">
                                            <a :href="cate.link_category">
                                                <img style="border-radius: 15px;padding: 5px 5px 0px;width: max-content;height: 135px;"
                                                     class="attachment-shop_single size-full size-shop_single"
                                                     :alt="cate.category_name"
                                                     :src="cate.file_url"
                                                ><br>
                                                <span class="product_title" itemprop="name">
													 {{ cate.category_name }}
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                    
                </ul>
            </div>
        </div>
        <script>
            var vm = new Vue({
                el: '#appproductdetail',
                data: {
                    category: <?php echo json_encode($category_related);  ?>,
                },
                methods:{
                    imgError(){
                        let defaultImage = "images/stories/virtuemart/category/banhocsinh4.jpg";
                        this.src = this.defaultImg;
                    },
                    productdetail(id)
                    {
                        let url = 'index.php?option=com_congtacvien&task=goodideas.getProductDetailGoodIdeas&=virtuemart_product_id='+id;
                        var product = this;
                        jQuery.ajax({
                            url: url,
                            type: 'POST',
                            dataType:'JSON',
                            data:{
                                id
                            },
                            success: function(info)
                            {
                                product.if_child = true;
                                product.product = info;
                                console.log(product.if_child);
                            },
                        });

                        return true;
                    },

                },
                computed: {
                    if_child_show() {
                        console.log(this.if_child);

                        if(this.if_child == true)
                        {
                            return true;
                        }

                        return  false;
                    }
                }
            })
        </script>
        <style>
            span.product_title {
                text-transform: capitalize;
                font-size: 14px;
            }
        </style>
		<?php
	}
?>
