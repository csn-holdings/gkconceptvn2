<?php
	/**
	 *
	 * Show the product_buy_together
	 * @author BigPig
	 */
	// Check to ensure this file is included in Joomla!
	defined('_JEXEC') or die('Restricted access');
	$product = $viewData['product'];
	$currency = $viewData['currency'];
	if (empty($product->customfieldsSorted['related_products'])) {
		return;
	}
	
	$listCategory = $product->categoryItem;
	$category_related = array();
	$category_parent = array();
	$list_product = array();
	
	foreach ($listCategory as $keycategory => $valuecategory) {
		
		if ($valuecategory['category_parent_id'] == 0) {
			// Category Related
			$tempCategory = new stdClass();
			$tempCategory->virtuemart_category_id = $valuecategory['virtuemart_category_id'];
			$tempCategory->category_name = $valuecategory['category_name'];
			$tempCategory->slug = $valuecategory['slug'];
			$tempCategory->category_description = $valuecategory['category_description'];
			$tempCategory->metadesc = $valuecategory['metadesc'];
			$tempCategory->customtitle = $valuecategory['customtitle'];
			$tempCategory->metakey = $valuecategory['metakey'];
			// Product Related
			$productModel = VmModel::getModel('Product');
			$products = array();
			$products = $productModel->getProductListing(false, false, true, true, false, true, $tempCategory->virtuemart_category_id, false, 0);
			$productModel->addImages($products);
			
			$tempCategory->product_category_parent_related = $products;
			$list_product = array_merge($list_product, $products);
			$category_parent[] = $tempCategory;
			
			continue;
		}
		
		$tempCategory = new stdClass();
		$tempCategory->virtuemart_category_id = $valuecategory['virtuemart_category_id'];
		$tempCategory->category_name = $valuecategory['category_name'];
		$tempCategory->slug = $valuecategory['slug'];
		$tempCategory->category_description = $valuecategory['category_description'];
		$tempCategory->metadesc = $valuecategory['metadesc'];
		$tempCategory->customtitle = $valuecategory['customtitle'];
		$tempCategory->metakey = $valuecategory['metakey'];
		// Product Related
		$productModel = VmModel::getModel('Product');
		$products = array();
		$products = $productModel->getProductListing(false, false, true, true, false, true, $tempCategory->virtuemart_category_id, false, 0);
		$productModel->addImages($products);
		
		$tempCategory->product_category_related = $products;
		$list_product = array_merge($list_product, $products);
		$category_related[] = $tempCategory;
	}


?>
<div class="iq-card center">
	<div class="row text-center">
		<h3 class="card-title"><?php echo vmText::_('Mua Cùng :') ?></h3>
	</div>
	<div class="iq-card-body">
		<?php
			$i = 0;
			shuffle($list_product);
			foreach ($list_product as $key => $product) {
				if ($i >= 3) {
					break;
				}
				
				if ($product->prices['salesPrice'] <= 0) {
					continue;
				}
				
				?>
				<ul class="banner banner-slth1">
					<li>
						<a href="<?= $product->link ? $product->link : "#" ?>">
							<img alt="<?= $product->product_name ? $product->product_name : "alt" ?>"
								 src="<?= $product->file_url ? $product->file_url : 'https://gkconcept.vn/images/virtuemart/typeless/noimagegkconcept_800x800.jpg' ?>"
							>
						</a>
					</li>
				</ul>
				<?php
				if ($i < 2) {
					?>
					<i class="fa fa-plus pl-2 pr-2 pt-5" style="height: 100px;float: left;" aria-hidden="true"></i>
					<?php
				}
				
				$i++;
			}
		?>


		<table class="table table-hover">
			<tbody style="float: left;max-width: max-content;text-align: justify !important;text-transform: uppercase;">
			<?php
				$sum = 0;
				$i = 1;
				$currencyDisplay = CurrencyDisplay::getInstance();
				foreach ($list_product as $fieds) {
					$sum += $fieds->allPrices[0]['salesPrice'];
					?>
					<tr>
						<td><a href="<?= $fieds->link ? $fieds->link : "#" ?>"><?php echo $fieds->product_name ?></a>
						</td>
						<td><?php echo $currencyDisplay->priceDisplay($fieds->allPrices[0]['salesPrice']); ?></td>
					</tr>
					<?php
					if ($i == 3) {
						break;
					}
					$i++;
				}
			?>
			<tr>
				<td>Tổng Tiền:</td>
				<td><?php echo $currencyDisplay->priceDisplay($sum); ?></td>
			</tr>
			</tbody>
		</table>


	</div>
</div>
<style>
	.vm-img-desc {
		display: none;
	}
</style>
<div class="clear"></div>
