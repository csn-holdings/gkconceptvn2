<?php
	defined('_JEXEC') or die('Restricted access');
	$product = $viewData['product'];
	$currency = $viewData['currency'];

	$product_detail_introduce_category = "Chua Co Thong Tin";
	$product_detail_introduce_manufacturer = "Chua Co Thong Tin";
	$product_detail_introduce_produce = "Chua Co Thong Tin";
	$product_detail_introduce_country = "Chua Co Thong Tin";
	$product_detail_introduce_material = "Chua Co Thong Tin";
	$product_detail_infor = "Chua Co Thong Tin";
	$product_detail_box_delivery = "Chua Co Thong Tin";
	$product_detail_warranty_return = "Chua Co Thong Tin";
	$product_detail_use_storage = "Chua Co Thong Tin";
	$product_detail_tech_description = "Chua Co Thong Tin";
	$product_detail_technology = "Chi tiết kĩ thuật hiện tại chưa cập nhật !! ";
	$product_detail_committed_from_gkconcept = "Cam kết từ Gkconcept chưa cập nhật !! ";
	$product_detail_video_tutorial = '<iframe width="560" height="315" src="https://www.youtube.com/embed/XOH3QYYlN2g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
	$data = new stdClass();
	foreach ($product->customfieldsSorted['addtocart'] as $fied)
	{
		if($fied->custom_title == 'product_detail_introduce_category')
		{
			$data->product_detail_introduce_category =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_introduce_manufacturer')
		{
			$data->product_detail_introduce_manufacturer = $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_introduce_produce')
		{
			$data->product_detail_introduce_produce =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_introduce_country')
		{
			$data->product_detail_introduce_country =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_introduce_material')
		{
			$data->product_detail_introduce_material =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_infor')
		{
			$data->product_detail_infor =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_box_delivery')
		{
			$data->product_detail_box_delivery =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_warranty_return')
		{
			$data->product_detail_warranty_return =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_use_storage')
		{
			$data->product_detail_use_storage =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_tech_description')
		{
			$data->product_detail_tech_description =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_video_tutorial')
		{
			$data->product_detail_video_tutorial =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_color')
		{
			$data->product_detail_color =  $fied->customfield_value ? $fied->customfield_value : "Chua Co Thong Tin";
		}
		
		if($fied->custom_title == 'product_detail_style')
		{
			$data->product_detail_style =  $fied->customfield_value ? $fied->customfield_value : "Chưa Có Phong Cách";
		}
		
		if($fied->custom_title == 'product_detail_water')
		{
			$data->product_detail_water =  $fied->customfield_value ? $fied->customfield_value : "Đang cập nhật tính năng chống nước";
		}
		
		if($fied->custom_title == 'product_detail_double')
		{
			$data->product_detail_double =  $fied->customfield_value ? $fied->customfield_value : "Đang cập nhật tính năng gấp đôi";
		}
		
		if($fied->custom_title == 'product_detail_use_customer')
		{
			$data->product_detail_use_customer =  $fied->customfield_value ? $fied->customfield_value : "Đang cập nhật tính năng sử dụng chung cho khách hàng";
		}
		
		if ($fied->custom_title == 'product_detail_technology_editor')
		{
			$data->product_detail_technology_editor = $fied->customfield_value ? $fied->customfield_value : "Chi tiết kĩ thuật hiện tại chưa cập nhật !! ";
		}
		
		if($fied->custom_title == 'product_detail_committed_from_gkconcept_editor')
		{
			$data->product_detail_committed_from_gkconcept_editor =  $fied->customfield_value ? $fied->customfield_value : "Cam kết từ Gkconcept chưa cập nhật !! ";
		}
	}
?>
<div id="productdetailservicetabcsn">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="clearfix active">
            <a href="#desc" aria-controls="desc" role="tab"
               data-toggle="tab">Giới Thiệu Chung Về Sản Phẩm</a>
        </li>
      <!--   <li role="presentation">
            <a href="#chitietkithuat" aria-controls="chitietkithuat" role="tab"
               data-toggle="tab">Chi Tiết Kĩ Thuật</a>
        </li> -->
        <li role="presentation">
            <a href="#camkettugkconcept" aria-controls="camkettugkconcept" role="tab"
               data-toggle="tab">Cam Kết Từ GKCONCEPT</a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="desc">
            <div class="product-description" id="descmobile">
                <div class="no-support">
	                <?= $product->product_desc_bigpigshop ?>
                </div>
                <style>
                    .no-support {
                        text-align: left;
                        margin-left: auto;
                        margin-right: auto;
                        font-size: 14px;
                        font-family: sans-serif;
                        overflow: scroll;
                        height: 600px;
                    }
                    
                </style>
                <div class="clearfix"></div>
            </div>
        </div>
<!--         <div role="tabpanel" class="tab-pane" id="chitietkithuat">
            <textarea disabled style="height:500px;width: 100%"  rows="10"><?= trim($data->product_detail_technology_editor) ?></textarea>
        </div> -->
        <div role="tabpanel" class="tab-pane" id="camkettugkconcept">
            <div class="row">
                <div class="col-sm-6">
                    <img src="/images/2021/01/18/vach-ngan-254.-1.jpg" alt="camkettugkconcept" style="height: auto;width: 400px;text-align: center;">
                </div>
                <div class="col-sm-6">
                    <style>
                        .center {
                            margin: auto;
                            width: 80%;
                            padding: 10px;
                        }
                    </style>
                    <div style="text-align: justify;font-size: medium"><span class="center">Làm chủ chuỗi cung ứng từ nguyên liệu, thiết kế, sản xuất và phân phối, mang đến <strong style="font-size: larger">SẢN PHẨM CÓ GIÁ TRỊ CAO NHẤT SO VỚI GIÁ THÀNH, CAM KẾT GỖ THẬT 100%, BAO ĐỔI TRẢ</strong></span></div>
                    <div style="padding-top: 30px;font-size: medium">
                        <a style="color: #7aaf43 !important;font-size: larger;" href="https://gkconcept.vn/ve-gkconcept/lam-chu-chuoi-cung-ung-tu-nguyen-lieu-thiet-ke-san-xuat-va-phan-phoi">Tìm hiểu thêm về năng lực cốt lõi, mô hình kinh doanh, nhà xưởng và chính sách bảo hành, dịch vụ.</a>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div id="productdetailservicetabcsn2">
    <div class="row">
        <div class="col-sm-6">
            <img src="/images/stories/virtuemart/product/vach-ngannnnn3.jpg" alt="camkettugkconcept" style="height: auto;width: 400px;text-align: center;">
        </div>
        <div class="col-sm-6">
            <style>
                .center {
                    margin: auto;
                    width: 80%;
                    padding: 10px;
                }
            </style>
            <div style="text-align: justify;font-size: medium">
            <span class="center">
                <strong style="font-size: larger">DỊCH VỤ TÙY CHỈNH MỌI YÊU CẦU THIẾT KẾ, KÍCH THƯỚC VÀ CHẤT LIỆU.</strong> Trân quý tính duy nhất của mỗi không gian, bạn sẽ thật tự hào khi sở hữu một sản phẩm mang tính độc nhất cho không gian của mình.</span>
            </div>
            <div style="padding-top: 30px;font-size: medium">
                <a style="color: #7aaf43 !important;font-size: larger;" href="https://gkconcept.vn/danh-muc-san-pham">Khám phá kho sản phẩm có sẵn với số lượng mẫu mã, thiết kế đa dạng. Và tự do tuỳ chỉnh kích thước, thiết kế theo đúng gu thẩm mỹ và không gian riêng biệt của bạn. GKconcept cam kết mang đến giải pháp tối ưu cả về chất lượng và chi phí để bạn được là chính mình trong việc tôn tạo không gian.</a>
            </div>

        </div>
    </div>
</div>
<div id="productdetailservicetabcsn3">
	<?php
		
		if($product->virtuemart_category_id)
		{
			$urlCategory = $link_category = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $product->virtuemart_category_id, FALSE);
		}
		else
		{
			$urlCategory = 'https://gkconcept.vn/danh-muc-san-pham';
		}
	?>
    <div class="container-fluid no-padding">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= $urlCategory ?>">
                    <img src="https://gkconcept.vn/images/stories/virtuemart/product/banner-1-01.jpg" alt="placeholder 960" class="img-responsive" />
                </a>
            </div>
        </div>
    </div>
</div>

<style>
    .img-responsive {
        display: block;
        height: 450px;
        width: fit-content;
        border: #000 1px solid;
        margin: 20px 0px 0px 0px;
    }
    textarea {
        resize: none;
    }
    .readmore .moreText {
        display:none;
    }
    .readmore a.more {
        display:block;
    }
    div#productdetailservicetabcsn2 {
        border: #000 1px solid;
        padding: 10px;
    }
</style>
<script>
    jQuery(function(){
        jQuery('.readmore a.more').on('click', function(){
            var $parent = jQuery(this).parent();
            if($parent.data('visible')) {
                $parent.data('visible', false).find('.ellipsis').show()
                    .end().find('.moreText').hide()
                    .end().find('a.more').text('show more');
            } else {
                $parent.data('visible', true).find('.ellipsis').hide()
                    .end().find('.moreText').show()
                    .end().find('a.more').text('show less');
            }
        });
    });
    jQuery(document).ready(function (){
        jQuery("#myBtn").click(function (){
            jQuery('html, body').animate({
                scrollTop: jQuery("#descmobile").offset().top
            }, 300);
        });
    });
</script>