<?php
	defined('_JEXEC') or die('Restricted access');
	
	$product = $viewData['product'];
	$currency = $viewData['currency'];
	$position = $viewData['position'];
	$customTitle = isset($viewData['customTitle']) ? $viewData['customTitle'] : false;;
	if (isset($viewData['class'])) {
		$class = $viewData['class'];
	} else {
		$class = 'product-fields';
	}
	
	$listCategory = $product->categoryItem;
	$category_related = array();
	$category_parent = array();
	$list_product = array();
	
	foreach ($listCategory as $keycategory => $valuecategory) {
		
		if ($valuecategory['category_parent_id'] == 0) {
			// Category Related
			$tempCategory = new stdClass();
			$tempCategory->virtuemart_category_id = $valuecategory['virtuemart_category_id'];
			$tempCategory->category_name = $valuecategory['category_name'];
			$tempCategory->slug = $valuecategory['slug'];
			$tempCategory->category_description = $valuecategory['category_description'];
			$tempCategory->metadesc = $valuecategory['metadesc'];
			$tempCategory->customtitle = $valuecategory['customtitle'];
			$tempCategory->metakey = $valuecategory['metakey'];
			// Product Related
			$productModel = VmModel::getModel('Product');
			$products = array();
			$products = $productModel->getProductListing(false, false, true, true, false, true, $tempCategory->virtuemart_category_id, false, 0);
			$productModel->addImages($products);
			
			$tempCategory->product_category_parent_related = $products;
			$list_product = array_merge($list_product, $products);
			$category_parent[] = $tempCategory;
			
			continue;
		}
		
		// Category Related
		$tempCategory = new stdClass();
		$tempCategory->virtuemart_category_id = $valuecategory['virtuemart_category_id'];
		$tempCategory->category_name = $valuecategory['category_name'];
		$tempCategory->slug = $valuecategory['slug'];
		$tempCategory->category_description = $valuecategory['category_description'];
		$tempCategory->metadesc = $valuecategory['metadesc'];
		$tempCategory->customtitle = $valuecategory['customtitle'];
		$tempCategory->metakey = $valuecategory['metakey'];
		// Product Related
		$productModel = VmModel::getModel('Product');
		$products = array();
		$products = $productModel->getProductListing(false, false, true, true, false, true, $tempCategory->virtuemart_category_id, false, 0);
		$productModel->addImages($products);
		
		$tempCategory->product_category_related = $products;
		$list_product = array_merge($list_product, $products);
		$category_related[] = $tempCategory;
	}
	
	if (!empty($list_product))
	{
		?>
		<div class="module <?php echo $class ?>">
			<h3 class="modtitle product-fields-title-wrapper">
				<span class="product-fields-title">
					<?php echo vmText::_('Sản Phẩm Tương Tự') ?>
				</span>
			</h3>

			<div class="portfolio-product-wrapper" style="background-color: #f6f6f6 !important;padding: 10px;">
				<ul style="list-style: none;" class="portfolio-product-content portfolio-product-grid clearfix">
					<?php
//						shuffle($list_product);
						$i = 0;
						foreach ($list_product as $key => $product)
						{
							if ($i > 5)
							{
								break;
							}
							
//							if ($product->prices['salesPrice'] <= 0)
//							{
//								continue;
//							}
							
					?>
							<li class="portfolio-product-item col-lg-2 col-md-2 col-sm-6 col-xs-6 ">
								<div class="item-wrap">
									<div class="item-img products-thumb">
										<div class="item-img-slider text-center">
											<a href="<?= $product->link ? $product->link : "#" ?>">
												<img width="600" height="600"
													 class="attachment-shop_single size-full size-shop_single"
													 alt="<?= $product->product_name ? $product->product_name : "alt" ?>"
													 src="<?= is_file($product->file_url) ? $product->file_url : 'images/stories/virtuemart/product/51EQN7k8NcL._AC_SL1024_.jpg' ?>"
												>
												<span class="product_title" itemprop="name">
													 <?php
//														 echo $product->product_name = substr($product->product_name, 0, 70) . '...';
														 echo $product->product_name;
													 ?>
												</span>
												<div class="product-short-description">
													<p style="color: #BFBFC1;font-size: 15px;">
														SKU:<?php echo $product->product_sku; ?></p>
												</div>
											</a>
										</div>
									</div>
								</div>
							</li>
							<?php
							$i++;
						}
					?>
				</ul>
			</div>

			<style>
				.prices-wrap {
					display: none;
				}

				.ratingbox.dummy {
					display: none;
				}
			
				.related-info a {
					font-size: 14px;
					font-weight: 700;
					color: #FF7600;
				}

				.product-price {
					font-size: 23px;
					font-weight: 700;
					color: #f05122;
				}

				.portfolio-product-wrapper {
					padding: 20px 0px 20px 0px;
				}

				li.portfolio-product-item.col-lg-3.col-md-3.col-sm-6.col-xs-6 {
					background-color: #fff;
					border: 2px #f6f6f6 solid;
					border-radius: 15px;
				}

				.product-short-description .Priceproduct_price {
					font-size: 15px;
					font-weight: 700;
					width: max-content;
					color: #BFBFC1 !important;
					text-decoration: line-through;
				}
                .product-related-products img, .product-related-categories img{
                    margin: 0 auto;
                    width: max-content;
                }
			</style>
			<div class="clearfix"></div>
		</div>
		<?php
	}
?>
