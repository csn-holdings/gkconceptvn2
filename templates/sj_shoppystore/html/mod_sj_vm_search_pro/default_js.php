<?php
/**
 * @package SJ Search Pro for VirtueMart
 * @version 3.0.1
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @copyright (c) 2015 YouTech Company. All Rights Reserved.
 * @author YouTech Company http://www.smartaddons.com
 *
 */
defined('_JEXEC') or die;

?>
<script type="text/javascript">
// Autocomplete */

(function($) {
    $.fn.SjSearchautocomplete = function(option) {
        return this.each(function() {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function() {
                this.request();
            });

            // Blur
            $(this).on('blur', function() {
                setTimeout(function(object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function(event) {
                switch(event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function(event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }

            // Show
            this.show = function() {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu-sj').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu-sj').show();
            }

            // Hide
            this.hide = function() {
                $(this).siblings('ul.dropdown-menu-sj').hide();
            }

            // Request
            this.request = function() {
                clearTimeout(this.timer);

                this.timer = setTimeout(function(object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function(json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                        html += '<li class="media" data-value="' + json[i]['value'] + '" title="' + json[i]['label'] + '">';
                        if(json[i]['image'] && json[i]['show_image'] && json[i]['show_image'] == 1 ) {
                            html += '    <a class="media-left" href="' + json[i]['link'] + '">' + json[i]['image'] + '</a>';
                        }
                        html += '<div class="media-body">';
                        html += '<a href="' + json[i]['link'] + '" title="' + json[i]['label'] + '"><span class="sj-search-name"> ' + json[i]['label'] + '</span></a>';
                        if(json[i]['cate_name'] != '' && json[i]['show_catename'])
                        {
                            html += '<span class="sj-search-catename"> <?php echo JText::_("CATEGORY_NAME")?>'+json[i]['cate_name']+'</span>';
                        }
                        if(json[i]['salesPrice'] && json[i]['show_price'] && json[i]['show_price'] == 1){
                            html += '    <div class="box-price">';
                            html += json[i]['salesPrice'];
                            html += json[i]['discountAmount'];
                            html += '    </div>';
                        }
                        html += '</div></li>';
                        html += '';
                        }
                    }

                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu-sj').html(html);
            }

            $(this).after('<ul class="dropdown-menu-sj"></ul>');

        });
    }
})(window.jQuery);

jQuery(document).ready(function($) {
    var selector = '#search<?php  echo $module->id ?>';
    var total = 0;
    var character = <?php  echo ((int)$params['character'] != '' ? (int)$params['character'] : 3);?>;
    var showimage = <?php echo $params['show_image'];?>;
    var showprice = <?php echo $params['show_price'];?>;
    var showcatename = <?php echo $params['show_catename'];?>;
    var $_ajax_url = '<?php echo (string)JURI::getInstance(); ?>';

    function removeVietnameseTones(str) {
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
        str = str.replace(/đ/g,"d");
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
        str = str.replace(/Đ/g, "D");
        // Some system encode vietnamese combining accent as individual utf-8 characters
        // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
        str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
        str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
        // Remove extra spaces
        // Bỏ các khoảng trắng liền nhau
        str = str.replace(/ + /g," ");
        str = str.replace(/\%20/g, "");
        str = str.trim();
        // Remove punctuations
        // Bỏ dấu câu, kí tự đặc biệt
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
        return str;
    }
    
    

    $(selector).find('input[name=\'keyword\']').SjSearchautocomplete({
        delay: 20,
        source: function(request, response) {
            var category_id = $(".select_category select[name=\"virtuemart_category_id\"]").first().val();

            if(typeof(category_id) == 'undefined')
                category_id = 0;
            var limit = <?php echo ((int)$params['limit'] != '' ? (int)$params['limit'] : 5);?>;
            request = removeVietnameseTones(request);
            request = request.trim();
            if(request.length >= character){
                console.log(request)
                $.ajax({
                    type: 'POST',
                    url: $_ajax_url,
                    data: {
                        is_ajax_searchpro: 1,
                        search_module_id: <?php echo $module->id ?>,
                        search_category_id : category_id,
                        search_name : encodeURIComponent(request)
                    },
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            total = 0;
                            if(item.total){
                                total = item.total;
                            }

                            return {
                                salesPrice : item.salesPrice,
                                discountAmount: item.discountAmount,
                                label:   item.name,
                                cate_name:   item.category_name,
                                image:   item.image,
                                link:    item.link,
                                minimum:    item.minimum,
                                show_price:  showprice,
                                show_image:  showimage,
                                show_catename: showcatename    ,
                                value:   item.product_id,
                            }
                        }));
                    }
                });
            }
        },
    });
});

</script>
