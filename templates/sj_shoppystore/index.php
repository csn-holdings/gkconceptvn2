<?php
/*
 * ------------------------------------------------------------------------
 * Copyright (C) 2009 - 2013 The YouTech JSC. All Rights Reserved.
 * @license - GNU/GPL, http://www.gnu.org/licenses/gpl.html
 * Author: The YouTech JSC
 * Websites: http://www.smartaddons.com - http://www.cmsportal.net
 * ------------------------------------------------------------------------
*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
header('X-UA-Compatible: IE=edge');

// Object of class YtTemplate
$doc     = JFactory::getDocument();
$app     = JFactory::getApplication();
$option = $app->input->get('option');

// Check yt plugin
if(!defined('YT_FRAMEWORK')) throw new Exception(JText::_('INSTALL_YT_PLUGIN'));
if(!defined('J_TEMPLATEDIR') )define('J_TEMPLATEDIR', JPATH_SITE.J_SEPARATOR.'templates'.J_SEPARATOR.$this->template);

// Include file: frame_inc.php
 include_once (J_TEMPLATEDIR.J_SEPARATOR.'includes'.J_SEPARATOR.'frame_inc.php');

// Check direction for html
$dir = ($doc->direction == 'rtl') ? ' dir="rtl"' : '';

/** @var YTFramework */
$responsive = $yt->getParam('layouttype');
$favicon     = $yt->getParam('favicon');
$layoutType    = $yt->getParam('layouttype');

?>
<!DOCTYPE html>
<html <?php echo $dir; ?> lang="<?php echo $this->language; ?>">
<head>
    <jdoc:include type="head" />
    <meta name="p:domain_verify" content="8520f9641fbdd61ce0263fbdb86d2640"/>
    <meta name="HandheldFriendly" content="true"/>
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="YES" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="p:domain_verify" content="c3b41067999d3d360a99ae120be19060"/>
    <meta property = "fb: pages" content = "109411643983732" />
    <link href="templates/sj_shoppystore/css/css.scss" type="text/css">
<!--    <script src="https://www.wheelofpopups.com/api/v4492/widget.js" type="text/javascript" defer="defer"></script>-->
    <script src="templates/sj_shoppystore/js/your_js.js" type="text/javascript" defer="defer"></script>
    <!-- META FOR IOS & HANDHELD -->
    <?php if($responsive=='res'): ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
    <?php endif ?>

    <!-- LINK FOR FAVICON -->
    <?php if($favicon) : ?>
        <link rel="icon" type="image/x-icon" href="<?php echo $favicon?>" />
    <?php endif; ?>

    <?php
    // Include css, js
    include_once (J_TEMPLATEDIR.J_SEPARATOR.'includes'.J_SEPARATOR.'head.php');
    ?>

</head>


<?php
    //sub Menu Home page
    $menu = JFactory::getApplication()->getMenu();
    if (is_object( $menu->getActive() ) ) {
        $getMenu_route = $menu->getActive()->route;
        $subMenu_home = strpos($getMenu_route,'home/');
    }else{
        $subMenu_home ='';
    }


    //render a class for home page
    $cls_body = '';
    $cls_body .= $yt->isHomePage() || is_numeric($subMenu_home) ? 'homepage ' : '';

    //Add Layout
    $cls_body .= 'home-'.$layout. ' ';

    //For RTL direction
    $cls_body .= ($doc->direction == 'rtl') ? 'rtl' . ' ' : '';

    //add a class according to the template name
    $cls_body .= ($layoutType !='res') ? 'no-res'.'':'res';

    $type_layout = 'layout-'.$yt->getParam('typelayout');
?>
<body ng-app="myApp" id="bd" class="<?php echo $cls_body; ?>" >
    <!-- Load Facebook SDK for JavaScript -->
<!--    <div id="fb-root"></div>-->
<!--    <script>-->
<!--        window.fbAsyncInit = function() {-->
<!--            FB.init({-->
<!--                xfbml            : true,-->
<!--                version          : 'v9.0'-->
<!--            });-->
<!--        };-->
<!--    -->
<!--        (function(d, s, id) {-->
<!--            var js, fjs = d.getElementsByTagName(s)[0];-->
<!--            if (d.getElementById(id)) return;-->
<!--            js = d.createElement(s); js.id = id;-->
<!--            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';-->
<!--            fjs.parentNode.insertBefore(js, fjs);-->
<!--        }(document, 'script', 'facebook-jssdk'));</script>-->
    
    <!-- Your Chat Plugin code -->
<!--    <div class="fb-customerchat"-->
<!--         attribution=install_email-->
<!--         page_id="725801700949947">-->
<!--    </div>-->
    <jdoc:include type="modules" name="debug" />
<!--    <div class="sidenav">-->
<!--        <ul style="list-style-type: none;" id="sidenavul">-->
<!--            <li><a href="https://zalo.me/0817791179" rel="nofollow" target="_blank"><i class="icon-zalo-circle2"></i>Chat Zalo</a></li>-->
<!--            <li><a href="http://m.me/gkconceptstore" rel="nofollow" target="_blank"><i class="icon-messenger"></i>Messenger</a></li>-->
<!--            <li><a href="tel:0907870909" rel="nofollow" target="_blank"><i class="icon-call"></i></i>Call</a></li>-->
<!--        </ul>-->
<!--    </div>-->

    <!-- Load Facebook SDK for JavaScript -->
<!--    <div id="fb-root"></div>-->
<!--    <script>-->
<!--        window.fbAsyncInit = function() {-->
<!--            FB.init({-->
<!--                xfbml            : true,-->
<!--                version          : 'v9.0'-->
<!--            });-->
<!--        };-->
<!---->
<!--        (function(d, s, id) {-->
<!--            var js, fjs = d.getElementsByTagName(s)[0];-->
<!--            if (d.getElementById(id)) return;-->
<!--            js = d.createElement(s); js.id = id;-->
<!--            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';-->
<!--            fjs.parentNode.insertBefore(js, fjs);-->
<!--        }(document, 'script', 'facebook-jssdk'));</script>-->
<!---->
<!--    <!-- Your Chat Plugin code -->-->
<!--    <div class="fb-customerchat"-->
<!--         attribution="install_email"-->
<!--         page_id="116640790241743"-->
<!--         theme_color="#67b868">-->
<!--    </div>-->
    
    <div id="yt_wrapper" class="<?php echo $type_layout; ?>">
        <?php
        /*render blocks. for positions of blocks, please refer layouts folder. */
        foreach($yt_render->arr_TB as $tagBD) {
            //BEGIN Check if position not empty
            if( $tagBD["countModules"] > 0 ) {

                // BEGIN: Content Area
                if( ($tagBD["name"] == 'content') ) {
                    //class for content area
                    $cls_content  = $tagBD['class_content'];
                    $cls_content  .= ' block '. $option ;
                    echo "<{$tagBD['html5tag']} id=\"{$tagBD['id']}\" class=\"{$cls_content}\">";
                    ?>

                        <div  class="container-fluid">
                            <div  class="row">
                                <?php
                                $countL = $countR = $countM = 0;
                                // BEGIN: foreach position of block content
                                $yt->_countPosGroup($tagBD['positions']);
                                foreach($tagBD['positions'] as $position):
                                    include(J_TEMPLATEDIR.J_SEPARATOR.'includes'.J_SEPARATOR.'block-content.php');
                                endforeach;
                                // END: foreach position of block content
                                ?>
                            </div >
                        </div >

                    <?php
                    echo "</{$tagBD['html5tag']}>";
                    ?>
                    <?php
                // END: Content Area

                // BEGIN: For other blocks
                } elseif ($tagBD["name"] != 'content'){
                    echo "<{$tagBD['html5tag']} id=\"{$tagBD['id']}\" class=\"block\">";
                    ?>
                        <div class="container-fluid">
                            <div class="row">
                            <?php
                            if( !empty($tagBD["hasGroup"]) && $tagBD["hasGroup"] == "1"){
                                // BEGIN: For Group attribute
                                $flag = '';
                                $openG = 0;
                                $c = 0;
                                foreach( $tagBD['positions'] as $posFG ):
                                    $c = $c + 1;
                                    if( $posFG['group'] != "" && $posFG['group'] != $flag){
                                        $flag = $posFG['group'];
                                        if ($openG == 0) {
                                            $openG = 1;
                                            $groupnormal = 'group-' . $flag.$tagBD['class_groupnormal'];
                                            echo '<div class="' . $groupnormal . ' ' . $yt_render->arr_GI[$posFG['group']]['class'] . '">' ;
                                            echo $yt->renPositionsGroup($posFG);
                                            if($c == count( $tagBD['positions']) ) {
                                                echo '</div>';
                                            }
                                        } else {
                                            $openG = 0;
                                            $groupnormal = 'group-' . $flag;
                                            echo '</div>';
                                            echo '<div class="' . $groupnormal . ' '. $yt_render->arr_GI[$posFG['group']]['class'] . '">' ;
                                            echo $yt->renPositionsGroup($posFG);
                                        }
                                    } elseif ($posFG['group'] != "" && $posFG['group'] == $flag){
                                        echo $yt->renPositionsGroup($posFG);
                                        if($c == count( $tagBD['positions']) ) {
                                            echo '</div>';
                                        }
                                    }elseif($posFG['group']==""){
                                        if($openG ==1){
                                            $openG = 0;
                                            echo '</div>';
                                        }
                                        echo $yt->renPositionsGroup($posFG);
                                    }
                                endforeach;
                                // END: For Group attribute
                            }else{
                                // BEGIN: for Tags without group attribute
                                if(isset($tagBD['positions'])){

                                    echo $yt->renPositionsNormal($tagBD['positions'], $tagBD["countModules"],count($tagBD['positions']));
                                }
                                // END: for Tags without group attribute
                            }
                            ?>
                            </div>
                        </div>

                    <?php
                    echo "</{$tagBD['html5tag']}>";
                    ?>
            <?php
               }
               // END: For other blocks
            }
            // END Check if position not empty
        }
        //END: For
        ?>

       
    </div>

    <script>
        var navItems = document.querySelectorAll(".mobile-bottom-nav__item");
        navItems.forEach(function(e, i) {
            e.addEventListener("click", function(e) {
                navItems.forEach(function(e2, i2) {
                    e2.classList.remove("mobile-bottom-nav__item--active");
                })
                this.classList.add("mobile-bottom-nav__item--active");
            });
        });
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5ff561d9a9a34e36b969886e/1erb8cf87';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            s1.setAttribute("id", "id_tawk_gkconcept");
        })();
    </script>
    <!--End of Tawk.to Script-->
    <div id="mobile-bottom-nav">
        <nav class="mobile-bottom-nav" >
            <div class="mobile-bottom-nav__item mobile-bottom-nav__item--active">
                <div class="mobile-bottom-nav__item-content">
                    <a href="https://gkconcept.vn/">
                        <i class="material-icons">home</i>
                        Trang Chủ
                    </a>
                </div>
            </div>
<!--            <div class="mobile-bottom-nav__item">-->
<!--                <div class="mobile-bottom-nav__item-content">-->
<!--                    <script type="text/javascript">-->
<!--                        var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq ||-->
<!--                            {widgetcode:"39ad2845f03f9c5fabd83df7f2cc500e0c5612c94655c2a479553115008065661a2010ab7b6727677d37b27582c0e9c4", values:{},ready:function(){}};-->
<!--                        var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;-->
<!--                        s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");-->
<!--                    </script>-->
<!--                </div>-->
<!--            </div>-->
            <div class="mobile-bottom-nav__item">
                <div class="mobile-bottom-nav__item-content">
                    <a href="tel:02873067888" rel="nofollow">
                        <i class="material-icons">phone</i>
                        Gọi
                    </a>

                </div>
            </div>
<!--            <div class="mobile-bottom-nav__item">-->
<!--                <div class="mobile-bottom-nav__item-content">-->
<!--                    <a href="https://zalo.me/778853679784152759" rel="nofollow" target="_blank" >-->
<!--                        <i class="material-icons">person</i>-->
<!--                        Zalo-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->


        </nav></div>
    
    <?php
        $menubase = J_TEMPLATEDIR.J_SEPARATOR.'menusys';
        $templateMenuBase = new YTMenuBase(
        array(
            'menutype'    => $yt->getParam('menutype'),
            'menustyle'    => 'mobile',
            'basepath'    => str_replace('\\', '/', $menubase)
        ));
        $templateMenuBase->getMenu()->getContent();
        include_once (J_TEMPLATEDIR.J_SEPARATOR.'includes'.J_SEPARATOR.'special-position.php');
        include_once (J_TEMPLATEDIR.J_SEPARATOR.'includes'.J_SEPARATOR.'bottom.php');
    ?>

<!--    <script type="text/javascript">-->
<!--        var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"39ad2845f03f9c5fabd83df7f2cc500e0c5612c94655c2a479553115008065661a2010ab7b6727677d37b27582c0e9c4", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");-->
<!--    </script>-->

<!--    <div class="zalo-chat-widget"-->
<!--         data-oaid="3656918152669400396"-->
<!--         data-welcome-message="Rất vui khi được hỗ trợ bạn!"-->
<!--         data-autopopup="0"-->
<!--         data-width="450"-->
<!--         data-height="520"></div>-->
<!---->
<!--    <script src="https://sp.zalo.me/plugins/sdk.js"></script>-->

    <script>
        (function(w,d,t,u,n,a,m){w['MauticTrackingObject']=n;
            w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)},a=d.createElement(t),
                m=d.getElementsByTagName(t)[0];a.async=1;a.src=u;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://gkconcept.vn/mtc.js','mt');

        mt('send', 'pageview');
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=initialize" async defer></script>
<!--    <script>-->
<!--        (function(d, s, id, t) {-->
<!--            if (d.getElementById(id)) return;-->
<!--            var js, fjs = d.getElementsByTagName(s)[0];-->
<!--            js = d.createElement(s);-->
<!--            js.id = id;-->
<!--            js.src = 'https://widget.oncustomer.asia/js/index.js?token=' + t;-->
<!--            fjs.parentNode.insertBefore(js, fjs);}-->
<!--        (document, 'script', 'oc-chat-widget-bootstrap', '01af53104e856b26dd45cc4f7f687d7e'));-->
<!--    </script>-->

   

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                xfbml            : true,
                version          : 'v10.0'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your Chat Plugin code -->
    <div class="fb-customerchat"
         attribution="setup_tool"
         page_id="116640790241743">
    </div>

    <div class="zalo-chat-widget"
         data-oaid="3656918152669400396"
         data-welcome-message="Rất vui khi được hỗ trợ bạn!"
         data-autopopup="6"
         data-width="350"
         data-height="420">
    </div>

    <script src="https://sp.zalo.me/plugins/sdk.js"></script>









</body>
</html>
