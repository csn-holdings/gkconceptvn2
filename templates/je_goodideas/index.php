<?php

defined('_JEXEC') or die;

JLoader::import('joomla.filesystem.file');

// Output as HTML5
$this->setHtml5(true);

$config         = JFactory::getConfig();
$option         = JFactory::getApplication()->input->getCmd('option', '');


JHtml::_('jquery.framework');
JHtml::_('bootstrap.framework');
JHtml::_('jquery.ui');
	
JHtml::_('stylesheet', 'templates/'.$this->template.'/assets/css/app.css', array('version' => 'auto'));
JHtml::_('stylesheet', 'templates/'.$this->template.'/assets/css/theme.css', array('version' => 'auto'));
JHtml::_('stylesheet', '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css', array('version' => 'auto'));
JHtml::_('stylesheet', '//cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css', array('version' => 'auto'));
JHtml::_('stylesheet', '//cdn.jsdelivr.net/npm/placeholder-loading/dist/css/placeholder-loading.min.css', array('version' => 'auto'));
JHtml::_('stylesheet', 'templates/'.$this->template.'/assets/css/mediumish.css', array('version' => 'auto'));
JHtml::_('stylesheet', 'templates/'.$this->template.'/assets/css/bootstrap.min.css', array('version' => 'auto'));


JHtml::_('script', 'templates/'.$this->template.'/assets/js/app.js');
JHtml::_('script', 'templates/'.$this->template.'/assets/js/theme.js');
JHtml::_('script', 'templates/'.$this->template.'/assets/js/mediumish.js');
JHtml::_('script', 'templates/'.$this->template.'/assets/js/jquery.min.js');
JHtml::_('script', 'templates/'.$this->template.'/assets/js/ie10-viewport-bug-workaround.js');
JHtml::_('script', 'templates/'.$this->template.'/assets/js/bootstrap.min.js');


//require JPATH_ROOT . '/components/com_congtacvien/helpers/vmhelper.php';
//$vmHelper = new VmShopperHelper;
//$vendor = $vmHelper->getVendor();

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="p:domain_verify" content="8520f9641fbdd61ce0263fbdb86d2640"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ý Tưởng Hay - Bộ Sưu Tập - Gkconcept.vn</title>
    <script type="text/javascript">
        (function () {
            var css = document.createElement('link');
            css.href = 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css';
            css.rel = 'stylesheet';
            css.type = 'text/css';
            document.getElementsByTagName('head')[0].appendChild(css);
        })();
    </script>
    <link rel="stylesheet" href="templates/<?php echo $this->template;?>/assets/css/app.css">
    <link rel="stylesheet" href="templates/<?php echo $this->template;?>/assets/css/theme.css">
    <link rel="stylesheet" href="templates/<?php echo $this->template;?>/assets/css/theme.css">
    <link rel="stylesheet" href="templates/<?php echo $this->template;?>/assets/css/mediumish.css">
    <link rel="stylesheet" href="templates/<?php echo $this->template;?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/placeholder-loading/dist/css/placeholder-loading.min.css">
    

</head>
<body>


<jdoc:include type="component" />



<footer class="footer pt-5 pb-5 text-center">
    <div class="container">
        <p>©  <span class="credits font-weight-bold">
            <a target="_blank" class="text-dark" href="https://gkconcept.vn/">Gkconcept.vn</a>
          </span>
        </p>
        <div class="socials-media">
            <ul class="list-unstyled">
                <li class="d-inline-block ml-1 mr-1"><a href="#" class="text-dark"><i class="fa fa-facebook"></i></a></li>
                <li class="d-inline-block ml-1 mr-1"><a href="#" class="text-dark"><i class="fa fa-instagram"></i></a></li>
            </ul>
        </div>
    </div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>

<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="templates/<?php echo $this->template;?>/assets/js/main.js"></script>
<script src="templates/<?php echo $this->template;?>/assets/js/mediumish.js"></script>
<script src="templates/<?php echo $this->template;?>/assets/js/jquery.min.js"></script>
<script src="templates/<?php echo $this->template;?>/assets/js/ie10-viewport-bug-workaround.js"></script>
<script src="templates/<?php echo $this->template;?>/assets/js/bootstrap.min.js"></script>
<script src="templates/<?php echo $this->template;?>/assets/js/app.js"></script>
<script src="templates/<?php echo $this->template;?>/assets/js/theme.js"></script>
</body>

</html>