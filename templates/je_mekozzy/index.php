<?php

defined('_JEXEC') or die;

JLoader::import('joomla.filesystem.file');

// Output as HTML5
$this->setHtml5(true);

$config         = JFactory::getConfig();
$option         = JFactory::getApplication()->input->getCmd('option', '');


JHtml::_('jquery.framework');
JHtml::_('bootstrap.framework');
JHtml::_('jquery.ui');


JHtml::_('stylesheet', 'bootstrap.min.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'font-awesome.min.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'owl.carousel.min.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'animate.min.css', array('version' => 'auto', 'relative' => true));

JHtml::_('stylesheet', 'jquery-ui.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'slick.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'chosen.min.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'pe-icon-7-stroke.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'magnific-popup.min.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'lightbox.min.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'jquery.scrollbar.min.css', array('version' => 'auto', 'relative' => true));

JHtml::_('stylesheet', 'mobile-menu.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'style.css', array('version' => 'auto', 'relative' => true));

JHtml::_('stylesheet', 'templates/'.$this->template.'/javascript/fancybox/source/jquery.fancybox.css', array('version' => 'auto'));
JHtml::_('stylesheet', '//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i&amp;display=swap', array('version' => 'auto'));
JHtml::_('script', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js', array('version' => 'auto'));


JHtml::_('stylesheet', "//fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons", array('relative' => 'stylesheet'));
JHtml::_('stylesheet', "//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css", array('relative' => 'stylesheet'));
JHtml::_('stylesheet', "//cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css", array('relative' => 'stylesheet'));
JHtml::_('stylesheet', "//cdn.jsdelivr.net/npm/placeholder-loading/dist/css/placeholder-loading.min.css", array('relative' => 'stylesheet'));


JHtml::_('script', "//ajax.googleapis.com/ajax/libs/angularjs/1.7.9/angular.min.js");
JHtml::_('script', "//code.angularjs.org/1.4.2/angular-animate.min.js");
JHtml::_('script', "//cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/2.1.0/toaster.js");
JHtml::_('script', JURI::root(true)."media/com_congtacvien/js/ui-bootstrap-tpls-3.0.6.min.js");




require JPATH_ROOT . '/components/com_congtacvien/helpers/vmhelper.php';

$vmHelper = new VmShopperHelper;

$vendor = $vmHelper->getVendor();


?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

<head>
    <meta charset="UTF-8">
	<meta http-equiv="X-UA-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=yes"/>
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="YES" />
	<link rel="shortcut icon" type="image/x-icon" href="templates/<?php echo $this->template;?>/images/favicon.png"/>

    <jdoc:include type="head" />

    <script src="templates/<?php echo $this->template;?>/javascript/jquery.plugin-countdown.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/jquery-countdown.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/owl.carousel.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/magnific-popup.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/isotope.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/jquery.scrollbar.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/jquery-ui.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/mobile-menu.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/chosen.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/slick.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/jquery.elevateZoom.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/jquery.actual.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/fancybox/source/jquery.fancybox.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/lightbox.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/owl.thumbs.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/jquery.scrollbar.min.js"></script>
    <script src="templates/<?php echo $this->template;?>/javascript/frontend-plugin.js"></script>


</head>


<body class="home">
<div ng-app="myApp">
	<header class="header style7">
		<div class="top-bar">
			<div class="container">
				<div class="top-bar-left">
					<div class="header-message">
						Welcome to our online store!
					</div>
                    <jdoc:include type="modules" name="top2" />
				</div>
				<div class="top-bar-right">
                    <jdoc:include type="modules" name="top1" />
				</div>
			</div>
		</div>
		<div class="container">
			<div class="main-header">
				<div class="row">
					<div class="col-lg-3 col-sm-4 col-md-3 col-xs-7 col-ts-12 header-element">

                        <?php if ($this->countModules('shopper-logo')) : ?>
                            <div id="top">
                                <jdoc:include type="modules" name="shopper-logo" />
                            </div>
                        <?php endif; ?>

					</div>
					<div class="col-lg-7 col-sm-8 col-md-6 col-xs-5 col-ts-12">
						<div class="block-search-block">
							<form class="form-search form-search-width-category">
								<div class="form-content">
									<div class="category">
										<select title="cate" data-placeholder="All Categories" class="chosen-select"
												tabindex="1">
											<option value="United States">Accessories</option>
											<option value="United Kingdom">Chairs</option>
											<option value="Afghanistan">Tables</option>
											<option value="Aland Islands">Sofas</option>
											<option value="Albania">New Arrivals</option>
											<option value="Algeria">Storage</option>
										</select>
									</div>
									<div class="inner">
										<input type="text" class="input" name="s" value="" placeholder="Search here">
									</div>
									<button class="btn-search" type="submit">
										<span class="icon-search"></span>
									</button>
								</div>
							</form>
						</div>
					</div>
					<div class="col-lg-2 col-sm-12 col-md-3 col-xs-12 col-ts-12">

						<div class="header-control">

                            <jdoc:include type="modules" name="user2" />

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="header-nav-container rows-space-20">
			<div class="container">
				<div class="header-nav-wapper main-menu-wapper">
					<div class="vertical-wapper block-nav-categori">
						<div class="block-title">
							<span class="icon-bar">
								<span></span>
								<span></span>
								<span></span>
							</span>
							<span class="text">All Departments</span>
						</div>
						<div class="block-content verticalmenu-content">
							<ul class="vereesa-nav-vertical vertical-menu vereesa-clone-mobile-menu">
								<li class="menu-item">
									<a href="#" class="vereesa-menu-item-title" title="New Arrivals">New Arrivals</a>
								</li>
								<li class="menu-item">
									<a title="Hot Sale" href="#" class="vereesa-menu-item-title">Hot Sale</a>
								</li>
								<li class="menu-item menu-item-has-children">
									<a title="Accessories" href="#" class="vereesa-menu-item-title">Accessories</a>
									<span class="toggle-submenu"></span>
									<ul role="menu" class=" submenu">
										<li class="menu-item">
											<a title="Clock" href="#" class="vereesa-item-title">Clock</a>
										</li>
										<li class="menu-item">
											<a title="Chairs" href="#" class="vereesa-item-title">Chairs</a>
										</li>
										<li class="menu-item">
											<a title="New Arrivals" href="#" class="vereesa-item-title">New Arrivals</a>
										</li>
										<li class="menu-item">
											<a title="Accessories" href="#" class="vereesa-item-title">Accessories</a>
										</li>
										<li class="menu-item">
											<a title="Storage" href="#" class="vereesa-item-title">Storage</a>
										</li>
									</ul>
								</li>
								<li class="menu-item">
									<a title="Chairs" href="#" class="vereesa-menu-item-title">Chairs</a>
								</li>
								<li class="menu-item">
									<a title="Lamp" href="#" class="vereesa-menu-item-title">Lamp</a>
								</li>
								<li class="menu-item">
									<a title="Lighting" href="#" class="vereesa-menu-item-title">Lighting</a>
								</li>
								<li class="menu-item">
									<a title="Homewares" href="#" class="vereesa-menu-item-title">Homewares</a>
								</li>
								<li class="menu-item">
									<a title="Bottles" href="#" class="vereesa-menu-item-title">Bottles</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="header-nav">
						<div class="container-wapper">
							<ul class="vereesa-clone-mobile-menu vereesa-nav main-menu " id="menu-main-menu">
								<li class="menu-item  menu-item-has-children">
									<a href="index.html" class="vereesa-menu-item-title" title="Home">Home</a>
									<span class="toggle-submenu"></span>
									<ul class="submenu">
										<li class="menu-item">
											<a href="index.html">Home 01</a>
										</li>
										<li class="menu-item">
											<a href="home2.html">Home 02</a>
										</li>
										<li class="menu-item">
											<a href="home3.html">Home 03</a>
										</li>
									</ul>
								</li>
								<li class="menu-item menu-item-has-children">
									<a href="gridproducts.html" class="vereesa-menu-item-title" title="Shop">Shop</a>
									<span class="toggle-submenu"></span>
									<ul class="submenu">
										<li class="menu-item">
											<a href="gridproducts.html">Grid Fullwidth</a>
										</li>
										<li class="menu-item">
											<a href="gridproducts_leftsidebar.html">Grid Left sidebar</a>
										</li>
										<li class="menu-item">
											<a href="gridproducts_bannerslider.html">Grid Bannerslider</a>
										</li>
										<li class="menu-item">
											<a href="listproducts.html">List</a>
										</li>
									</ul>
								</li>
								<li class="menu-item  menu-item-has-children item-megamenu">
									<a href="#" class="vereesa-menu-item-title" title="Pages">Pages</a>
									<span class="toggle-submenu"></span>
									<div class="submenu mega-menu menu-page">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 menu-page-item">
												<div class="vereesa-custommenu default">
													<h2 class="widgettitle">Shop Pages</h2>
													<ul class="menu">
														<li class="menu-item">
															<a href="shoppingcart.html">Shopping Cart</a>
														</li>
														<li class="menu-item">
															<a href="checkout.html">Checkout</a>
														</li>
														<li class="menu-item">
															<a href="contact.html">Contact us</a>
														</li>
														<li class="menu-item">
															<a href="404page.html">404</a>
														</li>
														<li class="menu-item">
															<a href="login.html">Login/Register</a>
														</li>
													</ul>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 menu-page-item">
												<div class="vereesa-custommenu default">
													<h2 class="widgettitle">Product</h2>
													<ul class="menu">
														<li class="menu-item">
															<a href="productdetails-fullwidth.html">Product Fullwidth</a>
														</li>
														<li class="menu-item">
															<a href="productdetails-leftsidebar.html">Product left sidebar</a>
														</li>
														<li class="menu-item">
															<a href="productdetails-rightsidebar.html">Product right sidebar</a>
														</li>
													</ul>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 menu-page-item">

											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 menu-page-item">
											</div>
										</div>
									</div>
								</li>
								<li class="menu-item  menu-item-has-children">
									<a href="inblog_right-siderbar.html" class="vereesa-menu-item-title"
									   title="Blogs">Blogs</a>
									<span class="toggle-submenu"></span>
									<ul class="submenu">
										<li class="menu-item menu-item-has-children">
											<a href="#" class="vereesa-menu-item-title" title="Blog Style">Blog Style</a>
											<span class="toggle-submenu"></span>
											<ul class="submenu">
												<li class="menu-item">
													<a href="bloggrid.html">Grid</a>
												</li>
												<li class="menu-item">
													<a href="blogmasonry.html">Masonry</a>
												</li>
												<li class="menu-item">
													<a href="bloglist.html">List</a>
												</li>
												<li class="menu-item">
													<a href="bloglist-leftsidebar.html">List Sidebar</a>
												</li>
											</ul>
										</li>
										<li class="menu-item menu-item-has-children">
											<a href="#" class="vereesa-menu-item-title" title="Post Layout">Post Layout</a>
											<span class="toggle-submenu"></span>
											<ul class="submenu">
												<li class="menu-item">
													<a href="inblog_left-siderbar.html">Left Sidebar</a>
												</li>
												<li class="menu-item">
													<a href="inblog_right-siderbar.html">Right Sidebar</a>
												</li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="menu-item">
									<a href="about.html" class="vereesa-menu-item-title" title="About">About</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="header-device-mobile">
		<div class="wapper">
			<div class="item mobile-logo">
				<div class="logo">
					<a href="#">
						<img src="<?php echo $vendor?>" alt="img">
					</a>
				</div>
			</div>
			<div class="item item mobile-search-box has-sub">
				<a href="#">
						<span class="icon">
							<i class="fa fa-search" aria-hidden="true"></i>
						</span>
				</a>
				<div class="block-sub">
					<a href="#" class="close">
						<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<div class="header-searchform-box">
						<form class="header-searchform">
							<div class="searchform-wrap">
								<input type="text" class="search-input" placeholder="Enter keywords to search...">
								<input type="submit" class="submit button" value="Search">
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="item mobile-settings-box has-sub">
				<a href="#">
						<span class="icon">
							<i class="fa fa-cog" aria-hidden="true"></i>
						</span>
				</a>
				<div class="block-sub">
					<a href="#" class="close">
						<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<div class="block-sub-item">
						<h5 class="block-item-title">Currency</h5>
						<form class="currency-form vereesa-language">
							<ul class="vereesa-language-wrap">
								<li class="active">
									<a href="#">
											<span>
												English (USD)
											</span>
									</a>
								</li>
								<li>
									<a href="#">
											<span>
												French (EUR)
											</span>
									</a>
								</li>
								<li>
									<a href="#">
											<span>
												Japanese (JPY)
											</span>
									</a>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
			<div class="item menu-bar">
				<a class=" mobile-navigation  menu-toggle" href="#">
					<span></span>
					<span></span>
					<span></span>
				</a>
			</div>
		</div>
	</div>

	<div>
	    <div class="fullwidth-template">
			<div class="home-slider-banner rows-space-40">
				<div class="container">
					<div class="row10">
						<div class="col-lg-8 silider-wrapp">
							<div class="home-slider">
								<div class="slider-owl owl-slick equal-container nav-center"
									 data-slick='{"autoplay":true, "autoplaySpeed":9000, "arrows":false, "dots":true, "infinite":true, "speed":1000, "rows":1}'
									 data-responsive='[{"breakpoint":"2000","settings":{"slidesToShow":1}}]'>
									<div class="slider-item style7">
										<div class="slider-inner equal-element">
											<div class="slider-infor">
												<h5 class="title-small">
													Sale up to 40% off!
												</h5>
												<h3 class="title-big">
													The home scandinavian
												</h3>
												<div class="price">
													New Price:
													<span class="number-price">
														€270.00
													</span>
												</div>
												<a href="#" class="button btn-browse">Browse</a>
												<a href="#" class="button btn-shop-the-look bgroud-style">Shop Now</a>
											</div>
										</div>
									</div>
									<div class="slider-item style8">
										<div class="slider-inner equal-element">
											<div class="slider-infor">
												<h5 class="title-small">
													Black Friday Sale!
												</h5>
												<h3 class="title-big">
													Lighting Collection Big Sale
												</h3>
												<div class="price">
													Save Price:
													<span class="number-price">
														€170.00
													</span>
												</div>
												<a href="#" class="button btn-shop-product">Shop Now</a>
											</div>
										</div>
									</div>
									<div class="slider-item style9">
										<div class="slider-inner equal-element">
											<div class="slider-infor">
												<h5 class="title-small">
													Vereesa Lamp Collection
												</h5>
												<h3 class="title-big">
													The inspiration behind our store
												</h3>
												<a href="#" class="button btn-chekout">Shop Now</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 banner-wrapp">
							<div class="banner">
								<div class="item-banner style7">
									<div class="inner">
										<div class="banner-content">
											<h3 class="title">Lighting Collection</h3>
											<div class="description">
												Inspire ur lighting style with us.
											</div>
											<a href="#" class="button btn-lets-do-it">Shop Now</a>
										</div>
									</div>
								</div>
							</div>
							<div class="banner">
								<div class="item-banner style8">
									<div class="inner">
										<div class="banner-content">
											<h3 class="title">Norm Alarm Clock</h3>
											<div class="description">
												Vereesa style, day by day!
											</div>
											<span class="price">€379.00</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="vereesa-product produc-featured rows-space-65">
	            <div class="container">
	                <h3 class="custommenu-title-blog">
						Deal of the day
	                </h3>
	                <div class="owl-products owl-slick equal-container nav-center"  data-slick ='{"autoplay":false, "autoplaySpeed":1000, "arrows":false, "dots":true, "infinite":false, "speed":800, "rows":1}' data-responsive='[{"breakpoint":"2000","settings":{"slidesToShow":4}},{"breakpoint":"1200","settings":{"slidesToShow":3}},{"breakpoint":"992","settings":{"slidesToShow":2}},{"breakpoint":"480","settings":{"slidesToShow":1}}]'>
	                    <div class="product-item style-5">
	                        <div class="product-inner equal-element">
	                            <div class="product-top">
	                                <div class="flash">
											<span class="onnew">
												<span class="text">
													new
												</span>
											</span>
	                                </div>
	                                <div class="yith-wcwl-add-to-wishlist">
	                                    <div class="yith-wcwl-add-button">
	                                        <a href="#">Add to Wishlist</a>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="product-thumb">
	                                <div class="thumb-inner">
	                                    <a href="#">
	                                        <img src="templates/je_mekozzy/images/product-item-17.jpg" alt="img">
	                                    </a>
	                                </div>
	                                <a href="#" class="button quick-wiew-button">Quick View</a>
	                               	<div class="product-count-down">
	                                	<div class="vereesa-countdown" data-y="2020" data-m="9" data-w="2" data-d="30"  data-h="20" data-i="60" data-s="60"></div>
	                                </div>
	                            </div>
	                            <div class="product-info">
	                                <h5 class="product-name product_title">
	                                    <a href="#">Plastic Dining Chair</a>
	                                </h5>
	                                <div class="group-info">
	                                    <div class="stars-rating">
	                                        <div class="star-rating">
	                                            <span class="star-3"></span>
	                                        </div>
	                                        <div class="count-star">
	                                            (3)
	                                        </div>
	                                    </div>
	                                    <div class="price">
	                                        <del>
	                                            €65
	                                        </del>
	                                        <ins>
	                                            €45
	                                        </ins>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="loop-form-add-to-cart">
	                                <form class="cart">
	                                    <div class="single_variation_wrap">
	                                        <div class="quantity">
	                                            <div class="control">
	                                                <a class="btn-number qtyminus quantity-minus" href="#">-</a>
	                                                <input type="text" data-step="1" data-min="0" value="1" title="Qty"
	                                                       class="input-qty qty" size="4">
	                                                <a href="#" class="btn-number qtyplus quantity-plus">+</a>
	                                            </div>
	                                        </div>
	                                        <button class="single_add_to_cart_button button">Add to cart</button>
	                                    </div>
	                                    <div class="variations">
	                                        <div class="variation">
	                                            <label class="variation-label">
	                                                Capacity:
	                                            </label>
	                                            <div class="variation-value">
	                                                <a href="#" class="change-value text">
	                                                    <span>XS</span>
	                                                </a>
	                                                <a href="#" class="change-value text">
	                                                    <span>S</span>
	                                                </a>
	                                                <a href="#" class="change-value text">
	                                                    <span>M</span>
	                                                </a>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </form>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="product-item style-5">
	                        <div class="product-inner equal-element">
	                            <div class="product-top">
	                                <div class="flash">
											<span class="onnew">
												<span class="text">
													new
												</span>
											</span>
	                                </div>
	                                <div class="yith-wcwl-add-to-wishlist">
	                                    <div class="yith-wcwl-add-button">
	                                        <a href="#">Add to Wishlist</a>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="product-thumb">
	                                <div class="thumb-inner">
	                                    <a href="#">
	                                        <img src="templates/je_mekozzy/images/product-item-21.jpg" alt="img">
	                                    </a>
	                                </div>
	                                <a href="#" class="button quick-wiew-button">Quick View</a>
	                                <div class="product-count-down">
	                                	<div class="vereesa-countdown" data-y="2020" data-m="9" data-w="2" data-d="30"  data-h="20" data-i="60" data-s="60"></div>
	                                </div>
	                            </div>
	                            <div class="product-info">
	                                <h5 class="product-name product_title">
	                                    <a href="#">Plastic Dining Chair</a>
	                                </h5>
	                                <div class="group-info">
	                                    <div class="stars-rating">
	                                        <div class="star-rating">
	                                            <span class="star-3"></span>
	                                        </div>
	                                        <div class="count-star">
	                                            (3)
	                                        </div>
	                                    </div>
	                                    <div class="price">
	                                        <del>
	                                            €65
	                                        </del>
	                                        <ins>
	                                            €45
	                                        </ins>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="loop-form-add-to-cart">
	                                <form class="cart">
	                                    <div class="single_variation_wrap">
	                                        <div class="quantity">
	                                            <div class="control">
	                                                <a class="btn-number qtyminus quantity-minus" href="#">-</a>
	                                                <input type="text" data-step="1" data-min="0" value="1" title="Qty"
	                                                       class="input-qty qty" size="4">
	                                                <a href="#" class="btn-number qtyplus quantity-plus">+</a>
	                                            </div>
	                                        </div>
	                                        <button class="single_add_to_cart_button button">Add to cart</button>
	                                    </div>
	                                    <div class="variations">
	                                        <div class="variation">
	                                            <label class="variation-label">
	                                                Capacity:
	                                            </label>
	                                            <div class="variation-value">
	                                                <a href="#" class="change-value text">
	                                                    <span>M</span>
	                                                </a>
	                                                <a href="#" class="change-value text">
	                                                    <span>L</span>
	                                                </a>
	                                                <a href="#" class="change-value text">
	                                                    <span>XL</span>
	                                                </a>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </form>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="product-item style-5">
	                        <div class="product-inner equal-element">
	                            <div class="product-top">
	                                <div class="flash">
											<span class="onnew">
												<span class="text">
													new
												</span>
											</span>
	                                </div>
	                                <div class="yith-wcwl-add-to-wishlist">
	                                    <div class="yith-wcwl-add-button">
	                                        <a href="#">Add to Wishlist</a>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="product-thumb">
	                                <div class="thumb-inner">
	                                    <a href="#">
	                                        <img src="templates/je_mekozzy/images/product-item-20.jpg" alt="img">
	                                    </a>
	                                </div>
	                                <a href="#" class="button quick-wiew-button">Quick View</a>
	                                <div class="product-count-down">
	                                	<div class="vereesa-countdown" data-y="2020" data-m="9" data-w="2" data-d="30"  data-h="20" data-i="60" data-s="60"></div>
	                                </div>
	                            </div>
	                            <div class="product-info">
	                                <h5 class="product-name product_title">
	                                    <a href="#">Plastic Dining Chair</a>
	                                </h5>
	                                <div class="group-info">
	                                    <div class="stars-rating">
	                                        <div class="star-rating">
	                                            <span class="star-3"></span>
	                                        </div>
	                                        <div class="count-star">
	                                            (3)
	                                        </div>
	                                    </div>
	                                    <div class="price">
	                                        <del>
	                                            €65
	                                        </del>
	                                        <ins>
	                                            €45
	                                        </ins>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="loop-form-add-to-cart">
	                                <form class="cart">
	                                    <div class="single_variation_wrap">
	                                        <div class="quantity">
	                                            <div class="control">
	                                                <a class="btn-number qtyminus quantity-minus" href="#">-</a>
	                                                <input type="text" data-step="1" data-min="0" value="1" title="Qty"
	                                                       class="input-qty qty" size="4">
	                                                <a href="#" class="btn-number qtyplus quantity-plus">+</a>
	                                            </div>
	                                        </div>
	                                        <button class="single_add_to_cart_button button">Add to cart</button>
	                                    </div>
	                                    <div class="variations">
	                                        <div class="variation">
	                                            <label class="variation-label">
	                                                Capacity:
	                                            </label>
	                                            <div class="variation-value">
	                                                <a href="#" class="change-value text">
	                                                    <span>XS</span>
	                                                </a>
	                                                <a href="#" class="change-value text">
	                                                    <span>M</span>
	                                                </a>
	                                                <a href="#" class="change-value text">
	                                                    <span>L</span>
	                                                </a>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </form>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="product-item style-5">
	                        <div class="product-inner equal-element">
	                            <div class="product-top">
	                                <div class="flash">
											<span class="onnew">
												<span class="text">
													new
												</span>
											</span>
	                                </div>
	                                <div class="yith-wcwl-add-to-wishlist">
	                                    <div class="yith-wcwl-add-button">
	                                        <a href="#">Add to Wishlist</a>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="product-thumb">
	                                <div class="thumb-inner">
	                                    <a href="#">
	                                        <img src="templates/je_mekozzy/images/product-item-22.jpg" alt="img">
	                                    </a>
	                                </div>
	                                <a href="#" class="button quick-wiew-button">Quick View</a>
	                                <div class="product-count-down">
	                                	<div class="vereesa-countdown" data-y="2020" data-m="9" data-w="2" data-d="30"  data-h="20" data-i="60" data-s="60"></div>
	                                </div>
	                            </div>
	                            <div class="product-info">
	                                <h5 class="product-name product_title">
	                                    <a href="#">Plastic Dining Chair</a>
	                                </h5>
	                                <div class="group-info">
	                                    <div class="stars-rating">
	                                        <div class="star-rating">
	                                            <span class="star-3"></span>
	                                        </div>
	                                        <div class="count-star">
	                                            (3)
	                                        </div>
	                                    </div>
	                                    <div class="price">
	                                        <del>
	                                            €65
	                                        </del>
	                                        <ins>
	                                            €45
	                                        </ins>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="loop-form-add-to-cart">
	                                <form class="cart">
	                                    <div class="single_variation_wrap">
	                                        <div class="quantity">
	                                            <div class="control">
	                                                <a class="btn-number qtyminus quantity-minus" href="#">-</a>
	                                                <input type="text" data-step="1" data-min="0" value="1" title="Qty"
	                                                       class="input-qty qty" size="4">
	                                                <a href="#" class="btn-number qtyplus quantity-plus">+</a>
	                                            </div>
	                                        </div>
	                                        <button class="single_add_to_cart_button button">Add to cart</button>
	                                    </div>
	                                    <div class="variations">
	                                        <div class="variation">
	                                            <label class="variation-label">
	                                                Capacity:
	                                            </label>
	                                            <div class="variation-value">
	                                                <a href="#" class="change-value text">
	                                                    <span>XS</span>
	                                                </a>
	                                                <a href="#" class="change-value text">
	                                                    <span>S</span>
	                                                </a>
	                                                <a href="#" class="change-value text">
	                                                    <span>L</span>
	                                                </a>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </form>
	                            </div>
	                        </div>
	                    </div>
						<div class="product-item style-5">
							<div class="product-inner equal-element">
								<div class="product-top">
									<div class="flash">
											<span class="onnew">
												<span class="text">
													new
												</span>
											</span>
									</div>
									<div class="yith-wcwl-add-to-wishlist">
										<div class="yith-wcwl-add-button">
											<a href="#">Add to Wishlist</a>
										</div>
									</div>
								</div>
								<div class="product-thumb">
									<div class="thumb-inner">
										<a href="#">
											<img src="templates/je_mekozzy/images/product-item-19.jpg" alt="img">
										</a>
									</div>
									<a href="#" class="button quick-wiew-button">Quick View</a>
									<div class="product-count-down">
										<div class="vereesa-countdown" data-y="2020" data-m="9" data-w="2" data-d="30"  data-h="20" data-i="60" data-s="60"></div>
									</div>
								</div>
								<div class="product-info">
									<h5 class="product-name product_title">
										<a href="#">Plastic Dining Chair</a>
									</h5>
									<div class="group-info">
										<div class="stars-rating">
											<div class="star-rating">
												<span class="star-3"></span>
											</div>
											<div class="count-star">
												(3)
											</div>
										</div>
										<div class="price">
											<del>
												€65
											</del>
											<ins>
												€45
											</ins>
										</div>
									</div>
								</div>
								<div class="loop-form-add-to-cart">
									<form class="cart">
										<div class="single_variation_wrap">
											<div class="quantity">
												<div class="control">
													<a class="btn-number qtyminus quantity-minus" href="#">-</a>
													<input type="text" data-step="1" data-min="0" value="1" title="Qty"
														   class="input-qty qty" size="4">
													<a href="#" class="btn-number qtyplus quantity-plus">+</a>
												</div>
											</div>
											<button class="single_add_to_cart_button button">Add to cart</button>
										</div>
										<div class="variations">
											<div class="variation">
												<label class="variation-label">
													Capacity:
												</label>
												<div class="variation-value">
													<a href="#" class="change-value text">
														<span>XS</span>
													</a>
													<a href="#" class="change-value text">
														<span>M</span>
													</a>
													<a href="#" class="change-value text">
														<span>L</span>
													</a>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="product-item style-5">
							<div class="product-inner equal-element">
								<div class="product-top">
									<div class="flash">
											<span class="onnew">
												<span class="text">
													new
												</span>
											</span>
									</div>
									<div class="yith-wcwl-add-to-wishlist">
										<div class="yith-wcwl-add-button">
											<a href="#">Add to Wishlist</a>
										</div>
									</div>
								</div>
								<div class="product-thumb">
									<div class="thumb-inner">
										<a href="#">
											<img src="templates/je_mekozzy/images/product-item-18.jpg" alt="img">
										</a>
									</div>
									<a href="#" class="button quick-wiew-button">Quick View</a>
									<div class="product-count-down">
										<div class="vereesa-countdown" data-y="2020" data-m="9" data-w="2" data-d="30"  data-h="20" data-i="60" data-s="60"></div>
									</div>
								</div>
								<div class="product-info">
									<h5 class="product-name product_title">
										<a href="#">Plastic Dining Chair</a>
									</h5>
									<div class="group-info">
										<div class="stars-rating">
											<div class="star-rating">
												<span class="star-3"></span>
											</div>
											<div class="count-star">
												(3)
											</div>
										</div>
										<div class="price">
											<del>
												€65
											</del>
											<ins>
												€45
											</ins>
										</div>
									</div>
								</div>
								<div class="loop-form-add-to-cart">
									<form class="cart">
										<div class="single_variation_wrap">
											<div class="quantity">
												<div class="control">
													<a class="btn-number qtyminus quantity-minus" href="#">-</a>
													<input type="text" data-step="1" data-min="0" value="1" title="Qty"
														   class="input-qty qty" size="4">
													<a href="#" class="btn-number qtyplus quantity-plus">+</a>
												</div>
											</div>
											<button class="single_add_to_cart_button button">Add to cart</button>
										</div>
										<div class="variations">
											<div class="variation">
												<label class="variation-label">
													Capacity:
												</label>
												<div class="variation-value">
													<a href="#" class="change-value text">
														<span>XS</span>
													</a>
													<a href="#" class="change-value text">
														<span>S</span>
													</a>
													<a href="#" class="change-value text">
														<span>L</span>
													</a>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
	                </div>
	            </div>
	        </div>
			<div class="banner-wrapp">
				<div class="container">
					<div class="banner">
						<div class="item-banner style17">
							<div class="inner">
								<div class="banner-content">
									<h3 class="title">Living Room Furniture</h3>
									<div class="description">
										You have no chair & Are you <br />ready to grow? come & shop with us!
									</div>
									<div class="banner-price">
										Price from:
										<span class="number-price">€45.00</span>
									</div>
									<a href="#" class="button btn-shop-now">SHOP NOW</a>
									<a href="#" class="button btn-view-collection">VIEW COLLECTION</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="banner-wrapp rows-space-65">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="banner">
								<div class="item-banner style4">
									<div class="inner">
										<div class="banner-content">
											<h4 class="vereesa-subtitle">Chair Essential!</h4>
											<h3 class="title">Lounge Chair</h3>
											<div class="description">
												Hot Lounge Chair are now available in our stock.
											</div>
											<a href="#" class="button btn-shop-now">SHOP NOW</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="banner">
								<div class="item-banner style5">
									<div class="inner">
										<div class="banner-content">
											<h3 class="title">Lighting Room<br />Collection</h3>
											<span class="code">
												Use code:
												<span>
													VARESSA
												</span>
												Get
												<span>
													25% Off
												</span>
												for all Accessories items!
											</span>
											<a href="#" class="button btn-shop-now">SHOP NOW</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

            <div class="container">
                <jdoc:include type="message" />

                <jdoc:include type="component" />
            </div>

            <div class="vereesa-tabs  default">
				<div class="container">
					<div class="tab-head">
						<ul class="tab-link">
							<li class="active">
								<a data-toggle="tab" aria-expanded="true" href="#bestseller">Bestseller</a>
							</li>
							<li class="">
								<a data-toggle="tab" aria-expanded="true" href="#new_arrivals">New Arrivals </a>
							</li>
							<li class="">
								<a data-toggle="tab" aria-expanded="true" href="#top-rated">Top Rated</a>
							</li>
						</ul>
					</div>
					<div class="tab-container">
						<div id="bestseller" class="tab-panel active">
							<div class="vereesa-product">
								<ul class="row list-products auto-clear equal-container product-grid">
									<li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-1.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button">Quick View</a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text over">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-2.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text over">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-3.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text active">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-4.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text active">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text over">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-5.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-6.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text over">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-7.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text over">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-8.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text active">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div id="new_arrivals" class="tab-panel">
							<div class="vereesa-product">
								<ul class="row list-products auto-clear equal-container product-grid">
									<li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-9.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button">Quick View</a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text active">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-10.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text active">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-11.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-13.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-14.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-15.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text active">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-16.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-2.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div id="top-rated" class="tab-panel">
							<div class="vereesa-product">
								<ul class="row list-products auto-clear equal-container product-grid">
									<li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-10.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button">Quick View</a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text active">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-12.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text active">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-8.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text active">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-4.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-9.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-13.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item product-type-variable col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-16.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text active">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
									<li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
										<div class="product-inner equal-element">
											<div class="product-top">
												<div class="flash">
													<span class="onnew">
														<span class="text">
															new
														</span>
													</span>
												</div>
												<div class="yith-wcwl-add-to-wishlist">
													<div class="yith-wcwl-add-button">
														<a href="#">Add to Wishlist</a>
													</div>
												</div>
											</div>
											<div class="product-thumb">
												<div class="thumb-inner">
													<a href="#">
														<img src="templates/je_mekozzy/images/product-item-8.jpg" alt="img">
													</a>
												</div>
												<a href="#" class="button quick-wiew-button"></a>
											</div>
											<div class="product-info">
												<h5 class="product-name product_title">
													<a href="#">Plastic Dining Chair</a>
												</h5>
												<div class="group-info">
													<div class="stars-rating">
														<div class="star-rating">
															<span class="star-3"></span>
														</div>
														<div class="count-star">
															(3)
														</div>
													</div>
													<div class="price">
														<del>
															€65
														</del>
														<ins>
															€45
														</ins>
													</div>
												</div>
											</div>
											<div class="loop-form-add-to-cart">
												<form class="cart">
													<div class="single_variation_wrap">
														<div class="quantity">
															<div class="control">
																<a class="btn-number qtyminus quantity-minus" href="#">-</a>
																<input type="text" data-step="1" data-min="0" value="1"
																	   title="Qty" class="input-qty qty" size="4">
																<a href="#" class="btn-number qtyplus quantity-plus">+</a>
															</div>
														</div>
														<button class="single_add_to_cart_button button">Add to cart
														</button>
													</div>
													<div class="variations">
														<div class="variation">
															<label class="variation-label">
																Capacity:
															</label>
															<div class="variation-value">
																<a href="#" class="change-value text active">
																	<span>10ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>20ml</span>
																</a>
																<a href="#" class="change-value text">
																	<span>50ml</span>
																</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="newsletter-wrapp rows-space-70">
				<div class="container">
					<div class="vereesa-newsletter style3 ">
						<div class="newsletter-head">
							<h3 class="title">Sign up Newsletter</h3>
							<div class="subtitle">
								Subscribe to get more special Deals,<br />
								Events & Promotions
							</div>
						</div>
						<div class="newsletter-form-wrap">
							<input class="input-text email email-newsletter" type="email" name="email" placeholder="Your email letter">
							<button class="button btn-submit submit-newsletter">SUBSCRIBE</button>
						</div>
					</div>
				</div>
			</div>
			<div class="vereesa-blog-wraap default">
				<div class="container">
					<h3 class="custommenu-title-blog">
						Our Latest News
					</h3>
					<div class="vereesa-blog default">
						<div class="owl-slick equal-container nav-center"
							 data-slick='{"autoplay":false, "autoplaySpeed":1000, "arrows":false, "dots":true, "infinite":true, "speed":800, "rows":1}'
							 data-responsive='[{"breakpoint":"2000","settings":{"slidesToShow":3}},{"breakpoint":"1200","settings":{"slidesToShow":3}},{"breakpoint":"992","settings":{"slidesToShow":2}},{"breakpoint":"768","settings":{"slidesToShow":2}},{"breakpoint":"481","settings":{"slidesToShow":1}}]'>
							<div class="vereesa-blog-item equal-element layout1">
								<div class="post-thumb">
									<a href="#">
										<img src="templates/je_mekozzy/images/slider-blog-thumb-1.jpg" alt="img">
									</a>
									<div class="category-blog">
										<ul class="list-category">
											<li class="category-item">
												<a href="#">Skincare</a>
											</li>
										</ul>
									</div>
									<div class="post-item-share">
										<a href="#" class="icon">
											<i class="fa fa-share-alt" aria-hidden="true"></i>
										</a>
										<div class="box-content">
											<a href="#">
												<i class="fa fa-facebook"></i>
											</a>
											<a href="#">
												<i class="fa fa-twitter"></i>
											</a>
											<a href="#">
												<i class="fa fa-google-plus"></i>
											</a>
											<a href="#">
												<i class="fa fa-pinterest"></i>
											</a>
											<a href="#">
												<i class="fa fa-linkedin"></i>
											</a>
										</div>
									</div>
								</div>
								<div class="blog-info">
									<div class="blog-meta">
										<div class="post-date">
											Agust 17, 09:14 am
										</div>
										<span class="view">
											<i class="icon fa fa-eye" aria-hidden="true"></i>
											631
										</span>
										<span class="comment">
											<i class="icon fa fa-commenting" aria-hidden="true"></i>
											84
										</span>
									</div>
									<h2 class="blog-title">
										<a href="#">We bring you the best by working</a>
									</h2>
									<div class="main-info-post">
										<p class="des">
											Phasellus condimentum nulla a arcu lacinia, a venenatis ex congue.
											Mauris nec ante magna.
										</p>
									</div>
								</div>
							</div>
							<div class="vereesa-blog-item equal-element layout1">
								<div class="post-thumb">
									<a href="#">
										<img src="templates/je_mekozzy/images/slider-blog-thumb-2.jpg" alt="img">
									</a>
									<div class="category-blog">
										<ul class="list-category">
											<li class="category-item">
												<a href="#">HOW TO</a>
											</li>
										</ul>
									</div>
									<div class="post-item-share">
										<a href="#" class="icon">
											<i class="fa fa-share-alt" aria-hidden="true"></i>
										</a>
										<div class="box-content">
											<a href="#">
												<i class="fa fa-facebook"></i>
											</a>
											<a href="#">
												<i class="fa fa-twitter"></i>
											</a>
											<a href="#">
												<i class="fa fa-google-plus"></i>
											</a>
											<a href="#">
												<i class="fa fa-pinterest"></i>
											</a>
											<a href="#">
												<i class="fa fa-linkedin"></i>
											</a>
										</div>
									</div>
								</div>
								<div class="blog-info">
									<div class="blog-meta">
										<div class="post-date">
											Agust 17, 09:14 am
										</div>
										<span class="view">
											<i class="icon fa fa-eye" aria-hidden="true"></i>
											631
										</span>
										<span class="comment">
											<i class="icon fa fa-commenting" aria-hidden="true"></i>
											84
										</span>
									</div>
									<h2 class="blog-title">
										<a href="#">We know that buying furniture</a>
									</h2>
									<div class="main-info-post">
										<p class="des">
											Using Lorem Ipsum allows designers to put together layouts
											and the form content
										</p>
									</div>
								</div>
							</div>
							<div class="vereesa-blog-item equal-element layout1">
								<div class="post-thumb">
									<div class="video-vereesa-blog">
										<figure>
											<img src="templates/je_mekozzy/images/slider-blog-thumb-3.jpg" alt="img" width="370"
												 height="280">
										</figure>
										<a class="quick-install" href="#" data-videosite="vimeo"
										   data-videoid="134060140"></a>
									</div>
									<div class="category-blog">
										<ul class="list-category">
											<li class="category-item">
												<a href="#">VIDEO</a>
											</li>
										</ul>
									</div>
									<div class="post-item-share">
										<a href="#" class="icon">
											<i class="fa fa-share-alt" aria-hidden="true"></i>
										</a>
										<div class="box-content">
											<a href="#">
												<i class="fa fa-facebook"></i>
											</a>
											<a href="#">
												<i class="fa fa-twitter"></i>
											</a>
											<a href="#">
												<i class="fa fa-google-plus"></i>
											</a>
											<a href="#">
												<i class="fa fa-pinterest"></i>
											</a>
											<a href="#">
												<i class="fa fa-linkedin"></i>
											</a>
										</div>
									</div>
								</div>
								<div class="blog-info">
									<div class="blog-meta">
										<div class="post-date">
											Agust 17, 09:14 am
										</div>
										<span class="view">
											<i class="icon fa fa-eye" aria-hidden="true"></i>
											631
										</span>
										<span class="comment">
											<i class="icon fa fa-commenting" aria-hidden="true"></i>
											84
										</span>
									</div>
									<h2 class="blog-title">
										<a href="#">We design functional furniture</a>
									</h2>
									<div class="main-info-post">
										<p class="des">
											Risus non porta suscipit lobortis habitasse felis, aptent
											interdum pretium ut.
										</p>
									</div>
								</div>
							</div>
							<div class="vereesa-blog-item equal-element layout1">
								<div class="post-thumb">
									<a href="#">
										<img src="templates/je_mekozzy/images/slider-blog-thumb-4.jpg" alt="img">
									</a>
									<div class="category-blog">
										<ul class="list-category">
											<li class="category-item">
												<a href="#">Skincare</a>
											</li>
										</ul>
									</div>
									<div class="post-item-share">
										<a href="#" class="icon">
											<i class="fa fa-share-alt" aria-hidden="true"></i>
										</a>
										<div class="box-content">
											<a href="#">
												<i class="fa fa-facebook"></i>
											</a>
											<a href="#">
												<i class="fa fa-twitter"></i>
											</a>
											<a href="#">
												<i class="fa fa-google-plus"></i>
											</a>
											<a href="#">
												<i class="fa fa-pinterest"></i>
											</a>
											<a href="#">
												<i class="fa fa-linkedin"></i>
											</a>
										</div>
									</div>
								</div>
								<div class="blog-info">
									<div class="blog-meta">
										<div class="post-date">
											Agust 17, 09:14 am
										</div>
										<span class="view">
											<i class="icon fa fa-eye" aria-hidden="true"></i>
											631
										</span>
										<span class="comment">
											<i class="icon fa fa-commenting" aria-hidden="true"></i>
											84
										</span>
									</div>
									<h2 class="blog-title">
										<a href="#">We know that buying furniture</a>
									</h2>
									<div class="main-info-post">
										<p class="des">
											Class integer tellus praesent at torquent cras, potenti erat fames
											volutpat etiam.
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>



        </div>



	</div>



    <footer class="footer style7">
		<div class="container">
			<div class="container-wapper">
				<div class="row">
					<div class="box-footer col-xs-12 col-sm-6 col-md-6 col-lg-4">
						<div class="widget-box">
							<div class="single-img">
                                <a href="//<?php echo $vendor->vendor_url ?>.mekozzy.com" alt="<?php echo $vendor->customtitle ?>">
                                    <img style="height:90px" src="<?php echo $vendor->file_url?>" alt="img">
                                </a>
							</div>
							<div class="text-content-elememnt">
								<p>
									Shop the latest products right from
									your home .
								</p>
								<p>We have furniture supplies & accessories from top brands.</p>
							</div>
						</div>
						<div class="vereesa-socials">
							<ul class="socials">
								<li>
									<a href="#" class="social-item" target="_blank">
										<i class="icon fa fa-facebook"></i>
									</a>
								</li>
								<li>
									<a href="#" class="social-item" target="_blank">
										<i class="icon fa fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="#" class="social-item" target="_blank">
										<i class="icon fa fa-instagram"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="box-footer col-xs-12 col-sm-6 col-md-6 col-lg-2">
						<div class="vereesa-custommenu default">
							<h2 class="widgettitle">Links</h2>
							<ul class="menu">
								<li class="menu-item">
									<a href="#">Tables</a>
								</li>
								<li class="menu-item">
									<a href="#">Lighting</a>
								</li>
								<li class="menu-item">
									<a href="#">Lighting</a>
								</li>
								<li class="menu-item">
									<a href="#">Gift Vouchers</a>
								</li>
								<li class="menu-item">
									<a href="#">Accessories</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="box-footer col-xs-12 col-sm-6 col-md-6 col-lg-2">
						<div class="vereesa-custommenu default">
							<h2 class="widgettitle">Information</h2>
							<ul class="menu">
								<li class="menu-item">
									<a href="#">FAQs</a>
								</li>
								<li class="menu-item">
									<a href="#">Track Order</a>
								</li>
								<li class="menu-item">
									<a href="#">Delivery</a>
								</li>
								<li class="menu-item">
									<a href="#">Contact Us</a>
								</li>
								<li class="menu-item">
									<a href="#">Return</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="box-footer col-xs-12 col-sm-6 col-md-6 col-lg-4">
						<div class="vereesa-newsletter style1">
							<div class="newsletter-head">
								<h3 class="title">Newsletter</h3>
							</div>
							<div class="newsletter-form-wrap">
								<div class="list">
									Get notified of new products, limited releases, and more.
								</div>
								<input type="email" class="input-text email email-newsletter"
									   placeholder="Your email letter">
								<button class="button btn-submit submit-newsletter">SUBSCRIBE</button>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 border-custom">
						<span></span>
					</div>
				</div>
				<div class="footer-end">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="coppyright">
								Copyright © 2019
								<a href="#">Vereesa</a>
								. All rights reserved
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="vereesa-payment">
								<img src="templates/je_mekozzy/images/payments.png" alt="img">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="footer-device-mobile">
		<div class="wapper">
			<div class="footer-device-mobile-item device-home">
				<a href="index.html">
					<span class="icon">
						<i class="fa fa-home" aria-hidden="true"></i>
					</span>
					Home
				</a>
			</div>
			<div class="footer-device-mobile-item device-home device-wishlist">
				<a href="#">
					<span class="icon">
						<i class="fa fa-heart" aria-hidden="true"></i>
					</span>
					Wishlist
				</a>
			</div>
			<div class="footer-device-mobile-item device-home device-cart">
				<a href="#">
					<span class="icon">
						<i class="fa fa-shopping-basket" aria-hidden="true"></i>
						<span class="count-icon">
							0
						</span>
					</span>
					<span class="text">Cart</span>
				</a>
			</div>
			<div class="footer-device-mobile-item device-home device-user">
				<a href="login.html">
					<span class="icon">
						<i class="fa fa-user" aria-hidden="true"></i>
					</span>
					Account
				</a>
			</div>
		</div>
	</div>


    <a href="#" class="backtotop">
		<i class="pe-7s-angle-up"></i>
	</a>

    <jdoc:include type="modules" name="debug" />

</div>
</body>

<!-- BigPigShop -->
</html>