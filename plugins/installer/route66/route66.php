<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

class PlgInstallerRoute66 extends JPlugin
{
	public function onInstallerBeforePackageDownload(&$url, &$headers)
	{
		if (strpos($url, 'firecoders.com') && strpos($url, 'route'))
		{
			$separator = strpos($url, '?') !== false ? '&' : '?';
			$params = JComponentHelper::getParams('com_route66');
			$url .= $separator . 'dlid=' . $params->get('downloadId');
		}

		return true;
	}
}
