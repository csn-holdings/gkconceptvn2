<?php

/**
 * @version    CVS: 1.0.0
 * @package    plg_content_vm2share
 * @author     Nordmograph <contact@nordmograph.com>
 * @copyright  2017 Nordmograph
 * @license    GNU General Public License version 2 orversion ultérieure ; Voir LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
 
/**
 * Script file of HelloWorld module
 */
jimport('joomla.installer.installer');
jimport('joomla.installer.helper');
class plgcontentvm2shareInstallerScript
{
	
	/**
	 * Method to install the extension
	 * $parent is the class calling this method
	 *
	 * @return void
	 */
	function install($parent) 
	{
	}
 
	/**
	 * Method to uninstall the extension
	 * $parent is the class calling this method
	 *
	 * @return void
	 */
	function uninstall($parent) 
	{
		//echo '<p>The module has been uninstalled</p>';
	}
 
	/**
	 * Method to update the extension
	 * $parent is the class calling this method
	 *
	 * @return void
	 */
	function update($parent) 
	{
		
		
	}
 
	/**
	 * Method to run before an install/update/uninstall method
	 * $parent is the class calling this method
	 * $type is the type of change (install, update or discover_install)
	 *
	 * @return void
	 */
	function preflight($type, $parent) 
	{
		
	}
 
	/**
	 * Method to run after an install/update/uninstall method
	 * $parent is the class calling this method
	 * $type is the type of change (install, update or discover_install)
	 *
	 * @return void
	 */
	function postflight($type, $parent) 
	{
		$app = JFactory::getApplication();	
		$user = JFactory::getUser();	
		$db	= JFactory::getDBO();
		
		if($type=='install' OR $type=='update' )
		{
			if(file_exists (JPATH_ADMINISTRATOR.'/components/com_virtuemart/virtuemart.php') )
			{
				// enable joomla plugins in VM config
				$q = "SELECT config FROM #__virtuemart_configs WHERE virtuemart_config_id ='1'  ";
				$db->setQuery( $q );
				$vmconfig = $db->loadResult();
				if( strpos( $vmconfig , '|enable_content_plugin="1"')!==false)
					$app->enqueueMessage('<i class="icon-ok"></i> Joomla content plugins option allready enabled in Virtuemart configuration (VM2Share requirement). Well done !','message');
				else
				{
					$vmconfig = str_replace("|enable_content_plugin=\"0\"" , "|enable_content_plugin=\"1\"" , $vmconfig);
					$q = "UPDATE #__virtuemart_configs SET config='".$db->escape($vmconfig)."' WHERE virtuemart_config_id ='1' ";
					$db->setQuery( $q );
					$db->execute();	
					$app->enqueueMessage('<i class="icon-ok"></i> Joomla content plugins option is now enabled in Virtuemart configuration (VM2Share requirement).','message');
				}
			}	
		}					
	}
}