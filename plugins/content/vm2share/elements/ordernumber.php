<?php
/**
 * @version    CVS: 1.0.0
 * @author     Nordmograph <contact@nordmograph.com>
 * @copyright  2016 Nordmograph
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */
//v1.4
defined('JPATH_BASE') or die;
jimport('joomla.html.html');
jimport('joomla.form.formfield');
class JFormFieldOrdernumber extends JFormField
{
	protected $type = 'ordernumber';
	protected function getInput()
	{
		$product_url 	= 'https://www.nordmograph.com/extensions/index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=15&virtuemart_category_id=4&Itemid=58';
		$demo_url		= 'https://www.nordmograph.com/extensions/index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=15&virtuemart_category_id=4&Itemid=58';
		$doc_url		= '';
		$support_url	= 'https://www.nordmograph.com/extensions/index.php?option=com_kunena&view=category&catid=74&Itemid=645';
		$review_url		= 'http://extensions.joomla.org/extensions/extension/extension-specific/virtuemart-extensions/share-for-virtuemart';
		// Initialize variables.
		$app = JFactory::getApplication();
		$element_name = $app->input->get('component');
		if($element_name!='')
		{
			$element = 'component';
			$params = JComponentHelper::getParams( $element_name );	
			$pid = $params->get('pid');
			$catid = $params->get('catid');	
			$element_name_xml = $element_name;
		}
		else
		{
			if( $this->form->getValue('module')!='' )
			{
				$element = 'module';
				$element_name = $this->form->getValue($element);			
				$element_name_xml = $element_name;
			}
			elseif( $this->form->getValue('element')!='' )
			{
				$element = 'element'; // plugin
				$element_name = $this->form->getValue($element);
				$folder = $this->form->getValue('folder');
				$element_name_xml = 'plg_'.$folder.'_'.$element_name;
			}
			$params = $this->form->getValue('params');	
			if(isset($params->pid))
				$pid = $params->pid;
			if(isset($params->catid))
				$catid = $params->catid;
		}
		
		$html = array();
		$html[] = '<style>.alert{padding: 3px 15px 3px 14px}</style>';

		$domain = str_replace('www.','',$_SERVER['HTTP_HOST']);
		
		$db = JFactory::getDBO();		
		$q ="SELECT manifest_cache FROM #__extensions 
		WHERE element='".$element_name."' ";
		$db->setQuery($q);
		$version = json_decode( $db->loadResult()) ;
		$current_version = $version->{'version'};

		$updater_xml_url = 'https://www.nordmograph.com/extensions/updateserver/'.$element_name_xml.'-update.xml';
		$xml_content = $this->getXMLContent($updater_xml_url);
		// update the site
		if($this->value)
		{
			$q = $db->getQuery(true)
			->update('#__update_sites')
			->set('extra_query ="dlid='.trim($this->value).'&d='.urlencode(base64_encode($domain)).'"')
			->where('location = "'.$updater_xml_url.'"');
			$db->setQuery($q);
			$db->execute();
		}

		$xmlObject = simplexml_load_string($xml_content);
		if(isset($xmlObject->update))
			$latest_version = $xmlObject->update->version;
		else
			$latest_version = '-';
			
		$html[] =  '<div><div class="pull-left"><input type="text" name="'.$this->name.'" 
		id="jform_ordernumber" class="" value="'.trim($this->value).'" /></div>';
		
		if(!$this->value)
		{
			$html[] = ' <div class="alert alert-warning pull-left" ><i class="icon-warning"></i> 
			This is required to allow automatic updates via Joomla updater for commercial extensions. Login and get it from 
			your <a href="https://www.nordmograph.com/extensions/index.php?option=com_virtuemart&view=orders&layout=list" 
			target="_blank" class="btn btn-success btn-mini" >ORDER HISTORY</a></div>';
		}
		else
		{
			$checkorder_url = 'https://www.nordmograph.com/extensions/index.php?option=com_extupdater&view=checkorder&format=raw';
			$checkorder_url .= '&pid='.$pid.'&dlid='.trim($this->value).'&d='.urlencode(base64_encode($domain));
			$checkorder_result = $this->getXMLContent( $checkorder_url );
			
			if($checkorder_result[0]=='!')
				$html[] =  '<div class="alert alert-warning pull-left" ><i class="icon-warning"></i> '.substr($checkorder_result,1);
			else
				$html[] =  '<div class="alert alert-success pull-left" ><i class="icon-ok"></i> '.$checkorder_result;	
			$html[] =  '</div>';
			
		}
		$html[] =  '<div style="clear:both"></div></div>';
		
		if($this->value && $checkorder_result[0]!='!' )
		{
			$checkdomain_url = 'https://www.nordmograph.com/extensions/index.php?option=com_extupdater&view=checkdomain&format=raw';
			$checkdomain_url .= '&pid='.$pid.'&dlid='.trim($this->value).'&d='.urlencode(base64_encode($domain));
			$checkdomain_result = $this->getXMLContent( $checkdomain_url );
			
			if($checkdomain_result[0]=='!')
			{
				$html[] =  '<div>';
				$html[] =  '<div class="pull-left"><input type="text" name="domain"  readonly="readonly" class="" value="'.$domain.'" /></div>';	
				$html[] =  '<div class="alert alert-warning pull-left" ><i class="icon-warning"></i> '.substr($checkdomain_result,1).'</div>';
				$html[] =  '<div style="clear:both"></div>';
				$html[] = '</div>';
			}
		}
		
		$html[] = '<div>Latest version: <span class="badge">'.$latest_version.'</span>';	

		if(version_compare($latest_version, $current_version)>=1)
		{
			$html[] = ' - Your version: <span class="badge badge-warning">'.$current_version.'</span>';	
			if($this->value)
			{
				if($checkorder_result[0]!='!' && $checkdomain_result[0]!='!')
					$html[] = ' <a href="index.php?option=com_installer&view=update" class="btn btn-success btn-mini">UPDATE</a>';
			}
			else
			{
				$product_url = 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$pid;
				if($catid)
					$product_url .= '&virtuemart_category_id='.$catid;
				$product_url	.= '&Itemid=58';
				$html[] = ' <a href="'.$product_url.'" target="_blank" class="btn btn-success btn-mini">RENEW</a>';
			}
		}
		else
		{	
			$html[] = ' - Your version: <span class="badge badge-success">'.$current_version.'</span> <i class="icon-ok"></i> ';	
		}

		$html[] =  '<div style="clear:both"></div></div>';
		
		
		if($product_url)
			$html[] =  '<br /><a class="btn btn-mini" href="'.$product_url.'" target="_blank" ><i class="icon-cube" ></i> EXTENSION PAGE</a> ';
		if($demo_url)
			$html[] =  ' <a class="btn btn-mini" href="'.$demo_url.'" target="_blank" ><i class="icon-screen" ></i> DEMO</a> ';
		if($doc_url)
			$html[] =  ' <a class="btn btn-mini" href="'.$doc_url.'" target="_blank" ><i class="icon-book" ></i> DOCUMENTATION</a> ';
		if($support_url)
			$html[] =  ' <a class="btn btn-mini" href="'.$support_url.'" target="_blank" ><i class="icon-help" ></i> SUPPORT</a> ';
		if($review_url)
			$html[] =  ' <a class="btn btn-mini" href="'.$review_url.'" target="_blank" ><i class="icon-heart" ></i> REVIEW</a> ';
		
		return implode($html);
	}
	
	public function getXMLContent( $url)
	{
			if (function_exists('curl_init'))
			{
				$c = curl_init();
				curl_setopt($c, CURLOPT_URL, $url);
				curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
				$content = trim(curl_exec($c));
				curl_close($c);
			}
			elseif (ini_get('allow_url_fopen'))
			{
				$timeout = ini_set('default_socket_timeout', 30);
				$fp = @fopen($url, 'r');
				$content = @fread($fp, 4096);
				@fclose($fp);
			}
			return $content;
	}
}