<?php
/**
 * @version 3.7.0
 * @package Virtuemart VM2share
 * @author   Nordmograph
 * @link http://www.nordmograph.com
 * @license GNU General Public License version 2 or later; see LICENSE.txt
 * @copyright Copyright (C) 2017 nordmograph.com. All rights reserved.
*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class plgContentVm2share extends JPlugin 
{
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}
	public function get_string_between($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}
	public function onContentPrepare($context, &$row, &$params, $page = 0)
	{	
		$inarticles  = $this->params->get('inarticles', 0);
		$app = JFactory::getApplication();
		$page_format = $app->input->get->get('format');		
		if(
		( $context == 'com_virtuemart.productdetails' && (!$page_format or $page_format=='html') )
		OR
		( $context == 'com_content.article' && $inarticles)
		)
		{	
			$juri 				= JURI::base();
			$db 				= JFactory::getDBO();
			$doc				= JFactory::getDocument();
			$doc->addStyleSheet($juri.'plugins/content/vm2share/css/fontello.css');
			$doc->addStyleSheet($juri.'plugins/content/vm2share/css/style.css');		
			$lang 				= JFactory::getLanguage();
			$langtag 			= $lang->get('tag');
			$langtag			=str_replace("-","_",$langtag);
			if (!class_exists( 'VmConfig' ) && $context == 'com_virtuemart.productdetails' )
				require JPATH_ADMINISTRATOR . '/components/com_virtuemart/helpers/config.php';
			
			// Get Current URL	
			$uri 	 			= JFactory::getURI();
			$href				= $uri->toString();
			$href = str_replace('&showall=1','',$href);
			$encodedurl 		= urlencode($href);	
			$position 			= $this->params->get('position', 'top');
			$addtocartarea		= $this->params->get('addtocartarea', '.spacer-buy-area');
			
			$show_text			= $this->params->get('show_text', '');
			$size				= $this->params->get('size', 'share-btn-md');
			$style				= $this->params->get('style', 'share-btn-branded');
			
			$linkedin 			= $this->params->get('linkedin', 1);
			$whatsapp	 		= $this->params->get('whatsapp', 1);
			$tweet 				= $this->params->get('tweet', 1);
			$tw_related			= $this->params->get('tw_related');
			$pinterest			= $this->params->get('pinterest', 1);
			
			$stumbleupon		= $this->params->get('stumbleupon', 1);
			$tumblr				= $this->params->get('tumblr', 1);
			$reddit				= $this->params->get('reddit', 1);
			$telegram			= $this->params->get('telegram', 1);
			$show_more			= $this->params->get('show_more', 1);
			
			$facebooklike 		= $this->params->get('facebooklike', 1);
			$facebookcomments 	= $this->params->get('facebookcomments',1);
			$fbcomments_width	= $this->params->get('fbcomments_width','680');
			$numposts			= $this->params->get('numposts','5');
			$appid				= $this->params->get('appid');
			$default_og_image	= $this->params->get('default_og_image');
			$fb_lang			= $this->params->get('fb_lang', 0);
			$fb_colorscheme		= $this->params->get('fb_colorscheme', 'light');
			$fb_action 			= $this->params->get('fb_action', 'like');
			$fb_width			= $this->params->get('fb_width', '150');
			$fb_layout			= $this->params->get('fb_layout', 'button_count');
			$friends_faces		= $this->params->get('friends_faces', 'false');
			$include_share		= $this->params->get('include_share', 'true');
			$include_send		= $this->params->get('include_send', 1);
			$mixi				= $this->params->get('mixi', 0);
			$og_type			= $this->params->get('og_type', 'product');
			$og_type_article	= $this->params->get('og_type_article', 'article');
			$show_qrcode		= $this->params->get('show_qrcode', 1);
			$description_base	= $this->params->get('description_base', 'product_s_desc');
			
			$exclude_articles	= $this->params->get('exclude_articles', array() );
			
			$selection_sharer	= $this->params->get('selection_sharer', 1);
			
			$product_id = 0;
			if($context == 'com_virtuemart.productdetails')
			{
				$product_id			= $row->virtuemart_product_id;
				$q = "SELECT m.`file_url` FROM `#__virtuemart_medias` AS m 
				 LEFT JOIN `#__virtuemart_product_medias` AS pm ON pm.`virtuemart_media_id` = m.`virtuemart_media_id` 
				 WHERE pm.`virtuemart_product_id`=".$db->quote($product_id)."  AND (pm.ordering='0'  OR pm.ordering='1' )
				 AND SUBSTR( m.file_mimetype , 1 ,6 ) = 'image/' AND m.published='1' ";
				$db->setQuery( $q );
				$thumb = $juri.$db->loadResult();
				$thumb = str_replace(' ' ,'%20',$thumb);
				
				$q = "SELECT `product_s_desc` , `product_desc` , `product_name` 
				FROM `#__virtuemart_products_".VMLANG."` WHERE `virtuemart_product_id`=".$db->quote($product_id);
				$db->setQuery( $q );
				$prod_data = $db->loadObject();
				$prod_name = $prod_data->product_name;
				
				if($description_base=='product_desc')
				{
					$prod_desc = $prod_data->product_desc;
				}
				else
				{
					$prod_desc = $prod_data->product_s_desc;
				}
				
				if($prod_desc=='')
				{
					$prod_desc = $prod_data->product_desc;
				}
				if($prod_desc=='')
				{
					$prod_desc = $prod_data->product_s_desc;
				}
			}
			elseif($context == 'com_content.article')
			{
				if(in_array($row->id , $exclude_articles) )
				{
					return;
				}

				$img_json = json_decode($row->images);
				$thumb = $img_json->{'image_intro'};

				if(substr($thumb,0,4)!='http' && $thumb!='')
				{
					$thumb = $juri.$thumb;
				}



				if($row->introtext!='')
				{
					$prod_desc = strip_tags($row->introtext);
				}
				else
				{
					$prod_desc = strip_tags($row->fulltext);
				}
				
				if( $thumb=='')
					$thumb = $juri.$default_og_image;
					
				$prod_desc = substr($prod_desc, 0, 300);
				$prod_name = JFactory::getConfig()->get( 'sitename' );			
				$og_type = $og_type_article;
				if($position=='cart')
					$position='top';
			}

			$doc->addCustomTag('<meta property="fb:app_id" content="'.$appid.'"/>');
			$doc->addCustomTag('<meta property="og:type" content="'.$og_type.'"/>');
			if($thumb!='')
			{
				$doc->addCustomTag('<meta property="og:image" content="'.$thumb.'"/>');
			}
			$doc->addCustomTag('<meta property="og:site_name" content="'.$prod_name.'"/>');
			$doc->addCustomTag('<meta property="og:description" content="'.strip_tags($prod_desc).'"/>');
			$doc->addCustomTag('<meta property="og:title" content="'.$prod_name.'"/>');			
			$doc->addCustomTag('<meta property="og:url" content="'.$href.'"/>');
			
			if($selection_sharer)
			{
				JText::script('PLG_VM2SHARE_SHARE_SELECTION_ON');
				JText::script('PLG_VM2SHARE_SHARE_SELECTION_BY_EMAIL');
				JText::script('PLG_VM2SHARE_QUOTE_FROM');
				JText::script('PLG_VM2SHARE_FROM');
				JText::script('PLG_VM2SHARE_SHARE_SELECTION_READ');
				$doc->addStyleSheet($juri.'plugins/content/vm2share/css/selection-sharer.css');
				$doc->addScript($juri.'plugins/content/vm2share/js/chunkify.js');
				$doc->addScript($juri.'plugins/content/vm2share/js/selection-sharer.js');
			}
			
			if($whatsapp || $mixi || $show_qrcode)
			{
				$appWeb      = new JApplicationWeb;
			}
			
			if(@$row->allPrices[0]['product_price'] )
			{
				$q = "SELECT currency_code_3 , currency_decimal_place
				FROM #__virtuemart_currencies 
				WHERE virtuemart_currency_id=".$db->quote($row->allPrices[0]['product_currency']);
				$db->setQuery($q);
				$currency = $db->loadObject();
				$product_price = number_format($row->allPrices[0]['salesPrice'] , $currency->currency_decimal_place);
				//format price for FB opengraph
				$product_price = str_replace(',','.',$product_price );
				if( mb_substr_count( $product_price, '.')==2)
				{
					$strpos = strpos(  $product_price , '.');
					$product_price = substr_replace( $product_price ,'', $strpos ,1);
				}
				$doc->addCustomTag('<meta property="product:price:amount" content="'.$product_price.'"/>');
				$doc->addCustomTag('<meta property="product:price:currency" content="'.$currency->currency_code_3.'"/>');
				$doc->addCustomTag('<meta property="product:availability" content="instock"/>');
				$doc->addCustomTag('<meta property="product:condition" content="new"/>');
				$doc->addCustomTag('<meta property="product:is_product_shareable" content="true"/>');
			}
			
			$html ='<div class="vm2sharefb"><div class="vm2share" >
				<div class="clearfix">';
			if($position=='cart')
				$html .= '<br />';	
			
			if($facebooklike>0)
			{
				if(!$fb_lang)
					$fb_lang = $langtag;
						
				if($facebooklike==1)
				{
					$html .= '<a id="vm_fblike-iframe" class="share-btn-f '.$style.' '.$size.'">';
					$url ='https://www.facebook.com/plugins/like.php?locale='.$fb_lang.'&amp;href='.$encodedurl.'&amp;layout='.$fb_layout.'&amp;show_faces='.$friends_faces.'&amp;width='.$fb_width.'&amp;action='.$fb_action.'&amp;colorscheme='.$fb_colorscheme;
					if($appid!="")
						$url .='&amp;appId='.$appid;
					$fb_height = '20';
					if($fb_layout=='box_count')
						$fb_height = '65';
					$html .= '<iframe src="'.$url.'" scrolling="no" frameborder="0" allowTransparency="true" 
					style="border:none;overflow:hidden; width:'.$fb_width.'px; height:'.$fb_height.'px"></iframe>';
				}
				elseif($facebooklike==2)
				{
					$html .= '<a id="vm_fblike-html5" class="share-btn-f '.$style.' '.$size.'">';
					if($appid!="")
					{
						$html .= '<span id="fb-root" ></span>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/'.$fb_lang.'/sdk.js#xfbml=1&version=v2.8&appId='.$appid.'";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, \'script\', \'facebook-jssdk\'));</script>';
					}
					$html .= '<span class="fb-like" data-href="'.$href.'" 
					data-width="'.$fb_width.'" data-layout="'.$fb_layout.'" 
					data-action="'.$fb_action.'"  data-show-faces="'.$friends_faces.'" 
					></span>';
				}
				$html .= '</a>';
			}
			
			if($include_send)
			{
				$html .= '<a class="share-btn-f '.$style.' '.$size.' share-btn-send fb-send " data-href="'.$href.'"  ></a>';
			}
			
			if($include_share)
			{	
				$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-facebook"
				   href="https://www.facebook.com/sharer/sharer.php?u='.$encodedurl.'"
				   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , 'Facebook').'">
					<i class="vm2s-icon-facebook"></i>
					<span class="share-btn-text'.$show_text.'">Facebook</span>
				</a>';
			}
			
			
			if($facebookcomments)
			{
				if(strpos($_SERVER['SERVER_NAME'],'nordmograph') && !in_array($product_id , array('15','178') ) )
				{
					$fbcomments ='';
				}
				else
				{
					$scroll = "function scrollToAnchor(aid){
						    var aTag = jQuery(\"a[name='\"+ aid +\"']\");
						    jQuery('html,body').animate({scrollTop: aTag.offset().top},'slow');
						}";
					$doc->addScriptDeclaration($scroll);
					
					$fbcomments = '<div class="fb-comments" data-href="'.$href.'" 
					data-numposts="5" data-width="'.$fbcomments_width.'" 
					data-colorscheme="'.$fb_colorscheme.'"></div>';	
					$html .= '<a class="share-btn-c '.$style.' '.$size.' share-btn-facebook"
					    onclick="scrollToAnchor(\'fb-comments\');"
					   title="'.sprintf( JText::_('PLG_VM2SHARE_COMMENT_ON') , 'Facebook').'">
						<i class="vm2s-icon-comment"></i>
						<span class="share-btn-text-sr">'.sprintf( JText::_('PLG_VM2SHARE_COMMENT_ON') , 'Facebook').'</span>
					</a>';
				}	
				
			}
			
			if($tweet)
			{
				$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-twitter"
				   href="https://twitter.com/share?url='.$encodedurl.'&text='.$prod_name.'&via='.$tw_related.'&lang='.substr($langtag,0,2).'"
				   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , 'Twitter').'">
					<i class="vm2s-icon-twitter"></i>
					<span class="share-btn-text'.$show_text.'">Twitter</span>
				</a>';	
			}
			
			if($linkedin)
			{				
				$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-linkedin"
				   href="https://www.linkedin.com/shareArticle?mini=true&url='.$encodedurl.'"
				   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , 'LinkedIn').'">
					<i class="vm2s-icon-linkedin"></i>
					<span class="share-btn-text'.$show_text.'">LinkedIn</span>
				</a>';
			}	
			
			if($reddit)
			{
				$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-reddit"
				   href="https://www.reddit.com/submit?url='.$encodedurl.'"
				   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , 'Reddit').'">
					<i class="vm2s-icon-reddit"></i>
					<span class="share-btn-text'.$show_text.'">Reddit</span>
				</a>';
			}
			
			if($tumblr)
			{
				$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-tumblr"
				   href="https://www.tumblr.com/share/link?url='.$encodedurl.'"
				   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , 'Tumblr').'">
					<i class="vm2s-icon-tumbler"></i>
					<span class="share-btn-text'.$show_text.'">Tumblr</span>
				</a>';
			}
			
			if($stumbleupon)
			{
				$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-stumbleupon"
				   href="http://www.stumbleupon.com/submit?url='.$encodedurl.'"
				   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , 'Stumbleupon').'">
					<i class="vm2s-icon-stumbleupon"></i>
					<span class="share-btn-text'.$show_text.'">Stumbleupon</span>
				</a>';
			}

			
			if($whatsapp)
			{
				if ($appWeb->client->mobile )
				{
					$html .= '<a class="share-btn-c '.$style.' '.$size.' share-btn-whatsapp"
					   href="whatsapp://send?text='.JText::_('PLG_VM2SHARE_WHATSAPPSHARE').' '.$prod_name.' - '.$encodedurl.'"
					   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , 'Whatsapp').'" data-action="share/whatsapp/share" 
						data-text="'.$prod_name.'" data-href="'.$href.'">
						<i class="vm2s-icon-whatsapp"></i>
						<span class="share-btn-text'.$show_text.'">Whatsapp</span>
					</a>';
				}
			}
			
			if($pinterest )// breaks tubular video background
			{ 
				$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-pinterest"
				href="https://www.pinterest.com/pin/create/button/?url='.$encodedurl.'&media='.urlencode($thumb).'&description='.strip_tags($prod_desc).'"
				title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , 'Pinterest').'">
					<i class="vm2s-icon-pinterest"></i>
					<span class="share-btn-text'.$show_text.'">Pinterest</span>
				</a>';
			}
			
			if( $mixi)
			{
				if (!$appWeb->client->mobile )
				{
					$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-mixi"
					   href="https://profitquery.com/add-to/mixi/?url='.$encodedurl.'"
					   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , 'Mixi').'">
						<i class="vm2s-icon-mixi"></i>
						<span class="share-btn-text'.$show_text.'">Mixi</span>
					</a>';
				}
			}
			if( $telegram)
			{
				$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-telegram"
					   href="https://t.me/share/url?url='.$encodedurl.'"
					   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , 'Telegram').'">
						<i class="vm2s-icon-telegram"></i>
						<span class="share-btn-text'.$show_text.'">Telegram</span>
					</a>';
			}
			if($show_qrcode)
			{
				if (!$appWeb->client->mobile ){
					$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-qrcode"
					   href="//chart.googleapis.com/chart?cht=qr&chs=350x350&chld=L&choe=UTF-8&chl='.$encodedurl.'"
					   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , JText::_('PLG_VM2SHARE_YOUR_MOBILE')).'">
						<i class="vm2s-icon-qrcode"></i>
						<span class="share-btn-text'.$show_text.'">QR-Code</span>
					</a>';
				}
			}
			
			if($show_more)
			{
				$html .= '<a class="share-btn '.$style.' '.$size.' share-btn-more"
				   href="https://www.addtoany.com/share_save?linkurl='.$encodedurl.'"
				   title="'.sprintf( JText::_('PLG_VM2SHARE_SHARE_ON') , JText::_('PLG_VM2SHARE_MORE')).'">
					<i class="vm2s-icon-share"></i>
					<span class="share-btn-text'.$show_text.'">'. ucfirst(JText::_('PLG_VM2SHARE_MORE')).'</span>
				</a>';
			}
			
	
			$html .='<div class="clearfix"></div>';
			$html .='</div>
			</div>';
			
			
			$html .= "<script>(function(){
			  var shareButtons = document.querySelectorAll(\".share-btn\");
			  if (shareButtons) {
				  [].forEach.call(shareButtons, function(button) {
				  button.addEventListener(\"click\", function(event) {
							var width = 650, height = 450;
					event.preventDefault();
					window.open(this.href, 'Share Dialog', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width='+width+',height='+height+',top='+(screen.height/2-height/2)+',left='+(screen.width/2-width/2));
				  });
				});
			  }
				
			})();</script>";
			
			
			
			if($selection_sharer)	
			{
				$s = 
					"function decodeHtml(html) {
					    return jQuery('<div>').html(html).text();
					}
					function readOutLoud(textinput) {
						message = textinput.replace(/<\/?[^>]+(>|$)/g, \"\");
						message = decodeHtml(message);
						var utterance = new SpeechSynthesisUtterance(message);
						utterance.text = message;
						utterance.lang = '".$lang->getTag()."';
						speechUtteranceChunker(utterance, {
							chunkLength: 120
						}, function () {
							//console.log('end');
						});
					}
					jQuery(function(){jQuery('div').selectionSharer();});";
				$doc->addScriptDeclaration( $s );
			}
			
			if($facebookcomments && $fbcomments!='')
			{
				$html .=  '<br /><br /><a name="fb-comments"></a>'.$fbcomments . '</div>';
			}
			
			$script22 = "jQuery(function() {
					jQuery(jQuery('.vm2sharefb').contents()).appendTo('.spacer-buy-area');
					jQuery( document ).ajaxComplete(function() {
					  jQuery(jQuery('.vm2sharefb').contents()).appendTo('.spacer-buy-area');
					});
				});";
			
			$doc->addScriptDeclaration( $script22 );
			
			
			if($position=='bot')
			{
				$row->text .= '<br /><br />'. $html;
			}
			elseif($position=='top' OR $position=='cart')
			{
				$row->text =  $row->text . $html;
			}
			
			if($position=='cart')
			{
				$script = "jQuery(function() {
					jQuery(jQuery('.vm2share').contents()).appendTo('".$addtocartarea."');
					jQuery( document ).ajaxComplete(function() {
					  jQuery(jQuery('.vm2share').contents()).appendTo('".$addtocartarea."');
					});
				});";
				$doc->addScriptDeclaration( $script );
			}

		}
	}
}