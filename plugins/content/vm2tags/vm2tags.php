<?php
/**
 * @version 3.6
 * @package Virtuemart content VM2tags
 * @author   Nordmograph
 * @link https://www.nordmograph.com/extensions
 * @license GNU General Public License version 3 or later; see LICENSE.txt
 * @copyright Copyright (C) 2016 nordmograph.com. All rights reserved.
*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class plgContentVm2tags extends JPlugin 
{
	public function onContentPrepare($context, &$row, &$params, $page = 0)
	{	
		$app = JFactory::getApplication();
		$page_format = $app->input->get('format','','word');		
		if(
		( $context == 'com_virtuemart.productdetails' && (!$page_format or $page_format=='html') )
	
		)
		{	
			$juri 				= JURI::base();
			$db 				= JFactory::getDBO();
			$doc				= JFactory::getDocument();
					
			$langtag 			= JFactory::getLanguage()->get('tag');
			$dblangtag			= strtolower(str_replace("-","_",$langtag));
			
			$lang	= JFactory::getLanguage();
			$lang->load('com_vm2tags', JPATH_SITE ,  $lang->getTag() , $reload=false);
		
		
			
			
			if (!class_exists( 'VmConfig' ) && $context == 'com_virtuemart.productdetails' )
				require JPATH_ADMINISTRATOR . '/components/com_virtuemart/helpers/config.php';
				
			$position 			= $this->params->get('position', 'bot');
			$append		= $this->params->get('append', '.spacer-buy-area');
			
		
		
			$cparams 					= JComponentHelper::getParams('com_vm2tags');
			$mode	 					= $cparams->get('mode','1');
			$vm2tags_itemid 			= $cparams->get('vm2tags_itemid');
			if(!$vm2tags_itemid)
			{
				require_once JPATH_SITE.'/components/com_vm2tags/helpers/vm2tags.php';
				$vm2tags_itemid = Vm2tagsHelper::getVM2TItemid();
			}
			if($vm2tags_itemid=='')
				$vm2tags_itemid = $app->input->getInt('Itemid');
			
			$q = "SELECT metakey FROM #__virtuemart_products_".$dblangtag." 
			WHERE virtuemart_product_id='".$row->virtuemart_product_id."' ";
			$db->setQuery($q);
			$product_tags = $db->loadResult();
			if(!$product_tags or $product_tags=='' or $product_tags==' ')
				return false;
				
			$html = '<div class="product-field product-field-type-E">
					<span class="product-fields-title">
						<strong>'.ucwords(JText::_('COM_VM2TAGS_TAGS')).'</strong>
					</span>
					</div>';
				
			$doc->addStyleSheet($juri.'components/com_vm2tags/assets/css/fontello.css');	
			$doc->addStyleSheet($juri.'plugins/content/vm2tags/css/style.css');		
			$sep_tags = explode(',',$product_tags);
			
			$product_tags_html = '';
				
			for( $i=0;$i<count($sep_tags);$i++ )
			{
				$sep_tag = trim(mb_strtolower( $sep_tags[$i] ));
				$sep_tag = str_replace('-','--',$sep_tag );
				$sep_tag = str_replace(' ','-',$sep_tag );
				
				if($mode==1)
					$tag_url = JRoute::_('index.php?option=com_vm2tags&view=productslist&tag='.$sep_tag .'&Itemid='.$vm2tags_itemid  );
				else
					$tag_url = JRoute::_('index.php?searchword='.$sep_tag .'&ordering=newest&searchphrase=exact&option=com_search&Itemid='.$vm2tags_itemid  );
				if($sep_tag!='')
				{
					$product_tags_html .= '<a class="btn btn-mini" 
					href="'.$tag_url.'">'.trim($sep_tags[$i]) .'</a> ';
				}
			}
			$html .= '<div id="content-vm2tags" >';
			$html .= '<i class="vm2t-icon-tags"></i> ';
			$html .= $product_tags_html ;
			$html .= '</div>';
			$html .='<div style="clear:both;"></div>';
			
			if($position=='top')
				$row->text = $html . $row->text;
			else
				$row->text .= '<br /><br />'. $html;
				
			if($position=='append')
			{
				$script = "jQuery(function() { jQuery(jQuery('#content-vm2tags').contents()).appendTo('".$append."');});";
				$doc->addScriptDeclaration( $script );
			}
		}
	}
}