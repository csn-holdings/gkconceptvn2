<?php
	defined('_JEXEC') or die('Restricted access');
	
	jimport('joomla.plugin.plugin');
	
	class  plgredshop_shippingdefault_shipping extends JPlugin
	{
		public $total_fee = 0;
		public $chargeable_weight = 0;
		public $country = "VN";
		public $province = "";
		public $contact_name = "";
		public $address = "";
		public $phone = "";
		public $ward = "";
		
		public function onAjaxSendEmail()
		{
			if (
				isset($_POST['email']) && $_POST['email'] &&
				isset($_POST['name']) && $_POST['name'] &&
				isset($_POST['phone']) && $_POST['phone']
			)
			{
				$myUser = new stdClass();
				$myUser->email = $_POST['email'];
				$myUser->name = $_POST['name'];
				$myUser->confirmed = 1;
				$customFields = [];
				$customFields['5'] = $_POST['name'];
				$customFields['7'] = $_POST['yeucaukhachhang'];
				$customFields['9'] = $_POST['phone'];
				$userClass = acym_get('class.user');
				$userClass->sendConf = true;
				$userId = $userClass->save($myUser, $customFields);
			}
			
			header("Location: https://gkconcept.vn/go-quy-live-edge-viet-nam-tu-gkconcept");
			exit();
			
		}
		
		public function onAjaxGetData()
		{
			$tem['country'] = $_POST['country'];
			$tem['province'] = $_POST['province'];
			$tem['ward'] = $_POST['ward'];
			$tem['contact_name'] = $_POST['first_name'];
			$tem['address'] = $_POST['address'];
			$tem['phone'] = $_POST['phone'];
			$tem['infordelivery'] = $_POST['infordelivery'];
			$tem['total_fee'] = $_POST['total_fee'];
			$tem['subdistrict'] = $_POST['subdistrict'];
			
			if (!class_exists('VmConfig')) {
				require_once(JPATH_SITE . '/administrator/components/com_virtuemart/helpers/config.php');
				VmConfig::loadConfig();
			}
			if (!class_exists('VirtueMartCart')) {
				require_once(JPATH_SITE . '/components/com_virtuemart/helpers/cart.php');
			}
			$cart = VirtueMartCart::getCart(false);
			$data = $cart->prepareAjaxDataGitnetwork();
			
			$cart->orderboxme->total_fee = $tem['total_fee'];
			$GLOBALS["orderboxme"] = $tem;
			$cart->setCartIntoSession (true);
			
			$session = JFactory::getSession();
			$session->set('orderboxme', $tem);
			
		}
		
		public function plgCreateOrderBoxme(&$viewData)
		{
			if(empty($viewData['order_number']))
				return;
			
			$order_number = $viewData['order_number'];
			
			$this->onAjaxGetCreate($order_number);
		}
		
		public function onAjaxGetCreate($order_number)
		{
			$chargeable_weight = 0;
			
			if (!class_exists('VmConfig')) {
				require_once(JPATH_SITE . '/administrator/components/com_virtuemart/helpers/config.php');
				VmConfig::loadConfig();
			}
			if (!class_exists('VirtueMartCart')) {
				require_once(JPATH_SITE . '/components/com_virtuemart/helpers/cart.php');
			}
			$cart = VirtueMartCart::getCart();
			$data = $cart->prepareAjaxData();
			
			$parcels = array();
			$items = array();
			$i=0;
			foreach ($cart->products as $product)
			{
				$temp = new stdClass();
				$product_packaging = $product->product_packaging ? $product->product_packaging : 6;
				$product_packaging = $product_packaging*1000;
				$chargeable_weight += $product_packaging;
				
				$temp->sku = $product->product_sku;
				$temp->label_code = $product->product_name;
				$temp->origin_country = "VN";
				$temp->name = $product->product_name;
				$temp->desciption = "";
				$temp->weight = $product_packaging;
				$temp->amount = $product->allPrices[0]['product_override_price'] ? $product->allPrices[0]['product_override_price'] : $product->allPrices[0]['product_price'];
				$temp->quantity = $product->quantity;
				
				$items[] = $temp;
				$i++;
			}
			
			$session = JFactory::getSession();
			$boxme =  $session->get('orderboxme');
			
			$parcels[0]['weight'] = $chargeable_weight ? $chargeable_weight : 6000;
			$parcels[0]['description'] = "";
			$parcels[0]['items'] = $items;
			
			$pickup_id = 148703;
			// $pickup_id = 37529;
			
			$data = new stdClass();
			$data->ship_from->country = $boxme['country'];
			$data->ship_from->pickup_id = $pickup_id;
			
			$data->ship_to->contact_name = $boxme['contact_name'];
			$data->ship_to->address = $boxme['address'];
			$data->ship_to->phone = $boxme['phone'];
			$data->ship_to->country = "VN";
			$data->ship_to->province = $boxme['province'];
			$data->ship_to->district = $boxme['ward'];
			$data->ship_to->subdistrict = $boxme['subdistrict'];
			
			$data->shipments->content = "shipping Cod";
			$data->shipments->total_parcel = count($parcels);
			$data->shipments->total_amount = $i;
			$data->shipments->chargeable_weight = (int) $chargeable_weight;
			$data->shipments->description = "";
			$data->shipments->parcels = $parcels;
			
			$data->config->order_type = "normal";
			$data->config->delivery_service = $boxme['infordelivery'];
			$data->config->document = "Y";
			$data->config->currency = "VND";

			$data->payment->fee_paid_by = "receiver";
			$data->payment->cod_amount = $cart->cartPrices['billTotal'] - $boxme['total_fee'];
			
			$data->referral->order_number = $order_number;
			
			$body = json_encode($data);
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_URL, "https://s.boxme.asia/api/v1/courier/pricing/create_order");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Authorization: Token 424dce789c54156334f53818d2c86dc4daf6d9089672a5315ba2ad2a4e33ad88',
			));
			
			$output = curl_exec($ch);
			
			curl_close($ch);
			
		}
		
		public function onAjaxGetFeeProductDetail()
		{
			$country = !empty($_POST['country']) ? $_POST['country'] : 0;
			$province = !empty($_POST['province']) ? $_POST['province'] : 0;
			$ward = !empty($_POST['ward']) ? $_POST['ward'] : 0;
			$idProduct = !empty($_POST['idproduct']) ? $_POST['idproduct'] : '';
			$chargeableweight = 6000;
			
			if (!class_exists('VmConfig')) {
				require_once(JPATH_SITE . '/administrator/components/com_virtuemart/helpers/config.php');
				VmConfig::loadConfig();
			}
			if (!class_exists('VirtueMartCart')) {
				require_once(JPATH_SITE . '/components/com_virtuemart/helpers/cart.php');
			}
			
			$productModel = VmModel::getModel('Product');
			$product = $productModel->getProduct($idProduct);
			
			if($product->product_packaging < 0 )
			{
				$chargeableweight = (int) ($product->product_packaging ? $product->product_packaging : 6)*1000;
			}
			
			$pickup_id = 148703;
			
			$data = new stdClass();
			$data->ship_from->country = $country;
			$data->ship_from->pickup_id = $pickup_id;
			
			$data->ship_to->contact_name = "admin";
			$data->ship_to->address = "system";
			$data->ship_to->phone = "0938788091";
			$data->ship_to->country = "VN";
			$data->ship_to->province = $province;
			$data->ship_to->district = $ward;
			
			$data->shipments->content = "shipping Cod";
			$data->shipments->total_parcel = 1;
			$data->shipments->total_amount = 1;
			$data->shipments->chargeable_weight = (int) $chargeableweight;
			
			$data->config->order_type = "normal";
			$data->config->document = "Y";
			$data->config->currency = "VND";
			
			$data->payment->fee_paid_by = "receiver";
			$data->payment->tax_paid_by = "receiver";
			$data->payment->cod_amount = $product->allPrices[0]['product_override_price'];
			
			$body = json_encode($data);
			
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_URL, "http://s.boxme.asia/api/v1/courier/pricing/calculate");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Authorization: Token 424dce789c54156334f53818d2c86dc4daf6d9089672a5315ba2ad2a4e33ad88',
			));
			$output = curl_exec($ch);
			
			curl_close($ch);
			
			echo $output;
		}
		
		public function onAjaxGetFee()
		{
			$country = !empty($_POST['country']) ? $_POST['country'] : 0;
			$province = !empty($_POST['province']) ? $_POST['province'] : 0;
			$ward = !empty($_POST['ward']) ? $_POST['ward'] : 0;
			$subdistrict = !empty($_POST['subdistrict']) ? $_POST['subdistrict'] : 0;
			$contact_name = !empty($_POST['first_name']) ? $_POST['first_name'] : '';
			$address = !empty($_POST['address']) ? $_POST['address'] : '';
			$email = !empty($_POST['email']) ? $_POST['email'] : '';
			$phone = !empty($_POST['phone']) ? $_POST['phone'] : '';
			$chargeable_weight = 0;
			
			if (!class_exists('VmConfig')) {
				require_once(JPATH_SITE . '/administrator/components/com_virtuemart/helpers/config.php');
				VmConfig::loadConfig();
			}
			if (!class_exists('VirtueMartCart')) {
				require_once(JPATH_SITE . '/components/com_virtuemart/helpers/cart.php');
			}
			$cart = VirtueMartCart::getCart(false);
			$data = $cart->prepareAjaxDataGitnetwork();
			
			foreach ($cart->products as $product)
			{
				$chargeable_weight += ($product->product_packaging * $cart->productsQuantity[$product->virtuemart_product_id])*1000;
			}
			
			if($chargeable_weight == 0)
			{
				$chargeable_weight = 6000;
			}
			
			$pickup_id = 148703;
			
			$data = new stdClass();
			$data->ship_from->country = $country;
			$data->ship_from->pickup_id = $pickup_id;
			
			$data->ship_to->contact_name = $contact_name;
			$data->ship_to->address = $address;
			$data->ship_to->phone = $phone;
			$data->ship_to->country = "VN";
			$data->ship_to->province = $province;
			$data->ship_to->district = $ward;
			$data->ship_to->subdistrict = $subdistrict;
			
			$data->shipments->content = "shipping Cod";
			$data->shipments->total_parcel = 1;
			$data->shipments->total_amount = 1;
			$data->shipments->chargeable_weight = (int)$chargeable_weight;
			
			$data->config->order_type = "normal";
			$data->config->document = "Y";
			$data->config->currency = "VND";
			
			$data->payment->fee_paid_by = "receiver";
			$data->payment->tax_paid_by = "receiver";
			$data->payment->cod_amount = $cart->cartPrices['billTotal'];
			
			$body = json_encode($data);
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_URL, "https://s.boxme.asia/api/v1/courier/pricing/calculate");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Authorization: Token 424dce789c54156334f53818d2c86dc4daf6d9089672a5315ba2ad2a4e33ad88',
			));
			$output = curl_exec($ch);
			
			curl_close($ch);
			
			echo $output;
		}
		
		public function onAjaxUpdateCart()
		{
			$this->service_id = $_POST['service_id'];
			$this->service_name = $_POST['service_name'];
			$this->courier_name = $_POST['courier_name'];
			$this->total_fee = $_POST['total_fee'];
			$this->subdistrict = !empty($_POST['subdistrict']) ? $_POST['subdistrict'] : 0;
			
			
			if (!class_exists('VmConfig')) {
				require_once(JPATH_SITE . '/administrator/components/com_virtuemart/helpers/config.php');
				VmConfig::loadConfig();
			}
			if (!class_exists('VirtueMartCart')) {
				require_once(JPATH_SITE . '/components/com_virtuemart/helpers/cart.php');
			}
			$cart = VirtueMartCart::getCart(true);
			
			$cart->orderboxme->total_fee = $this->total_fee;
			$cart->orderboxme->service_id = $this->service_id;
			$cart->orderboxme->service_name = $this->service_name;
			$cart->orderboxme->courier_name = $this->courier_name;
			$cart->orderboxme->subdistrict = $this->subdistrict;
			
			$session = JFactory::getSession();
			$session->set('orderboxme', $cart->orderboxme );
			
			$data = $cart->prepareAjaxDataGitnetwork();
			$cart->cartPrices['shipmentValue'] = $this->total_fee;
			$cart->pricesUnformatted['shipmentValue'] = $this->total_fee;
			$cart->setCartIntoSession(true,true);
			
			echo json_encode($data);
			
		}
		
		protected function loadView($layoutName)
		{
			$layoutName = trim(strval($layoutName));
			$path = JPath::clean('plugins/system/onestepcheckout/views/tmpl/' . $layoutName . '.php');
			
			if (!file_exists($path) || !is_file($path)) {
				JFactory::getApplication()->enqueueMessage('Layout file ' . $path . ' not found.', 'error');
				return '';
			}
			
			ob_start();
			require_once($path);
			$layout = ob_get_contents();
			ob_end_clean();
			
			return $layout;
		}
		
		public function plgVmOnSelectCheckPaymentMekozzy(&$method)
		{
			$method->shipment_cost = $this->total_fee;
		}
	}

?>