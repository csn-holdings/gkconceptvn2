<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

if (file_exists(JPATH_ADMINISTRATOR . '/components/com_k2/lib/k2plugin.php'))
{
	require_once JPATH_ADMINISTRATOR . '/components/com_k2/lib/k2plugin.php';
}

require_once JPATH_SITE . '/components/com_k2/helpers/route.php';
require_once JPATH_ADMINISTRATOR . '/components/com_route66/lib/seoanalyzer.php';

JLoader::register('Route66ModelSeo', JPATH_ADMINISTRATOR . '/components/com_route66/models/seo.php');

class PlgK2Route66Seo extends K2Plugin
{
	public $pluginName = 'route66seo';
	public $pluginNameHumanReadable = 'Route 66 SEO';
	protected $autoloadLanguage = true;
	protected $enabled = false;

	public function __construct(&$subject, $config = array())
	{
		parent::__construct($subject, $config);
		$params = JComponentHelper::getParams('com_route66');
		$this->enabled = $params->get('seo', true);
	}

	public function onRenderAdminForm(&$item, $type, $tab = '')
	{
		if (!$this->enabled)
		{
			$tab = 'nowhere';

			return parent::onRenderAdminForm($item, $type, $tab);
		}

		if ($tab != 'other')
		{
			$tab = 'nowhere';

			return parent::onRenderAdminForm($item, $type, $tab);
		}

		// Set data
		$data = array();

		if ($item->id)
		{
			$model = JModelLegacy::getInstance('Seo', 'Route66Model');
			$data = (array) $model->fetch('com_k2.item', $item->id);
		}

		// Analyzer
		$options = $this->getScriptOptions($item->id, $item->catid);
		$params = JComponentHelper::getParams('com_route66');
		$position = $params->get('seo_display_position', 'form');
		$Route66SeoAnalyzer = new Route66SeoAnalyzer($data, $options, $position);
		$output = $Route66SeoAnalyzer->render();

		$plugin = new stdClass;
		$plugin->name = $this->pluginNameHumanReadable;
		$plugin->fields = $output;

		return $plugin;
	}

	public function getScriptOptions($id, $catid = 0)
	{
		$options = array();
		$options['option'] = 'com_k2';
		$options['view'] = 'item';
		$options['id'] = $id;
		$options['aliasToken'] = '{itemAlias}';
		$options['fields'] = array('title' => '#title', 'alias' => '#alias', 'text' => '#text', 'description' => 'textarea[name="metadesc"]');
		$params = JComponentHelper::getParams('com_k2');

		if (!$params->get('mergeEditors'))
		{
			$options['fields']['text'] = '#fulltext';
		}
		$options['route'] = K2HelperRoute::getItemRoute((int) $id . ':' . $options['aliasToken'], (int) $catid);
		$options['split'] = false;

		return $options;
	}

	public function onAfterK2Save(&$item, $isNew)
	{
		if (!$this->enabled)
		{
			return true;
		}

		$application = JFactory::getApplication();
		$data = $application->input->post->get('jform', array(), 'array');

		if (isset($data['route66seo']) && is_array($data['route66seo']) && isset($data['route66seo']['score']))
		{
			$context = 'com_k2.item';
			$score = (int) $data['route66seo']['score'];
			$keyword = $data['route66seo']['keyword'];
			$resourceId = (int) $item->id;

			$model = JModelLegacy::getInstance('Seo', 'Route66Model');
			$model->delete($context, $resourceId);
			$model->save($context, $resourceId, $keyword, $score);
		}
	}
}
