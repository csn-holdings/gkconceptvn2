<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE . '/plugins/system/route66/lib/plugin.php';

class plgRoute66Virtuemart extends Route66Plugin
{
	protected $rules = array('product', 'category');

	public function __construct(&$subject, $config = array())
	{
		parent::__construct($subject, $config);

		if (class_exists('VmConfig'))
		{
			VmConfig::loadConfig();
		}
		elseif (file_exists(JPATH_SITE . '/administrator/components/com_virtuemart/helpers/config.php'))
		{
			include_once JPATH_SITE . '/administrator/components/com_virtuemart/helpers/config.php';
			VmConfig::loadConfig();
		}
		else
		{
			$this->rules = array();
		}
	}

	public function getSitemapItems($feed, $offset, $limit)
	{

		// No support for news sitemap
		if ($feed->settings->get('type') == 'news')
		{
			return array();
		}

		// Get model
		$model = $this->getModel();
		$model->setState('offset', $offset);
		$model->setState('limit', $limit);

		// Set category filter
		if ($feed->sources->get('virtuemart') == 2 && is_array($feed->sources->get('virtuemartCategories')) && count($feed->sources->get('virtuemartCategories')))
		{
			$model->setState('categories', $feed->sources->get('virtuemartCategories'));
		}

		$items = $model->getSitemapItems();
		$application = JFactory::getApplication();
		$ssl = $application->get('force_ssl') == 2 ? 1 : 2;

		if (count($items))
		{
			$keys = array();
			$rows = array();

			foreach ($items as $item)
			{
				$keys[] = $item->virtuemart_product_id;

				if ($item->product_parent_id)
				{
					$keys[] = $item->product_parent_id;
				}
				$item->images = array();
				$item->videos = array();
				$rows[$item->virtuemart_product_id] = $item;
			}
			$categories = $model->getProductCategories($keys);

			if ($feed->settings->get('images'))
			{
				$images = $model->getProductImages(array_keys($rows));

				foreach ($images as $image)
				{
					$entry = new stdClass();
					$entry->url = JUri::root(false) . $image->file_url;
					$entry->caption = trim($image->file_title) ? $image->file_title : $rows[$image->virtuemart_product_id]->product_name;
					$rows[$image->virtuemart_product_id]->images[] = $entry;
				}
			}

			foreach ($rows as $item)
			{
				if ($item->product_parent_id && isset($categories[$item->product_parent_id]))
				{
					$item->virtuemart_category_id = $categories[$item->product_parent_id]['virtuemart_category_id'];
				}
				elseif (isset($categories[$item->virtuemart_product_id]))
				{
					$item->virtuemart_category_id = $categories[$item->virtuemart_product_id]['virtuemart_category_id'];
				}
				else
				{
					$item->virtuemart_category_id = 0;
				}
				$link = 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $item->virtuemart_product_id;

				if ($item->virtuemart_category_id)
				{
					$link .= '&virtuemart_category_id=' . $item->virtuemart_category_id;
				}
				$item->url = JRoute::_($link, true, $ssl);
			}

			$items = array_values($rows);
		}

		return $items;
	}

	public function countSitemapItems($feed)
	{

		// No support for news sitemap
		if ($feed->settings->get('type') == 'news')
		{
			return 0;
		}

		// Get model
		$model = $this->getModel();

		// Set category filter
		if ($feed->sources->get('virtuemart') == 2 && is_array($feed->sources->get('virtuemartCategories')) && count($feed->sources->get('virtuemartCategories')))
		{
			$model->setState('categories', $feed->sources->get('virtuemartCategories'));
		}

		return $model->countSitemapItems();
	}

	public function onRoute66IsExtensionInstalled()
	{
		$component = JComponentHelper::getComponent('com_virtuemart');

		return (int) $component->id;
	}
}
