<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE . '/plugins/system/route66/lib/rule.php';

class Route66RuleVirtuemartProduct extends Route66Rule
{
	private static $cache = array();
	protected $variables = array('option' => 'com_virtuemart', 'view' => 'productdetails', 'virtuemart_product_id' => '@', 'virtuemart_category_id' => '', 'id' => '');

	public function getTokensValues($query)
	{
		// Cache key
		$key = (int) $query['virtuemart_product_id'];

		// Check cache
		if (isset(self::$cache[$key]))
		{
			return self::$cache[$key];
		}

		// Get database
		$db = JFactory::getDbo();

		// Get query
		$dbQuery = $db->getQuery(true);

		// Initialize values
		$values = array();

		// Get model
		JModelLegacy::addIncludePath(JPATH_SITE . '/plugins/route66/virtuemart/models');
		$model = JModelLegacy::getInstance('Virtuemart', 'Route66Model', array('ignore_request' => true));

		// Iterate over the tokens
		foreach ($this->tokens as $token)
		{

			// ID
			if ($token == '{productId}')
			{
				$values[] = (int) $query['virtuemart_product_id'];
				$dbQuery->select($db->qn('product.virtuemart_product_id'));
			}
			// Alias
			elseif ($token == '{productAlias}')
			{
				if (strpos($query['virtuemart_product_id'], ':'))
				{
					$parts = explode(':', $query['virtuemart_product_id']);
					$values[] = $parts[1];
				}
				$dbQuery->select($db->qn('productData.slug'));
			}
			// Product SKU
			elseif ($token == '{productSku}')
			{
				$dbQuery->select($db->qn('product.product_sku'));
			}
			// Product year
			elseif ($token == '{productYear}')
			{
				$dbQuery->select('YEAR(' . $db->qn('product.created_on') . ')');
			}
			// Product month
			elseif ($token == '{productMonth}')
			{
				$dbQuery->select('DATE_FORMAT(' . $db->qn('product.created_on') . ', "%m")');
			}
			// Product day
			elseif ($token == '{productDay}')
			{
				$dbQuery->select('DATE_FORMAT(' . $db->qn('product.created_on') . ', "%d")');
			}
			// Product date
			elseif ($token == '{productDate}')
			{
				$dbQuery->select('DATE(' . $db->qn('product.created_on') . ')');
			}
			// Product author
			elseif ($token == '{productAuthor}')
			{
				$dbQuery->select($db->qn('product.created_by'));
			}
			// Category alias
			elseif ($token == '{categoryAlias}')
			{
				$virtuemart_category_id = $model->getCategoryIdFromProductId($query['virtuemart_product_id']);

				if ($virtuemart_category_id)
				{
					$categoryAlias = $model->getCategorySlug($virtuemart_category_id);
				}
				else
				{
					$categoryAlias = 'uncategorized';
				}
			}
			// Category path
			elseif ($token == '{categoryPath}')
			{
				$virtuemart_category_id = $model->getCategoryIdFromProductId($query['virtuemart_product_id']);

				if ($virtuemart_category_id)
				{
					$category = $model->getCategory($virtuemart_category_id);
					$categoryPath = $model->getCategoryPath($category->virtuemart_category_id, $category->category_parent_id, array($category->slug));
				}
				else
				{
					$categoryPath = 'uncategorized';
				}
				$values[] = $categoryPath;
				$dbQuery->select($db->q($categoryPath));
			}
		}

		// Check if we already have what we need
		if (count($this->tokens) === count($values))
		{
			self::$cache[$key] = $values;

			return $values;
		}

		// If not let's query the database
		$dbQuery->from($db->qn('#__virtuemart_products', 'product'));
		$dbQuery->innerJoin($db->qn('#__virtuemart_products_' . VmConfig::$vmlang, 'productData') . ' ON ' . $db->qn('product.virtuemart_product_id') . ' = ' . $db->qn('productData.virtuemart_product_id'));
		$dbQuery->where($db->qn('product.virtuemart_product_id') . ' = ' . (int) $query['virtuemart_product_id']);
		$db->setQuery($dbQuery);
		$values = $db->loadRow();

		// Insert the category alias
		if (isset($categoryAlias))
		{
			array_splice($values, array_search('{categoryAlias}', $this->tokens), 0, array($categoryAlias));
		}

		// Some values need processing
		$author = array_search('{productAuthor}', $this->tokens);

		if ($author !== false)
		{
			if (is_numeric($values[$author]))
			{
				$values[$author] = JFactory::getUser($values[$author])->name;
			}
			$values[$author] = JFilterOutput::stringURLUnicodeSlug($values[$author]);
		}
		self::$cache[$key] = $values;

		return $values;
	}

	public function getQueryValue($key, $tokens)
	{
		if ($key == 'virtuemart_product_id')
		{

			// First check that ID is not already in the URL
			if (isset($tokens['{productId}']))
			{
				return $tokens['{productId}'];
			}

			// Check for alias
			if (isset($tokens['{productAlias}']))
			{
				return $this->getProductIdFromAlias($tokens['{productAlias}']);
			}

			// Check for SKU
			if (isset($tokens['{productSku}']))
			{
				return $this->getProductIdFromSku($tokens['{productSku}']);
			}
		}
		else
		{
			return;
		}
	}

	public function getItemid($variables)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('virtuemart_category_id'))->from($db->qn('#__virtuemart_product_categories'))->where($db->qn('virtuemart_product_id') . ' = ' . $db->q($variables['virtuemart_product_id']))->order($db->qn('ordering'));
		$db->setQuery($query, 0, 1);
		$variables['virtuemart_category_id'] = $db->loadResult();

		if (!function_exists('virtuemartBuildRoute'))
		{
			include_once JPATH_SITE . '/components/com_virtuemart/router.php';
		}
		virtuemartBuildRoute($variables);
		$Itemid = isset($variables['Itemid']) ? $variables['Itemid'] : '';

		return $Itemid;
	}

	private function getProductIdFromAlias($alias)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('virtuemart_product_id'))->from($db->qn('#__virtuemart_products_' . VmConfig::$vmlang))->where($db->qn('slug') . ' = ' . $db->q($alias));
		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;
	}

	private function getProductIdFromSku($sku)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('virtuemart_product_id'))->from($db->qn('#__virtuemart_products'))->where('product_sku = ' . $db->q($sku));
		$db->setQuery($query);
		$id = $db->loadResult();

		return $id;
	}
}
