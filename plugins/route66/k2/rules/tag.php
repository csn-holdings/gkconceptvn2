<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE . '/plugins/system/route66/lib/rule.php';

class Route66RuleK2Tag extends Route66Rule
{
	private $cache = array();
	protected $variables = array('option' => 'com_k2', 'view' => 'itemlist', 'task' => 'tag', 'tag' => '@', 'id' => '');

	public function getTokensValues($query)
	{
		// Cache key
		$key = $query['tag'];

		// Check cache
		if (isset($this->cache[$key]))
		{
			return $this->cache[$key];
		}

		// Get database
		$db = JFactory::getDbo();

		// Get query
		$dbQuery = $db->getQuery(true);

		// Initialize values
		$values = array();

		// Iterate over the tokens
		foreach ($this->tokens as $token)
		{

			// Name
			if ($token == '{tagName}')
			{
				$dbQuery->select($db->qn('name'));
				$values[] = $query['tag'];
			}
			// ID
			elseif ($token == '{tagId}')
			{
				$dbQuery->select($db->qn('id'));
			}
		}

		// Check if we already have what we need
		if (count($this->tokens) === count($values))
		{
			$this->cache[$key] = $values;

			return $values;
		}

		// If not let's query the database
		$dbQuery->from($db->qn('#__k2_tags'));
		$dbQuery->where($db->qn('name') . ' = ' . $db->q($query['tag']));
		$db->setQuery($dbQuery);
		$values = $db->loadRow();
		$this->cache[$key] = $values;

		return $values;
	}

	public function getQueryValue($key, $tokens)
	{
		if ($key == 'tag')
		{
			if (isset($tokens['{tagName}']))
			{
				// Do not rely on the name on the URL, ensure it exists in K2 tags table. TODO: Do the same for ID variables in other rules to avoid conflicts? Maybe add an option for this
				return $this->getTagNameFromTagName($tokens['{tagName}']);
			}

			if (isset($tokens['{tagId}']))
			{
				return $this->getTagNameFromId($tokens['{tagId}']);
			}
		}
		else
		{
			return;
		}
	}

	public function getItemid($variables)
	{
		$route = K2HelperRoute::getTagRoute($variables['tag']);
		parse_str($route, $result);
		$Itemid = isset($result['Itemid']) ? $result['Itemid'] : '';

		return $Itemid;
	}

	private function getTagNameFromId($id)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('name')->from('#__k2_tags')->where('id = ' . $db->q($id));
		$db->setQuery($query);
		$name = $db->loadResult();

		return $name;
	}

	private function getTagNameFromTagName($tagName)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('name')->from('#__k2_tags')->where('name = ' . $db->q($tagName));
		$db->setQuery($query);
		$name = $db->loadResult();

		return $name;
	}
}
