<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

abstract class Route66K2HelperRoute
{
	public static function getItemRoute($id, $catid, $language = '*')
	{
		$link = 'index.php?option=com_k2&view=item&id=' . $id;

		$items = self::getMenuItems($language);

		if (!is_array($items))
		{
			return $link;
		}

		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] == 'item' && isset($item->query['id']) && $item->query['id'] == $id)
			{
				$link .= '&Itemid=' . $item->id;

				return $link;
			}
		}

		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] == 'itemlist' && isset($item->query['id']) && $item->query['id'] == $catid)
			{
				$link .= '&Itemid=' . $item->id;

				return $link;
			}
		}

		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] == 'itemlist' && (!isset($item->query['task']) || $item->query['task'] == ''))
			{
				$menuParams = json_decode($item->params);
				$categories = isset($menuParams->categories) ? $menuParams->categories : array();

				if (in_array($catid, $categories))
				{
					$link .= '&Itemid=' . $item->id;

					return $link;
				}
			}
		}

		return $link;
	}

	private static function getMenuItems($language = '*')
	{
		$application = JFactory::getApplication();
		$menus = $application->getMenu('site');
		$component = JComponentHelper::getComponent('com_k2');

		$conditions = array('component_id');
		$values = array($component->id);

		if ($application->isClient('site') && $application->getLanguageFilter())
		{
			$conditions[] = 'language';
			$languages = array('*');

			if ($language != '*')
			{
				$languages[] = $language;
			}
			$values[] = $languages;
		}

		$items = $menus->getItems($conditions, $values);

		return $items;
	}
}
