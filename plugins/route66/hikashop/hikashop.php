<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_SITE . '/plugins/system/route66/lib/plugin.php';

class plgRoute66Hikashop extends Route66Plugin
{
	protected $rules = array('product', 'category');

	public function __construct(&$subject, $config = array())
	{
		parent::__construct($subject, $config);

		if (!file_exists(JPATH_SITE . '/components/com_hikashop/hikashop.php'))
		{
			$this->rules = array();
		}
	}

	public function getSitemapItems($feed, $offset, $limit)
	{

		// No support for news sitemap
		if ($feed->settings->get('type') == 'news')
		{
			return array();
		}

		// Get model
		$model = $this->getModel();
		$model->setState('offset', $offset);
		$model->setState('limit', $limit);

		// Set category filter
		if ($feed->sources->get('hikashop') == 2 && is_array($feed->sources->get('hikashopCategories')) && count($feed->sources->get('hikashopCategories')))
		{
			$model->setState('categories', $feed->sources->get('hikashopCategories'));
		}

		$items = $model->getSitemapItems();
		$application = JFactory::getApplication();
		$ssl = $application->get('force_ssl') == 2 ? 1 : 2;

		if (count($items))
		{
			$config = hikashop_config();
			$pathway_sef_name = $config->get('pathway_sef_name', 'category_pathway');
			$rows = array();

			foreach ($items as $item)
			{
				$item->images = array();
				$item->videos = array();
				$itemId = $this->getItemId($item->product_id, $item->category_id);
				$alias = $item->product_alias ? $item->product_alias : JFilterOutput::stringURLSafe($item->product_name);
				$item->url = rtrim(HIKASHOP_LIVE, '/') . JRoute::_(hikashop_completeLink('product&task=show&cid=' . (int) $item->product_id . '&name=' . $alias . '&' . $pathway_sef_name . '=' . $item->category_id . '&Itemid=' . $itemId), true, -1);
				$rows[$item->product_id] = $item;
			}

			if ($feed->settings->get('images'))
			{
				$images = $model->getProductImages(array_keys($rows));
				$uploadFolder = $config->get('uploadfolder', 'images/com_hikashop/upload/');
				$uploadFolder = ltrim($uploadFolder, '/');
				$uploadFolder = rtrim($uploadFolder, '/');

				foreach ($images as $image)
				{
					$entry = new stdClass();
					$entry->url = JUri::root(false) . $uploadFolder . '/' . $image->file_path;
					$entry->caption = trim($image->file_description) ? $image->file_description : $rows[$image->file_ref_id]->product_name;
					$rows[$image->file_ref_id]->images[] = $entry;
				}
			}

			$items = array_values($rows);
		}

		return $items;
	}

	public function countSitemapItems($feed)
	{

		// No support for news sitemap
		if ($feed->settings->get('type') == 'news')
		{
			return 0;
		}

		// Get model
		$model = $this->getModel();

		// Set category filter
		if ($feed->sources->get('hikashop') == 2 && is_array($feed->sources->get('hikashopCategories')) && count($feed->sources->get('hikashopCategories')))
		{
			$model->setState('categories', $feed->sources->get('hikashopCategories'));
		}

		return $model->countSitemapItems();
	}

	public function onRoute66IsExtensionInstalled()
	{
		$component = JComponentHelper::getComponent('com_hikashop');

		return (int) $component->id;
	}

	private function getItemId($id, $catid)
	{
		$application = JFactory::getApplication('site');
		$menus = $application->getMenu('site');
		$component = JComponentHelper::getComponent('com_hikashop');
		$items = $menus->getItems('component_id', $component->id);

		if (!is_array($items))
		{
			return;
		}

		$match = null;

		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] == 'product' && isset($item->query['layout']) && $item->query['layout'] == 'show' && $item->params->get('product_id') == $id)
			{
				$match = $item->id;

				break;
			}
		}

		if ($match)
		{
			return $match;
		}

		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] == 'product' && isset($item->query['layout']) && $item->query['layout'] == 'listing' && is_array($item->params->get('hk_product', array())) && count($item->params->get('hk_product', array())))
			{
				$options = $item->params->get('hk_product', array());

				if (isset($options['category']) && $options['category'] == $catid)
				{
					$match = $item->id;

					break;
				}
			}
		}

		if ($match)
		{
			return $match;
		}

		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] == 'category' && is_array($item->params->get('hk_category', array())) && count($item->params->get('hk_category', array())))
			{
				$options = $item->params->get('hk_category', array());

				if (isset($options['category']) && $options['category'] == $catid)
				{
					$match = $item->id;

					break;
				}
			}
		}

		if ($match)
		{
			return $match;
		}

		foreach ($items as $item)
		{
			if (isset($item->query['view']) && $item->query['view'] == 'category')
			{
				$match = $item->id;

				break;
			}
		}

		return $match;
	}
}
