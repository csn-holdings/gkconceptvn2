<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

JFormHelper::loadFieldClass('list');

class JFormFieldHikashopCategories extends JFormFieldList
{
	protected $type = 'HikashopCategories';

	protected function getOptions()
	{
		if (!file_exists(JPATH_ADMINISTRATOR . '/components/com_hikashop/helpers/helper.php'))
		{
			return array_merge(parent::getOptions(), array());
		}
		require_once JPATH_ADMINISTRATOR . '/components/com_hikashop/helpers/helper.php';
		require_once JPATH_ADMINISTRATOR . '/components/com_hikashop/classes/category.php';
		$categories = new hikashopCategoryClass();
		$rows = $categories->getList('product', 0, false);
		$options = array();

		foreach ($rows as $row)
		{
			$options[] = JHtml::_('select.option', $row->category_id, str_repeat('-', ((int) $row->category_depth) - 1) . $row->category_name);
		}

		return array_merge(parent::getOptions(), $options);
	}
}
