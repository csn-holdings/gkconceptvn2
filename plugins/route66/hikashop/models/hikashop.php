<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2020 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

require_once JPATH_ADMINISTRATOR . '/components/com_hikashop/helpers/helper.php';

class Route66ModelHikashop extends JModelLegacy
{
	private static $cache = array();

	public function getSitemapItems()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('product.product_id') . ',' . $db->qn('product.product_name') . ',' . $db->qn('product.product_alias') . ',' . $db->qn('category.category_id'))->from($db->qn(hikashop_table('product'), 'product'));
		$query->where($db->qn('product.product_published') . ' = 1')->where($db->qn('product.product_type') . ' = ' . $db->q('main'))->where($db->qn('product.product_access') . ' = ' . $db->q('all'));
		$query->innerJoin($db->qn(hikashop_table('product_category'), 'xref') . ' ON ' . $db->qn('product.product_id') . ' = ' . $db->qn('xref.product_id'));
		$query->innerJoin($db->qn(hikashop_table('category'), 'category') . ' ON ' . $db->qn('xref.category_id') . ' = ' . $db->qn('category.category_id'));

		if ($this->getState('categories'))
		{
			$query->where($db->qn('category.category_id') . ' IN(' . implode(',', $this->getState('categories')) . ')');
		}
		$query->order($db->qn('product.product_id'));
		$db->setQuery($query, $this->getState('offset'), $this->getState('limit'));
		$items = $db->loadObjectList();

		return $items;
	}

	public function countSitemapItems()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('COUNT(product.product_id)')->from($db->qn(hikashop_table('product'), 'product'));
		$query->where($db->qn('product.product_published') . ' = 1')->where($db->qn('product.product_type') . ' = ' . $db->q('main'))->where($db->qn('product.product_access') . ' = ' . $db->q('all'));
		$query->innerJoin($db->qn(hikashop_table('product_category'), 'xref') . ' ON ' . $db->qn('product.product_id') . ' = ' . $db->qn('xref.product_id'));
		$query->innerJoin($db->qn(hikashop_table('category'), 'category') . ' ON ' . $db->qn('xref.category_id') . ' = ' . $db->qn('category.category_id'));

		if ($this->getState('categories'))
		{
			$query->where($db->qn('category.category_id') . ' IN(' . implode(',', $this->getState('categories')) . ')');
		}
		$db->setQuery($query);

		return $db->loadResult();
	}

	public function getProductImages($ids)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('file_ref_id') . ',' . $db->qn('file_description') . ',' . $db->qn('file_path'))->from($db->qn(hikashop_table('file')));
		$query->where($db->qn('file_type') . ' = ' . $db->q('product'))->where($db->qn('file_ref_id') . ' IN (' . implode(',', $ids) . ')');
		$query->order($db->qn('file_ordering'));
		$db->setQuery($query);

		return $db->loadObjectList();
	}

	public function getCategoryPath($id, $parent, $path)
	{
		$parent = (int) $parent;

		if (!$parent)
		{
			return implode('/', array_reverse($path));
		}
		$category = $this->getCategory($parent);
		$path[] = $category->category_alias;

		if ($category->category_parent_id > 1)
		{
			return $this->getCategoryPath($category->category_id, $category->category_parent_id, $path);
		}
		else
		{
			$result = implode('/', array_reverse($path));

			return $result;
		}
	}

	public function getCategory($id)
	{
		$key = (int) $id;

		if (!isset(self::$cache[$key]))
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('category_id');
			$query->select('category_parent_id');
			$query->select('category_alias');
			$query->from($db->qn('#__hikashop_category'));
			$query->where($db->qn('category_id') . ' = ' . $key);
			$db->setQuery($query);
			self::$cache[$key] = $db->loadObject();
		}

		return self::$cache[$key];
	}
}
