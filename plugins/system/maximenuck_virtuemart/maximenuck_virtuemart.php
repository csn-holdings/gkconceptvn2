<?php

/**
 * @copyright	Copyright (C) 2014 Cédric KEIFLIN alias ced1870
 * http://www.joomlack.fr
 * @license		GNU/GPL
 * */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.event.plugin');

class plgSystemMaximenuck_virtuemart extends JPlugin {

	function plgSystemMaximenuck_virtuemart(&$subject, $params) {
		parent::__construct($subject, $params);
	}

	/**
	 * @param       JForm   The form to be altered.
	 * @param       array   The associated data for the form.
	 * @return      boolean
	 * @since       1.6
	 */
	function onContentPrepareForm($form, $data) {
		if ($form->getName() != 'com_modules.module' 
			&& $form->getName() != 'com_advancedmodules.module' 
			|| ($form->getName() == 'com_modules.module' && $data && $data->module != 'mod_maximenuck') 
			|| ($form->getName() == 'com_advancedmodules.module' && $data && $data->module != 'mod_maximenuck')
			)
			return;

		JForm::addFormPath(JPATH_SITE . '/plugins/system/maximenuck_virtuemart/params');
		// get the language
		$this->loadLanguage();

		$form->loadFile('maximenuck_virtuemart_params', false);
	}

	function onAfterRender() {

		$mainframe = JFactory::getApplication();
		$document = JFactory::getDocument();
		$doctype = $document->getType();

		// si pas en frontend, on sort
		if ($mainframe->isAdmin()) {
			return false;
		}

		// si pas HTML, on sort
		if ($doctype !== 'html') {
			return;
		}

		// renvoie les donnees dans la page
		$body = JResponse::getBody();
		$regex = "#{maximenu}(.*?){/maximenu}#s"; // masque de recherche
		$body = preg_replace($regex, '', $body);
		JResponse::setBody($body);
	}
}