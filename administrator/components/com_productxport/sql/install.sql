CREATE TABLE IF NOT EXISTS `#__px_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` varchar(255) NOT NULL DEFAULT '',
  `remote_addr` varchar(50) NOT NULL DEFAULT '',
  `user_agent` varchar(128) NOT NULL DEFAULT '',
  `items` int(11) NOT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `#__px_vm_images` (
  `virtuemart_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `virtuemart_media_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`virtuemart_product_id`)
) ENGINE=InnoDB;

