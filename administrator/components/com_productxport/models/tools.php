<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * productxport Component Server Model
 *
 * @package		Joomla
 * @subpackage	Content
 * @since 1.5
 */
class productxportModelTools extends JModelLegacy
{
	var $_visible_checker = null;
	var $_hidden_checker = null;

	/**
	 * Array of paths needed by the installer
	 * @var array
	 */
	var $_paths = array();

	/**
	 * A database connector object
	 * @var object
	 */
	var $_db = null;

	/**
	 * Associative array of package installer handlers
	 * @var array
	 */
	var $_adapters = array();


	/**
	 * Overridden constructor
	 * @access	protected
	 */
	function __construct()
	{
		parent::__construct();

	}


	function getVisibleChecker()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$lang = JFactory::getLanguage();
			$langTag = $lang->getTag();

			$filelist = array();
			$errors = array();
			$files = array(
			'configuration.php',
			''
			);

			foreach ($files as $file) {
				if (trim($file)) {
					if (is_file(JPATH_ROOT.'/'.$file)) {
						$filelist[]=JPATH_ROOT.'/'.$file;
					}
				}
			}


			foreach ($filelist as $checking_file) {

				$path_info = pathinfo($checking_file);
				$err_key = 'File: '.$checking_file;
				$this->_visible_checker[$err_key]=array('result'=>true, 'desc'=>'');

				$data = JFile::read($checking_file);
				if ($path_info['extension'] == 'php') {
					$handle = fopen($checking_file, "r");
					if ($handle) {
						// test beginning of the file
						if (fread($handle, 2) != '<?')  {
							$this->_visible_checker[$err_key]['result']=false;
							$this->_visible_checker[$err_key]['desc'] = JText::_('Wrong beginning of the file');
						}
						
						if (strpos($data, '?>')) {
							// test end of the file
							fseek($handle, -2, SEEK_END);
							$end1=fread($handle, 2);
							fseek($handle, -3, SEEK_END);
							$end2=fread($handle, 3);
							if (($end1 != '?>') && ($end2 != '?>'.chr(0x0A)))  {
								$this->_visible_checker[$err_key]['result']=false;
								$this->_visible_checker[$err_key]['desc'] = JText::_('Wrong ending of the file');
							}
						}
						
					} else {
						$this->_visible_checker[$err_key]['result']=false;
						$this->_visible_checker[$err_key]['desc'] = JText::_('Opening failed');
					}
				}
			}
		}

		$functions=array('base64_encode', 'base64_decode');
		foreach ($functions as $f) {
			$err_key = 'Function: '.$f;
			$this->_visible_checker[$err_key]=array('result'=>true, 'desc'=>'');
			if (!function_exists($f)) {
				$this->_visible_checker[$err_key]['result']=false;
				$this->_visible_checker[$err_key]['desc'] = JText::_('Unavailable');
			}
		}

		return $this->_visible_checker;
	}

	function getHiddenChecker()
	{

		$this->_hidden_checker = array();

		$err_key = 'Function: eval';
		$this->_hidden_checker[$err_key]=array('result'=>true, 'desc'=>'');
		if (!defined('EVAL_AVAILABLE')) {
			$this->_hidden_checker[$err_key]['result']=false;
			$this->_hidden_checker[$err_key]['desc'] = JText::_('Unavailable');
		}


		$functions=array('convert_uudecode', 'convert_uuencode', 'fsockopen');
		foreach ($functions as $f) {
			$err_key = 'Function: '.$f;
			$this->_hidden_checker[$err_key]=array('result'=>true, 'desc'=>'');
			if (!function_exists($f)) {
				$this->_hidden_checker[$err_key]['result']=false;
				$this->_hidden_checker[$err_key]['desc'] = JText::_('Unavailable');
			}
		}
		return $this->_hidden_checker;
	}

}
