<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * productXport Component Server Model
 *
 * @package		Joomla
 * @subpackage	Content
 * @since 1.5
 */
class productxportModelServer extends JModelLegacy
{
	var $_data = null;
	var $_paths = array();
	var $_manifest = null;
	var $_overwrite = false;
	var $_db = null;
	var $_adapters = array();
	var $_stepStack = array();

	/**
	 * The output from the install/uninstall scripts
	 * @var string
	 */
	var $message = null;

	/**
	 * Overridden constructor
	 * @access	protected
	 */
	function __construct()
	{
		parent::__construct();

	}


	function getConfig($code = null) {
		$input = JFactory::getApplication()->input;

		if (!$code) {
			$code = strtolower($input->get('code'));
		}
		
		$this->_config = array();
		$this->_config['code'] = $code;
		$this->_config['fields'] = array();

		if ($code) {
			$file = JPATH_ROOT.'/components/com_productxport/exports/'.$code.'/setting.xml'; 
			jimport('joomla.filesystem.file');
			jimport('joomla.utilities.simplexml');
			if (JFile::exists($file)) {
				$xml=simplexml_load_file($file);

				foreach( $xml->node as $child ) {
					$attr = $child->attributes();
					switch ($attr['type']) {
						case 'string':
							$this->_config[(string)$attr['name']] = (string)$child;
							break;
						case 'integer':
							$this->_config[(string)$attr['name']] = (string)$child;
							break;
						case 'object':
							switch ($attr['name']) {
								case 'fields':
									if (isset($child->node)) {
										foreach( $child->node as $field ) {
											$field_attr = $field->attributes();
											$this->_config['fields'][(string)$field_attr['name']]=array();
											foreach( $field->node as $entry ) {
												$entry_attr = $entry->attributes();
												$this->_config['fields'][(string)$field_attr['name']][(string)$entry_attr['name']]=(string)$entry;
											}
										}
									}
									break;
								case 'format_params':
									if (isset($child->node)) {
										foreach( $child->node as $entry ) {
											$entry_attr = $entry->attributes();
											switch ($entry_attr['name']) {
												case 'xml_header':
													$value = base64_decode((string)$entry);
													break;
												case 'xml_footer':
													$value = base64_decode((string)$entry);
													break;
												case 'line_format':
													$value = base64_decode((string)$entry);
													break;
												case 'header_line':
													$value = base64_decode((string)$entry);
													break;
												case 'replacements':
													$value = base64_decode((string)$entry);
													break;
												default:
													$value = (string)$entry;
													break;
											}
											$this->_config['format_params'][(string)$entry_attr['name']]=$value;
										}
									}
									break;
							}
							break;
					}
				}

			} else {
				$app = JFactory::getApplication('administrator');
				if ($app->isAdmin()) {
					$link = 'index.php?option=com_productxport&view=server';
					JError::raiseWarning(0, JText::_("Server code is wrong!"));
					$app->redirect( $link );
					exit;
				} else {
					die("Server code in the URL is invalid!");
				}
			}
		}
		return $this->_config;
	}

	/**
	 * Method to get servers item data
	 *
	 * @access public
	 * @return array
	 */
	function getItems()
	{
		jimport('joomla.filesystem.folder');
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$folders = JFolder::folders(JPATH_ROOT.'/components/com_productxport/exports');
			$this->_data = array();
			foreach ($folders as $folder) {
				$config = $this->getConfig($folder);
				$this->_data[$folder]=$config;
			}
		}

		return $this->_data;
	}
}
