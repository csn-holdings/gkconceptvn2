<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * productxport Component Component Model
 *
 * @package		Joomla
 * @subpackage	Content
 * @since 1.5
 */
class productxportModelComponent extends JModelLegacy
{
	/**
	 * Category ata array
	 *
	 * @var array
	 */
	var $_data = null;

	/**
	 * Array of paths needed by the installer
	 * @var array
	 */
	var $_paths = array();

	/**
	 * Method to get components item data
	 *
	 * @access public
	 * @return array
	 */
	function getItems()
	{
		jimport('joomla.filesystem.folder');
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$folder = JPATH_ROOT.'/components/com_productxport/plg_component';
			$this->_data = JFolder::folders($folder);
		}

		return $this->_data;
	}

	/**
	 * Overridden constructor
	 * @access	protected
	 */
	function __construct()
	{
		parent::__construct();

	}
}

