<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
//the name of the class must be the name of your component + InstallerScript
//for example: com_contentInstallerScript for com_content.
class com_productxportInstallerScript
{
	/*
	 * $parent is the class calling this method.
	 * $type is the type of change (install, update or discover_install, not uninstall).
	 * preflight runs before anything else and while the extracted files are in the uploaded temp folder.
	 * If preflight returns false, Joomla will abort the update and undo everything already done.
	 */
	function preflight( $type, $parent ) {
		$jversion = new JVersion();

		// Installing component manifest file version
		$this->release = $parent->get( "manifest" )->version;
		
		// Manifest file minimum Joomla version
		$this->minimum_joomla_release = $parent->get( "manifest" )->attributes()->version;   

		// Show the essential information at the install/update back-end
		/*
		echo '<p>Installing component manifest file version = ' . $this->release;
		echo '<br />Current manifest cache commponent version = ' . $this->getParam('version');
		echo '<br />Installing component manifest file minimum Joomla version = ' . $this->minimum_joomla_release;
		echo '<br />Current Joomla version = ' . $jversion->getShortVersion();
		*/

		// abort if the current Joomla release is older
		if( version_compare( $jversion->getShortVersion(), $this->minimum_joomla_release, 'lt' ) ) {
			Jerror::raiseWarning(null, 'Cannot install com_productxport in a Joomla release prior to '.$this->minimum_joomla_release);
			return false;
		}
 
		// abort if the component being installed is not newer than the currently installed version
		if ( $type == 'update' ) {
			$oldRelease = $this->getParam('version');
			$rel = $oldRelease . ' to ' . $this->release;
			if ( version_compare( $oldRelease, '4.1', '<' ) ) {
				Jerror::raiseWarning(null, 'YOU HAVE TO DO FRESH INSTALL (UNINSTALL THE OLD VERSION OF PRODUCTXPORT FIRST)!');
				return false;
			}
			if ( version_compare( $this->release, $oldRelease, 'le' ) ) {
				Jerror::raiseWarning(null, 'Incorrect version sequence. Cannot upgrade ' . $rel);
				return false;
			}
			if (strpos($oldRelease, 'prof') > 0) {
				Jerror::raiseWarning(null, 'Cannot install free version over professional');
				return false;
			}
		}
		else { $rel = $this->release; }
 
		jimport( 'joomla.filesystem.file' );
		jimport( 'joomla.filesystem.folder' );
		jimport( 'joomla.filesystem.path' );

		JPath::setPermissions(JPATH_ADMINISTRATOR.'/components/com_productxport');
		JPath::setPermissions(JPATH_ROOT.'/components/com_productxport');
		if (JFolder::exists(JPATH_ROOT.'/plugins/system/productxport')) {
			JPath::setPermissions(JPATH_ROOT.'/plugins/system/productxport');
		}

		# transform setting from older J! version
		if (JFolder::exists(JPATH_ROOT.'/components/com_productxport/plg_component/virtuemart-2.0')) {
			JFolder::move(JPATH_ROOT.'/components/com_productxport/plg_component/virtuemart-2.0', JPATH_ROOT.'/components/com_productxport/plg_component/virtuemart');
		}
		
		if (JFile::exists(JPATH_ROOT.'/components/com_productxport/setting.ini.bak'))
			JFile::delete(JPATH_ROOT.'/components/com_productxport/setting.ini.bak');
		if (JFile::exists(JPATH_ROOT.'/components/com_productxport/setting.ini'))
			JFile::copy(JPATH_ROOT.'/components/com_productxport/setting.ini' , JPATH_ROOT.'/components/com_productxport/setting.ini.bak');

		//echo '<p>' . $type . ' ' . $rel . '</p>';
	}
 
	/*
	 * $parent is the class calling this method.
	 * install runs after the database scripts are executed.
	 * If the extension is new, the install method is run.
	 * If install returns false, Joomla will abort the install and undo everything already done.
	 */
	function install( $parent ) {
		JFolder::delete(JPATH_ROOT.'/components/com_productxport/exports');
		JFile::move(JPATH_ROOT.'/components/com_productxport/exports.dist', JPATH_ROOT.'/components/com_productxport/exports');
		JFile::move(JPATH_ROOT.'/components/com_productxport/plg_component/virtuemart/setting.ini.dist' , JPATH_ROOT.'/components/com_productxport/plg_component/virtuemart/setting.ini');
		JFile::move(JPATH_ROOT.'/components/com_productxport/setting.ini.dist' , JPATH_ROOT.'/components/com_productxport/setting.ini');
	}
 
	/*
	 * $parent is the class calling this method.
	 * update runs after the database scripts are executed.
	 * If the extension exists, then the update method is run.
	 * If this returns false, Joomla will abort the update and undo everything already done.
	 */
	function update( $parent ) {
		$db = JFactory::getDBO();

		if ((int)$this->getParam('version') <= 3) {
      		$queries = JFile::read(JPATH_ADMINISTRATOR.'/components/com_productxport/sql/updates/4.0.sql');
      		$queries = $db->splitSql($queries);
			if (is_array($queries))
				foreach ($queries as $query)
					if (JString::trim($query)) {
						$db->setQuery($query);
						$db->query();
					}
		}

		jimport( 'joomla.filesystem.folder' );
		$files = JFolder::files(JPATH_ROOT.'/components/com_productxport/exports.dist', '.', true, true);
		foreach ($files as $file) {
			$file_old = str_replace('/exports.dist/', '/exports/', $file);
			$path_info=pathinfo($file_old);
			if (!JFile::exists($file_old)) {
				if (!JFolder::exists($path_info['dirname'])) JFolder::create($path_info['dirname']);
				JFile::copy($file, $file_old);
			}
		}
		
		JFolder::delete(JPATH_ROOT.'/components/com_productxport/exports.dist');

		JFile::delete(JPATH_ROOT.'/components/com_productxport/plg_component/virtuemart/setting.ini.dist');
		JFile::delete(JPATH_ROOT.'/components/com_productxport/setting.ini.dist');
		JFile::move(JPATH_ROOT.'/components/com_productxport/setting.ini.bak' , JPATH_ROOT.'/components/com_productxport/setting.ini');

		# transform setting from older J! version
		$mainSetting = new JRegistry();
		$mainSetting->loadFile(JPATH_ROOT.'/components/com_productxport/setting.ini', 'INI');
		if ($mainSetting->get('COMPONENT_PLUGIN')=='virtuemart-2.0') {
			$mainSetting->set('COMPONENT_PLUGIN', 'virtuemart');
			$buffer = $mainSetting->toString('INI');
			JFile::write(JPATH_ROOT.'/components/com_productxport/setting.ini', $buffer); 
		}

		if (JFolder::exists(JPATH_ROOT.'/components/com_productxport/plg_component/virtuemart-1.1'))
			JFolder::delete(JPATH_ROOT.'/components/com_productxport/plg_component/virtuemart-1.1');

		$files = JFolder::files(JPATH_ADMINISTRATOR.'/language', '.*com_productxport.menu.ini', true, true);
		foreach ($files as $file) JFile::delete($file);

		# specific change setting - zbozi.cz:
		$zbozi_cz_file=JPATH_ROOT.'/components/com_productxport/exports/zbozi.cz/setting.xml';
		$zbozi_cz_xml=JFile::read($zbozi_cz_file);
		$zbozi_cz_xml=str_replace('<node name="xml_root_element" type="string">SHOP</node>','<node name="xml_root_element" type="string">SHOP xmlns="http://www.zbozi.cz/ns/offer/1.0"</node>',$zbozi_cz_xml);
		if (strpos($zbozi_cz_xml, '>ITEM_ID<') === false)
			$zbozi_cz_xml=str_replace('<node name="fields" type="object">','<node name="fields" type="object"><node name="product_id" type="object"><node name="enabled" type="integer">1</node><node name="fieldname" type="string">ITEM_ID</node></node>',$zbozi_cz_xml);
		if (strpos($zbozi_cz_xml, '>PRODUCTNAME<') === false)
			$zbozi_cz_xml=str_replace('>PRODUCT<','>PRODUCTNAME<',$zbozi_cz_xml);
		$zbozi_cz_xml=str_replace('<node name="price" type="object"><node name="enabled" type="integer">1</node><node name="fieldname" type="string">PRICE</node><node name="decimals" type="string">2</node></node>','',$zbozi_cz_xml);
		$zbozi_cz_xml=str_replace('<node name="price" type="object"><node name="enabled" type="integer">1</node><node name="fieldname" type="string">PRICE</node><node name="decimals" type="string">0</node></node>','',$zbozi_cz_xml);
		$zbozi_cz_xml=str_replace('<node name="vat" type="object"><node name="enabled" type="integer">1</node><node name="fieldname" type="string">VAT</node><node name="format" type="string">percent</node></node>','',$zbozi_cz_xml);
		$zbozi_cz_xml=str_replace('<node name="vat" type="object"><node name="enabled" type="integer">1</node><node name="fieldname" type="string">VAT</node><node name="format" type="string">decimal</node></node>','',$zbozi_cz_xml);
		$zbozi_cz_xml=str_replace('<node name="vat" type="object"><node name="enabled" type="integer">1</node><node name="fieldname" type="string">VAT</node><node name="format" type="string">decimaltext</node></node>','',$zbozi_cz_xml);
		JFile::write($zbozi_cz_file, $zbozi_cz_xml);

		# change tag name: category_full to category
		$files = JFolder::files(JPATH_ROOT.'/components/com_productxport/exports','setting.xml',true, true);
		foreach ($files as $file) {
			$setting_xml=JFile::read($file);
			$setting_xml=str_replace('category_full', 'category', $setting_xml);
			JFile::write($file, $setting_xml);
			
		} 
		// You can have the backend jump directly to the newly updated component configuration page
		// $parent->getParent()->setRedirectURL('index.php?option=com_democompupdate');
	}
 
	/*
	 * get a variable from the manifest file (actually, from the manifest cache).
	 */
	function getParam( $name ) {
		$db = JFactory::getDbo();
		$db->setQuery('SELECT manifest_cache FROM #__extensions WHERE element = "com_productxport"');
		$manifest = json_decode( $db->loadResult(), true );
		return $manifest[ $name ];
	}
}
