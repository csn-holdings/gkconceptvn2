<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/*
 * Make sure the user is authorized to view this page
 */
$user = JFactory::getUser();

// Require the base controller
require_once (JPATH_COMPONENT.'/controller.php');
require_once (JPATH_COMPONENT.'/classes/common.php');

JHtml::stylesheet('administrator/components/com_productxport/css/style.css');

$controller	= new productxportController( );

PX::init();

$componentPluginName = PX::getGlobalSetting()->get('COMPONENT_PLUGIN');
$componentPluginCode = PX::name2code($componentPluginName);
PX::setComponentSetting(JPATH_ROOT.'/components/com_productxport/plg_component/'.$componentPluginName.'/setting.ini', 'INI');

$subMenus = array(
	JText::_('COM_SERVER_MANAGER') => 'server',
	JText::_('COM_ESHOP_SETTING') => 'component',
	JText::_('COM_LOG') => 'log',
	JText::_('COM_TOOLS') => 'tools');


foreach ($subMenus as $name => $view) {
	JSubMenuHelper::addEntry($name, "index.php?option=com_productxport&view=$view");
}


// Perform the Request task
$input = JFactory::getApplication()->input;
$controller->execute( $input->get('task'));

$controller->redirect();
