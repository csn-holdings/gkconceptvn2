<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

/**
 * productXport Controller
 *
 * @package		Joomla
 * @subpackage	productXport
 * @since 1.5
 */
class productxportController extends JControllerLegacy
{
	function __construct($config = array())
	{
		parent::__construct($config);

		// Register Extra tasks
		$this->registerTask( 'add',  'display' );
		$this->registerTask( 'edit', 'display' );
	}

	function display( $cachable = false, $urlparams = false )
	{
		$input = JFactory::getApplication()->input;

		// Set a default view if none exists
		if ( ! $input->get( 'view' ) ) {
			$input->set('view', 'default' );
		}
		$view = $input->get( 'view' );

		switch($this->getTask())
		{
			case 'server.add'     :
			case 'add'     :
			{
				$input->set( 'hidemainmenu', 1 );
				$input->set( 'layout', 'form'  );
				$input->set( 'edit', false );

				// Load page
//				$model = $this->getModel($view);
			} break;
			case 'edit'    :
			{
				$input->set( 'hidemainmenu', 1 );
				$input->set( 'layout', 'form'  );
				$input->set( 'view'  , $view);
				$input->set( 'edit', true );

				// Load page
//				$model = $this->getModel($view);
			} break;
		}

		parent::display();
	}

	function apply()
	{
		$app = JFactory::getApplication();
		$input = JFactory::getApplication()->input;

		$this->_save();

		$input->set( 'hidemainmenu', 1 );
		$input->set( 'layout', 'form'  );
		$input->set( 'edit', false );

		$view	= $input->get('view');
		$code	= strtolower($input->get('name'));

		$hash=$input->get('current_tab','','string');
		if ($hash) $hash;

		$msg="";
		switch ($view) {
			case 'component':
				$link = 'index.php?option=com_productxport&view='.$view.'&layout=form&code='.$code.$hash;
				$this->setRedirect($link, $msg);
				break;
			case 'server':
			default:
				$code	= $input->get('code');
				$link = 'index.php?option=com_productxport&view='.$view.'&layout=form&code='.$code.$hash;
				$this->setRedirect($link, $msg);
//				parent::display();
				break;
		}

	}

	function save()
	{
		$input = JFactory::getApplication()->input;

		$this->_save();

		$view	= $input->get('view');

		$link = 'index.php?option=com_productxport&view='.$view;
		$this->setRedirect($link, $msg);
	}


	function _save()
	{
		$app = JFactory::getApplication();
		$input = JFactory::getApplication()->input;

		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$view	= $input->get('view');
		$name	= $input->get('name');
		$type	= $input->get('type');

		// Get data from the model
		$code	= strtolower($input->get('code'));

		switch ($type) {
			case 'server':

				$registry_sub	= (object)null;
				$fields = explode(',', $input->get('ordering', null, 'string'));

				foreach ($fields as $field) {
					switch ($field) {
						case 'product_id':
							$product_id_object = (object)null;
							$product_id_object->enabled=1;
							$product_id_object->fieldname=$input->get('product_id', null, 'string');
							$product_id_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->product_id=$product_id_object;
							break;
						case 'product_sku':
							$product_sku_object = (object)null;
							$product_sku_object->enabled=1;
							$product_sku_object->fieldname=$input->get('product_sku', null, 'string');
							$product_sku_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->product_sku=$product_sku_object;
							break;
						case 'url':
							$url_object = (object)null;
							$url_object->enabled=1;
							$url_object->fieldname=$input->get('url', null, 'string');
							$url_object->url_type=$input->get('url_type', null, 'string');
							$url_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->url=$url_object;
							break;
						case 'name':
							$name_object = (object)null;
							$name_object->enabled=1;
							$name_object->fieldname=$input->get('name', null, 'string');
							$name_object->condition=$input->get('name_condition', null, 'string');
							$name_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->name=$name_object;
							break;
						case 'short_description':
							$description_object = (object)null;
							$description_object->enabled=1;
							$description_object->fieldname=$input->get('short_description', null, 'string');
							$description_object->strip_tags=$input->get('short_description_strip_tags', null, 'string');
							$description_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->short_description=$description_object;
							break;
						case 'long_description':
							$description_object = (object)null;
							$description_object->enabled=1;
							$description_object->fieldname=$input->get('long_description', null, 'string');
							$description_object->strip_tags=$input->get('long_description_strip_tags', null, 'string');
							$description_object->max_length=$input->get('long_description_max_length', null, 'string');
							$description_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->long_description=$description_object;
							break;
						case 'image':
							$image_object = (object)null;
							$image_object->enabled=1;
							$image_object->fieldname=$input->get('image', null, 'string');
							$image_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->image=$image_object;
							break;
						case 'stock':
							$stock_object = (object)null;
							$stock_object->enabled=1;
							$stock_object->fieldname=$input->get('stock', null, 'string');
							$stock_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->stock=$stock_object;
							break;
						case 'currency':
							$currency_object = (object)null;
							$currency_object->enabled=1;
							$currency_object->fieldname=$input->get('currency', null, 'string');
							$currency_object->currency_value=$input->get('currency_value', null, 'string');
							$currency_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->currency=$currency_object;
							break;
						case 'category':
							$category_object = (object)null;
							$category_object->enabled=1;
							$category_object->fieldname=$input->get('category', null, 'string');
							$category_object->delimiter=$input->get('category1_delimiter', null, 'string');
							$category_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->category=$category_object;
							break;
						case 'category_alternative':
							$category_alternative_object = (object)null;
							$category_alternative_object->enabled=1;
							$category_alternative_object->fieldname=$input->get('category_alternative', null, 'string');
							$category_alternative_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->category_alternative=$category_alternative_object;
							break;
						case 'delivery_date':
							$delivery_date_object = (object)null;
							$delivery_date_object->enabled=1;
							$delivery_date_object->fieldname=$input->get('delivery_date', null, 'string');
							$delivery_date_object->time_format=$input->get('delitery_date_time_format', null, 'string');
							$delivery_date_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->delivery_date=$delivery_date_object;
							break;
						case 'delivery_date_in':
							$delivery_date_in_object = (object)null;
							$delivery_date_in_object->enabled=1;
							$delivery_date_in_object->fieldname=$input->get('delivery_date_in', null, 'string');
							$delivery_date_in_object->time_format=$input->get('delitery_date_in_time_format', null, 'string');
							$delivery_date_in_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->delivery_date_in=$delivery_date_in_object;
							break;
						case 'delivery_date_date':
							$delivery_date_date_object = (object)null;
							$delivery_date_date_object->enabled=1;
							$delivery_date_date_object->fieldname=$input->get('delivery_date_date', null, 'string');
							$delivery_date_date_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->delivery_date_date=$delivery_date_date_object;
							break;
						case 'vat':
							$vat_object = (object)null;
							$vat_object->enabled=1;
							$vat_object->fieldname=$input->get('vat', null, 'string');
							$vat_object->format=$input->get('vat_format', null, 'string');
							$vat_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->vat=$vat_object;
							break;
						case 'price':
							$price_object = (object)null;
							$price_object->enabled=1;
							$price_object->fieldname=$input->get('price', null, 'string');
							$price_object->decimals=$input->get('price_decimals', null, 'string');
							$price_object->decimal_symbol=$input->get('price_decimal_symbol', null, 'string');
							$price_object->currency=$input->get('price_currency', null, 'string');
							$price_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->price=$price_object;
							break;
						case 'price_vat':
							$price_vat_object = (object)null;
							$price_vat_object->enabled=1;
							$price_vat_object->fieldname=$input->get('price_vat', null, 'string');
							$price_vat_object->decimals=$input->get('price_vat_decimals', null, 'string');
							$price_vat_object->decimal_symbol=$input->get('price_vat_decimal_symbol', null, 'string');
							$price_vat_object->currency=$input->get('price_vat_currency', null, 'string');
							$price_vat_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->price_vat=$price_vat_object;
							break;
						case 'orig_price':
							$orig_price_object = (object)null;
							$orig_price_object->enabled=1;
							$orig_price_object->fieldname=$input->get('orig_price', null, 'string');
							$orig_price_object->decimals=$input->get('orig_price_decimals', null, 'string');
							$orig_price_object->decimal_symbol=$input->get('orig_price_decimal_symbol', null, 'string');
							$orig_price_object->currency=$input->get('orig_price_currency', null, 'string');
							$orig_price_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->orig_price=$orig_price_object;
							break;
						case 'orig_price_vat':
							$orig_price_vat_object = (object)null;
							$orig_price_vat_object->enabled=1;
							$orig_price_vat_object->fieldname=$input->get('orig_price_vat', null, 'string');
							$orig_price_vat_object->decimals=$input->get('orig_price_vat_decimals', null, 'string');
							$orig_price_vat_object->decimal_symbol=$input->get('orig_price_vat_decimal_symbol', null, 'string');
							$orig_price_vat_object->currency=$input->get('orig_price_vat_currency', null, 'string');
							$orig_price_vat_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->orig_price_vat=$orig_price_vat_object;
							break;
						case 'weight':
							$weight_object = (object)null;
							$weight_object->enabled=1;
							$weight_object->fieldname=$input->get('weight', null, 'string');
							$weight_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->weight=$weight_object;
							break;
						case 'manufacturer':
							$manufacturer_object = (object)null;
							$manufacturer_object->enabled=1;
							$manufacturer_object->fieldname=$input->get('manufacturer', null, 'string');
							$manufacturer_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->manufacturer=$manufacturer_object;
							break;
						case 'product_attributes':
							$product_attributes_object = (object)null;
							$product_attributes_object->enabled=1;
							$product_attributes_object->fieldname=$input->get('product_attributes', null, 'string');
							$product_attributes_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->product_attributes=$product_attributes_object;
							break;
						case 'ean':
							$ean_object = (object)null;
							$ean_object->enabled=1;
							$ean_object->fieldname=$input->get('ean', null, 'string');
							$ean_object->source=$input->get('ean_source', null, 'string');
							$ean_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->ean=$ean_object;
							break;
						case 'mpn': // reverved
							$mpn_object = (object)null;
							$mpn_object->enabled=1;
							$mpn_object->fieldname=$input->get('mpn', null, 'string');
							$mpn_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->mpn=$mpn_object;
							break;
						case 'extended1':
						case 'extended2':
						case 'extended3':
						case 'extended4':
						case 'extended5':
						case 'extended6':
							$extended_object = (object)null;
							$extended_object->enabled=1;
							$extended_object->fieldname=$input->get($field, null, 'string');
							$extended_object->extended_value=$input->get($field.'_value', null, 'string');
							$extended_object->cdata=$input->get($field.'_cdata', 0, 'int');
							$registry_sub->$field=$extended_object;
							break;
					}
				}

				switch ($input->get('file_format')) {
					case 'xml':
						$file_format_params = (object)null;
						$file_format_params->xml_header = base64_encode(JRequest::getVar('xml_header', null, 'post', 'string', JREQUEST_ALLOWRAW));
						$file_format_params->xml_footer = base64_encode(JRequest::getVar('xml_footer', null, 'post', 'string', JREQUEST_ALLOWRAW));
						$file_format_params->xml_root_element = stripslashes(htmlspecialchars($input->get('xml_root_element', null, 'string')));
						$file_format_params->xml_product_element = htmlspecialchars($input->get('xml_product_element', null, 'string'));
						break;
					case 'csv':
						$file_format_params = (object)null;
						$file_format_params->header_line = base64_encode(JRequest::getVar('header_line', null, 'post', 'string', JREQUEST_ALLOWRAW));
						$file_format_params->line_format = base64_encode(JRequest::getVar('line_format', null, 'post', 'string', JREQUEST_ALLOWRAW));
						$file_format_params->line_delimiter = htmlspecialchars(JRequest::getVar('line_delimiter', null, 'post', 'string'));
						$file_format_params->replacements = base64_encode(JRequest::getVar('replacements', null, 'post', 'string', JREQUEST_ALLOWRAW));
						break;
				}

				$registry	= new JRegistry();
				$data = array(
					'code'=>$code,
					'format'=>$input->get('file_format'),
					'debug_mode'=>$input->get('debug_mode'),
					'format_params'=>$file_format_params,
					'fields'=>$registry_sub,
				);

				$registry->loadArray($data);	

				jimport('joomla.filesystem.file');
				jimport('joomla.filesystem.folder');

				if (!JFolder::create(JPATH_ROOT.'/components/com_productxport/exports/'.$code)) {
					$link = 'index.php?option=com_productxport&view='.$view;
					JError::raiseWarning(0, JText::_('Filesystems does not allow to create direcotry').': '.JPATH_ROOT.'/components/com_productxport/exports/'.$code.'/' );
					$app->redirect( $link );
					exit;
				}
				$setting_string=$registry->toString('XML');
				if (!JFile::write(JPATH_ROOT.'/components/com_productxport/exports/'.$code.'/'.'setting.xml', $setting_string)) {
					JError::raiseWarning(0, JText::_('Filesystems does not allow to write following file').': '.JPATH_ROOT.'/components/com_productxport/exports/'.$code.'/setting.xml' );
					$link = 'index.php?option=com_productxport&view='.$view;
					$app->redirect( $link );
					exit;
				}

				if ($input->get('direct_url') == 1) {
$indexphp_content="<?php
\$dirs = explode(DIRECTORY_SEPARATOR, dirname(__FILE__));

define('REL_PATH_SCRIPT',implode('/',array_slice(\$dirs, count(\$dirs)-4)).'/');
define('EXPORT_CODE', array_pop(\$dirs));

include('../../export.php');
?>";
					JFile::write(JPATH_ROOT.'/components/com_productxport/exports/'.$code.'/index.php', $indexphp_content); 
				} else {
					if (JFile::exists(JPATH_ROOT.'/components/com_productxport/exports/'.$code.'/index.php')) {
						JFile::delete(JPATH_ROOT.'/components/com_productxport/exports/'.$code.'/index.php'); 
					}
				}
				if (!JFile::exists(JPATH_ROOT.'/components/com_productxport/exports/'.$code.'/index.html')) {
					$indexhtml_content='<html><body bgcolor="#FFFFFF"></body></html>';
					JFile::write(JPATH_ROOT.'/components/com_productxport/exports/'.$code.'/index.html', $indexhtml_content); 
				}


				break;
			case 'component':
				if ($name) {
					$code = PX::name2code($name);
					// Try to load the extension class
					if (file_exists(JPATH_ROOT.'/components/com_productxport/plg_component/'.$name.'/'.$name.'.php')) {
						require_once(JPATH_ROOT.'/components/com_productxport/plg_component/'.$name.'/'.$name.'.php');

						// Create the plugin object
						$classname	= 'plgComponent'.ucfirst($code);
						$plugin = new $classname( );

						$setting=$plugin->adminBuildConfig();
						if ($setting) {
							$registry	= new JRegistry();

							$registry->loadArray($setting);	

							jimport('joomla.filesystem.file');
							echo $registry->toString();

							JFile::write(JPATH_ROOT.'/components/com_productxport/plg_component/'.$name.'/setting.ini', $registry->toString('INI')); 
						}
					}
				}
				break;
		}
	}

	function remove()
	{
		$input = JFactory::getApplication()->input;

		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$db		= JFactory::getDBO();

		$view	= $input->get('view');
		switch($view) {
			case 'server':
				jimport('joomla.filesystem.file');
				jimport('joomla.filesystem.folder'); 

				$cid = $input->get( 'cid', array(), 'array' );

				if (count( $cid ) < 1) {
					JError::raiseError(500, JText::_( 'Select an item to delete' ) );
				}
				foreach ($cid as $code ) {
					JFolder::delete(JPATH_ROOT.'/components/com_productxport/exports/'.$code);
				}

				$this->setRedirect( 'index.php?option=com_productxport&view='.$view );

				break;
		}
	}



	function cancel()
	{
		$input = JFactory::getApplication()->input;
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$view	= $input->get('view');

		$this->setRedirect( 'index.php?option=com_productxport&view='.$view );
	}

	function makeDefault()
	{
		$input = JFactory::getApplication()->input;

		$plugin	= $input->get( 'cid', array(), 'array' );
		switch (count($plugin)) {
			case 0:
				echo "<script> alert('".JText::_( 'Choose an item to set as active' )."'); window.history.go(-1); </script>\n";
				break;
			case 1:
				$globalSetting	= new JRegistry();
				$globalSetting->loadFile(JPATH_ROOT.'/components/com_productxport/setting.ini', 'INI');

				$data = $globalSetting->toArray();

				$data['COMPONENT_PLUGIN']=$plugin[0];

				$registry	= new JRegistry();

				$registry->loadArray($data);	

				jimport('joomla.filesystem.file');

				JFile::write(JPATH_ROOT.'/components/com_productxport/setting.ini', $registry->toString('INI')); 
				break;
			default:
				echo "<script> alert('".JText::_( 'Choose just one item to set as active' )."'); window.history.go(-1); </script>\n";
				break;
		}


		$this->setRedirect( 'index.php?option=com_productxport&view=component' );
	}
}
