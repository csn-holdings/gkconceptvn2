<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the productXport component
 *
 * @static
 * @package		Joomla
 * @subpackage	productXport
 * @since 1.0
 */
class ProductxportViewComponent extends JViewLegacy
{
	function display($tpl = null)
	{
		global $option;

		$db		= JFactory::getDBO();
		$uri	= JFactory::getURI();
		$input = JFactory::getApplication()->input;
		

		switch ($this->getLayout()) {
			case 'form':
				// Get data from the model
				$item		= $this->get( 'Info');
				$code		= $input->get('code');

				$document=JFactory::getDocument();
				$document->addScript("components/com_productxport/js/autosize.min.js");
				$document->addScriptDeclaration("
				window.addEvent('domready', function(){
					autosize(document.querySelectorAll('textarea'));
					\$\$('.nav-tabs').addEvent('click', function(){
						setTimeout('autosize.update(document.querySelectorAll(\'textarea\'));', 50);
					}); 					
				});");

				if ($code) {
					// Try to load the extension class
					if (file_exists(JPATH_ROOT.'/components/com_productxport/plg_component/'.$code.'/'.$code.'.php')) {
						require_once(JPATH_ROOT.'/components/com_productxport/plg_component/'.$code.'/'.$code.'.php');
						// Create the plugin object
						$classnameCode=PX::name2code($code);
						$classnameCode=str_replace('-','', $classnameCode);
						$classnameCode=str_replace('_','', $classnameCode);
						$classnameCode=str_replace('.','', $classnameCode);
						$classname	= 'plgComponent'.ucfirst($classnameCode);
						$componentPlugin = new $classname();

						$html_output=$componentPlugin->adminGetForm();

						$this->assignRef('html_output',		$html_output);
						$this->assignRef('code',		$code);
						$this->assignRef('setting',		$registry);
					}
				}

				break;
			default:
				$globalSetting	= new JRegistry();
				$globalSetting->loadFile(JPATH_ROOT.'/components/com_productxport/setting.ini', 'INI');

				$default_component = $globalSetting->get('COMPONENT_PLUGIN');

				// Get data from the model
				$items		= $this->get( 'Items');
				$this->assignRef('default_component',		$default_component);
				$this->assignRef('items',		$items);
				break;
		}

		$this->assignRef('setting',		$setting);
		parent::display($tpl);
	}
}
