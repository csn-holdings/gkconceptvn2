<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JHTML::_('behavior.tooltip');

	$titleName=$this->code?$this->code:JText::_('New');

	JToolBarHelper::title(   JText::_( 'productXport - Component plugin setting' ).': <small><small>[ ' . $titleName.' ]</small></small>', 'cpanel.png'  );
	JToolBarHelper::save();
	JToolBarHelper::apply();
	JToolBarHelper::cancel();
		// for existing items the button is renamed `close`
//		JToolBarHelper::cancel( 'cancel', 'Close' );
	//JToolBarHelper::help( 'screen..edit' );
?>

<script language="javascript" type="text/javascript">
	var isCtrl;
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}

		submitform( pressbutton );
	}
	window.addEvent('keyup',function(event) {
    	if(event.code == 17) isCtrl=false;
	});
	window.addEvent('keydown',function(event) {
    	if(event.code == 17) isCtrl=true;
    	if(event.code == 83 && isCtrl == true) {
			isCtrl=false
			submitbutton('apply');
        	return false;
    	}
		return true;
	});

?>
</script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php
	echo $this->html_output; ?>

<div class="clr"></div>

	<input type="hidden" name="option" value="com_productxport" />
	<input type="hidden" name="view" value="<?php echo $this->_name; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
