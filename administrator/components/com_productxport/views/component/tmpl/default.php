<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JHTML::_('behavior.tooltip');

	// Set toolbar items for the page
	JToolBarHelper::title(   JText::_( 'productXport - Component manager' ), 'cpanel.png' );
	JToolBarHelper::makeDefault('makeDefault');
	//JToolBarHelper::publishList();
	//JToolBarHelper::unpublishList();
	//JToolBarHelper::addNewX();
	//JToolBarHelper::editListX();
	//JToolBarHelper::deleteList();
	//JToolBarHelper::preferences('com_disctest', '360');
	//JToolBarHelper::help( 'screen.disctest' );
?>
<form action="index.php" method="post" name="adminForm">
<?php if (!empty( $this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
	<table class="table table-striped">
	<thead>
		<tr>
			<th width="20" align="center">
			</th>
			<th class="title" width="65%" align="left">
				<?php echo JText::_( 'Plugin name' );?>
			</th>
			<th class="title" width="30%" align="left">
				<?php echo JText::_( 'Active' );?>
			</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$k = 0;
	foreach ($this->items as $name)
	{

		$link 	= JRoute::_( 'index.php?option=com_productxport&view=component&layout=form&code='. $name );
		$showName=str_replace('virtuemart', 'VirtueMart', $name);

		//$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		if ($this->default_component==$name) {
			$default_img='<img src="components/com_productxport/images/icon-16-default.png" border="0" />';
			$checked_input="checked";
		} else {
			$default_img='';
			$checked_input="";
		}
		$checked= '<input type="radio" name="default_plugin" value="'.$name.'" '.$checked_input.' />';


		?>
		<tr class="<?php echo "row$k"; ?>">
			<td align="center">
				<?php echo $checked; ?>
			</td>
			<td>
				<span><a href="<?php echo $link; ?>"><?php echo $showName; ?></a></span>
			</td>
			<td align="center">
				<span><?php echo $default_img; ?></span>
			</td>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	</tbody>
	</table>
</div>

	<input type="hidden" name="option" value="com_productxport" />
	<input type="hidden" name="view" value="<?php echo $this->_name; ?>" />
	<input type="hidden" name="type" value="<?php echo $this->_name; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>


