<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

	$time=time();
	$titleName=$this->code?$this->code:JText::_('New');
	$document = JFactory::getDocument();

	JToolBarHelper::title(   JText::_( 'productXport - server setting' ).': <small><small>[ ' . $titleName .' ]</small></small>', 'cpanel.png'  );
	if ($this->code) {
		JToolBarHelper::save();
	}
	JToolBarHelper::apply();
	JToolBarHelper::cancel();

?>
<style>
#container {
  width: 145px;
  margin: 20px 0;
  overflow: hidden;
  border: 1px solid #999;
}

.item {
  float: left;
  height: 40px;
  width: 40px;
  margin: 4px;
  background-color: #78BA91;
  cursor: pointer;
}

.new {
  background-color: #6B7B95;
}
.handle {
	display:inline-block;
	background-image:url("components/com_productxport/images/cursor-move.png");
	width: 24px;
	height: 28px;
}
.arrow {
	display:inline-block;
	width: 28px;
	height: 28px;
	cursor: pointer;
	background:url("components/com_productxport/images/arrow.png") top left;
}
.arrow.open {
	background:url("components/com_productxport/images/arrow.png") top right;
}
#tag_list li {
	border-bottom: 1px solid #999;
	list-style-type:none;
}
td.el_list {
	padding: 5px;
}
.extended_line {
	line-height: 20px;
}
.prefix {
	display: none;
}	
.prefix.variable {
	position: relative;
	display: inline-block;
	font-family:courier;
	top: -5px;
}	
.extended_value.variable {
	font-family:courier;
}
a.delete img {
	margin-left: 10px;
}
.top-buttons {
	line-height: 20px;
}
#field_type {
	margin-bottom: 0;	
}	
</style>

<?php function get_product_id_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Product ID"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?>
			</div>
			<div class="span8">
				<input type="text" name="product_id" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_product_sku_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Product code /product_sku/"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="product_sku" size="50" value="<?php echo $field_config['fieldname'];?>" />
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_url_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Product URL"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Type of exported URL"); ?>
			</div>
			<div class="span8">
				<fieldset id="jform_url_type" class="radio">
					<input type="radio" id="jform_url_type0" name="url_type" value="sef" <?php echo @$field_config['url_type']=='sef'?'checked':''; ?> />
					<label for="jform_url_type0"><?php echo JText::_("SEF URL /if exists/:"); ?></label>
					<input type="radio" id="jform_url_type1" name="url_type" value="nonsef" <?php echo @$field_config['url_type']=='nonsef'?'checked':''; ?> />
					<label for="jform_url_type1"><?php echo JText::_("NonSEF URL:"); ?></label>
				</fieldset>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_name_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Product name"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_short_description_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Short description"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Strip html tags"); ?>
			</div>
			<div class="span8">
				<fieldset id="jform_short_description_strip_tags" class="radio">
					<input type="radio" id="jform_short_description_strip_tags0" name="short_description_strip_tags" value="1" <?php echo @$field_config['strip_tags']=='1'?'checked':''; ?> />
					<label for="jform_short_description_strip_tags0"><?php echo JText::_( 'JYES' ); ?></label>
					<input type="radio" id="jform_short_description_strip_tags1" name="short_description_strip_tags" value="0" <?php echo @$field_config['strip_tags']=='0'?'checked':''; ?> />
					<label for="jform_short_description_strip_tags1"><?php echo JText::_( 'JNO' ); ?></label>
				</fieldset>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_long_description_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Long description"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Strip html tags"); ?>
			</div>
			<div class="span8">
				<fieldset id="jform_long_description_strip_tags" class="radio">
					<input type="radio" id="jform_long_description_strip_tags0" name="long_description_strip_tags" value="1" <?php echo @$field_config['strip_tags']=='1'?'checked':''; ?> />
					<label for="jform_long_description_strip_tags0"><?php echo JText::_( 'JYES' ); ?></label>
					<input type="radio" id="jform_long_description_strip_tags1" name="long_description_strip_tags" value="0" <?php echo @$field_config['strip_tags']=='0'?'checked':''; ?> />
					<label for="jform_long_description_strip_tags1"><?php echo JText::_( 'JNO' ); ?></label>
				</fieldset>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Max length:"); ?>
			</div>
			<div class="span8">
				<input type="text" name="long_description_max_length" size="10" value="<?php echo @$field_config['max_length'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_image_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Image URL of the product"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_stock_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Stock of product /number/"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_currency_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Currency"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Value:"); ?>
			</div>
			<div class="span8">
				<input type="text" name="currency_value" size="50" value="<?php echo @$field_config['currency_value'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_category_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Category"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div id="category1_delimiter_row" class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("VirtueMart category delimiter"); ?>
			</div>
			<div class="span8">
				<input type="text" name="category1_delimiter" size="5" value="<?php echo @$field_config['delimiter'];?>"/><br />
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_category_alternative_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Alternative category"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_delivery_date_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Date of delivery"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span11">
				<i><?php echo JText::_('The value of delivery date must be set in the Eshop setting'); ?></i>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_vat_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("VAT of the price"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Choose VAT format:"); ?>
			</div>
			<div class="span8">
				<select name="vat_format">
					<option value="percent" <?php echo @$field_config['format']=='percent'?'selected':'';?>><?php echo JText::_("Number of percent /eg. 9.75/"); ?></option>
					<option value="decimal" <?php echo @$field_config['format']=='decimal'?'selected':'';?>><?php echo JText::_("Decimal number /eg. 0.0975/"); ?></option>
					<option value="decimalext" <?php echo @$field_config['format']=='decimalext'?'selected':'';?>><?php echo JText::_("Decimal number + one /eg. 1.0975/"); ?></option>
				</select>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_price_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Final price /without VAT/"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Round to:"); ?>
			</div>
			<div class="span8">
				<select name="price_decimals">
					<option value="0" <?php echo @$field_config['decimals']=='0'?'selected':'';?>>0</option>
					<option value="1" <?php echo @$field_config['decimals']=='1'?'selected':'';?>>1</option>
					<option value="2" <?php echo @$field_config['decimals']=='2'?'selected':'';?>>2</option>
					<option value="3" <?php echo @$field_config['decimals']=='3'?'selected':'';?>>3</option>
					<option value="4" <?php echo @$field_config['decimals']=='4'?'selected':'';?>>4</option>
				</select>
				<?php echo JText::_("decimals"); ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Decimal symbol:"); ?>
			</div>
			<div class="span8">
				<select name="price_decimal_symbol">
					<option value="." <?php echo @$field_config['decimal_symbol']=='.'?'selected':'';?>>.</option>
					<option value="," <?php echo @$field_config['decimal_symbol']==','?'selected':'';?>>,</option>
				</select>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Currency code:"); ?>
			</div>
			<div class="span8">
				<select name="price_currency">
					<option value="0" <?php echo @$field_config['currency']=='0'?'selected':'';?>><?php echo JText::_('JNO');?></option>
					<option value="1" <?php echo @$field_config['currency']=='1'?'selected':'';?>><?php echo JText::_('JYES');?></option>
				</select>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_price_vat_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Final price /with VAT/"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Round to:"); ?>
			</div>
			<div class="span8">
				<select name="price_vat_decimals">
					<option value="0" <?php echo @$field_config['decimals']=='0'?'selected':'';?>>0</option>
					<option value="1" <?php echo @$field_config['decimals']=='1'?'selected':'';?>>1</option>
					<option value="2" <?php echo @$field_config['decimals']=='2'?'selected':'';?>>2</option>
					<option value="3" <?php echo @$field_config['decimals']=='3'?'selected':'';?>>3</option>
					<option value="4" <?php echo @$field_config['decimals']=='4'?'selected':'';?>>4</option>
				</select>
				<?php echo JText::_("decimals"); ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Decimal symbol:"); ?>
			</div>
			<div class="span8">
				<select name="price_vat_decimal_symbol">
					<option value="." <?php echo @$field_config['decimal_symbol']=='.'?'selected':'';?>>.</option>
					<option value="," <?php echo @$field_config['decimal_symbol']==','?'selected':'';?>>,</option>
				</select>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Currency code:"); ?>
			</div>
			<div class="span8">
				<select name="price_vat_currency">
					<option value="0" <?php echo @$field_config['currency']=='0'?'selected':'';?>><?php echo JText::_('JNO');?></option>
					<option value="1" <?php echo @$field_config['currency']=='1'?'selected':'';?>><?php echo JText::_('JYES');?></option>
				</select>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_orig_price_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Basic price before discount /without VAT/"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Round to:"); ?>
			</div>
			<div class="span8">
				<select name="orig_price_decimals">
					<option value="0" <?php echo @$field_config['decimals']=='0'?'selected':'';?>>0</option>
					<option value="1" <?php echo @$field_config['decimals']=='1'?'selected':'';?>>1</option>
					<option value="2" <?php echo @$field_config['decimals']=='2'?'selected':'';?>>2</option>
					<option value="3" <?php echo @$field_config['decimals']=='3'?'selected':'';?>>3</option>
					<option value="4" <?php echo @$field_config['decimals']=='4'?'selected':'';?>>4</option>
				</select>
				<?php echo JText::_("decimals"); ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Decimal symbol:"); ?>
			</div>
			<div class="span8">
				<select name="orig_price_decimal_symbol">
					<option value="." <?php echo @$field_config['decimal_symbol']=='.'?'selected':'';?>>.</option>
					<option value="," <?php echo @$field_config['decimal_symbol']==','?'selected':'';?>>,</option>
				</select>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Currency code:"); ?>
			</div>
			<div class="span8">
				<select name="orig_price_currency">
					<option value="0" <?php echo @$field_config['currency']=='0'?'selected':'';?>><?php echo JText::_('JNO');?></option>
					<option value="1" <?php echo @$field_config['currency']=='1'?'selected':'';?>><?php echo JText::_('JYES');?></option>
				</select>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span11">
				<i><?php echo JText::_('If basic price equals final price, this tag is NOT exported'); ?></i>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_orig_price_vat_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Basic price before discount /with VAT/"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Round to:"); ?>
			</div>
			<div class="span8">
				<select name="orig_price_vat_decimals">
					<option value="0" <?php echo @$field_config['decimals']=='0'?'selected':'';?>>0</option>
					<option value="1" <?php echo @$field_config['decimals']=='1'?'selected':'';?>>1</option>
					<option value="2" <?php echo @$field_config['decimals']=='2'?'selected':'';?>>2</option>
					<option value="3" <?php echo @$field_config['decimals']=='3'?'selected':'';?>>3</option>
					<option value="4" <?php echo @$field_config['decimals']=='4'?'selected':'';?>>4</option>
				</select>
				<?php echo JText::_("decimals"); ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Decimal symbol:"); ?>
			</div>
			<div class="span8">
				<select name="orig_price_vat_decimal_symbol">
					<option value="." <?php echo @$field_config['decimal_symbol']=='.'?'selected':'';?>>.</option>
					<option value="," <?php echo @$field_config['decimal_symbol']==','?'selected':'';?>>,</option>
				</select>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Currency code:"); ?>
			</div>
			<div class="span8">
				<select name="orig_price_vat_currency">
					<option value="0" <?php echo @$field_config['currency']=='0'?'selected':'';?>><?php echo JText::_('JNO');?></option>
					<option value="1" <?php echo @$field_config['currency']=='1'?'selected':'';?>><?php echo JText::_('JYES');?></option>
				</select>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span11">
				<i><?php echo JText::_('If basic price equals final price, this tag is NOT exported'); ?></i>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_weight_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Weight of the product"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span11">
				<i><?php echo JText::_('Exported only if weight is > 0'); ?></i>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_manufacturer_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Manufacturer"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_ean_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("EAN-13/UPC code"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_mpn_html($key, $field_config=array()) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']=strtoupper($key); ?>
<div id="<?php echo $key;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("MPN /Manufacturer part number/"); ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="<?php echo $key;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php function get_extended_html($key, $field_config=array(), $no) { ?>
<?php if (!@$field_config['fieldname']) $field_config['fieldname']="EXTENDED $no"; ?>
<div id="extended<?php echo $no;?>" class="adminform row-fluid">
	<div class="row-fluid header-line">
		<div class="span1">
			<div class="handle"></div>
			<div class="arrow" rel="<?php echo $key;?>"></div>
		</div>
		<div class="span3 text">
			<strong><?php echo JText::_("Extended user field").' '.$no; ?></strong>
		</div>
		<div class="span6 text">
			<span class="field_name"><?php echo $field_config['fieldname'];?></span>
		</div>
		<div class="span2 right text">
			<a href="#" class="delete" onClick="delete_field('<?php echo $key; ?>');"><div style="float: right;"><?php echo JText::_('Delete');?> <div style="float: right;"><img src="components/com_productxport/images/publish_x.png" /></div></div></a>
		</div>
	</div>
	<div class="detail row-fluid">
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Field name").':'; ?> 
			</div>
			<div class="span8">
				<input type="text" name="extended<?php echo $no;?>" size="50" value="<?php echo $field_config['fieldname'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Value:"); ?>
			</div>
			<div class="span8" class="extended_line">
				<input type="text" class="extended_value " id="extended<?php echo $no;?>_value" name="extended<?php echo $no;?>_value" size="50" value="<?php echo @$field_config['extended_value'];?>"/>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span11">
				<i><?php echo JText::_('USE THIS FIELD FOR YOUR PURPOSE. THIS FIELD IS STATIC /THE SAME FOR ALL THE PRODUCTS/');?></i>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>		
			<div class="span3">
				<?php echo JText::_("Output as CDATA"); ?>
			</div>
			<div class="span8">
				<input type="checkbox" id="jform_<?php echo $key;?>_cdata" name="<?php echo $key;?>_cdata" value="1" <?php echo @$field_config['cdata']=='1'?'checked':''; ?> />
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script language="javascript" type="text/javascript">
	function validate_code(form) {
		var reg = /^([A-Za-z0-9_\-\.])+$/;
		var address = form.code.value;
		if(reg.test(address)) {
			return true;
		} else {
			return false;
		}
	}
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}

		// do field validation
		if (form.code.value == ""){
			alert( "<?php echo JText::_( 'You must define server code' ); ?>" );
		} else if (!validate_code(form)) {
			alert('<?php echo JText::_('Invalid server code / domain name'); ?>');
		} else {
			submitform( pressbutton );
		}
	}
</script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
	<?php
	echo JHtml::_('bootstrap.startTabSet', 'config-pane'.$time, array('active' => 'tab1'));
	echo JHtml::_('bootstrap.addTab', 'config-pane'.$time, 'tab1', JText :: _('Basic setting'));
	?>
	<fieldset class="adminform">
		<legend><?php echo JText::_('Basic setting'); ?></legend>
		<table class="admintable" width="100%">

		<tr>
			<td width="100" align="right" class="key">
				<label for="code">
					<?php echo JText::_('Server code / domain'); ?>:
				</label>
			</td>
			<td>
				<input type="text" name="code" size="40" value="<?php echo $this->code; ?>" <?php echo $this->code?'READONLY':''; ?> />
			</td>
			<td width="50%">
				<?php echo JText::_('Server code - enter the code for this server. Will be used  in URL provided to server.'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<label for="file_format">
					<?php echo JText::_('Export type - file format'); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->format; ?>
			</td>
			<td width="50%">
				<?php echo JText::_('Choose the type of export. Selected format defines the plugin which is used to generate data.'); ?>	
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Export URL'); ?>:
			</td>
			<td colspan="2">
				<?php echo JText::_('Use any of the following URLs, which works'); ?></br />
				<input type="text" style="width:100%;" value="<?php 
					//echo JURI::root().JRoute::_('index.php?option=com_productxport&view=server&code='.$this->code.'&tmpl=component'); 
					echo JURI::root().'index.php?option=com_productxport&view=server&code='.$this->code.'&tmpl=component'; 
					?>" />
					<br />
				<input type="text" style="width:100%;" value="<?php 
					echo JURI::root().JRoute::_('component/productxport/server/'.$this->code.'/'); 
					?>" />

			</td>
		</tr>

	</table>
	</fieldset>
<?php
	echo JHtml::_('bootstrap.endTab');
	echo JHtml::_('bootstrap.addTab', 'config-pane'.$time, 'tab2', JText :: _('Filetype setting'));
 ?>

<div class="clr"></div>
<?php
switch (@$this->config['format']) {
	case 'xml':
		?>
	<fieldset class="adminform">
		<legend><?php echo JText::_('XML setting'); ?></legend>
		<table class="admintable" width="100%" border="0">

		<tr>
			<td width="100" align="right" class="key">
				<label for="xml_header">
					<?php echo JText::_('XML header line'); ?>:
				</label>
			</td>
			<td>
				<textarea name="xml_header" rows="2" cols="40"><?php echo @$this->xml_params->xml_header; ?></textarea>
			</td>
			<td width="50%">
				<?php echo JText::_('This is the XML header of the file. <br /><b>Do not change, until you know, why</b>'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<label for="xml_footer">
					<?php echo JText::_('XML footer line'); ?>:
				</label>
			</td>
			<td>
				<textarea name="xml_footer" rows="2" cols="40"><?php echo @$this->xml_params->xml_footer; ?></textarea>
			</td>
			<td width="50%">
				<?php echo JText::_('This is the XML footer of the file.'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<label for="xml_root_element">
					<?php echo JText::_('Root XML element'); ?>:
				</label>
			</td>
			<td>
				<input type="text" name="xml_root_element" size="40" value="<?php echo @$this->xml_params->xml_root_element; ?>" />
			</td>
			<td width="50%">
				<?php echo JText::_('Root element of the exported XML file'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<label for="xml_product_element">
					<?php echo JText::_('Product/item XML element'); ?>:
				</label>
			</td>
			<td>
				<input type="text" name="xml_product_element" size="40" value="<?php echo @$this->xml_params->xml_product_element; ?>" />
			</td>
			<td width="50%">
				<?php echo JText::_('Product/item element of the exported XML file'); ?>	
			</td>
		</tr>

	</table>
	</fieldset>
		<?php
		break;
	case 'csv':
		?>
	<fieldset class="adminform">
		<legend><?php echo JText::_('CSV setting'); ?></legend>
		<table class="admintable" width="100%" border="0">

		<tr>
			<td width="100" align="right" class="key">
				<label for="xml_footer">
					<?php echo JText::_('Header line'); ?>:
				</label>
			</td>
			<td>
				<input type="text" name="header_line" size="100" value="<?php echo @$this->csv_params->header_line; ?>" />
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<label for="xml_footer">
					<?php echo JText::_('CSV line format'); ?>:
				</label>
			</td>
			<td>
				<input type="text" name="line_format" size="100" value="<?php echo str_replace('"', '&quot;', @$this->csv_params->line_format); ?>" />
			</td>
			<td width="50%">
				<?php echo JText::_('Line format - description'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key" valign="top">
				<label for="line_delimiter">
					<?php echo JText::_('Line delimiter'); ?>:
				</label>
			</td>
			<td valign="top">
				<input type="text" name="line_delimiter" size="10" value="<?php echo @$this->csv_params->line_delimiter; ?>" />
			</td>
			<td width="50%" rowspan="3">
				<?php echo JText::_('You can use conventional characters or the following strings as delimiters:<br /><pre>\t as tab key<br />').'\n '.JText::_('as line feed /LF/<br />\r as /CR - carriage return/</pre>'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key" valign="top">
				<label for="line_delimiter">
					<?php echo JText::_('Replacements'); ?>:
				</label>
			</td>
			<td valign="top">
				<textarea name="replacements" rows="3" cols="20"><?php echo @$this->csv_params->replacements; ?></textarea>
			</td>
			<td width="50%" rowspan="3">
				<?php echo JText::_('Replacements - description'); ?>	
			</td>
		</tr>

	</table>
	</fieldset>
		<?php
		break;
}


?>
<div class="clr"></div>

<?php
	echo JHtml::_('bootstrap.endTab');
 	echo JHtml::_('bootstrap.addTab', 'config-pane'.$time, 'tab3', JText :: _('Fields setting'));
 ?>
	<fieldset class="adminform">
		<legend><?php echo JText::_('Fields setting'); ?></legend>
		<div style="display: none;"><input type="text" id="ordering" name="ordering" size="40" /></div>
		<div id="tag_list_container">
		<div class="row-fluid top-buttons">
			<div class="span1"></div>
			<div class="span3">
				<a href="#" onClick=openAll();><?php echo JText::_('Open all');?></a>
				<a href="#" onClick=closeAll();><?php echo JText::_('Close all');?></a>
			</div>
			<div class="span8" style="text-align: right;">
			<select id="field_type">
				<option value=""><?php echo JText::_('-- Choose field --'); ?></option>
				<option value="product_id" <?php echo array_key_exists('product_id', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Product ID'); ?></option>
				<option value="product_sku" <?php echo array_key_exists('product_sku', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Product code /product_sku/'); ?></option>
				<option value="url" <?php echo array_key_exists('url', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Product URL'); ?></option>
				<option value="name" <?php echo array_key_exists('name', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Product name'); ?></option>
				<option value="short_description" <?php echo array_key_exists('short_description', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Short description'); ?></option>
				<option value="long_description" <?php echo array_key_exists('long_description', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Long description'); ?></option>
				<option value="image" <?php echo array_key_exists('image', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Product image /URL/'); ?></option>
				<option value="stock" <?php echo array_key_exists('stock', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Stock of product'); ?></option>
				<option value="currency" <?php echo array_key_exists('currency', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Currency of product price'); ?></option>
				<option value="category" <?php echo array_key_exists('category', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Category'); ?></option>
				<option value="category_alternative" <?php echo array_key_exists('category_alternative', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Category alternative'); ?></option>
				<option value="delivery_date" <?php echo array_key_exists('delivery_date', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Delivery date /number of days or exact day/'); ?></option>
				<option value="vat" <?php echo array_key_exists('vat', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('VAT'); ?></option>
				<option value="price" <?php echo array_key_exists('price', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Product price /without VAT)'); ?></option>
				<option value="price_vat" <?php echo array_key_exists('price_vat', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Product price /with VAT/'); ?></option>
				<option value="orig_price" <?php echo array_key_exists('orig_price', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Basic price before discount /without VAT/'); ?></option>
				<option value="orig_price_vat" <?php echo array_key_exists('orig_price_vat', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Basic price before discount /with VAT/'); ?></option>
				<option value="weight" <?php echo array_key_exists('weight', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Weight of product'); ?></option>
				<option value="manufacturer" <?php echo array_key_exists('manufacturer', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('Manufacturer'); ?></option>
				<option value="ean" <?php echo array_key_exists('ean', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('EAN/UPC/GTIN code of the product'); ?></option>
				<option value="mpn" <?php echo array_key_exists('mpn', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('MPN /Manufacturer part number/'); ?></option>
				<option value="extended1" <?php echo array_key_exists('extended1', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('User extended field').' 1'; ?></option>
				<option value="extended2" <?php echo array_key_exists('extended2', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('User extended field').' 2'; ?></option>
				<option value="extended3" <?php echo array_key_exists('extended3', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('User extended field').' 3'; ?></option>
				<option value="extended4" <?php echo array_key_exists('extended4', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('User extended field').' 4'; ?></option>
				<option value="extended5" <?php echo array_key_exists('extended5', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('User extended field').' 5'; ?></option>
				<option value="extended6" <?php echo array_key_exists('extended6', $this->config['fields'])?'disabled="disabled"':''; ?>><?php echo JText::_('User extended field').' 6'; ?></option>
			</select>
			<input id="addDivBeginning" type="button" value="<?php echo JText::_('Add field to beginning'); ?>" />
			<input id="addDivEnd" type="button" value="<?php echo JText::_('Add field to end'); ?>" />

			</div>
		</div>

		<ul id="tag_list" >
<?php
	if (is_array($this->config['fields'])) {
		foreach ($this->config['fields'] as $key=>$field_config) {
			switch ($key) {
				case 'extended1':
				case 'extended2':
				case 'extended3':
				case 'extended4':
				case 'extended5':
				case 'extended6':
						echo "<li id=\"li_".$key."\">\n";
						echo call_user_func('get_extended_html', $key, $field_config, substr($key, -1));
						echo "</li>";
				default:
					if (function_exists('get_'.$key.'_html')) {
						echo "<li id=\"li_".$key."\">\n";
						echo call_user_func('get_'.$key.'_html', $key, $field_config);
						echo "</li>";
					}
			}
		}
	}
?>
		</ul>
		</div>
	</fieldset>
<?php
 	echo JHtml::_('bootstrap.endTab');
	echo JHtml::_('bootstrap.addTab', 'config-pane'.$time, 'tab4', JText :: _('Extended setting'));
 ?>

	<fieldset class="adminform">
		<legend><?php echo JText::_('Extended setting'); ?></legend>
		<table class="admintable" width="100%">

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_("Debug mode"); ?>
			</td>

			<td valign="top" class="el_list">
				<fieldset id="jform_debug_mode" class="radio">
					<input type="radio" id="jform_debug_mode1" name="debug_mode" value="1" <?php echo @$this->config['debug_mode']=='1'?'checked':''; ?> />
					<label for="jform_debug_mode0"><?php echo JText::_( 'JYES' ); ?></label>
					<input type="radio" id="jform_debug_mode0" name="debug_mode" value="0" <?php echo @$this->config['debug_mode']=='0'?'checked':''; ?> />
					<label for="jform_debug_mode1"><?php echo JText::_( 'JNO' ); ?></label>
				</fieldset>
			</td>
		</tr>
	</table>
	</fieldset>
<div class="clr"></div>
<?php
	echo JHtml::_('bootstrap.endTab');
 ?>

	<input type="hidden" id="current_tab" name="current_tab">
	<input type="hidden" name="type" value="server" />
	<input type="hidden" name="option" value="com_productxport" />
	<input type="hidden" name="view" value="<?php echo $this->_name; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
<script>
function li_ordering(ul) {
	ordering=document.getElementById('ordering');
	ordering.value='';
	var li = ul.querySelectorAll ("li");
	for (var i = 0; i < li.length; i++) {
		ordering.value+=li[i].id.substr(3)+',';
	}
}

function delete_field(key) {
	var version = MooTools.version.substr(0,3);
	for (var i = 0; i < document.getElementById('field_type').options.length; i++) {
		if(document.getElementById('field_type').options[i].value == key) {
			document.getElementById('field_type').options[i].disabled=false;
		}
	}
  	var li = $('li_'+key);
	if (version > 1.1) {
		li.dispose();
	} else {
		li.remove();
	}

	li_ordering(document.getElementById('tag_list'));
	return false;
}
function addDiv(position) {
	var tag_code = document.getElementById('field_type').options[document.getElementById('field_type').selectedIndex].value;
	document.getElementById('field_type').options[document.getElementById('field_type').selectedIndex].disabled=true;
	if (tag_code) {
		tag_el = new Element('li', {
			'id' : 'li_'+tag_code
		});
		tag_el.inject(tag_list, position);
		var clone = document.id(tag_code).clone().inject(tag_el);

		new Sortables(document.id('tag_list'), {
			constrain   : true,
			clone      : true,
			handles   : $$('div.handle'),
			handle   : 'div.handle',
			opacity: 0,
			onComplete: function(element, ghost){
				element.setStyle('opacity', 1);
				//ghost.remove();
				//this.trash.remove();
				li_ordering(document.id('tag_list'));
			},
			onDragComplete: function(element, ghost){
				element.setStyle('opacity', 1);
				ghost.remove();
				this.trash.remove();
				li_ordering(document.id('tag_list'));
			}
		});
		li_ordering(document.id('tag_list'));
		tag_el.getElements('.detail').setStyle('display', 'block');
		tag_el.getElements('.arrow').addClass('open');
		tag_el.getElements('select').setProperty('disabled', false);
		tag_el.getElements('input').setProperty('disabled', false);
		jQuery('#li_'+tag_code+' .arrow').click(function() {
			if (jQuery(this).hasClass('open')) {
				jQuery(this).removeClass('open');
				jQuery('#li_'+jQuery(this).attr('rel')+' .detail').slideUp();
			} else {
				jQuery(this).addClass('open');
				jQuery('#li_'+jQuery(this).attr('rel')+' .detail').slideDown();
			}
		});
	}
}
function openAll() {
	jQuery('#tag_list .arrow').addClass('open');
	jQuery('#tag_list .detail').slideDown();
	return false;
}
function closeAll() {
	jQuery('#tag_list .arrow.open').removeClass('open');
	jQuery('#tag_list .detail').slideUp();
	return false;
}
window.addEvent('domready', function() {

	var tag_list = $('tag_list');

	// Add new divs
	document.id('addDivEnd').addEvent('click', function(){
		addDiv('bottom');
	});
	document.id('addDivBeginning').addEvent('click', function(){
		addDiv('top');
	});
	new Sortables(document.id('tag_list'), {
		constrain   : true,
		clone      : true,
		handles   : $$('div.handle'),
		handle   : 'div.handle',
		opacity: 0,
		onComplete: function(element, ghost){
			element.setStyle('opacity', 1);
			//ghost.remove();
			//this.trash.remove();
			li_ordering(document.id('tag_list'));
		},
		onDragComplete: function(element, ghost){
			element.setStyle('opacity', 1);
			ghost.remove();
			this.trash.remove();
			li_ordering(document.id('tag_list'));
		}
	});
	li_ordering(document.id('tag_list'));
	$('hidden_fields').getElements('input').setProperty('disabled', true);
	$('hidden_fields').getElements('select').setProperty('disabled', true);

	window.addEvent('keyup',function(event) {
    	if(event.code == 17) isCtrl=false;
	});
	window.addEvent('keydown',function(event) {
    	if(event.code == 17) isCtrl=true;
    	if(event.code == 83 && isCtrl == true) {
			isCtrl=false
			submitbutton('apply');
        	return false;
    	}
		return true;
	});
	$$('textarea').addEvent('keydown', function(event){
		if (event.code==9 && !event.shift) {
      		var start = this.selectionStart;
      		var end = this.selectionEnd;
      		this.value = this.value.substring(0, start)
                  + "\t"
                  + this.value.substring(end);
			this.selectionStart = this.selectionEnd = start + 1;				            	
          	if(event.preventDefault) {
                event.preventDefault();
  	        }			
			return false;
		}
	});

});
</script>
<?php
$document->addScriptDeclaration("
jQuery( document ).ready(function( $ ) {
	$('#config-pane".$time."Tabs a').click(function (e)
	{
		e.preventDefault();
		var hash=$(this).prop('href').substr($(this).prop('href').indexOf('#'))+'.';
		window.location.hash=hash;
		$('#current_tab').val(hash);
	});
	var hash=window.location.hash.replace('.','');
	if ($(hash).length && window.location.hash.substr(-1)=='.') {
		if ($(hash).hasClass('tab-pane')) {
			$('#current_tab').val(window.location.hash);
			$('#config-pane".$time."Tabs li').removeClass('active');
			$('#config-pane".$time."Content .tab-pane.active').removeClass('active')
			$(hash).addClass('active');
			$('#config-pane".$time."Tabs a[href='+hash+']').parent().addClass('active');
			setTimeout('autosize.update(document.querySelectorAll(\'textarea\'));', 50);
		}
	}
	$('.detail').slideUp(0);
	$('#tag_list .arrow').click(function() {
		if ($(this).hasClass('open')) {
			$(this).removeClass('open');
			$('#'+$(this).attr('rel')+' .detail').slideUp();
		} else {
			$(this).addClass('open');
			$('#'+$(this).attr('rel')+' .detail').slideDown();
		}
	});
});
");
?>
<div style="display: none;" id="hidden_fields">
<?php
echo get_product_id_html('product_id');
echo get_product_sku_html('product_sku');
echo get_url_html('url');
echo get_name_html('name');
echo get_short_description_html('short_description');
echo get_long_description_html('long_description');
echo get_image_html('image');
echo get_stock_html('stock');
echo get_currency_html('currency');
echo get_category_html('category');
echo get_category_alternative_html('category_alternative');
echo get_delivery_date_html('delivery_date');
echo get_vat_html('vat');
echo get_price_html('price');
echo get_price_vat_html('price_vat');
echo get_orig_price_html('orig_price');
echo get_orig_price_vat_html('orig_price_vat');
echo get_weight_html('weight');
echo get_manufacturer_html('manufacturer');
echo get_ean_html('ean');
echo get_mpn_html('mpn');
echo get_extended_html('extended1', array(), 1);
echo get_extended_html('extended2', array(), 2);
echo get_extended_html('extended3', array(), 3);
echo get_extended_html('extended4', array(), 4);
echo get_extended_html('extended5', array(), 5);
echo get_extended_html('extended6', array(), 6);
?>
</div>
