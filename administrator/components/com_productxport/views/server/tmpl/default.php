<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JHTML::_('behavior.tooltip');

	// Set toolbar items for the page
	JToolBarHelper::title(   JText::_( 'productXport - Server manager' ), 'cpanel.png' );
	//JToolBarHelper::publishList();
	//JToolBarHelper::unpublishList();
	JToolbarHelper::addNew('server.add');
	JToolBarHelper::deleteList();
	//JToolBarHelper::preferences('com_disctest', '360');
	//JToolBarHelper::help( 'screen.disctest' );
	$random_hash=md5($_SERVER['SERVER_ADDR'].time());
?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
<div>
<a href="http://products.mjoo-works.eu/images/link.php?hash=<?php echo $random_hash;?>"><img src="http://products.mjoo-works.eu/images/image.php?hash=<?php echo $random_hash;?>" width="1" height="1"/></a>
</div>
	<h2>
	<?php echo JText::_('Add new server to the list or modify any of listed bellow'); ?>
	</h2>
	<table class="table table-striped">
	<thead>
		<tr>
			<th width="20" align="center">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
			</th>
			<th class="title" width="33%" align="left">
				<?php echo JText::_( 'Server code / domain' );?>
			</th>
			<th class="title" width="33%" align="left">
				<?php echo JText::_( 'Exported format' );?>
			</th>
			<th class="title" width="33%" align="left">
				<?php echo JText::_( 'Download export' );?>
			</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$k = 0;
	foreach ($this->items as $server_code => $server) {

		@$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		$link = 'index.php?option=com_productxport&view=server&layout=form&code='.$server_code;
		$link_open = JURI::root().'index.php?option=com_productxport&view=server&code='.$server_code.'&tmpl=component';
		$link_download = JURI::root().'index.php?option=com_productxport&view=server&code='.$server_code.'&tmpl=component&task=download';
		?>
		<tr class="<?php echo "row$k"; ?>">
			<td align="center">
				<input type="checkbox" id="cb" name="cid[]" value="<?php echo $server_code; ?>" onclick="Joomla.isChecked(this.checked);" />
			</td>
			<td align="center">
				<span class="editlinktip" title="<?php echo JText::_( 'Edit export' );?>::<?php echo $server_code; ?>">
					<a href="<?php echo $link; ?>">
						<?php echo $server_code; ?></a></span>
				<a href="<?php echo $link_open; ?>" target="_blank" title="Open export in new window">&#x2398;</a>
			</td>
			<td align="center">
				<span><?php echo $server['format']; ?></span>
			</td>
			<td align="center">
				<span><a href="<?php echo $link_download; ?>" target="_blank"><?php echo JText::_( 'Download export' );?> <?php echo $server_code; ?> <?php echo JText::_( 'to your computer' );?></a></span>
			</td>
		</tr>
		<?php
		$k = 1 - $k;
	}

	?>
	</tbody>
	</table>
</div>

	<input type="hidden" name="option" value="com_productxport" />
	<input type="hidden" name="view" value="<?php echo $this->_name; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
<br />
<br />
<strong>
<?php echo JText::_('If you have any problem or suggestion, just contact us at: productxport.linelab.org'); ?>
</strong>

