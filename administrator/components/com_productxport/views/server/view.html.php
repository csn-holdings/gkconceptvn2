<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the productXport component
 *
 * @static
 * @package		Joomla
 * @subpackage	productXport
 * @since 1.0
 */
class ProductxportViewServer extends JViewLegacy
{
	function display($tpl = null)
	{
		$db		= JFactory::getDBO();
		$uri	= JFactory::getURI();
		$input = JFactory::getApplication()->input;
		
		switch ($this->getLayout()) {
			case 'form':
				JHtml::_('behavior.framework', 'More');

				// Get data from the model
				$item		= $this->get( 'Info');
				$config		= $this->get( 'Config');
				$code		= $input->get('code');
				$cid = $input->get( 'cid', array(), 'array' );
				if (count($cid)>0) {
					$code=$cid[0];
				}

				$document=JFactory::getDocument();
				$document->addScript("components/com_productxport/js/autosize.min.js");
				$document->addScriptDeclaration("
				window.addEvent('domready', function(){
					autosize(document.querySelectorAll('textarea'));
					\$\$('.nav-tabs').addEvent('click', function(){
						setTimeout('autosize.update(document.querySelectorAll(\'textarea\'));', 50);
					}); 					
				});");

				// Get data from the model
				//$items		= & $this->get( 'Plugins');

				// assemble menu items to the array
				$format_options 	= array();
					
				
				$format_options[] = JHTML::_('select.option',  '', JText::_('-- Choose exported file format --') );
				$format_options[] = JHTML::_('select.option',  'xml', JText::_('XML') );
				$format_options[] = JHTML::_('select.option',  'csv', JText::_('CSV') );
//				$format_options[] = JHTML::_('select.option',  'googlebasexml', JText::_('Google base products XML (RSS 2.0)') );

				@$format = JHTML::_('select.genericlist',   $format_options, 'file_format', 'class="inputbox" ', 'value', 'text', $config['format'] );

				$xml_params = new JObject;
				if (@$config['format_params']['xml_header']) {
					@$xml_params->xml_header=htmlspecialchars($config['format_params']['xml_header']);
				} else {
					@$xml_params->xml_header=htmlspecialchars('<?xml version="1.0" encoding="utf-8"?>');
				}
				@$xml_params->xml_footer=htmlspecialchars($config['format_params']['xml_footer']);
				if (@$config['format_params']['xml_root_element']) {
					$xml_params->xml_root_element=htmlspecialchars($config['format_params']['xml_root_element']);
//				} else {
//					$xml_params->xml_root_element='SHOP';
				}
				if (@$config['format_params']['xml_product_element']) {
					@$xml_params->xml_product_element=htmlspecialchars($config['format_params']['xml_product_element']);
				} else {
					@$xml_params->xml_product_element='SHOPITEM';
				}

				$csv_params = new JObject;
				if (@$config['format_params']['line_format']) {
					@$csv_params->line_format=htmlspecialchars($config['format_params']['line_format']);
				} else {
					@$csv_params->line_format='{PRODUCT_ID},{NAME},{PRICE_VAT}';
				}
				if (@$config['format_params']['line_delimiter']) {
					@$csv_params->line_delimiter=htmlspecialchars($config['format_params']['line_delimiter']);
				} else {
					@$csv_params->line_delimiter='\n';
				}
				@$csv_params->header_line=htmlspecialchars($config['format_params']['header_line']);
				@$csv_params->replacements=htmlspecialchars($config['format_params']['replacements']);

				$this->assignRef('code',		$code);
				$this->assignRef('format',		$format);
				$this->assignRef('lists',		$lists);
				$this->assignRef('config',		$config);

				$this->assignRef('xml_params',		$xml_params);
				$this->assignRef('csv_params',		$csv_params);


				break;
			default:

				// Get data from the model
				$items		= $this->get( 'Items');
				$this->assignRef('items',		$items);
				break;
		}
		$this->assignRef('setting',		$setting);
		parent::display($tpl);
	}
}
