<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

/**
 * HTML View class for the Plugins component
 *
 * @static
 * @package		Joomla
 * @subpackage	Plugins
 * @since 1.0
 */
class ProductxportViewLog extends JViewLegacy
{
	function display( $tpl = null )
	{
		global $framework;
		$db			= JFactory::getDBO();

		$records = PX::getLogs(100);

		$this->assignRef('records',		$records);
		
		parent::display($tpl);
	}
}
