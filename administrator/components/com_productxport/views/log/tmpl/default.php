<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JToolBarHelper::title( JText::_( 'productXport - Log' ), 'cpanel.png' );
?>

<table class="adminform">
	<thead>
		<tr class="row1">
			<th>ID</th>
			<th>Server</th>
			<th>IP address</th>
			<th>HTTP User Agent</th>
			<th>#</th>
			<th>Logtime</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$k = 0;
		/*
		foreach ($this->records as $record) {
		?>
		<tr class="<?php echo "row$k"; ?>">
			<td><?php echo $record->logtime; ?></td>
			<td><?php echo $record->server; ?></td>
			<td><?php echo $record->plugin; ?></td>
			<td><?php echo $record->remote_addr; ?></td>
			<td><?php echo $record->user_agent; ?></td>
			<td><?php echo $record->items; ?></td>
		</tr>
		<?
			$k = 1 - $k;
		}
		*/
		$this->records = str_replace("\n", "</td></tr><tr><td>", $this->records);
		$this->records = str_replace("\t", "</td><td>", $this->records);
		echo "<tr><td>";
		echo $this->records;
		echo "</td></tr>";
		?>
	</tbody>
</table>

