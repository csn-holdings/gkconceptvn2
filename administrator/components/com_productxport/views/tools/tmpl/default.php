<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JToolBarHelper::title( JText::_( 'productXport Joomla installation checker / hosting checker' ), 'cpanel.png' );

	$time=time();

	echo JHtml::_('bootstrap.startTabSet', 'config-pane'.$time, array('active' => 'tab1'));
	echo JHtml::_('bootstrap.addTab', 'config-pane'.$time, 'tab1', JText :: _('Server info'));
?>
	<fieldset class="adminform">
		<legend><?php echo JText::_('Info of your server'); ?></legend>
		<table class="admintable" width="100%">

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Memory limit'); ?>:
			</td>
			<td>
				<?php echo ini_get('memory_limit'); ?>
			</td>
			<td width="50%">
				<?php echo JText::_('Value of variable'); ?>: memory_limit
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Max execution time'); ?>:
			</td>
			<td>
				<?php echo ini_get('max_execution_time'); ?>s
			</td>
			<td width="50%">
				<?php echo JText::_('Value of variable'); ?>: max_execution_time
			</td>
		</tr>
		</table>

	</fieldset>

<?php	
	echo JHtml::_('bootstrap.endTab');
	echo JHtml::_('bootstrap.addTab', 'config-pane'.$time, 'tab2', JText :: _('Installation checker'));

?>
<table class="adminform">
<?php foreach ($this->visibleChecker as $key => $ch) { ?>
    <tr>
        <td valign="top" width="300">
			<strong><?php echo $key; ?></strong>
        </td>
		<?php if ($ch['result']) : ?>
			<td valign="top"><img src="components/com_productxport/images/tick.png" /></td>
			<td valign="top"><font color="green"><?php echo $ch['desc']; ?></font></td>
		<?php else: ?>
			<td valign="top"><img src="components/com_productxport/images/publish_x.png" /></td>
			<td valign="top"><font color="red"><?php echo $ch['desc']; ?></font></td>
		<?php endif; ?>
    </tr>
<?php } ?>
<?php foreach ($this->hiddenChecker as $key => $ch) { ?>
	<?php if ($ch['result']===false) : ?>
    <tr>
        <td valign="top" width="300">
			<strong><?php echo $key; ?></strong>
        </td>
		<?php if ($ch['result']) : ?>
			<td valign="top"><img src="images/tick.png" /></td>
			<td valign="top"><font color="green"><?php echo $ch['desc']; ?></font></td>
		<?php else: ?>
			<td valign="top"><img src="images/publish_x.png" /></td>
			<td valign="top"><font color="red"><?php echo $ch['desc']; ?></font></td>
		<?php endif; ?>
    </tr>
	<?php endif; ?>
<?php } ?>
</table>

<?php echo JText::_('Cron info - checker text'); ?>
<?php
	echo JHtml::_('bootstrap.endTab');
