<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

/**
 * HTML View class for the Plugins component
 *
 * @static
 * @package		Joomla
 * @subpackage	productXport
 * @since 1.0
 */
class ProductxportViewTools extends JViewLegacy
{
	function display( $tpl = null )
	{
		$db			= JFactory::getDBO();

		$visibleChecker		= $this->get( 'VisibleChecker');
		$hiddenChecker		= $this->get( 'HiddenChecker');

		$this->assignRef('visibleChecker',		$visibleChecker);
		$this->assignRef('hiddenChecker',		$hiddenChecker);
		
		parent::display($tpl);
	}
}
