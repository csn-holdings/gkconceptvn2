<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JToolBarHelper::title( JText::_( 'productXport Administration' ), 'cpanel.png' );
?>

<table class="adminform">
    <tr>
        <td width="100%" valign="top">
			<div id="cpanel">

				<div style="float:left;">
					<div class="icon">
						<a href="index.php?option=com_productxport&amp;view=server">
							<img src="<?php echo JURI::base(); ?>components/com_productxport/images/icon-48-extension.png" alt="<?php echo JText::_( 'Server manager' ); ?>"  /><span><?php echo JText::_( 'COM_SERVER_MANAGER' ); ?></span>
						</a>
					</div>
				</div>
				<div style="float:left;">
					<div class="icon">
						<a href="index.php?option=com_productxport&amp;view=component">
							<img src="<?php echo JURI::base(); ?>components/com_productxport/images/icon-48-extension.png" alt="<?php echo JText::_( 'Component manager' ); ?>"  /><span><?php echo JText::_( 'COM_ESHOP_SETTING' ); ?></span>
						</a>
					</div>
				</div>
				<div style="float:left;">
					<div class="icon">
						<a href="index.php?option=com_productxport&amp;view=log">
							<img src="<?php echo JURI::base(); ?>components/com_productxport/images/icon-48-extension.png" alt="<?php echo JText::_( 'Log records' ); ?>"  /><span><?php echo JText::_( 'COM_LOG' ); ?></span>
						</a>
					</div>
				</div>
				<div style="float:left;">
					<div class="icon">
						<a href="index.php?option=com_productxport&amp;view=tools">
							<img src="<?php echo JURI::base(); ?>components/com_productxport/images/icon-48-extension.png" alt="<?php echo JText::_( 'Log records' ); ?>"  /><span><?php echo JText::_( 'Tools' ); ?></span>
						</a>
					</div>
				</div>
			</div>
        </td>
    </tr>
</table>
<strong>
<?php echo JText::_('If you have any problem or suggestion, just contact us at: productxport.linelab.org'); ?>
</strong>

