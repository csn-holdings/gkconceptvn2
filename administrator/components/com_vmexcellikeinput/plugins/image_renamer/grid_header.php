<?php
if(isset($SETTINGS->image_renamer)){
	if($SETTINGS->image_renamer){
		
		for($IN = 0 ; $IN < $SETTINGS->image_renamer->images; $IN++){
			?>
			,"<?php echo pelm_sprintf("Image[". ($IN + 1) ."] Name");?>"
			<?php	
			if($SETTINGS->image_renamer->thumbs_names){
				?>
				,"<?php echo pelm_sprintf("Thumb[". ($IN + 1) ."] Name");?>"
				<?php	
			}
		}
	}
}
?>