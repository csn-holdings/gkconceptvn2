<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @package          sh404SEF
 * @copyright        (c) Yannick Gaultier - Weeblr llc - 2020
 * @author           Yannick Gaultier
 * @license          http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version          4.22.1.4233
 * @date                2020-12-03
 */

namespace Weeblr\Wblib\V_SH4_4233\System;

use Weeblr\Wblib\V_SH4_4233\Factory,
	Weeblr\Wblib\V_SH4_4233\Joomla\Uri;

// no direct access
defined('WBLIB_V_SH4_4233_ROOT_PATH') || die;

class Http
{

	// return code
	const RETURN_OK = 200;
	const RETURN_CREATED = 201;
	const RETURN_ACCEPTED = 202;
	const RETURN_NO_CONTENT = 204;
	const RETURN_MOVED = 301;
	const RETURN_FOUND = 302;
	const RETURN_SEE_OTHER = 303;
	const RETURN_NOT_MODIFIED = 304;
	const RETURN_BAD_REQUEST = 400;
	const RETURN_UNAUTHORIZED = 401;
	const RETURN_FORBIDDEN = 403;
	const RETURN_NOT_FOUND = 404;
	const RETURN_PROXY_AUTHENTICATION_REQUIRED = 407;
	const RETURN_INTERNAL_ERROR = 500;
	const RETURN_SERVICE_UNAVAILABLE = 503;

	/**
	 * Creates an HTTP client from the platform used
	 */
	public static function getClient()
	{
	}

	/**
	 * Abort the current HTTP response
	 *
	 * @param   int     $code
	 * @param   string  $cause
	 */
	public static function abort($code = self::RETURN_NOT_FOUND, $cause = '')
	{

		$header = self::getHeader($code, $cause);

		// clean all buffers
		if (ob_get_length())
		{
			ob_end_clean();
		}

		$msg = empty($cause) ? $header->msg : $cause;
		if (!headers_sent())
		{
			header($header->raw);
		}
		die($msg);
	}

	/**
	 * Get HTTP header for response based on status
	 *
	 * @param $code
	 * @param $cause
	 *
	 * @return stdClass
	 */
	public static function getHeader($code, $cause)
	{

		$code   = intval($code);
		$header = new \stdClass();

		switch ($code)
		{

			case self::RETURN_OK:
				$header->raw = 'HTTP/1.0 200 OK';
				$header->msg = 'OK';
				break;
			case self::RETURN_CREATED:
				$header->raw = 'HTTP/1.0 201 CREATED';
				$header->msg = 'Created';
				break;
			case self::RETURN_NO_CONTENT:
				$header->raw = 'HTTP/1.0 204 OK';
				$header->msg = 'No content';
				break;

			case self::RETURN_BAD_REQUEST:
				$header->raw = 'HTTP/1.0 400 BAD REQUEST';
				$header->msg = '<h1>Unauthorized</h1>';
				break;
			case self::RETURN_UNAUTHORIZED:
				$header->raw = 'HTTP/1.0 401 UNAUTHORIZED';
				$header->msg = '<h1>Unauthorized</h1>';
				break;
			case self::RETURN_FORBIDDEN:
				$header->raw = 'HTTP/1.0 403 FORBIDDEN';
				$header->msg = '<h1>Forbidden access</h1>';
				break;
			case self::RETURN_NOT_FOUND:
				$header->raw = 'HTTP/1.0 404 NOT FOUND';
				$header->msg = '<h1>Page not found</h1>';
				break;
			case self::RETURN_PROXY_AUTHENTICATION_REQUIRED:
				$header->raw = 'HTTP/1.0 407 PROXY AUTHENTICATION REQUIRED';
				$header->msg = '<h1>Proxy authentication required</h1>';
				break;
			case self::RETURN_INTERNAL_ERROR:
				$header->raw = 'HTTP/1.0 500 INTERNAL ERROR';
				$header->msg = 'Internal error';
				break;
			case self::RETURN_SERVICE_UNAVAILABLE:
				$header->raw = 'HTTP/1.0 503 SERVICE UNAVAILABLE';
				$header->msg = '<h1>Service unavailable</h1>';
				break;

			default:
				$header->raw = 'HTTP/1.0 ' . $code;
				$header->msg = $cause;
				break;
		}

		return $header;
	}

	public static function getAllHeaders()
	{
		static $headers = null;

		if (is_null($headers))
		{
			if (\function_exists('getallheaders'))
			{
				// If php is working under Apache, there is a special function
				$headers = getallheaders();
			}
			else
			{
				// Else we fill headers from $_SERVER variable
				$headers = [];

				foreach ($_SERVER as $name => $value)
				{
					if (substr($name, 0, 5) == 'HTTP_')
					{
						$headers[str_replace(' ', '-', strtolower(str_replace('_', ' ', substr($name, 5))))] = $value;
					}
				}
			}

			// normalize everything to lowercase.
			$headers = array_change_key_case($headers);
		}

		return $headers;
	}

	/**
	 * Get the value of a specific request header. Null if header is not set.
	 *
	 * @param   string  $header
	 *
	 * @return string |null
	 */
	public static function getRequestHeader($header)
	{
		$headers = static::getAllHeaders();

		return wbArrayIsSet($headers, $header) ? $headers[$header] : null;
	}

	public static function getIpAddress()
	{

		static $address;

		if (is_null($address))
		{
			// Check for proxies as well.
			if (isset($_SERVER['REMOTE_ADDR']))
			{
				$address = $_SERVER['REMOTE_ADDR'];
			}
			elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
				$address = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			elseif (isset($_SERVER['HTTP_CLIENT_IP']))
			{
				$address = $_SERVER['HTTP_CLIENT_IP'];
			}
			else
			{
				$address = false;
			}
		}

		return $address;
	}

	public static function buildUri($url = '')
	{
		if (empty($url))
		{
			$url = Factory::get()->getThe('platform')->getCurrentUrl();
		}

		return new Uri\Uri($url);
	}

	public static function isError($status)
	{
		$status = (int) $status;

		return $status > 399;
	}

	public static function isRedirect($status)
	{
		$status = (int) $status;

		return $status > 299 and $status < 400;
	}

	public static function isSuccess($status)
	{
		$status = (int) $status;

		return $status > 199 and $status < 300;
	}

	/**
	 * Renders an http response and end processing of request
	 *
	 * @param   int     $code        http status to use for response
	 * @param   string  $cause       Optional text to use as response body
	 * @param   string  $type
	 * @param   array   $otherHeaders
	 * @param   bool    $endRequest  Whether to just flush the response without ending the request.
	 */
	public static function render($code = self::RETURN_NOT_FOUND, $cause = '', $type = 'text/html', $otherHeaders = array(), $endRequest = true)
	{
		$header = self::getHeader($code, $cause);

		// clean all buffers
		if (ob_get_length())
		{
			ob_end_clean();
		}

		// final version of the content output
		$msg = empty($cause) ? $header->msg : $cause;

		// Build up headers
		$otherHeaders['Content-type'] = $type;
		if (!$endRequest)
		{
			// special processing if there's code to run after the response
			// has been sent.

			$otherHeaders['Connection']        = 'close';
			$otherHeaders['Content-Length']    = strlen($msg);
			$otherHeaders['Content-Encoding']  = 'none';
			$otherHeaders['Cache-control']     = 'no-cache, must-revalidate';
			$otherHeaders['X-Accel-Buffering'] = 'no';
			$otherHeaders['Surrogate-Control'] = 'BigPipe/1.0';

			// turn off gzip compression: this must be ran before
			// headers are sent
			if (function_exists('apache_setenv'))
			{
				apache_setenv('no-gzip', 1);
			}

			ini_set('zlib.output_compression', 0);
		}

		// output headers
		if (!headers_sent())
		{
			header($header->raw);
		}
		self::outputHeaders($otherHeaders);

		// Output content
		if ($endRequest)
		{
			if (!is_null($msg))
			{
				echo $msg;
			}
			die();
		}
		else
		{
			if (!is_null($msg))
			{
				echo $msg;
			}
			self::flushResponse();
		}
	}

	/**
	 * Flush all buffers and send back response.
	 */
	public static function flushResponse()
	{
		if (is_callable('fastcgi_finish_request'))
		{
			if (session_id())
			{
				session_write_close();
			}
			fastcgi_finish_request();

			return;
		}

		if (wbContains(php_sapi_name(), 'fcgi'))
		{
			// try to flush the mod_fcgi buffer
			echo str_repeat(' ', 66000);
		}

		ignore_user_abort(true);
		$levels = ob_get_level();
		for ($i = 0; $i < $levels; $i++)
		{
			ob_end_flush();
		}
		flush();
		ob_start();
	}

	/**
	 * Output an array of headers.
	 *
	 * @param   array  $headers  Key/value array of headers
	 */
	public static function outputHeaders($headers)
	{
		if (ob_get_length())
		{
			ob_end_clean();
		}
		if (!headers_sent())
		{
			foreach ($headers as $key => $value)
			{
				header($key . ': ' . $value);
			}
		}
	}

	/**
	 * Perform a server-side 301 redirect to the target URL.
	 *
	 * @param   string  $target
	 */
	public static function redirectPermanent($target)
	{
		if (ob_get_length())
		{
			ob_end_clean();
		}
		if (headers_sent())
		{
			echo '<html><head><meta http-equiv="content-type" content="text/html; charset="UTF-8"'
				. '" /><script>document.location.href=\'' . $target . '\';</script></head><body></body></html>';
		}
		else
		{
			header('Cache-Control: no-cache'); // prevent Firefox5+ and IE9+ to consider this a cacheable redirect
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $target);
		}
		exit();
	}

	/**
	 * Extract a header identified by its key from an array of response headers.
	 *
	 * @param   array   $headers
	 * @param   string  $name
	 * @param   bool    $asString
	 * @param   bool    $caseInsensitive
	 *
	 * @return array|mixed|string|null
	 */
	public static function extractResponseHeader($headers, $name, $asString = false, $caseInsensitive = true)
	{
		if ($caseInsensitive)
		{
			$headers = array_change_key_case($headers);
			$name    = strtolower($name);
		}

		$header = wbArrayGet(
			$headers,
			$name
		);

		if (empty($header))
		{
			return $asString
				? ''
				: [];
		}

		if (is_array($header))
		{
			$header = array_shift($header);
		}

		return $header;
	}
}
