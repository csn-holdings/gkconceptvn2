<?php
/**
 * @build_title_build       @
 *
 * @package                 sh404SEF
 * @copyright               (c) Yannick Gaultier - Weeblr llc - 2020
 * @author                  Yannick Gaultier
 * @license                 http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version                 4.22.1.4233
 *
 * 2020-12-03
 */

namespace Weeblr\Wblib\V_SH4_4233\Db;

use Weeblr\Wblib\V_SH4_4233\Base;
use Weeblr\Wblib\V_SH4_4233\System;

/** ensure this file is being included by a parent file */
defined('WBLIB_V_SH4_4233_ROOT_PATH') || die;

/**
 * Database persistence for data objects.
 * Data is stored in the state it will be stored to the database.
 *
 */
class Dataobject extends Base\Dataobject
{
	/**
	 * @var string[] List of columns and their id which can be searched.
	 */
	protected $searchableColumns = [];

	/**
	 * @var string[] List of columns and their id which can be ordered by.
	 */
	protected $orderableColumns = [];

	/**
	 * @var array List of data key that should be ignored when storing to the DB.
	 */
	protected $dbIgnore = [];

	/**
	 * @var string Database table associated with this instance.
	 */
	protected $table = '';

	/**
	 * @var string Database table key name. Only single key supported.
	 */
	protected $keyName = 'id';

	/**
	 * @var Weeblr\Wblib\Db\Dbhelper Database helper instance.
	 */
	protected $db = null;

	/**
	 * Associate this instance to a database table.
	 *
	 * @param   string  $table
	 *
	 * @throws \Exception
	 */
	public function __construct($table = '')
	{
		parent::__construct();
		$this->table = !empty($table) ? $table : $this->table;
		$this->db    = $this->factory->getThe('db');

		if (
			empty($this->table)
			||
			empty($this->data)
			||
			empty($this->keyName)
		)
		{
			throw new \Exception(get_class($this) . ': invalid database or data specification.', 500);
		}
	}

	/**
	 * Validate whether data for a given key is ok and can be used by the object. Possible
	 * processing is allowed to fix/update things here.
	 *
	 * @param   string  $key
	 *
	 * @return $this
	 * @throws \Exception
	 */
	protected function validateKey($key)
	{
		if (empty($key))
		{
			throw new \Exception('Trying to set/get empty key on ' . __CLASS__ . ' data object', 500);
		}

		// if replacing a keyed item, that key must be one of the data set
		if (
			!array_key_exists(
				$key,
				$this->data
			)
			&&
			!in_array(
				$key,
				$this->dbIgnore
			))
		{
			throw new \Exception('Trying to set/get unknown key ' . print_r($key, true) . ' / ' . print_r($this->data, true)
				. ' / Db Ignore: ' . print_r($this->dbIgnore, true)
				. ' on ' . __CLASS__ . ' data object ' . print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), true), 500);
		}

		return $this;
	}

	/**
	 * Delete the database record:
	 * - current one if no key supplied and data as been loaded already (ie we have a key value)
	 * - if key is a string, the record identified by that key.
	 * - if array of keys, the records identified by those keys.
	 *
	 * @param   null|string|array  $keys
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function delete($keys = null)
	{
		$currentKey = wbArrayGet(
			$this->data,
			$this->keyName,
			null
		);
		if (empty($keys) && is_null($currentKey))
		{
			throw new \Exception('wbLib: cannot delete item from database without a key value.', 500);
		}
		if (empty($keys))
		{
			$keys = [$currentKey];
		}

		$keys = $this->beforeDelete(
			wbArrayEnsure($keys)
		);
		if (empty($keys))
		{
			return $this;
		}

		$this->db->deleteIn(
			$this->table,
			$this->keyName,
			$keys
		);

		$this->afterDelete($keys);

		return $this;
	}

	/**
	 * Filter list of keys to be deleted. Return false or empty
	 * array to cancel the deletion operation.
	 *
	 * @param   array  $keys
	 *
	 * @return mixed
	 */
	protected function beforeDelete($keys)
	{
		return $keys;
	}

	/**
	 * Perform actions after a deletion has taken place.
	 *
	 * @param   array  $keys
	 *
	 * @return Dataobject
	 */
	protected function afterDelete($keys)
	{
		return $this;
	}

	/**
	 * Loads a record from database and store its content in this object.
	 *
	 * @param   null|string  $key
	 * @param   bool         $reload
	 *
	 * @return $this
	 */
	public function load($key = null, $reload = false)
	{
		$currentKey = wbArrayGet($this->data, $this->keyName, '');
		if (
			!empty($currentKey)
			&&
			$currentKey == $key
			&&
			!$reload
		)
		{
			// already have the data
			return $this;
		}

		// don't have the data, or force reload
		$key        = empty($key) ? $currentKey : $key;
		$this->data = $this->db->selectAssoc(
			$this->table,
			'*',
			[
				$this->keyName => $key
			]
		);

		$this->data = $this->afterLoad(
			$this->data,
			$key
		);

		return $this;
	}

	/**
	 * Filter the returned data after a get.
	 *
	 * @param   mixed  $data
	 * @param   null   $key
	 *
	 * @return mixed
	 */
	protected function afterLoad($data, $key = null)
	{
		return array_merge(
			$this->defaults,
			empty($data)
				? []
				: $data
		);
	}

	/**
	 * Loads an item from DB based on specific column value.
	 * Key value must be provided and loaded data replaces any data
	 * already stored in this instance if some data is found in db.
	 * Data is always loaded from DB, on each call.
	 *
	 * @param   string  $keyName
	 * @param   mixed   $value
	 *
	 * @return $this
	 */
	public function loadPerColumn($keyName, $value)
	{
		$dbData = $this->db->selectAssoc(
			$this->table,
			'*',
			[
				$keyName => $value
			]
		);
		if (!empty($dbData))
		{
			$this->data = $this->afterLoad(
				$dbData,
				$value
			);
		}

		return $this;
	}

	/**
	 * Loads an item from db based on a set of column values.
	 * Passed key => value array is considered a set of
	 * where clauses, stitched together with AND operators.
	 *
	 * @param [] $where
	 *
	 * @param   array  $whereData
	 *
	 * @return $this
	 */
	public function loadWhere($where, $whereData = [])
	{
		$dbData = $this->db->selectAssoc(
			$this->table,
			'*',
			$where
		);
		if (!empty($dbData))
		{
			$this->data = $this->afterLoad(
				$dbData
			);;
		}

		return $this;
	}

	/**
	 * Stores current data to the database.
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function store()
	{
		$this->validate()
			->beforeStore();

		// filter out any unwanted data
		$data = $this->data;
		if (!empty($this->dbIgnore))
		{
			$data = array_diff_key(
				$data,
				array_flip($this->dbIgnore)
			);
		}

		// finally store/update
		$key = wbArrayGet($this->data, $this->keyName, '');
		if (empty($key))
		{
			// create
			$this->db->insert(
				$this->table,
				$data
			);

			$this->data[$this->keyName] = $this->db->getInsertId();
		}
		else
		{
			// update
			$this->db->update(
				$this->table,
				$data,
				array(
					$this->keyName => $key
				)
			);
		}

		return $this;
	}

	/**
	 * A chance to massage data before storing it.
	 *
	 * @return $this
	 */
	public function beforeStore()
	{
		return $this;
	}

	/**
	 * Returns the record key from the db.
	 *
	 * @return int|null
	 */
	public function getId()
	{
		return wbArrayGet(
			$this->data,
			$this->keyName,
			null
		);
	}

	/**
	 * @return string Getter for this dataObject main associated database table.
	 */
	public function tableName()
	{
		return $this->table;
	}

	/**
	 * @return string[] Getter for this dataObject list of searchable columns and their ids.
	 */
	public function searchableColumnsList()
	{
		return $this->searchableColumns;
	}

	/**
	 * @return string[] Getter for this dataObject list of orderable columns and their ids.
	 */
	public function orderableColumnsList()
	{
		return $this->orderableColumns;
	}
}
