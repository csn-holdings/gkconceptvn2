<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author           Yannick Gaultier
 * @copyright        (c) Yannick Gaultier - Weeblr llc - 2020
 * @package          sh404SEF
 * @license          http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version          4.22.1.4233
 * @date                2020-12-03
 */

namespace Weeblr\Wblib\V_SH4_4233\System;

// no direct access
defined('WBLIB_V_SH4_4233_ROOT_PATH') || die;

class Memo
{
	private static $cache = [];

	/**
	 * Get a possibly memoized value, returning a default value if not found.
	 *
	 * @param   string  $key      An array of nested keys to get to the desired config item
	 * @param   mixed   $default  Optional default value if config not set
	 *
	 * @return mixed
	 */
	public function get($key, $default = null)
	{
		return wbArrayGet(
			self::$cache,
			md5($key),
			$default
		);
	}

	/**
	 * Sets a value under a specific key.
	 *
	 * @param   string  $key
	 * @param   mixed   $value
	 *
	 * @return Memo
	 */
	public function set($key, $value)
	{
		self::$cache[md5($key)] = $value;

		return $this;
	}

	/**
	 * Check if some data has been memoized under a specific key.
	 *
	 * @param   string  $key
	 *
	 * @return bool
	 */
	public function has($key)
	{
		return wbArrayIsSet(
			self::$cache,
			$key
		);
	}
}
