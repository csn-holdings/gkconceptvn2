<?php
/**
 * @build_title_build       @
 *
 * @package                 sh404SEF
 * @copyright               (c) Yannick Gaultier - Weeblr llc - 2020
 * @author                  Yannick Gaultier
 * @license                 http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version                 4.22.1.4233
 *
 * 2020-12-03
 */

namespace Weeblr\Wblib\V_SH4_4233\System;

use Weeblr\Wblib\V_SH4_4233\Joomla\StringHelper\StringHelper;
use Weeblr\Wblib\V_SH4_4233\Joomla\Uri\Uri;
use Weeblr\Wblib\V_SH4_4233\Factory;

defined('WBLIB_V_SH4_4233_ROOT_PATH') || die;

/**
 * Route helper
 *
 */
class Route
{
	/**
	 * Turn a relative-to-page URL into an absolute one, using the site canonical domain if any
	 *
	 * @param   string  $url
	 * @param   bool    $forceDomain          if URL is already absolute, we won't fully qualify it with a domain (if relative we
	 *                                        still prepend the full domain)
	 * @param   string  $currentUrl
	 * @param   bool    $skipRewritingprefix  Whether the URL is for an asset: no need for URL rewriting.
	 *
	 * @return string
	 * @throws \Exception
	 */
	public static function absolutify($url, $forceDomain = false, $currentUrl = null, $skipRewritingprefix = false)
	{
		static $scheme = null;

		// is it already absolute?
		if (
			self::isFullyQualified($url)
			&&
			!self::isProtocolRelative($url)
		)
		{
			return $url;
		}

		$platform      = Factory::get()->getThe('platform');
		$canonicalRoot = self::getCanonicalRoot();
		if (is_null($currentUrl))
		{
			$currentUrl = $canonicalRoot;
		}

		if (is_null($scheme))
		{
			$scheme = $platform->getScheme();
		}

		if (self::isProtocolRelative($url))
		{
			// protocol relative URL, remove domain and path
			$rootRelative = wbLTrim($canonicalRoot, 'https:');
			$rootRelative = wbLTrim($rootRelative, 'http:');
			$originalUrl  = $url;
			$url          = wbLTrim($url, $rootRelative);
			if ($url == $originalUrl)
			{
				// trimming the root URL had not effect, this is a relative
				// URL to another domain (?), don't do anything
				return $originalUrl;
			}
			else
			{
				return $scheme . $originalUrl;
			}
		}
		else if (wbStartsWith($url, '/'))
		{
			// already a root-relative URL, only add domain if asked to
			if ($forceDomain)
			{
				// remove root path
				$rootUri  = new Uri($canonicalRoot);
				$rootPath = $rootUri->getPath();
				$url      = wbLTrim($url, $rootPath);
				if (empty($url))
				{
					$url = $canonicalRoot;
				}
				else
				{
					$url = StringHelper::rtrim($canonicalRoot, '/') . ($skipRewritingprefix ? '' : $platform->getUrlRewritingPrefix()) . '/' . StringHelper::ltrim($url, '/');
				}
			}
			else
			{
				$url = ($skipRewritingprefix ? '' : $platform->getUrlRewritingPrefix()) . $url;
			}

			return $url;
		}
		else
		{
			return self::resolveRelativeUrl(
				$url,
				$currentUrl,
				$skipRewritingprefix
			);
		}

		throw new \Exception('wbLib: internal error: ' . __FILE__ . ' ' . __FUNCTION__ . ' ' . __LINE__);
	}

	/**
	 * Finds if a URL is fully qualified, ie starts with a scheme
	 * Protocal-relative URLs are considered fully qualified
	 *
	 * @param $url
	 *
	 * @return bool
	 */
	public static function isFullyQualified($url)
	{
		return StringHelper::substr($url, 0, 7) == 'http://' || StringHelper::substr($url, 0, 8) == 'https://' || self::isProtocolRelative($url);
	}

	/**
	 * Whether a URL is protocol-relative.
	 *
	 * @param   string  $url
	 *
	 * @return bool
	 */
	public static function isProtocolRelative($url)
	{
		return wbStartsWith(
			$url,
			'//'
		);
	}

	/**
	 * Make a url fully qualified and protocol relative
	 *
	 * @param   string  $url
	 *
	 * @return mixed|string
	 */
	public static function makeProtocolRelative($url)
	{
		$url = self::absolutify($url, true);
		$url = preg_replace('#^https?://#', '//', $url);

		return $url;
	}

	/**
	 * Make a URL relative to the site root.
	 *
	 * @param   string  $url                 An internal URL.
	 * @param   bool    $removeLeadingSlash  Whether to strip the leading slash after making the URL relative to root.
	 * @param   string  $currentUrl
	 *
	 * @return string
	 * @throws \Exception
	 */
	public static function makeRootRelative($url, $removeLeadingSlash = false, $currentUrl = null)
	{
		if (!self::isInternal($url))
		{
			return $url;
		}

		$sefRewritePrefix = Factory::get()->getThe('platform')->getUrlRewritingPrefix();

		$base = Factory::get()->getThe('platform')->getBaseUrl();
		if (wbStartsWith($url, $base))
		{
			$url = wbLTrim(
				$url,
				$base
			);
			if (!empty($sefRewritePrefix))
			{
				$url = wbLTrim(
					$url,
					$sefRewritePrefix
				);
			}

			$url = $removeLeadingSlash
				? wbLTrim($url, '/')
				: $url;

			return $url;
		}

		$url = self::absolutify($url, true, $currentUrl);
		$url = wbLTrim(
			$url,
			self::getCanonicalRoot()
		);

		if (!wbStartsWith($url, '/'))
		{
			$url = '/' . $url;
		}

		if (!empty($sefRewritePrefix))
		{
			$url = wbLTrim(
				$url,
				$sefRewritePrefix
			);
		}

		if (!wbStartsWith($url, '/') && !$removeLeadingSlash)
		{
			$url = '/' . $url;
		}

		$url = $removeLeadingSlash
			? wbLTrim($url, '/')
			: $url;

		return $url;
	}

	/**
	 * Resolve a URL based on the URL of the page it's relative to.
	 *
	 * @param   string  $url         The relative URL.
	 * @param   string  $currentUrl  The URL it's relative to.
	 * @param   bool    $isAsset     No need for URL rewriting prefix if an asset.
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public static function resolveRelativeUrl(string $url, string $currentUrl = null, $isAsset = false)
	{
		$platform = Factory::get()->getThe('platform');
		Log::libraryAlert('%s::%d %s', __METHOD__, __LINE__, 'wbLib: resolveRelativeUrl: url: ' . $url . ', current URL: ' . $currentUrl . ', platform URL: ' . $platform->getCurrentUrl());
		$currentUrl = is_null($currentUrl)
			? $platform->getCurrentUrl()
			: $currentUrl;

		if (wbStartsWith(
			$url,
			[
				'https://',
				'http://',
				'//',
				'/'
			]
		))
		{
			// not a relative link
			return $url;
		}
		if (!wbStartsWith(
			$currentUrl,
			[
				'https://',
				'http://',
				'//',
				'/'
			]
		))
		{
			// current URL is relative, cannot relate to that
			throw new \Exception('wbLib: resolveRelativeUrl: current URL is relative, cannot use as reference: url: ' . $url . ', current URL: ' . $currentUrl);
		}

		// remove anchors
		$baseUrl = preg_replace(
			'~#.*$~',
			'',
			$currentUrl
		);
		// remove query
		$baseUrl = preg_replace(
			'~\?.*$~',
			'',
			$baseUrl
		);

		// remove anything trailing that's not a slash
		$baseUrl = preg_replace(
			'~[^/]+$~',
			'',
			$baseUrl
		);

		// special case: if $baseUrl == root URL, we need to include the rewrite prefix
		$canonicalRoot = self::getCanonicalRoot();
		if (
			!$isAsset
			&&
			$canonicalRoot == $baseUrl
			&&
			// don't add rewrite prefix on home page
			!empty($url)
		)
		{
			$baseUrl = wbSlashTrimJoin(
				$baseUrl,
				$platform->getUrlRewritingPrefix()
			);
		}

		return wbSlashTrimJoin(
			$baseUrl,
			$url
		);
	}

	/**
	 * Builds and return the canonical domain of the page.
	 *
	 * @param   null|bool  $isAdmin
	 *
	 * @return string
	 */
	public static function getCanonicalRoot($isAdmin = null)
	{
		return Factory::get()->getThe('platform')->getCanonicalRoot($isAdmin);
	}

	/**
	 * Finds if an URL is internal, ie on the same site
	 *
	 * @param   string  $url
	 *
	 * @return bool
	 */
	public static function isInternal($url)
	{
		// absolutify, prepending domain if missing
		$url = self::absolutify($url, true);

		$canonicalRootUrl = self::getCanonicalRoot();
		if (self::isProtocolRelative($url))
		{
			$canonicalRootUrl = wbLTrim($canonicalRootUrl, 'https:');
			$canonicalRootUrl = wbLTrim($canonicalRootUrl, 'http:');
		}

		// is it local?

		/**
		 * Filter whether a URL is internal to the site.
		 *
		 * @api
		 * @package wblib\filter\route
		 * @var wblib_url_is_internal
		 *
		 * @param   bool    $urlIsInternal     Whether the URL is internal
		 * @param   string  $url               The fully qualified URL we want to find about
		 * @param   string  $canonicalRootUrl  The root URL of the site, as reported by WP
		 *
		 * @return string
		 * @since   1.0.4
		 *
		 */
		return Factory::get()->getThe('hook')->filter(
			'wblib_url_is_internal',
			wbStartsWith($url, $canonicalRootUrl),
			$url,
			$canonicalRootUrl
		);
	}

	/**
	 * Append a query variable with a random value to bust caching
	 *
	 * @param   String  $url
	 *
	 * @return string|string[]|null
	 */
	public static function cacheBust($url)
	{
		if (false !== strpos('_wb_bust=', $url))
		{
			// already there, just update the value
			$url = preg_replace(
				'/_wb_bust=[^&?]+/',
				'_wb_bust=' . mt_rand(),
				$url
			);
		}
		else
		{
			$separator = false == strpos($url, '?') ? '?' : '&';
			$url       .= $separator . '_wb_bust=' . mt_rand();
		}

		return $url;
	}

	/**
	 * Removes any _wb_bust query var that may have been added
	 * to a URL
	 *
	 * @param   String  $url
	 *
	 * @return string
	 */
	public static function removeCacheBust($url)
	{
		$uri = new Uri($url);
		$uri->delVar('_wb_bust');

		return $uri->toString();
	}

	/**
	 * Append a query string (param=1&param=2...) to an existing URL. Should make minimal modification
	 * to the original URL, including handling existing query string, fragment, path, protocol and domain if any.
	 *
	 * @param   string  $rawUrl
	 * @param   string  $append
	 *
	 * @return string
	 */
	public static function appendQuery($rawUrl, $append)
	{
		// fix for protocol-relative URLs
		$isProtocolRelative = wbStartsWith($rawUrl, '//');

		$uri   = new Uri($rawUrl);
		$query = $uri->getQuery();
		$uri->setQuery(
			wbJoin('&', $query, $append)
		);

		return ($isProtocolRelative ? '//' : '') . $uri->toString();
	}

	/**
	 * Canonicalize a URL path that may contain . and ..
	 *
	 * From https://www.php.net/manual/en/function.realpath.php#71334
	 *
	 * @param   string  $path
	 *
	 * @return string|string[]
	 */
	public static function normalizePath($path)
	{
		$path = str_replace(
			'\\',
			'/',
			$path
		);
		$path = explode('/', $path);
		$keys = array_keys($path, '..');

		foreach ($keys as $keypos => $key)
		{
			array_splice($path, $key - ($keypos * 2 + 1), 2);
		}

		$path = implode('/', $path);
		$path = str_replace('./', '', $path);

		return $path;
	}

	/**
	 * Execute a URL match rule agains a request URL, and returns any match.
	 *
	 * Rule specs:
	 * {*} => any URL
	 * xxxx => exactly 'xxxxx'
	 * xxx{?}yyy => 'xxx' + any character + 'yyy'
	 * xxx{*}yyy => 'xxx' + any string + 'yyy'
	 * {*}xxxx => any string + 'xxxxx'
	 * xxxx{*} => 'xxxx' + any string
	 * {*}xxxx{*} => any string + 'xxxxx' + any string
	 * {*}xxxx{*}yyyy => any string + 'xxxxx' + any string + 'yyyy'
	 *
	 * @param   string  $rule
	 * @param   string  $path  the path relative to the root of the site, starting with a /
	 *
	 * @param   string  $wildChar
	 * @param   string  $singleChar
	 * @param   string  $regexpChar
	 *
	 * @return array
	 */
	public static function findUrlRuleMatch($rule, $path, $wildChar = '{*}', $singleChar = '{?}', $regexpChar = '~')
	{
		// shortcuts
		if ($wildChar == $rule)
		{
			// simulate a regexp match
			return array(
				$path,
				$path
			);
		}

		// build a reg exp based on rule
		if (StringHelper::substr($rule, 0, 1) == $regexpChar)
		{
			// this is a regexp, use it directly
			$regExp = $rule;
		}
		else
		{
			// actually build the reg exp
			$saneStarBits = array();
			$starBits     = explode($wildChar, $rule);
			foreach ($starBits as $sBit)
			{
				// same thing with ?
				$questionBits = explode($singleChar, $sBit);
				$saneQBit     = array();
				foreach ($questionBits as $qBit)
				{
					$saneQBit[] = preg_quote($qBit);
				}

				$saneStarBits[] = implode($singleChar, $saneQBit);
			}

			// each part has been preg_quoted
			$sanitized = implode($wildChar, $saneStarBits);
			$regExp    = str_replace($singleChar, '(.)', $sanitized);
			$regExp    = str_replace($wildChar, '(.*)', $regExp);
			$regExp    = '~^' . $regExp . '$~uU';
		}

		// execute and return
		preg_match($regExp, $path, $matches);

		return $matches;
	}

	/**
	 * Execute a URL match rule agains a request URL, and returns a boolean if a match occured.
	 *
	 * Rule specs:
	 * {*} => any URL
	 * xxxx => exactly 'xxxxx'
	 * xxx{?}yyy => 'xxx' + any character + 'yyy'
	 * xxx{*}yyy => 'xxx' + any string + 'yyy'
	 * {*}xxxx => any string + 'xxxxx'
	 * xxxx{*} => 'xxxx' + any string
	 * {*}xxxx{*} => any string + 'xxxxx' + any string
	 * {*}xxxx{*}yyyy => any string + 'xxxxx' + any string + 'yyyy'
	 *
	 * @param   string  $rule
	 * @param   string  $path  the path relative to the root of the site, starting with a /
	 *
	 * @param   string  $wildChar
	 * @param   string  $singleChar
	 * @param   string  $regexpChar
	 *
	 * @return bool
	 */
	public static function matchUrlRule($rule, $path, $wildChar = '{*}', $singleChar = '{?}', $regexpChar = '~')
	{
		$matches = self::findUrlRuleMatch($rule, $path, $wildChar, $singleChar, $regexpChar);

		return !empty($matches[0]);
	}

	/**
	 * Finds the host for a given URL, making FQDN if not already.
	 *
	 * @param   string  $url
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public static function getHost($url)
	{
		$uri = new Uri(
			static::absolutify(
				$url,
				true
			)
		);

		$host = $uri->getHost();

		return empty($host)
			? ''
			: $host;
	}

	/**
	 * Finds the scheme for a given URL, making FQDN if not already.
	 *
	 * @param   string  $url
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public static function getScheme($url)
	{
		$uri = new Uri(
			static::absolutify(
				$url,
				true
			)
		);

		$scheme = $uri->getScheme();

		return empty($scheme)
			? ''
			: $scheme;
	}

	/**
	 * Verify if a given URL is on the specified host.
	 *
	 * @param   string  $host
	 * @param   string  $url
	 * @param   bool    $strict  If true, there must be an exact host match. If not, only end of string must match (ie weeblr.com will match support.weeblr.com)
	 *
	 * @return bool
	 */
	public static function hostMatch($host, $url, $strict = false)
	{
		$parsed = parse_url($url, PHP_URL_HOST);

		return $parsed !== false
			&&
			(
				($strict && $parsed == $host)
				||
				(!$strict && wbEndsWith($parsed, $host))
			);
	}

	/**
	 * Drop the query part of a URL provided as a string.
	 *
	 * @param   string  $url
	 *
	 * @return string
	 * @throws \Exception
	 */
	public static function trimQuery($url)
	{
		$bits = explode('?', $url);

		return wbArrayGet($bits, 0, '');

	}
}
