<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author       Yannick Gaultier
 * @copyright    (c) Yannick Gaultier - Weeblr llc - 2020
 * @package      sh404SEF
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version      4.22.1.4233
 * @date        2020-12-03
 */

namespace Weeblr\Wblib\V_SH4_4233\Html\Remoteimage;

// Security check to ensure this file is being included by a parent file.
defined('WBLIB_V_SH4_4233_ROOT_PATH') || die;

/**
 * Derived from:
 *
 * FastImage - Because sometimes you just want the size!
 * Based on the Ruby Implementation by Steven Sykes (https://github.com/sdsykes/fastimage)
 *
 * Copyright (c) 2012 Tom Moor
 * Tom Moor, http://tommoor.com
 *
 * MIT Licensed
 * @version 0.1
 *
 * and
 *
 * FasterImage - Because sometimes you just want the size, and you want them in
 * parallel!
 *
 * Based on the PHP stream implementation by Tom Moor (http://tommoor.com)
 * which was based on the original Ruby Implementation by Steven Sykes
 * (https://github.com/sdsykes/fastimage)
 *
 * MIT Licensed
 *
 * @version 0.01
 */
class Fasterimage
{
	/**
	 * The default timeout
	 *
	 * @var int
	 */
	protected $timeout = 5;
	protected $stream = null;
	protected $parser = null;
	protected $transportType = null;
	protected $transport = null;

	/**
	 * Finds out which http transport we can use and initialize
	 * accordingly
	 *
	 * Html\Remoteimage\FasterImage constructor.
	 *
	 * @param   array  $options
	 */
	public function __construct($options = [])
	{
		if (!empty($options['timeout']))
		{
			$this->timeout = $options['timeout'];
		}

		// get stream and parser
		$this->stream = new Stream;
		$this->parser = new Parser($this->stream);

		// determine which transport to use
		$this->discoverTransport()
			// initialize this transport
			->buildTransport()
			->setTimeout(
				$this->timeout
			);
	}

	protected function discoverTransport()
	{
		switch (true)
		{
			case (function_exists('curl_version') && curl_version()):
				$this->transportType = 'curl';
				break;
			case (function_exists('fopen') && is_callable('fopen') && ini_get('allow_url_fopen')):
				$this->transportType = 'stream';
				break;
		}

		return $this;
	}

	protected function buildTransport()
	{
		$className = 'Weeblr\Wblib\V_SH4_4233\Html\Remoteimage\\' . ucfirst($this->transportType) . 'transport';

		$this->transport = new $className(
			$this->stream,
			$this->parser
		);

		return $this->transport;
	}

	/**
	 * @param $url
	 *
	 * @return resource
	 */
	public function getSize($url)
	{
		$this->parser->reset();

		// fetch and get size
		return $this->transport->getSize($url);
	}
}
