<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author       Yannick Gaultier
 * @copyright    (c) Yannick Gaultier - Weeblr llc - 2020
 * @package      sh404SEF
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version      4.22.1.4233
 * @date        2020-12-03
 */

namespace Weeblr\Wblib\V_SH4_4233\Html;

use Weeblr\Wblib\V_SH4_4233\Base;
use Weeblr\Wblib\V_SH4_4233\System;
use Weeblr\Wblib\V_SH4_4233\Joomla\StringHelper\StringHelper;
use Weeblr\Wblib\V_SH4_4233\Html\Remoteimage as Remoteimage;

// Security check to ensure this file is being included by a parent file.
defined('WBLIB_V_SH4_4233_ROOT_PATH') || die;

class Image extends Base\Base
{
	const IMAGE_SEARCH_NONE = 0;
	const IMAGE_SEARCH_FIRST = 1;
	const IMAGE_SEARCH_LARGEST = 2;

	/**
	 * Get an image size from the file
	 *
	 * @param   string  $url
	 *
	 * @return array Width/height of the image, 0/0 if not found
	 */
	public function getImageSize($url)
	{
		static $rootPath = '';
		static $pathLength = 0;
		static $rootUrl = '';
		static $rootLength = 0;
		static $protocoleRelRootUrl = '';
		static $protocoleRelRootLength = 0;

		if (empty($rootPath))
		{
			$rootUrl                = $this->platform->getRootUrl(false);
			$rootLength             = StringHelper::strlen($rootUrl);
			$protocoleRelRootUrl    = str_replace(array('https://', 'http://'), '//', $rootUrl);
			$protocoleRelRootLength = StringHelper::strlen($protocoleRelRootUrl);
			$rootPath               = $this->platform->getBaseUrl(true);
			$pathLength             = StringHelper::strlen($rootPath);
			if ($this->platform->isBackend())
			{
				$rootPath = str_replace('/administrator', '', $rootPath);
			}
		}

		// default values ?
		$dimensions = array('width' => 0, 'height' => 0);

		if (wbContains($url, array('_wblapi=', 'wbl_api=')))
		{
			// softcron, bail out
			return $dimensions;
		}

		// build the physical path from the URL
		if (StringHelper::substr($url, 0, $rootLength) == $rootUrl)
		{
			$cleanedPath = $this->trimQuery(StringHelper::substr($url, $rootLength));
		}
		else if (!empty($rootPath) && StringHelper::substr($url, 0, $pathLength) == $rootPath)
		{
			$cleanedPath = $this->trimQuery(StringHelper::substr($url, $pathLength));
		}
		else if (StringHelper::substr($url, 0, 2) == '//' && StringHelper::substr($url, 0, $protocoleRelRootLength) == $protocoleRelRootUrl)
		{
			// protocol relative URL
			$cleanedPath = $this->trimQuery(StringHelper::substr($url, $protocoleRelRootLength));
		}
		else if (System\Route::isFullyQualified($url))
		{
			// a URL, but not on this site, try to download it and read its size
			$remoteDimensions = $this->getCachedRemoteImageDimensions($url);

			return empty($remoteDimensions) ? $dimensions : $remoteDimensions;
		}
		else
		{
			$cleanedPath = $this->trimQuery($url);
		}

		$cleanedPath = !empty($rootPath) && substr($cleanedPath, 0, $pathLength) == $rootPath ? substr($url, $pathLength) : $cleanedPath;
		$imagePath   = trim($this->platform->getRootPath() . '/' . wbLTrim($cleanedPath, '/'));
		if (file_exists($imagePath))
		{
			$imageInfos = getimagesize($imagePath);
			if (!empty($imageInfos))
			{
				$dimensions = [
					'width'  => $imageInfos[0],
					'height' => $imageInfos[1]
				];
			}
		}

		return $dimensions;
	}

	private function trimQuery($url)
	{
		$bits = explode('?', $url);

		return $bits[0];
	}

	public function getCachedRemoteImageDimensions($url)
	{
		$key = md5($url);

		// was it cached?
		$dimensionsCache  = $this->platform->getCache(
			'output',
			[
				'caching'      => 1,
				'lifetime'     => 10080,
				'defaultgroup' => 'wblib_remote_img_size'
			]
		);
		$cachedDimensions = $dimensionsCache->get($key);
		if (!empty($cachedDimensions))
		{
			return $cachedDimensions;
		}

		$dimensionsRead = $this->getRemoteImageDimensions($url);

		// format response
		if (is_array($dimensionsRead))
		{
			$cachedDimensions = $dimensionsRead;
		}
		else if (!empty($dimensionsRead))
		{
			$cachedDimensions = [
				'width'  => $dimensionsRead[0],
				'height' => $dimensionsRead[1]
			];
		}

		$dimensionsCache->store(
			$cachedDimensions,
			$key
		);

		return $cachedDimensions;
	}

	/**
	 * Read a remote image site. Result should be cached for some time
	 * by the calling party, as this is an expensive operation.
	 *
	 * @param   string  $url
	 *
	 * @return array|bool|mixed
	 */
	public function getRemoteImageDimensions($url)
	{
		$dimensions = false;
		$lockKey    = md5($url);

		// get lock cache object
		$lockCache = $this->platform->getCache(
			'output',
			[
				'caching'      => 1,
				'lifetime'     => 1,
				'defaultgroup' => 'wblib_remote_img_lock'
			]
		);

		$storedLock = $lockCache->get($lockKey);
		if (!empty($storedLock))
		{
			return $dimensions;
		}
		$lockCache->store($url, $lockKey);

		// use utility class to fetch remote image
		$sizeReader     = new Remoteimage\Fasterimage();
		$dimensionsRead = $sizeReader->getSize($url);

		// clear lock
		$lockCache->remove($lockKey);

		// format response
		if (is_array($dimensionsRead))
		{
			$dimensions = [
				'width'  => $dimensionsRead[0],
				'height' => $dimensionsRead[1]
			];
		}

		return $dimensions;
	}

	/**
	 * Lookup an image tag in some html content, and returns the src attribute,
	 * based on a selection process:
	 * - none, first found or largest image selection
	 * - an array of minimal width/height the image must have to be included in the selection process
	 *
	 * @param   string  $content        the raw content
	 * @param   int     $selectionMode  one of this class constant for search mode
	 * @param   array   $requiredSize   a minimal width/height specification
	 *
	 * @return array|null Record with image URL (as found in the content (ie relative or absolute)), width, height and number of pixels.
	 */
	public function getBestImage($content, $selectionMode = self::IMAGE_SEARCH_NONE, $requiredSize = ['width' => 0, 'height' => 0])
	{
		$bestImage = [
			'url'    => '',
			'width'  => 0,
			'height' => 0,
			'pixels' => 0,
			'alt'    => ''
		];

		// save time if no image in content
		if (empty($content) || !wbContains($content, '<img') || $selectionMode == self::IMAGE_SEARCH_NONE)
		{
			return null;
		}

		// check for a "disable auto search tag" in content
		if (wbContains($content, '{4seo_disable_auto_meta_image_detection}'))
		{
			return null;
		}

		// collect images, and select one according to settings
		$regex = '#<img([^>]*)>#Uum';
		$found = preg_match_all($regex, $content, $matches, PREG_SET_ORDER);
		if (!empty($found))
		{
			$bestImageSize = 0;
			foreach ($matches as $match)
			{
				$imageUrl = '';
				if (!empty($match[1]))
				{
					$attributes = System\Strings::parseAttributes($match[1]);
					if (!empty($attributes['src']))
					{
						$imageUrl = $attributes['src'];
					}
				}
				if (!empty($imageUrl))
				{
					// validate size (200x200)
					$imageSize = $this->getImageSize($imageUrl);

					// is it big enough?
					if ($this->isLargeEnough($imageSize, $requiredSize))
					{
						if (self::IMAGE_SEARCH_FIRST == $selectionMode)
						{
							// we got a winner
							$bestImage = [
								'url'    => $imageUrl,
								'width'  => $imageSize['width'],
								'height' => $imageSize['height'],
								'pixels' => $imageSize['width'] * $imageSize['height'],
								'alt'    => wbArrayGet($attributes, 'alt', '')
							];
							break;
						}
						else
						{
							// looking for the biggest one
							// store current image size
							$currentImageSize = (int) $imageSize['width'] + (int) $imageSize['height'];
							if ($currentImageSize > $bestImageSize)
							{
								$bestImage     = [
									'url'    => $imageUrl,
									'width'  => $imageSize['width'],
									'height' => $imageSize['height'],
									'pixels' => $imageSize['width'] * $imageSize['height'],
									'alt'    => wbArrayGet($attributes, 'alt', '')
								];
								$bestImageSize = $currentImageSize;
							}
						}
					}
				}
			}
		}

		return $bestImage;
	}

	/**
	 * Checks an image dimensions against a required minimal width/height/pixels combination.
	 * Both width and height checks must be present and pass. Pixels check is optional.
	 *
	 * Image size equals to required size is valid.
	 *
	 * @param   array  $imageSize
	 * @param   array  $requiredSize
	 *
	 * @return bool
	 */
	private function isLargeEnough($imageSize, $requiredSize)
	{
		$isLargeEnough = (!empty($imageSize['width']) && $imageSize['width'] >= $requiredSize['width'])
			&&
			(!empty($imageSize['height']) && $imageSize['height'] >= $requiredSize['height']);

		if (!$isLargeEnough)
		{
			return false;
		}

		$requiredPixels = wbArrayGet(
			$requiredSize,
			'pixels'
		);

		if (!empty($requiredPixels))
		{
			$isLargeEnough = $imageSize['width'] * $imageSize['height'] > $requiredPixels;
		}

		return $isLargeEnough;
	}
}
