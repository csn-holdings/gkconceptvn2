<?php
/**
 * @build_title_build       @
 *
 * @package                 sh404SEF
 * @copyright               (c) Yannick Gaultier - Weeblr llc - 2020
 * @author                  Yannick Gaultier
 * @license                 http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version                 4.22.1.4233
 *
 * 2020-12-03
 */

namespace Weeblr\Wblib\V_SH4_4233\Platform;

use Weeblr\Wblib\V_SH4_4233\System;
use Joomla\CMS\Table\Table;
use Joomla\CMS\Factory;
use Joomla\CMS\Filesystem\Folder;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Http\HttpFactory;
use Joomla\CMS\Cache\Cache;
use Joomla\Registry\Registry;
use Joomla\CMS\Plugin;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Session\Session;
use Joomla\CMS\Toolbar\ToolbarHelper;
use Joomla\CMS\Filter;
use Joomla\String\StringHelper;

// Security check to ensure this file is being included by a parent file.
defined('WBLIB_V_SH4_4233_ROOT_PATH') || die;

/**
 */
class JoomlaPlatform extends Platform implements Platforminterface
{
	protected $_name = 'joomla';

	protected $app = null;

	private static $hooks = array();

	private static $hooksStack = array();

	private static $hooksRuns = array();

	private static $canonicalRoot = null;


	public function __construct()
	{
		$this->app = Factory::getApplication();
	}

	/**
	 * Run any initialization code.
	 */
	public function boot()
	{

	}

	/**
	 * Detects whether running on Joomla.
	 *
	 * @return bool
	 */
	public function detect()
	{
		$detected =
			defined('_JEXEC')
			&&
			defined('JPATH_BASE');

		return $detected;
	}

	/**
	 * Returns major version of the running platform
	 */
	public function majorVersion()
	{
		$version = new \JVersion;

		return $version::MAJOR_VERSION;
	}

	/**
	 * Return full version of the running platform
	 */
	public function version()
	{
		$version = new \JVersion;

		return $version->getShortVersion();
	}

	public function getLayoutOverridePath()
	{
		return array();
	}

	public function getUniqueId()
	{
		return Factory::getConfig()->get('secret');
	}

	public function getUser($id = null)
	{
		return Factory::getUser($id);
	}

	public function isGuest()
	{
		return (bool) Factory::getUser()->guest;
	}

	public function sanitizeInput($type, $input)
	{
		switch ($type)
		{
			case 'string':
				$output = Filter\InputFilter::getInstance()->clean($input, $type);
				break;
			case 'html':
				$output = Filter\InputFilter::getInstance(null, null, 1, 1)->clean($input, $type);
				break;
			default:
				$output = $input;
				break;
		}

		return $output;
	}

	public function getCSRFToken($forceNew = false)
	{
		return Session::getFormToken($forceNew);
	}

	public function checkCSRFToken($method = 'post')
	{
		$method = strtolower($method);
		$token  = $this->getCSRFToken();

		// Check from header first
		if ($token === System\Http::getRequestHeader('x-wblr-csrf-token'))
		{
			return true;
		}

		// Then fallback to HTTP query
		if (!$this->app->input->$method->get($token, '', 'alnum'))
		{
			return false;
		}

		return true;
	}

	public function getCurrentUrl()
	{
		return Uri::getInstance()->toString();
	}

	public function getCurrentContentType()
	{
		return $this->app->input->getCmd('option');
	}

	public function getCurrentRequestCategory()
	{
		$input       = $this->app->input;
		$contentType = $input->getCmd('option');
		$view        = $input->getCmd('view');
		$layout      = $input->getCmd('layout');
		$id          = $input->getInt('id');
		$catid       = $input->getInt('catid');

		if ('category' == $view)
		{
			$catid = $id;
		}

		if (
			empty($catid)
			&&
			'article' == $view
		)
		{
			switch ($contentType)
			{
				case 'com_banners':
					$table = Table::getInstance('Banners');
					$table->load($id);
					$catid = $table->catid;
					break;
				case 'com_content':
					$table = Table::getInstance('Content');
					$table->load($id);
					$catid = $table->catid;
					break;
				case 'com_contact':
					$table = Table::getInstance('Contact');
					$table->load($id);
					$catid = $table->catid;
					break;
				case 'com_newsfeeds':
					$table = Table::getInstance('Newsfeeds');
					$table->load($id);
					$catid = $table->catid;
					break;
			}
		}

		if (!empty($catid))
		{
			$allCategories = $this->getCategories($contentType);
			foreach ($allCategories as $category)
			{
				if ($category->id == $catid)
				{
					return $category;
				}
			}
		}

		return false;
	}

	public function getSitename()
	{
		return $this->app->get('sitename');
	}

	public function getBaseUrl($pathOnly = true)
	{
		return Uri::base($pathOnly);
	}

	public function getRootUrl($pathOnly = true)
	{
		return Uri::root($pathOnly);
	}

	public function isHomePage()
	{
		static $isHomePage = null;

		if (is_null($isHomePage))
		{
			$lang_code  = $this->app->getLanguage()->getDefault();
			$item       = $this->app->getMenu()->getDefault($lang_code);
			$homeUrl    = StringHelper::trim($this->route('index.php?Itemid=' . $item->id), '/');
			$homeUrl    = wbRTrim($homeUrl, '/index.php');
			$currentUrl = Uri::getInstance()->toString(
				[
					'path',
					'query'
				]
			);
			$currentUrl = StringHelper::trim($currentUrl, '/');
			$isHomePage = $homeUrl == $currentUrl;
		}

		return $isHomePage;
	}

	/**
	 * Builds and return the canonical domain of the page, taking into account
	 * the optional canonical domain in SEF plugin, and including the base path, if any
	 * Can be called from admin side, to get front end links, by passing true as param
	 *
	 * @param   null  $isAdmin  If === true or === false, disable using JApplication::isAdmin, for testing
	 *
	 * @return null|string
	 */
	public function getCanonicalRoot($isAdmin = null)
	{
		if (is_null(self::$canonicalRoot))
		{
			$sefPlugin      = Plugin\PluginHelper::getPlugin('system', 'sef');
			$sefPlgParams   = new Registry($sefPlugin->params);
			$canonicalParam = StringHelper::trim($sefPlgParams->get('domain', ''));
			if (empty($canonicalParam))
			{
				$base = $this->getRootUrl(false);
				if ($isAdmin === true || ($isAdmin !== false && $this->isBackend()))
				{
					$base = wbLTrim(
						$base,
						[
							'administrator/',
							'administrator',
						]
					);
				}
				self::$canonicalRoot = $base;
			}
			else
			{
				self::$canonicalRoot = $canonicalParam;
			}
			self::$canonicalRoot = StringHelper::rtrim(self::$canonicalRoot, '/') . '/';
		}

		return self::$canonicalRoot;
	}

	public function getUrlRewritingPrefix()
	{
		static $sefRewritePrefix = null;

		if (is_null($sefRewritePrefix))
		{
			$sefRewrite       = $this->app->get('sef_rewrite');
			$sefRewritePrefix = empty($sefRewrite)
				? '/index.php'
				: '';
		}

		return $sefRewritePrefix;
	}

	public function getExtensions($type)
	{
		static $extensions = [];

		if (!isset($extensions[$type]))
		{
			switch ($type)
			{
				case 'components':
					$db    = Factory::getDbo();
					$query = $db->getQuery(true)
						->select($db->quoteName(['extension_id', 'name', 'type', 'element', 'folder', 'enabled', 'params'], ['id', null, null, null, null, null, null]))
						->from($db->quoteName('#__extensions'))
						->where($db->quoteName('type') . ' = ' . $db->quote('component'))
						->where($db->quoteName('state') . ' = 0');
					$db->setQuery($query);

					$extensionsList = $db->loadObjectList('name');
					break;
			}

			$extensions[$type] = empty($extensionsList)
				? []
				: $extensionsList;
		}

		return $extensions[$type];
	}

	public function getCategories($extensions = [], $language = '')
	{
		static $allCategories = null;

		if (is_null($allCategories))
		{
			$db    = Factory::getDbo();
			$query = $db->getQuery(true)
				->select($db->quoteName(['id', 'parent_id', 'lft', 'rgt', 'level', 'path', 'extension', 'title', 'alias', 'published', 'metadesc', 'language']))
				->where($db->quoteName('extension') . ' != ' . $db->quote('system'))
				->from($db->quoteName('#__categories'));
			$db->setQuery($query);
			$allCategories = $db->loadObjectList();
		}

		$extensions = wbArrayEnsure($extensions);

		// filter out by extension
		if (!empty($extensions))
		{
			$categories = array_values(
				array_filter(
					$allCategories,
					function ($category) use ($extensions) {
						return in_array(
							$category->extension,
							$extensions
						);
					}
				)
			);
		}
		else
		{
			$categories = $allCategories;
		};

		if (!empty($language))
		{
			$categories = array_values(
				array_filter(
					$categories,
					function ($category) use ($language) {
						return $category->language == $language;
					}
				)
			);
		}

		return empty($categories)
			? []
			: $categories;
	}

	public function getHttpInput()
	{
		return $this->app->input;
	}

	public function getHttpClient($options = array())
	{
		return HttpFactory::getHttp(
			new Registry(
				$options
			)
		);
	}

	public function getCache($type, $options = array())
	{
		return Cache::getInstance(
			$type,
			$options
		);
	}

	/**
	 * Get current request http method in uppercase.
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function getMethod()
	{
		return strtoupper($this->app->input->getMethod());
	}

	public function getHost()
	{
		return Uri::getInstance()->getHost();
	}

	public function getScheme()
	{
		return Uri::getInstance()->getScheme();
	}

	public function getRootPath()
	{
		return JPATH_ROOT;
	}

	public function getLogsPath()
	{
		return Factory::getConfig()->get('log_path');
	}

	public function getLayoutOverridesPath()
	{
		return array();
	}

	public function getCurrentLanguageTag($full = true)
	{
		return Factory::getLanguage()->getTag();
	}

	public function getCurrentLanguageDirection()
	{
		return Factory::getLanguage()->isRtl() ? 'rtl' : 'ltr';
	}

	public function getLanguageOverrides($extension)
	{
		if (empty($extension))
		{
			return array();
		}
		$extension = strtoupper($extension . '_');
		try
		{
			$language      = Factory::getLanguage();
			$r             = new \ReflectionClass('\Joomla\CMS\Language\Language');
			$overridesProp = $r->getProperty('override');
			$overridesProp->setAccessible(true);
			$overrides = $overridesProp->getValue($language);
			// only keep our overrides
			$mergedOverrides = [];
			// Keys can have a sub-level. In Overrides this is marked with a double underscore:
			// COM_FORSEO_MAIN_MENU__DASHBOARD
			foreach ($overrides as $key => $langString)
			{
				if (wbStartsWith($key, $extension))
				{
					$keys = explode('__', strtolower(wbLTrim($key, $extension)));
					if (count($keys) == 1)
					{
						$mergedOverrides[] = $langString;
					}
					else
					{
						$mergedOverrides[$keys[0]]           = isset($mergedOverrides[$keys[0]]) ? $mergedOverrides[$keys[0]] : array();
						$mergedOverrides[$keys[0]][$keys[1]] = $langString;
					}
				}
			}

			return $mergedOverrides;
		}
		catch (\Throwable $e)
		{
			return array();
		}
		catch (\Exception $e)
		{
			return array();
		}
	}

	public function loadLanguageFile($name, $location = '')
	{
		$language = Factory::getLanguage();
		$location = 'admin' == $location ? JPATH_ADMINISTRATOR : '';
		$language->load($name, $location);
	}

	public function t($key, $options = array('js_safe' => false, 'lang' => ''))
	{
		$options['jsSafe'] = !empty($options['js_safe']);

		return Text::_($key, $options);
	}

	public function tprintf($key)
	{
		$args = func_get_args();

		return call_user_func_array('\Joomla\CMS\Language\Text::sprintf', $args);
	}

	public function getTimezone()
	{
		return $this->app->get('offset');
	}

	// html operations
	public function setHttpStatus($code, $message)
	{
		$this->app->setHeader('status', $code);

		return $this;
	}

	public function getHttpStatus()
	{
		return System\Http::RETURN_OK;
	}

	//public function addScript($url, $type = "text/javascript", $defer = false, $async = false);
	public function addScript($url, $options = array(), $attribs = array())
	{
		Factory::getDocument()->addScript($url, $options, $attribs);

		return $this;
	}

	public function addScripts($scripts)
	{
		if (!is_array($scripts))
		{
			return $this;
		}

		foreach ($scripts as $script)
		{
			$this->addScript(
				wbArrayGet($script, 'url', ''),
				wbArrayGet($script, 'options', array()),
				wbArrayGet($script, 'attr', array())
			);
		}

		return $this;
	}

	public function addScriptDeclaration($content, $type = 'text/javascript')
	{
		Factory::getDocument()->addScriptDeclaration($content, $type);

		return $this;
	}

	//public function addStyleSheet($url, $type = 'text/css', $media = null, $attribs = array());
	public function addStyleSheet($url, $options = array(), $attribs = array())
	{
		Factory::getDocument()->addStyleSheet($url, $options, $attribs);

		return $this;
	}

	public function addStyleSheets($stylesheets)
	{
		if (!is_array($stylesheets))
		{
			return $this;
		}

		foreach ($stylesheets as $stylesheet)
		{
			$this->addStyleSheet(
				wbArrayGet($stylesheet, 'url', ''),
				wbArrayGet($stylesheet, 'options', array()),
				wbArrayGet($stylesheet, 'attr', array())
			);
		}

		return $this;
	}

	public function addStyleDeclaration($content, $type = 'text/css')
	{

		Factory::getDocument()->addStyleDeclaration($content, $type);

		return $this;
	}

	public function setTitle($title)
	{
		Factory::getDocument()->setTitle($title);

		return $this;
	}

	public function getTitle()
	{
		$title = Factory::getDocument()->getTitle();

		return empty($title)
			? ''
			: $title;
	}

	public function setAdminTitle($title)
	{
		ToolbarHelper::title($title);

		return $this;
	}

	public function setDescription($description)
	{
		Factory::getDocument()->setDescription($description);

		return $this;
	}

	public function getDescription()
	{
		$desc = Factory::getDocument()->getDescription();

		return empty($desc)
			? ''
			: $desc;
	}

	public function getCanonical()
	{
		$headData = Factory::getDocument()->getHeadData();

		$headLinks = wbArrayGet(
			$headData,
			[
				'links',
			],
			[]
		);
		foreach ($headLinks as $href => $headLink)
		{
			if (wbArrayGet($headLink, 'relation', '') == 'canonical')
			{
				return $href;
			}
		}

		return '';
	}

	public function addHeadLink($href, $relation, $relType = 'rel', $attribs = array())
	{
		Factory::getDocument()->addHeadLink($href, $relation, $relType, $attribs);

		return $this;
	}

	public function addCustomTag($html)
	{
		Factory::getDocument()->addCustomTag($html);

		return $this;
	}

	public function setHeader($name, $value, $override = false)
	{
		$this->app->setHeader($name, $value, $override);

		return $this;
	}

	public function setResponseType($type = 'html', $filename = 'document')
	{
		switch ($type)
		{
			case 'json':
				Factory::getDocument()
					->setType('json')
					->setMimeEncoding('application/json');
				break;
			case 'js':
				Factory::getDocument()
					->setType('text/html')
					->setMimeEncoding('application/javascript');
				break;
			case 'css':
				Factory::getDocument()
					->setType('text/css')
					->setMimeEncoding('text/css');
				break;
			case 'raw':
			case 'html':
			default:
				break;
		}

		return $this;
	}

	// workflow operations
	public function triggerEvent($event)
	{
		return $this;
	}

	public function isFrontend()
	{
		return $this->app->isClient('site');
	}

	public function isBackend()
	{
		return $this->app->isClient('administrator');
	}

	/**
	 * Whether the current page is an edit page.
	 * Note that we don't memoize the value in case this is called prior to
	 * onAfterRoute and the request has not been parsed yet. We should not do
	 * that but we may do it anyway.
	 *
	 * @return bool
	 */
	public function isFrontendEditPage()
	{
		// unlikely to be editing without being logged in
		// also rules out admin
		if ($this->isGuest())
		{
			return false;
		}

		$input  = $this->app->input;
		$option = $input->getCmd('option');
		$view   = $input->get('view');
		if (wbContains($view, '.'))
		{
			$view = explode('.', $view);
			$view = array_pop($view);
		}
		$task = $input->getCmd('task');
		if (wbContains($task, '.'))
		{
			$task = explode('.', $task);
			$task = array_pop($task);
		}

		$layout = $input->getCmd('layout');

		$isEditPage =
			in_array($option, ['com_config', 'com_contentsubmit', 'com_cckjseblod'])
			||
			($option == 'com_comprofiler' && in_array($task, ['', 'userdetails']))
			||
			in_array($task, ['edit', 'form', 'submission'])
			||
			in_array($view, ['edit', 'form'])
			||
			in_array($layout, ['edit', 'form', 'write']);

		return
			$this->app->isClient('site')
			&&
			$isEditPage;
	}

	public function isOffline()
	{
		return $this->app->get('offline');
	}

	public function enableOfflineMode()
	{
		return $this->app->set('offline', 1);
	}

	public function disableOfflineMode()
	{
		return $this->app->set('offline', 0);
	}

	public function isHtmlDocument()
	{
		return 'html' == Factory::getDocument()->getType();
	}

	public function isFeedDocument()
	{
		return 'feed' == Factory::getDocument()->getType();
	}

	public function isDocumentType()
	{
		return Factory::getDocument()->getType();
	}

	public function getDocument()
	{
		return $this->app->getDocument();
	}

	public function isDebugEnabled()
	{
		return (bool) $this->app->get('debug');
	}

	// hooks
	public function getHooksPath()
	{
		return JPATH_ROOT . '/libraries/weeblr';
	}

	public function addHook($id, $callback, $priority = 100)
	{
		$added = false;

		if (!empty($id) && is_string($id) && is_callable($callback))
		{
			$priority                      = (int) $priority;
			self::$hooks[$id]              = empty(self::$hooks[$id]) ? array() : self::$hooks[$id];
			self::$hooks[$id][$priority]   = empty(self::$hooks[$id][$priority]) ? array() : self::$hooks[$id][$priority];
			self::$hooks[$id][$priority][] = array(
				'callback' => $callback,
				'hash'     => System\Auth::callbackUniqueId($callback)
			);

			// re-order by priority
			ksort(self::$hooks[$id]);

			$added = true;
		}

		return $added;
	}

	//
	public function removeHook($id, $callback, $priority = null)
	{
		$removed = false;

		// cannot remove, hook does not exist
		if (!array_key_exists($id, self::$hooks))
		{
			return $removed;
		}

		// do not remove a hook that is being executed
		if (in_array($id, self::$hooksStack))
		{
			return $removed;
		}

		$hash = $this->callbackUniqueId($callback);
		if (is_null($priority))
		{
			// if no priority was specified, we remove the hook
			// callback from all priority levels
			foreach (self::$hooks[$id] as $priority => $hookRecord)
			{
				$removed = $this->removeHookCallback($id, $priority, $hash);
				if ($removed)
				{
					break;
				}
			}
		}
		else
		{
			// a priority was specified, we only remove the callback
			// from that priority
			$removed = $this->removeHookCallback($id, $priority, $hash);
		}

		return $removed;
	}

	/**
	 * @param   string  $id        Dot-joined unique identifier for the hook
	 * @param   int     $priority  Restrict removal to a given priority level
	 * @param   string  $hash
	 *
	 * @return bool true if callback was remo
	 */
	private function removeCallback($id, $priority, $hash)
	{
		$removed = false;
		foreach (self::$hooks[$id][$priority] as $index => $hookRecord)
		{
			if ($hash == $hookRecord['hash'])
			{
				$removed = true;
				unset(self::$hooks[$id][$priority][$index]);
			}
		}

		return $removed;
	}

	public function executeHook($filter, $params)
	{
		// remove the filter id from params array
		$id = array_shift($params);

		// default returned value
		$currentValue = null;
		if (count($params) > 0)
		{
			$currentValue = $params[0];
		}

		// invalid hook id
		if (!is_string($id))
		{
			return $currentValue;
		}

		// no hook registered
		if (empty(self::$hooks[$id]))
		{
			return $currentValue;
		}

		// already running. We don't allow nesting
		self::$hooksStack[] = $id;

		// increase run counter
		self::$hooksRuns[$id] = isset(self::$hooksRuns[$id]) ? self::$hooksRuns[$id]++ : 1;

		// iterate over registered hook handlers
		foreach (self::$hooks[$id] as $priority => $callbackList)
		{
			foreach ($callbackList as $callbackRecord)
			{
				if ($filter)
				{
					$params[0] = call_user_func_array($callbackRecord['callback'], $params);
				}
				else
				{
					call_user_func_array($callbackRecord['callback'], $params);
				}
			}
		}

		$newValue = null;
		if ($filter)
		{
			$newValue = isset($params[0]) ? $params[0] : null;
		}

		array_pop(self::$hooksStack);

		return $newValue;
	}

	public function hasHook($id)
	{
		$hasHook = false;
		if (!empty($id) && is_string($id))
		{
			$hasHook = !empty(self::$hooks[$id]);
		}

		return $hasHook;
	}

	// display, or handle error
	public function handleError($request)
	{
		return $this;
	}

	public function handleMessage($msg, $type = 'info')
	{
		$this->app->enqueueMessage($msg, $type);

		return $this;
	}

	// routing, redirect
	public function route($url, $xhtml = true, $ssl = null)
	{
		return Route::_($url, $xhtml, $ssl);
	}

	public function redirectTo($redirectTo, $redirectMethod = 301)
	{
		if (!empty($redirectTo))
		{
			// redirect to target $redirectTo
			$this->app->redirect(
				$redirectTo,
				$redirectMethod);
		}
	}

	// authorization
	public function authorize($action, $subject, $userId = null)
	{
		return Factory::getUser($userId)->authorise($action, $subject);
	}

	// filesystem
	public function createFolders($folders)
	{
		$folders = wbArrayEnsure($folders);
		foreach ($folders as $folder)
		{
			Folder::create($folder);
		}
	}

	// Display
	public function defaultItemsPerPage()
	{
		return $this->app->get('list_limit');
	}
}
