<?php
/**
 * @build_title_build       @
 *
 * @package                 sh404SEF
 * @copyright               (c) Yannick Gaultier - Weeblr llc - 2020
 * @author                  Yannick Gaultier
 * @license                 http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version                 4.22.1.4233
 *
 * 2020-12-03
 */

namespace Weeblr\Wblib\V_SH4_4233\Html;

use Weeblr\Wblib\V_SH4_4233\Joomla\StringHelper\StringHelper;
use Weeblr\Wblib\V_SH4_4233\System;

defined('WBLIB_V_SH4_4233_ROOT_PATH') || die;

/**
 * HTML content extractor helper
 *
 */
class Extract
{
	/**
	 * Extract all href links from an html text buffer.
	 *
	 * @param   string  $buffer   HTML content to scan for links.
	 *
	 * @param   array   $options  Set of options:
	 *
	 *      bool     normalize                  If true, links are normalized to root-relative links.
	 *      bool     removeLeadingSlash         If true, leading slash of normalized URLs are removed.
	 *      bool     removeLeadingIndexPhp      If true, leading index.php/ string of normalized URLs is removed
	 *                                              (but not index.php? or index.php)
	 *      bool     removeTrailingIndexPhp     If true, trailing /index.php string of normalized URLs is removed
	 *                                              (but not index.php?)
	 *      bool     onlyInternal               If true, only internal links are returned
	 *      bool     skipAnchors                If true, anchors links are not included.
	 *      bool     skipRelative               If true, only fully qualified or root-relative links are extracted. The safest, enabled by default.
	 *      bool     skipJavascript             If true, links with href starting with 'javacript' are not collected.
	 *      string   currentUrl                 The current URL, needed to normalize relative URLs
	 *
	 * @return array
	 * @throws \Exception
	 */
	static public function extractLinks($buffer, $options = [])
	{
		$links = [];

		// save time
		if (empty($buffer))
		{
			return $links;
		}

		$normalize              = wbArrayGet($options, 'normalize', true);
		$removeLeadingSlash     = wbArrayGet($options, 'removeLeadingSlash', true);
		$onlyInternal           = wbArrayGet($options, 'onlyInternal', true);
		$skipAnchors            = wbArrayGet($options, 'skipAnchors', true);
		$skipTargetBlank        = wbArrayGet($options, 'skipTargetBlank', false);
		$skipRelative           = wbArrayGet($options, 'skipRelative', true);
		$skipJavascript         = wbArrayGet($options, 'skipJavascript', false);
		$removeTrailingIndexPhp = wbArrayGet($options, 'removeTrailingIndexPhp', true);
		$currentUrl             = wbArrayGet($options, 'currentUrl', '/');

		$dom   = self::domFromContent($buffer);
		$aTags = $dom->getElementsByTagName('a');
		foreach ($aTags as $aTag)
		{
			$href = StringHelper::trim(
				$aTag->getAttribute('href')
			);

			if (empty($href))
			{
				continue;
			}

			if (
				$skipAnchors
				&&
				wbStartsWith($href, '#')
			)
			{
				continue;
			}

			if (
				$skipJavascript
				&&
				wbStartsWith($href, 'javascript')
			)
			{
				continue;
			}

			if (
				$skipRelative
				&&
				(
					!wbStartsWith($href, '#') // anchors are ok
					&&
					!System\Route::isFullyQualified($href)
				)
			)
			{
				continue;
			}

			if ($skipTargetBlank)
			{
				$target = StringHelper::trim(
					$aTag->getAttribute('target')
				);
				if ('_blank' == strtolower($target))
				{
					continue;
				}
			}

			$href = System\Route::resolveRelativeUrl(
				$href,
				$currentUrl
			);

			if (
				$onlyInternal
				&&
				!System\Route::isInternal($href)
			)
			{
				continue;
			}

			if ($normalize)
			{
				$href = System\Route::makeRootRelative(
					$href,
					$removeLeadingSlash
				);
				$href = $removeTrailingIndexPhp
					? wbRTrim($href, 'index.php')
					: $href;
			}

			if (!in_array($href, $links))
			{
				$links[] = $href;
			}
		}

		return $links;
	}

	/**
	 * Build a DOMDocument from a string representing an HTML fragment.
	 *
	 * @param   string  $content  The HTML content.
	 *
	 * @param   bool    $wrap     If true, content is wrapped inside a dummy HTML document before loading it into the
	 *                            DOMDocument object. Not needed if content is a full HTML doc.
	 *
	 * @return false|\DOMDocument
	 */
	public static function domFromContent($content, $wrap = true)
	{

		$libxml_previous_state = libxml_use_internal_errors(true);

		$dom = new \DOMDocument;
		// Wrap in dummy tags, since XML needs one parent node.
		// It also makes it easier to loop through nodes.
		// We can later use this to extract our nodes.
		// Add utf-8 charset so loadHTML does not have problems parsing it. See: http://php.net/manual/en/domdocument.loadhtml.php#78243
		$content = $wrap
			? '<html lang=""><head><title></title><meta http-equiv="content-type" content="text/html; charset=utf-8"></head><body>' . $content . '</body></html>'
			: $content;
		$result  = $dom->loadHTML($content);

		libxml_clear_errors();
		libxml_use_internal_errors($libxml_previous_state);

		if (!$result)
		{
			return false;
		}

		return $dom;
	}
}
