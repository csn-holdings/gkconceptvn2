<?php
/**
 * @build_title_build       @
 *
 * @author                  Yannick Gaultier
 * @copyright               (c) Yannick Gaultier - Weeblr llc - 2020
 * @package                 sh404SEF
 * @license                 http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version                 4.22.1.4233
 *
 * 2020-12-03
 */

/** ensure this file is being included by a parent file */
defined('WBLIB_V_SH4_4233_ROOT_PATH') || die;

use Weeblr\Wblib\V_SH4_4233\Mvc;

/**
 * A set of syntactic sugar for outputting content.
 *
 */

if (!function_exists('wblGetLayoutOutput'))
{
	/**
	 * Wrapper around the Layout helper.
	 *
	 * @param   string  $layoutFile
	 * @param   mixed   $__data
	 * @param   string  $basePath
	 * @param   string  $theme
	 *
	 * @return string
	 */
	function wblGetLayout($layoutFile, $__data = null, $basePath = '', $theme = '')
	{
		return Mvc\LayoutHelper::render(
			$layoutFile,
			$__data,
			$basePath,
			$theme
		);
	}
}

if (!function_exists('wblEchoLayoutOutput'))
{
	/**
	 * Wrapper around the Layout helper.
	 *
	 * @param   string  $layoutFile
	 * @param   mixed   $__data
	 * @param   string  $basePath
	 * @param   string  $theme
	 */
	function wblEchoLayout($layoutFile, $__data = null, $basePath = '', $theme = '')
	{
		echo Mvc\LayoutHelper::render(
			$layoutFile,
			$__data,
			$basePath,
			$theme
		);
	}
}
