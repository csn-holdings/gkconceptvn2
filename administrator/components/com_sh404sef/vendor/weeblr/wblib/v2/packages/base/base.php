<?php
/**
 * @build_title_build       @
 *
 * @author                  Yannick Gaultier
 * @copyright               (c) Yannick Gaultier - Weeblr llc - 2020
 * @package                 sh404SEF
 * @license                 http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version                 4.22.1.4233
 *
 * 2020-12-03
 */

namespace Weeblr\Wblib\V_SH4_4233\Base;

use Weeblr\Wblib\V_SH4_4233\Factory;
use Weeblr\Wblib\V_SH4_4233\Platform\Platform;

/** ensure this file is being included by a parent file */
defined('WBLIB_V_SH4_4233_ROOT_PATH') || die;

/**
 * Base class to access the factory.
 *
 */
class Base
{
	/**
	 * @var Factory Unique instance of the factory.
	 */
	protected $factory = null;

	/**
	 * @var Platform The platform instance.
	 */
	protected $platform = null;

	/**
	 * Stores factory instance.
	 *
	 * @param   array  $options  Can inject custom factory and platform.
	 */
	public function __construct($options = [])
	{
		$this->factory = wbArrayGet(
			$options,
			'factory',
			Factory::get()
		);

		$this->platform = wbArrayGet(
			$options,
			'platform',
			$this->factory->getThe('platform')
		);
	}
}
