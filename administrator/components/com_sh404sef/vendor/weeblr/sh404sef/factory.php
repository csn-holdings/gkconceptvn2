<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @package                 sh404SEF
 * @copyright               (c) Yannick Gaultier - Weeblr llc - 2020
 * @author                  Yannick Gaultier
 * @license                 http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version                 4.22.1.4233
 *
 * 2020-12-03
 */

namespace Weeblr\Sh404sef;

use Weeblr\Wblib\V_SH4_4233\Factory;
use Weeblr\Wblib\V_SH4_4233\System;
use Weeblr\Wblib\V_SH4_4233\Html;
use Weeblr\Sh404sef\Helper;

// Security check to ensure this file is being included by a parent file.
defined('WBLIB_EXEC') || die;

/**
 * Extends the standard factory, builds a few specific objects
 */

Factory::get()->getThe('hook')->add(
	'wblib_factory_build_object_filter',
	function ($object, $factory, $method, $class, $args, $key) {

		switch ($class)
		{
			case 'helper.analytics':
				$factory->enforceSingleton(
					$class,
					$method
				);

				return new Helper\Analytics();

				break;

			// gather all versions info
			case 'sh404sef.version_info':
				$factory->enforceSingleton(
					$class,
					$method
				);

				return System\Version::get('sh404sef');

				break;

			case 'sh404sef.logger':
				$factory->enforceSingleton(
					$class,
					$method
				);

				return new System\Log('forseo', System\Log::LOGGING_PRODUCTION);

				break;

			// get sh404SEF configuration object
			case 'sh404sef.config':
				$factory->enforceSingleton(
					$class,
					$method
				);

				return \Sh404sefFactory::getConfig();

				break;

			case 'sh404sef.assetsManager':
				$factory->enforceSingleton(
					$class,
					$method
				);

				return new Html\Assetsmanager(
					array(
						'filesPath' => '/media/com_sh404sef/assets'
					)
				);

				break;
		}

		return $object;
	}
);

