<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author       Yannick Gaultier
 * @copyright    (c) Yannick Gaultier - Weeblr llc - 2020
 * @package      sh404SEF
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version      4.22.1.4233
 * @date        2020-12-03
 */

namespace Weeblr\Sh404sef\Api;

use Weeblr\Wblib\V_SH4_4233\Base;

// no direct access
defined('WBLIB_EXEC') || die;

class Hooks extends Base\Base
{
	public function add()
	{
	}
}
