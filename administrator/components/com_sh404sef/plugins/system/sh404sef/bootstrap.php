<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author       Yannick Gaultier
 * @copyright    (c) Yannick Gaultier - Weeblr llc - 2020
 * @package      sh404SEF
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version      4.22.1.4233
 * @date        2020-12-03
 */

use Weeblr\Wblib\V_SH4_4233\Factory;

// no direct access
defined('_JEXEC') || die;

defined('WBLIB_EXEC') or define('WBLIB_EXEC', true);

if (file_exists(__DIR__ . '/dev_defines.php'))
{
	include_once(__DIR__ . '/dev_defines.php');
}
else if (file_exists(__DIR__ . '/defines.php'))
{
	include_once(__DIR__ . '/defines.php');
}

if (!defined('SH404SEF_APP_PATH'))
{
	return;
}

// init library
$wbLibRootFile = WBLIB_V_SH4_4233_ROOT_PATH . '/wblib.php';
if (!file_exists($wbLibRootFile))
{
	return;
}

include_once($wbLibRootFile);
$wbLib = new \Weeblr\Wblib\V_SH4_4233\Wblib;
$wbLib->boot();

/**
 * Legacy function. Previously was in wbLib but not compatible with having multiple instance
 * of wbLib running on the same site (as only one factory will be used depending on which
 * app register this function first.
 * This was never documented publicly and sh404SEF was the only one using it but
 * added a safety copy here, in case someone used it for a filter.
 */
if (!function_exists('wbAddHook'))
{
	/**
	 * Add a hook, identified by an id (wblib.some_name),
	 * a callback and a priority
	 *
	 * @param string   $hookName    Unique identifier for the hook
	 * @param Callable $hookHandler Callback to execute
	 * @param int      $priority    Higher priorities are executed first. Default to 100.
	 */
	function wbAddHook($hookName, $hookHandler, $priority = 100)
	{
		Factory::get()->getThe('hook')->add(
			$hookName,
			$hookHandler,
			$priority
		);
	}
}
