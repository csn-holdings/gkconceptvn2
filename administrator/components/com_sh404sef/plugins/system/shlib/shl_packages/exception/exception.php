<?php
/**
 * Shlib - programming library
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier 2020
 * @package     shlib
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     0.4.0.715
 * @date				2020-12-02
 */

// Security check to ensure this file is being included by a parent file.
defined('_JEXEC') or die;

/**
 * Base class to group our exceptions
 *
 * @author shumisha
 *
 */
class ShlException extends \Exception {

}
