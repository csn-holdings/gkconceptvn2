<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */
 
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class MobilemenuckViewMenu extends JViewLegacy {

	function display($tpl = null) {
		$input = JFactory::getApplication()->input;

		$user = JFactory::getUser();
		$authorised = ($user->authorise('core.create', 'com_mobilemenuck') || $user->authorise('core.edit', 'com_mobilemenuck') || (count($user->getAuthorisedCategories('com_mobilemenuck', 'core.create'))));

		if ($authorised !== true)
		{
			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}

		// dislay the page title
		JToolBarHelper::title(JText::_('COM_MOBILEMENUCK') . ' - ' . JText::_('CK_MENU_EDITION'), 'logo_mobilemenuck_large.png');

		// load the styles helper and the interface
		require_once MOBILEMENUCK_ADMIN_PATH . '/helpers/ckstyles.php';
		require_once MOBILEMENUCK_ADMIN_PATH . '/helpers/ckinterface.php';
		require_once MOBILEMENUCK_ADMIN_PATH . '/helpers/ckfields.php';

		$this->item = $this->get('Data');
		$this->interface = new CKInterface();
		$this->fields = new CKFields();
		$this->input = new CKInput();

		if (JFactory::getApplication()->isClient('administrator')) $this->addToolbar();
		parent::display($tpl);
	}


	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar() {
		MobilemenuckHelper::loadCkbox();

		JFactory::getApplication()->input->set('hidemainmenu', true);
		$user		= JFactory::getUser();
		$userId		= $user->get('id');
		$isNew		= ($this->item->id == 0);
//		$checkedOut	= !($this->item->checked_out == 0 || $this->item->checked_out == $userId);
		$state = $this->get('State');
		$canDo = MobilemenuckHelper::getActions();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		// For new records, check the create permission.
		if ($isNew && $user->authorise('core.create', 'com_mobilemenuck'))
		{
			$html = '<button class="btn btn-small btn-success" onclick="ckSaveMenu()">
					<span class="icon-apply icon-white"></span> ' . CKText::_('CK_SAVE') . '
					</button>';
			$bar->appendButton('Custom', $html);
//			JToolbarHelper::apply('style.apply');
//			JToolbarHelper::save('style.save');
			// JToolbarHelper::save2new('page.save2new');
			JToolbarHelper::cancel('menus.cancel');
		} else
		{
			// Can't save the record if it's checked out.
//			if (!$checkedOut)
//			{
				// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
				if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_by == $userId))
				{
//					JToolbarHelper::apply('style.apply');
//					JToolbarHelper::save('style.save');
//					JToolbarHelper::custom('style.restore', 'archive', 'archive', 'CK_RESTORE', false);
					// We can save this record, but check the create permission to see if we can return to make a new one.
//					if ($canDo->get('core.create'))
//					{
						// JToolbarHelper::save2new('page.save2new');
//					}
					$html = '<button class="btn btn-small btn-success" onclick="ckSaveMenu()">
					<span class="icon-apply icon-white"></span> ' . CKText::_('CK_SAVE') . '
					</button>';
					$bar->appendButton('Custom', $html);
		//			JToolbarHelper::apply('style.apply');
		//			JToolbarHelper::save('style.save');
					// JToolbarHelper::save2new('page.save2new');
					JToolbarHelper::cancel('menus.cancel', 'JTOOLBAR_CLOSE');
				}
//			}

			// If checked out, we can still save
			if ($canDo->get('core.create'))
			{
				// JToolbarHelper::save2copy('page.save2copy');
			}

//			JToolbarHelper::cancel('style.cancel', 'JTOOLBAR_CLOSE');
		}
	}

	protected function render_previewmenu() {
		?>
		<div id="mobilemenuck-preview-mobile-bar" class="mobilemenuck-bar" style="display:block;">
			<span class="mobilemenuck-bar-title">Menu</span>
			<div class="mobilemenuck-bar-button" onclick="jQuery('#mobilemenuck-preview-mobile-bar').hide();jQuery('#mobilemenuck-preview-mobile').show();">&#x2261;</div>
		</div>
		<div id="mobilemenuck-preview-mobile" class="mobilemenuck" style="position: relative !important; z-index: 100000; display: none;">
				<div class="mobilemenuck-topbar">
					<span class="mobilemenuck-title">Menu</span>
					<span class="mobilemenuck-button" onclick="jQuery('#mobilemenuck-preview-mobile').hide();jQuery('#mobilemenuck-preview-mobile-bar').show();">×</span>
				</div>
				<div class="mobilemenuck-item">
					<div class="maximenuck first level1 ">
						<a href="javascript:void(0)">
							<span class="mobiletextck">Lorem</span>
						</a>
					</div>
				</div>
				<div class="mobilemenuck-item">
					<div class="maximenuck parent level1">
						<a href="javascript:void(0)">
							<span class="mobiletextck">Ipsum</span>
						</a>
					</div>
					<div class="mobilemenuck-submenu">
						<div class="mobilemenuck-item">
							<div class="maximenuck parent first level2 ">
								<a href="javascript:void(0)">
									<span class="mobiletextck">Dolor sit</span>
								</a>
							</div>
							<div class="mobilemenuck-submenu">
								<div class="mobilemenuck-item">
									<div class="maximenuck first level3 ">
										<a href="javascript:void(0)">
											<span class="mobiletextck">Consectetur</span>
										</a>
									</div>
								</div>
								<div class="mobilemenuck-item">
									<div class="maximenuck last level3 ">
										<a href="javascript:void(0)">
											<span class="mobiletextck">Adipiscing</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="mobilemenuck-item">
							<div class="maximenuck parent level2 ">
								<a href="javascript:void(0)">
									<span class="mobiletextck">Sed maximus</span>
								</a>
							</div>
							<div class="mobilemenuck-submenu">
								<div class="mobilemenuck-item">
									<div class="maximenuck first level3 ">
										<a href="javascript:void(0)">
											<span class="mobiletextck">Vivamus</span>
										</a>
									</div>
								</div>
								<div class="mobilemenuck-item">
									<div class="maximenuck level3 ">
										<a href="javascript:void(0)">
											<span class="mobiletextck">Fusce porta</span>
										</a>
									</div>
								</div>
								<div class="mobilemenuck-item">
									<div class="maximenuck last level3 ">
										<a href="javascript:void(0)">
											<span class="mobiletextck">Pellentesque</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="mobilemenuck-item">
					<div class="maximenuck parent level1 accordionmobileck">
						<a href="javascript:void(0)">
							<span class="mobiletextck">Maecenas</span>
						</a>
						<div class="mobilemenuck-togglericon" onclick="jQuery(this).parent().toggleClass('open')"></div>
					</div>
					<div class="mobilemenuck-submenu">
						<div class="mobilemenuck-item">
							<div class="maximenuck parent first level2 accordionmobileck">
								<a href="javascript:void(0)">
									<span class="mobiletextck">Vel convallis</span>
								</a>
								<div class="mobilemenuck-togglericon" onclick="jQuery(this).parent().toggleClass('open')"></div>
							</div>
							<div class="mobilemenuck-submenu">
								<div class="mobilemenuck-item">
									<div class="maximenuck first level3 ">
									<a href="javascript:void(0)">
									<span class="mobiletextck">Facilisis</span>
									</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
	}
}
