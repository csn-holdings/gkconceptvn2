<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2018. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */
 
// no direct access
defined('_JEXEC') or die;
MobilemenuckHelper::loadCkbox();
$doc = JFactory::getDocument();
$doc->addStylesheet(MOBILEMENUCK_MEDIA_URI . '/assets/ckframework.css');
$doc->addScript(MOBILEMENUCK_MEDIA_URI . '/assets/jquery-uick-custom.js');
$doc->addScript(MOBILEMENUCK_MEDIA_URI . '/assets/admin.js');

// vars
$input	= JFactory::getApplication()->input;
$modal = $input->get('layout', '') == 'modal' ? true : false;

$user = JFactory::getUser();
$userId = $user->get('id');

// for ordering
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$isModal = $input->get('layout', '', 'string') == 'modal';
$function = $input->get('returnFunc', 'ckMobilemenuSelectStyle', 'string');
$appendUrl = $isModal ? '&layout=modal&tmpl=component' : '';
$style = $isModal ? 'style="padding:10px;"' : '';
?>
<div class="ckadminarea">
<script>
function ckMobilemenuSelectStyle(styleid, name, close) {
	var id = jQuery(jQuery('.editing').parents('.ckrow')[0]).attr('data-id');
	ckSetStyle(id, styleid, 'ckUpdateStyle');
}

function ckMobilemenuSelectCustomMenuStyle(styleid, name, close) {
	var id = jQuery(jQuery('.editing').parents('.ckrow')[0]).attr('data-id');
	ckSetCustomMenuStyle(id, styleid, 'ckUpdateStyle');
}

function ckRemoveStyle() {
	var id = jQuery(jQuery('.editing').parents('.ckrow')[0]).attr('data-id');
	ckSetStyle(id, 0, 'ckUpdateStyle');
}

function ckUpdateStyle(id, styleid, name) {
	if (! id) id = jQuery(jQuery('.editing').parents('.ckrow')[0]).attr('data-id');
	if (styleid > 0) {
		jQuery('.ckrow[data-id="' + styleid + '"]').find('.styleid').text('ID ' + styleid);
		var namehtml = '<a data-id="'+ styleid +'" onclick="if (jQuery(this).attr(\'data-id\'))  { CKBox.open({handler:\'iframe\', fullscreen: true, url:\'index.php?option=com_mobilemenuck&amp;view=style&amp;layout=modal&amp;tmpl=component&amp;id=\' + jQuery(this).attr(\'data-id\')}) }" href="#">' + name + '</a>';
		jQuery('.ckrow[data-id="' + id + '"]').find('.stylename').html(namehtml).parent().attr('data-id', styleid);
		jQuery('.ckrow[data-id="' + id + '"]').removeClass('editing');
	} else {
		jQuery('.ckrow[data-id="' + id + '"]').find('.styleid').text('<?php echo JText::_('CK_NONE') ?>');
		jQuery('.ckrow[data-id="' + id + '"]').find('.stylename').text('').parent().attr('data-id', '0');
		jQuery('.ckrow[data-id="' + id + '"]').removeClass('editing');
	}
	CKBox.close();
}

function ckUpdateMobileState(id, state) {
	if (! id) id = jQuery(jQuery('.editing').parents('.ckrow')[0]).attr('data-id');
	var stateclass = state == '1' ? 'publish' : 'unpublish';
	jQuery('.ckrow[data-id="' + id + '"]').find('.ckstate').removeClass('icon-publish').removeClass('icon-unpublish').addClass('icon-' + stateclass).attr('data-state', state);
	jQuery('.ckrow[data-id="' + id + '"]').removeClass('editing');
	CKBox.close();
}

// needed for ajax
var CKTOKEN = '<?php echo MobilemenuckHelper::getToken() ?>=1';
</script>
<div class="ckinterface">
	<div>
		<div class="ckinterfacetablink current" data-tab="tab_modules" data-group="main"><?php echo JText::_('CK_EXISTING_MODULES'); ?></div>
		<div class="ckinterfacetablink" data-tab="tab_custom_menus" data-group="main"><?php echo JText::_('CK_CUSTOM_MENUS'); ?></div>
		<div class="ckclr" style="clear:both;"></div>
	</div>
	<div class="ckinterfacetab current" id="tab_modules" data-group="main">
		<form action="<?php echo JRoute::_('index.php?option=com_mobilemenuck&view=items'.$appendUrl); ?>" method="post" name="adminForm" id="adminForm" <?php echo $style ?>>
			<div id="filter-bar" class="ckbutton-toolbar">
				<div class="filter-search ckbutton-group pull-left">
					<label for="filter_search" class="element-invisible"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
					<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" class="hasTooltip" title="" />
				</div>
				<div class="ckbutton-group pull-left hidden-phone">
					<button type="submit" class="ckbutton hasTooltip" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
					<button type="button" class="ckbutton hasTooltip" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.getElementById('filter_search').value = '';
							this.form.submit();"><i class="icon-remove"></i></button>
				</div>
				<div class="ckbutton-group pull-right hidden-phone">
					<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
				</div>
			</div>
			<div class="ckclr" style="clear:both;"></div>
			<table class="cktable cktable-striped cktable-hover cktable-single-bordered" id="ckitemslist">
				<thead>
					<tr>
						<?php if (! $isModal) { ?>
						<th width="1%" style="display:none;">
							<input type="checkbox" name="checkall-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" value="" onclick="Joomla.checkAll(this)" />
						</th>
						<?php } ?>
						<th width="15%" class='left'>
							<?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'a.module', $listDirn, $listOrder); ?>
						</th>
						<th width="5%" class="nowrap">
							<?php echo JHtml::_('grid.sort', 'CK_TYPE', 'a.module', $listDirn, $listOrder); ?>
						</th>
						<th width="10%" class="nowrap">
							<?php echo JHtml::_('grid.sort', 'CK_POSITION', 'a.position', $listDirn, $listOrder); ?>
						</th>
						<th width="1%" class="nowrap center">
							<?php echo JHtml::_('grid.sort', 'JSTATUS', 'a.published', $listDirn, $listOrder); ?>
						</th>
						<th width="1%" class="nowrap center">
							<?php echo JText::_('CK_MOBILE_ENABLED'); ?>
						</th>
						<th width="20%" class="nowrap">
							<?php echo JText::_('CK_STYLE'); ?>
						</th>
						<th width="15%" class="nowrap">
							<?php echo JText::_('CK_MERGED_WITH'); ?>
						</th>
						<th width="1%" class="nowrap">
							<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="10">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php
					foreach ($this->items as $i => $item) :
						$item->params = new JRegistry($item->params);
						$style = $item->params->get('mobilemenuck_styles');
						$styleName = \Mobilemenuck\Helper::getStyleNameById($style);
						$mergeid = $item->params->get('mobilemenuck_merge');
						$stylespanstyle = $mergeid ? 'display: none;' : 'display: inline-block;';
						$link = 'index.php?option=com_modules&task=module.edit&id=' . $item->id;
		//				$stylelink = 'index.php?option=com_mobilemenuck&view=style&layout=modal&tmpl=component&id=' . $style;
						?>
						<tr class="row<?php echo $i % 2; ?> ckrow" data-id="<?php echo (int) $item->id; ?>">
							<?php if (! $isModal) { ?>
							<td class="center" style="display:none;">
								<?php echo JHtml::_('grid.id', $i, $item->id); ?>
							</td>
							<?php } ?>

							<td>
								<a href="<?php echo JUri::root(true) . '/administrator/' . $link ?>"><?php echo $item->title; ?></a>
							</td>
							<td class="">
								<span class="cklabel">
									<?php echo $item->module; ?>
								</span>
							</td>
							<td class="">
								<span class="cklabel">
									<?php echo $item->position ? $item->position : '::' . JText::_('CK_NONE') . '::'; ?>
								</span>
							</td>
							<td class="center">
								<div class="ckbutton-group">
									<span class="icon-<?php echo ($item->published ? '' : 'un'); ?>publish" style="font-size:12px;"></span>
									<?php //echo JHtml::_('modules.state', $item->published, $i, false, 'cb'); 

									?>
								</div>
							</td>
							<td class="center">
								<div class="">
									<span data-id="<?php echo (int) $item->id; ?>" data-state="<?php echo ($item->params->get('mobilemenuck_enable', '0') ? '1' : '0'); ?>" class="ckstate ckbutton icon-<?php echo ($item->params->get('mobilemenuck_enable', '0') ? '' : 'un'); ?>publish input-group-text" style="font-size:12px;" onclick="ckToggleMobileState(jQuery(this).attr('data-id'), jQuery(this).attr('data-state'))"></span>
								</div>
							</td>
							<td class="style">
								<?php if ($style) : ?>
								<a style="<?php echo $stylespanstyle ?>" data-id="<?php echo $style ?>" onclick="if (jQuery(this).attr('data-id'))  { CKBox.open({handler:'iframe', fullscreen: true, url:'<?php echo JUri::root(true) ?>/administrator/index.php?option=com_mobilemenuck&view=style&layout=modal&tmpl=component&id=' + jQuery(this).attr('data-id')}) }" href="#"><span class="stylename"><?php echo $styleName; ?></span></a>
								<span class="cklabel styleid" style="<?php echo $stylespanstyle ?>">
								ID <?php echo $style; ?>
								</span>
								<?php else : ?>
								<span style="<?php echo $stylespanstyle ?>" class="stylename"></span>
								<span style="<?php echo $stylespanstyle ?>" class="cklabel styleid">
								<?php echo JText::_('JNONE'); ?>
								</span>
								<?php endif; ?>
							
								<span style="<?php echo $stylespanstyle ?>" class="ckbutton" onclick="ckAddEditingClass(this);CKBox.open({url: 'index.php?option=com_mobilemenuck&view=styles&tmpl=component&layout=modal&returnFunc=ckMobilemenuSelectStyle'})"><?php echo JText::_('CK_SELECT') ?></span>
								<span style="<?php echo $stylespanstyle ?>" class="ckbutton" onclick="ckAddEditingClass(this);ckRemoveStyle()"><?php echo JText::_('CK_REMOVE') ?></span>
							</td>
							<td class="">
								<?php if ($mergeid) : 
								$mergerorderstyle = 'display: inline-block;';
								$module = \Mobilemenuck\Helper::getModuleById($mergeid);
								?>
								<span class="mergename"><?php echo $module->title; ?></span>
								<span class="cklabel mergeid">
								ID <?php echo $mergeid; ?>
								</span>
								<?php else : 
									$mergerorderstyle = 'display: none;';
									?>
									<span class="mergename"></span>
									<span class="cklabel mergeid"></span>
								<?php endif; ?>
								<span class=""><input type="text" class="mergeorder hasTooltip" style="width: 20px;margin-bottom: 0;<?php echo $mergerorderstyle ?>" title="<?php echo JText::_('CK_ORDER_VALUE') ?>" onchange="ckSetMergeOrder(this, this.value)" value="<?php echo $item->params->get('mobilemenuck_mergeorder'); ?>"/></span>
								<span class="ckbutton" onclick="ckAddEditingClass(this);CKBox.open({url: 'index.php?option=com_mobilemenuck&view=items&tmpl=component&layout=select&returnFunc=ckSetMerge'})"><?php echo JText::_('CK_SELECT') ?></span>
								<span class="ckbutton" onclick="ckAddEditingClass(this);ckRemoveMerge()"><?php echo JText::_('CK_REMOVE') ?></span>
							</td>

							<?php if (isset($this->items[0]->id)) {
								?>
								<td class="center">
								<?php echo (int) $item->id; ?>
								</td>
							<?php } ?>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
			<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</form>
	</div>
	<div class="ckinterfacetab" id="tab_custom_menus" data-group="main">
		<div class="toolbar">
			<a href="<?php echo MOBILEMENUCK_ADMIN_URI ?>&view=menu&layout=edit&id=0" class="ckbutton"><span class="dashicons dashicons-welcome-add-page"></span><?php echo JText::_('Add new') ?></a>
			<a href="javascript:void(0)" class="ckbutton" onclick="CKsubmitform('menu.edit')"><span class="dashicons dashicons-welcome-write-blog"></span><?php echo JText::_('Edit') ?></a>
			<a href="javascript:void(0)" class="ckbutton" onclick="CKsubmitform('menu.copy')"><span class="dashicons dashicons-admin-page"></span><?php echo JText::_('Copy') ?></a>
			<a href="javascript:void(0)" class="ckbutton" onclick="CKsubmitform('menu.delete')"><span class="dashicons dashicons-trash"></span><?php echo JText::_('Delete') ?></a>
		</div>
		<form action="<?php echo JRoute::_('index.php?option=com_mobilemenuck&view=items'.$appendUrl); ?>" method="post" name="customMenusForm" id="customMenusForm" <?php echo $style ?>>
			<div class="ckclr" style="clear:both;"></div>
			<table class="cktable cktable-striped cktable-hover cktable-single-bordered" id="ckitemslist">
				<thead>
					<tr>
						<?php if (! $isModal) { ?>
						<th width="1%">
							<input type="checkbox" name="checkall-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" value="" onclick="Joomla.checkAll(this)" />
						</th>
						<?php } ?>
						<th width="10%" class='left'>
							<?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'a.module', $listDirn, $listOrder); ?>
						</th>
						<th width="1%" class="nowrap center">
							<?php echo JText::_('CK_MOBILE_ENABLED'); ?>
						</th>
						<th width="30%" class="nowrap">
							<?php echo JText::_('CK_STYLE'); ?>
						</th>
						<th width="1%" class="nowrap">
							<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($this->menus as $i => $item) :
						$item->params = new JRegistry(unserialize($item->params));
						$style = $item->style;
						$styleName = \Mobilemenuck\Helper::getStyleNameById($style);
						$mergeid = $item->params->get('mobilemenuck_merge');
						$stylespanstyle = $mergeid ? 'display: none;' : 'display: inline-block;';
						$link = MOBILEMENUCK_ADMIN_URI . '&task=menu.edit&id=' . $item->id;
		//				$stylelink = 'index.php?option=com_mobilemenuck&view=style&layout=modal&tmpl=component&id=' . $style;
						?>
						<tr class="row<?php echo $i % 2; ?> ckrow" data-id="<?php echo (int) $item->id; ?>">
							<?php if (! $isModal) { ?>
							<td class="center">
								<?php echo JHtml::_('grid.id', $i, $item->id); ?>
							</td>
							<?php } ?>

							<td>
								<a href="<?php echo $link ?>"><?php echo (isset($item->name) ? $item->name : 'no name'); ?></a>
							</td>
							<td class="center">
								<div class="">
									<span data-id="<?php echo (int) $item->id; ?>" data-state="<?php echo ($item->state ? '1' : '0'); ?>" class="ckstate icon-<?php echo ($item->state ? '' : 'un'); ?>publish" style="font-size:12px;" ></span>
								</div>
							</td>
							<td class="style">
								<?php if ($style) : ?>
								<a style="<?php echo $stylespanstyle ?>" data-id="<?php echo $style ?>" onclick="if (jQuery(this).attr('data-id'))  { CKBox.open({handler:'iframe', fullscreen: true, url:'<?php echo JUri::root(true) ?>/administrator/index.php?option=com_mobilemenuck&view=style&layout=modal&tmpl=component&id=' + jQuery(this).attr('data-id')}) }" href="#"><span class="stylename"><?php echo $styleName; ?></span></a>
								<span class="cklabel styleid" style="<?php echo $stylespanstyle ?>">
								ID <?php echo $style; ?>
								</span>
								<?php else : ?>
								<span style="<?php echo $stylespanstyle ?>" class="stylename"></span>
								<span style="<?php echo $stylespanstyle ?>" class="cklabel styleid">
								<?php echo JText::_('JNONE'); ?>
								</span>
								<?php endif; ?>
							
								<span style="<?php echo $stylespanstyle ?>" class="ckbutton" onclick="ckAddEditingClass(this);CKBox.open({url: 'index.php?option=com_mobilemenuck&view=styles&tmpl=component&layout=modal&returnFunc=ckMobilemenuSelectCustomMenuStyle'})"><?php echo JText::_('CK_SELECT') ?></span>
								<span style="<?php echo $stylespanstyle ?>" class="ckbutton" onclick="ckAddEditingClass(this);ckRemoveStyle()"><?php echo JText::_('CK_REMOVE') ?></span>
							</td>

							<?php if (isset($this->items[0]->id)) {
								?>
								<td class="center">
								<?php echo (int) $item->id; ?>
								</td>
							<?php } ?>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
			<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</form>
	</div>
</div>
<script>
document.addEventListener('DOMContentLoaded', function() {
	ckMakeItemsSortable();
	ckInitInterfaceTabs();
});

function CKsubmitform(action) {
	if (action == 'menu.delete') {
		var c = confirm('<?php echo CKText::_('Are you sure to want to delete ?') ?>');
		if (c == false) return;
	}

	var form = document.getElementById('customMenusForm');
	// var selector = document.getElementsByName('action')[0];
	var selector = form.task;
	selector.value = action;
	var selection = CKgetcheckedItems();
	if (! selection.length) {
		alert('Please select an item');
		return;
	}
	form.submit();
}

function CKgetcheckedItems() {
	var form = document.getElementById('customMenusForm');
	var list = form.querySelectorAll('[name="cid[]"]');
	var results = [];
	for(var i = 0; i < list.length; i++){
		list[i].checked ? results.push(list[i]):"";
	}
	return results;
}
</script>