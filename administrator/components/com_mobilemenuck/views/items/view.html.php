<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * View class for a list of Maximenuck.
 */
class MobilemenuckViewItems extends JViewLegacy {

	protected $items;

	protected $pagination;

	protected $state;

	protected $menus;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		// fix the name column issue in 1.0.8
		$this->updateTableMenu();

		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->menus = $this->get('Menus');
		$this->pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
	// var_dump($errors);die;
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		// require_once JPATH_COMPONENT . '/helpers/mobilemenuck.php';

		// Load the left sidebar.
		MobilemenuckHelper::addSubmenu(JFactory::getApplication()->input->get('view', 'items'));
		// Load the title
		JToolBarHelper::title(JText::_('COM_MOBILEMENUCK') . ' - ' . JText::_('CK_MENUS_LIST'), 'logo_mobilemenuck_large.png');
		$canDo = MobilemenuckHelper::getActions();
		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_mobilemenuck');
		}

		parent::display($tpl);
	}

	private function updateTableMenu() {
		$db = JFactory::getDbo();
		$tableName = '#__mobilemenuck_menus';
		$columnsList = $db->getTableColumns($tableName);
		if (! isset($columnsList['name'])) {
			$query = 'ALTER TABLE `' . $tableName . '` CHANGE title name varchar(50);';
			$db->setQuery($query);
			if (! $db->execute($query)) {
				echo '<p class="alert alert-danger">Error during table ' . $tableName . ' update process ! Name column not updated</p>';
			} else {
				echo '<p class="alert alert-success">Table ' . $tableName . ' updated with success !</p>';
			}
		}
	}
	/**
	 * Get the list of mobilemenuck modules
	 */
	/*private function getItems() {
		// Create a new query object.
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select('a.id, a.title, a.state, a.params');
		$query->from($db->quoteName('#__fields') . ' AS a');
		$query->where('(a.state IN (0, 1))');
//		$search = $db->Quote('%mobilemenuck_enable":"1%');
//		$query->where('(' . 'a.params LIKE ' . $search . ' )');

		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObjectList();

		return $results;
	}*/
}
