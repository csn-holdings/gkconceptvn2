<?php
/**
 * @name		Mobile Menu CK
 * @package		com_mobilemenuck
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */

defined('_JEXEC') or die;

$input = JFactory::getApplication()->input;
$imagespath = MOBILEMENUCK_MEDIA_URI .'/images/';

JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
$doc->addStylesheet(MOBILEMENUCK_MEDIA_URI . '/assets/ckframework.css');
$doc->addStylesheet(MOBILEMENUCK_MEDIA_URI . '/assets/ckbox.css');
$doc->addStylesheet(MOBILEMENUCK_MEDIA_URI . '/assets/ckpopup.css');
$doc->addStylesheet(MOBILEMENUCK_MEDIA_URI . '/assets/admin.css');
$doc->addStylesheet(MOBILEMENUCK_MEDIA_URI . '/assets/font-awesome.min.css');

$doc->addScript(MOBILEMENUCK_MEDIA_URI . '/assets/jscolor/jscolor.js');
$doc->addScript(MOBILEMENUCK_MEDIA_URI . '/assets/ckbox.js');
$doc->addScript(MOBILEMENUCK_MEDIA_URI . '/assets/ckframework.js');
$doc->addScript(MOBILEMENUCK_MEDIA_URI . '/assets/admin.js');

$popupclass = ($input->get('layout', '', 'string') === 'modal') ? 'ckpopupwizard' : '';

// Load the JS strings
JText::script('CK_DOWNLOAD');
JText::script('CK_PLEASE_GIVE_NAME');
JText::script('CK_CONFIRM_SAVE_AND_COPY');
?>
<style>
#stylescontainerleft, #stylescontainerright {
	float :left;
	width: 50%;
	padding: 10px;
	box-sizing: border-box;
}

#previewarea {
	padding: 10px;
	max-height: calc(100% - 50px);
	overflow: auto;
}

body.contentpane {
	padding: 0;
}
</style>
<div id="ckpopupstyleswizard" class="<?php echo $popupclass; ?>">
	<input type="hidden" id="initialname" name="initialname" value="<?php echo $this->item->name; ?>" />
	<input type="hidden" id="id" name="id" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" id="layoutcss" name="layoutcss" value="" />
	<input type="hidden" id="params" name="params" value="<?php echo htmlspecialchars($this->item->params); ?>" />
	<input type="hidden" id="returnFunc" name="returnFunc" value="<?php echo htmlspecialchars($input->get('returnFunc', '', 'cmd')); ?>" />
	<?php if ($input->get('layout', '', 'string') === 'modal') {
		echo $this->loadTemplate('mainmenu'); 
	} ?>
	<div id="ckpopupstyleswizardgfont"></div>
<div id="stylescontainer" style="min-width: 1030px;" class="animateck">
	<div id="stylescontainerleft" class="ckinterface">
		<label for="name" style="display: inline-block;"><?php echo JText::_('CK_NAME'); ?></label>
		<input type="text" id="name" name="name" value="<?php echo $this->item->name; ?>" />
		<div id="styleswizard_options" class="styleswizard">
			<div class="menulink current" tab="tab_menubar" data-group="main"><?php echo JText::_('CK_MENU_BAR'); ?></div>
			<div class="menulink" tab="tab_mobilemenu" data-group="main"><?php echo JText::_('CK_MOBILE_MENU'); ?></div>
			<div class="menulink" tab="tab_customcss" data-group="main"><?php echo JText::_('CK_CUSTOM_CSS'); ?></div>
			<div class="menulink" tab="tab_presets" data-group="main"><?php echo JText::_('CK_PRESETS'); ?></div>
			<div class="clr"></div>
			<div class="tab current" id="tab_menubar" data-group="main">
				<div class="menulink current" data-group="menubar" tab="tab_menubar_main"><?php echo JText::_('CK_MENU_BAR'); ?></div>
				<div class="menulink" data-group="menubar" tab="tab_menubar_button"><?php echo JText::_('CK_MENU_BUTTON'); ?></div>
				<div class="clr"></div>
				<div class="tab current menustylescustom" id="tab_menubar_main" data-group="menubar" data-prefix="menubar" data-rule="[menu-bar]">
					<div class="ckheading"><?php echo JText::_('CK_APPEARANCE_LABEL'); ?></div>
					<?php 
					echo $this->interface->createBackgroundColor('menubar');
					echo $this->interface->createBackgroundImage('menubar');
					echo $this->interface->createBorders('menubar');
					echo $this->interface->createRoundedCorners('menubar');
					echo $this->interface->createShadow('menubar');
					echo $this->interface->createMargins('menubar');
					?>
					<div class="ckheading"><?php echo JText::_('CK_TEXT_LABEL'); ?></div>
					<?php
					echo $this->interface->createText('menubar');
					?>
				</div>
				<div class="tab menustylescustom" id="tab_menubar_button" data-group="menubar" data-prefix="menubarbutton" data-rule="[menu-bar-button]">
					<div class="ckrow">
						<div class="ckbutton-group">
							<label for="menubarbuttoncontent"><?php echo JText::_('CK_BUTTON_CONTENT_LABEL'); ?></label>
							<img class="iconck" src="<?php echo $this->interface->imagespath ?>/style_edit.png" />
							<input class="ckbutton menubarbutton" type="radio" value="hamburger" id="menubarbuttoncontenthamburger" name="menubarbuttoncontent" />
							<label class="ckbutton"  for="menubarbuttoncontenthamburger" style="width:auto;font-size:24px;padding-left:10px;padding-right:10px;">&#x2261;
							</label><input class="ckbutton menubarbutton customtextswitcher" type="radio" value="custom" id="menubarbuttoncontentcustom" name="menubarbuttoncontent" />
							<label class="ckbutton"  for="menubarbuttoncontentcustom" style="width:auto;"><?php echo JText::_('CK_CUSTOM_TEXT'); ?></label>
							<input class="menubarbutton customtextvalue" type="text" value="" id="menubarbuttoncontentcustomtext" name="menubarbuttoncontentcustomtext" />
							<input class="ckbutton menubarbutton" type="radio" value="none" id="menubarbuttoncontentnone" name="menubarbuttoncontent" />
							<label class="ckbutton"  for="menubarbuttoncontentnone" style="width:auto;"><?php echo JText::_('CK_NONE'); ?></label>
						</div>
					</div>
					<div class="ckheading"><?php echo JText::_('CK_DIMENSIONS_LABEL'); ?></div>
					<?php
					echo $this->interface->createDimensions('menubarbutton');
					?>
					<div class="ckheading"><?php echo JText::_('CK_APPEARANCE_LABEL'); ?></div>
					<?php 
					echo $this->interface->createBackgroundColor('menubarbutton');
					echo $this->interface->createBackgroundImage('menubarbutton');
					echo $this->interface->createBorders('menubarbutton');
					echo $this->interface->createRoundedCorners('menubarbutton');
					echo $this->interface->createShadow('menubarbutton');
					echo $this->interface->createMargins('menubarbutton');
					?>
					<div class="ckheading"><?php echo JText::_('CK_TEXT_LABEL'); ?></div>
					<?php
					echo $this->interface->createText('menubarbutton');
					?>
				</div>
			</div>
			<div class="tab" id="tab_mobilemenu" data-group="main">
				<div class="menulink current" data-group="mobilemenu" tab="tab_menu"><?php echo JText::_('CK_MENU'); ?></div>
				<div class="menulink" data-group="mobilemenu" tab="tab_topbar"><?php echo JText::_('CK_BAR'); ?></div>
				<div class="menulink" data-group="mobilemenu" tab="tab_topbarbutton"><?php echo JText::_('CK_BUTTON'); ?></div>
				<div class="menulink" data-group="mobilemenu" tab="tab_level1menuitem"><?php echo JText::_('CK_LEVEL1_MENUITEMS'); ?></div>
				<div class="menulink" data-group="mobilemenu" tab="tab_level2menuitem"><?php echo JText::_('CK_LEVEL2_MENUITEMS'); ?></div>
				<div class="menulink" data-group="mobilemenu" tab="tab_level3menuitem"><?php echo JText::_('CK_LEVEL3_MENUITEMS'); ?></div>
				<div class="menulink" data-group="mobilemenu" tab="tab_togglericon"><?php echo JText::_('CK_ACCORDION_ICONS'); ?></div>
				<div class="clr"></div>
				
				<div class="tab current menustylescustom" id="tab_menu" data-group="mobilemenu" data-prefix="menu" data-rule="[menu]">
					<div class="ckheading"><?php echo JText::_('CK_APPEARANCE_LABEL'); ?></div>
					<?php 
					echo $this->interface->createBackgroundColor('menu');
					echo $this->interface->createBackgroundImage('menu');
					echo $this->interface->createBorders('menu');
					echo $this->interface->createRoundedCorners('menu');
					echo $this->interface->createShadow('menu');
					echo $this->interface->createMargins('menu');
					?>
					<div class="ckheading"><?php echo JText::_('CK_TEXT_LABEL'); ?></div>
					<?php
					echo $this->interface->createText('menu');
					?>
				</div>
				<div class="tab menustylescustom" id="tab_topbar" data-group="mobilemenu" data-prefix="topbar" data-rule="[menu-topbar]">
					<div class="ckheading"><?php echo JText::_('CK_DIMENSIONS_LABEL'); ?></div>
					<div class="ckrow">
						<label for="topbarheight"><?php echo JText::_('CK_HEIGHT_LABEL'); ?></label>
						<img class="iconck" src="<?php echo $this->interface->imagespath ?>/height.png" />
						<input type="text" id="topbarheight" name="topbarheight" class="cktip topbar" title="<?php echo JText::_('CK_HEIGHT_DESC'); ?>"/>
					</div>
					<div class="ckheading"><?php echo JText::_('CK_APPEARANCE_LABEL'); ?></div>
					<?php 
					echo $this->interface->createBackgroundColor('topbar');
					echo $this->interface->createBackgroundImage('topbar');
					echo $this->interface->createBorders('topbar');
					echo $this->interface->createRoundedCorners('topbar');
					echo $this->interface->createShadow('topbar');
					echo $this->interface->createMargins('topbar');
					?>
					<div class="ckheading"><?php echo JText::_('CK_TEXT_LABEL'); ?></div>
					<?php
					echo $this->interface->createText('topbar');
					?>
				</div>
				
				<div class="tab menustylescustom" id="tab_topbarbutton" data-group="mobilemenu" data-prefix="topbarbutton" data-rule="[menu-topbar-button]">

					<div class="ckrow">
						<div class="ckbutton-group">
							<label for="topbarbuttoncontent"><?php echo JText::_('CK_BUTTON_CONTENT_LABEL'); ?></label>
							<img class="iconck" src="<?php echo $this->interface->imagespath ?>/style_edit.png" />
							<input class="ckbutton topbarbutton" type="radio" value="close" id="topbarbuttoncontenthamburger" name="topbarbuttoncontent" />
							<label class="ckbutton"  for="topbarbuttoncontenthamburger" style="width:auto;font-size:24px;padding-left:10px;padding-right:10px;">×
							</label><input class="ckbutton topbarbutton customtextswitcher" type="radio" value="custom" id="topbarbuttoncontentcustom" name="topbarbuttoncontent" />
							<label class="ckbutton"  for="topbarbuttoncontentcustom" style="width:auto;"><?php echo JText::_('CK_CUSTOM_TEXT'); ?></label>
							<input class="topbarbutton customtextvalue" type="text" value="" id="topbarbuttoncontentcustomtext" name="topbarbuttoncontentcustomtext" />
							<input class="ckbutton topbarbutton" type="radio" value="none" id="topbarbuttoncontentnone" name="topbarbuttoncontent" />
							<label class="ckbutton"  for="topbarbuttoncontentnone" style="width:auto;"><?php echo JText::_('CK_NONE'); ?></label>
						</div>
					</div>
					<div class="ckheading"><?php echo JText::_('CK_DIMENSIONS_LABEL'); ?></div>
					<?php
					echo $this->interface->createDimensions('topbarbutton');
					?>
					<div class="ckheading"><?php echo JText::_('CK_APPEARANCE_LABEL'); ?></div>
					<?php 
					echo $this->interface->createBackgroundColor('topbarbutton');
					echo $this->interface->createBackgroundImage('topbarbutton');
					echo $this->interface->createBorders('topbarbutton');
					echo $this->interface->createRoundedCorners('topbarbutton');
					echo $this->interface->createShadow('topbarbutton');
					echo $this->interface->createMargins('topbarbutton');
					?>
					<div class="ckheading"><?php echo JText::_('CK_TEXT_LABEL'); ?></div>
					<?php
					echo $this->interface->createText('topbarbutton');
					?>
				</div>
				<div class="tab menustylescustom" id="tab_level1menuitem" data-group="mobilemenu" data-prefix="level1menuitem" data-rule="[level1menuitem]">
					<div class="ckheading"><?php echo JText::_('CK_DIMENSIONS_LABEL'); ?></div>
					<div class="ckrow">
						<label for="level1menuitemheight"><?php echo JText::_('CK_HEIGHT_LABEL'); ?></label>
						<img class="iconck" src="<?php echo $this->interface->imagespath ?>/height.png" />
						<input type="text" id="level1menuitemheight" name="level1menuitemheight" class="cktip level1menuitem" title="<?php echo JText::_('CK_HEIGHT_DESC'); ?>"/>
					</div>
					<div class="ckheading"><?php echo JText::_('CK_APPEARANCE_LABEL'); ?></div>
					<?php 
					echo $this->interface->createBackgroundColor('level1menuitem');
					echo $this->interface->createBackgroundImage('level1menuitem');
					echo $this->interface->createBorders('level1menuitem');
					echo $this->interface->createRoundedCorners('level1menuitem');
					echo $this->interface->createShadow('level1menuitem');
					echo $this->interface->createMargins('level1menuitem');
					?>
					<div class="ckheading"><?php echo JText::_('CK_TEXT_LABEL'); ?></div>
					<?php
					echo $this->interface->createText('level1menuitem');
					?>
				</div>
				
				<div class="tab menustylescustom" id="tab_level2menuitem" data-group="mobilemenu" data-prefix="level2menuitem" data-rule="[level2menuitem]">
					<div class="ckheading"><?php echo JText::_('CK_DIMENSIONS_LABEL'); ?></div>
					<div class="ckrow">
						<label for="level2menuitemheight"><?php echo JText::_('CK_HEIGHT_LABEL'); ?></label>
						<img class="iconck" src="<?php echo $this->interface->imagespath ?>/height.png" />
						<input type="text" id="level2menuitemheight" name="level2menuitemheight" class="cktip level2menuitem" title="<?php echo JText::_('CK_HEIGHT_DESC'); ?>"/>
					</div>
					<div class="ckheading"><?php echo JText::_('CK_APPEARANCE_LABEL'); ?></div>
					<?php 
					echo $this->interface->createBackgroundColor('level2menuitem');
					echo $this->interface->createBackgroundImage('level2menuitem');
					echo $this->interface->createBorders('level2menuitem');
					echo $this->interface->createRoundedCorners('level2menuitem');
					echo $this->interface->createShadow('level2menuitem');
					echo $this->interface->createMargins('level2menuitem');
					?>
					<div class="ckheading"><?php echo JText::_('CK_TEXT_LABEL'); ?></div>
					<?php
					echo $this->interface->createText('level2menuitem');
					?>
				</div>
				
				<div class="tab menustylescustom" id="tab_level3menuitem" data-group="mobilemenu" data-prefix="level3menuitem" data-rule="[level3menuitem]">
					<div class="ckheading"><?php echo JText::_('CK_DIMENSIONS_LABEL'); ?></div>
					<div class="ckrow">
						<label for="level3menuitemheight"><?php echo JText::_('CK_HEIGHT_LABEL'); ?></label>
						<img class="iconck" src="<?php echo $this->interface->imagespath ?>/height.png" />
						<input type="text" id="level3menuitemheight" name="level3menuitemheight" class="cktip level3menuitem" title="<?php echo JText::_('CK_HEIGHT_DESC'); ?>"/>
					</div>
					<div class="ckheading"><?php echo JText::_('CK_APPEARANCE_LABEL'); ?></div>
					<?php 
					echo $this->interface->createBackgroundColor('level3menuitem');
					echo $this->interface->createBackgroundImage('level3menuitem');
					echo $this->interface->createBorders('level3menuitem');
					echo $this->interface->createRoundedCorners('level3menuitem');
					echo $this->interface->createShadow('level3menuitem');
					echo $this->interface->createMargins('level3menuitem');
					?>
					<div class="ckheading"><?php echo JText::_('CK_TEXT_LABEL'); ?></div>
					<?php
					echo $this->interface->createText('level3menuitem');
					?>
				</div>
				<div class="tab menustylescustom" id="tab_togglericon" data-group="mobilemenu" data-prefix="togglericon" data-rule="[togglericon]">
					<div class="ckrow">
						<div class="ckbutton-group">
							<label for="togglericoncontentclosed"><?php echo JText::_('CK_TOGGLE_CLOSED_ICON_LABEL'); ?></label>
							<img class="iconck" src="<?php echo $this->interface->imagespath ?>/style_edit.png" />
							<input class="ckbutton togglericon" type="radio" value="+" id="togglericoncontentclosedplus" name="togglericoncontentclosed" />
							<label class="ckbutton"  for="togglericoncontentclosedplus" style="width:20px;font-size:24px;">+
							</label><input class="ckbutton togglericon customtextswitcher" type="radio" value="custom" id="togglericoncontentclosedcustom" name="togglericoncontentclosed" />
							<label class="ckbutton"  for="togglericoncontentclosedcustom" style="width:auto;"><?php echo JText::_('CK_CUSTOM_TEXT'); ?></label>
							<input class="togglericon" type="text" value="" id="togglericoncontentclosedcustomtext" name="togglericoncontentclosedcustomtext" />
							<input class="ckbutton togglericon" type="radio" value="" id="togglericoncontentclosednone" name="togglericoncontentclosed" />
							<label class="ckbutton"  for="togglericoncontentclosednone" style="width:auto;"><?php echo JText::_('CK_NONE'); ?></label>
						</div>
					</div>
					<div class="ckrow">
						<div class="ckbutton-group">
							<label for="togglericoncontentopened"><?php echo JText::_('CK_TOGGLE_OPENED_ICON_LABEL'); ?></label>
							<img class="iconck" src="<?php echo $this->interface->imagespath ?>/style_edit.png" />
							<input class="ckbutton togglericon" type="radio" value="-" id="togglericoncontentopenedminus" name="togglericoncontentopened" />
							<label class="ckbutton"  for="togglericoncontentopenedminus" style="width:20px;font-size:24px;">-
							</label><input class="ckbutton togglericon customtextswitcher" type="radio" value="custom" id="togglericoncontentopenedcustom" name="togglericoncontentopened" />
							<label class="ckbutton"  for="togglericoncontentopenedcustom" style="width:auto;"><?php echo JText::_('CK_CUSTOM_TEXT'); ?></label>
							<input class="togglericon customtextvalue" type="text" value="" id="togglericoncontentopenedcustomtext" name="togglericoncontentopenedcustomtext" />
							<input class="ckbutton togglericon" type="radio" value="" id="togglericoncontentopenednone" name="togglericoncontentopened" />
							<label class="ckbutton"  for="togglericoncontentopenednone" style="width:auto;"><?php echo JText::_('CK_NONE'); ?></label>
						</div>
					</div>
					<div class="ckheading"><?php echo JText::_('CK_DIMENSIONS_LABEL'); ?></div>
					<?php
					echo $this->interface->createDimensions('togglericon');
					?>
					<div class="ckheading"><?php echo JText::_('CK_APPEARANCE_LABEL'); ?></div>
					<?php 
					echo $this->interface->createBackgroundColor('togglericon');
					echo $this->interface->createBackgroundImage('togglericon');
					echo $this->interface->createBorders('togglericon');
					echo $this->interface->createRoundedCorners('togglericon');
					echo $this->interface->createShadow('togglericon');
					echo $this->interface->createMargins('togglericon');
					?>
					<div class="ckheading"><?php echo JText::_('CK_TEXT_LABEL'); ?></div>
					<?php
					echo $this->interface->createText('togglericon');
					?>
				</div>
			</div>

			<div class="tab" id="tab_customcss" data-group="main">
				<textarea id="customcss" name="customcss" style="width:450px;height:300px;"></textarea>
			</div>
			<div class="tab" id="tab_presets" data-group="main">
				<?php include_once('default_presets.php'); ?>
			</div>
		</div>
	</div>
	<div id="stylescontainerright">
		<div id="previewarea" style="width: 300px;">
			<div class="ckstyle"></div>
			<?php $this->render_previewmenu() ?>
		</div>
	</div>
	<div style="clear:both;"></div>
</div>
<?php echo $this->loadTemplate('importexport'); ?>
</div>
<script type="text/javascript">
	var URIROOT = "<?php echo JUri::root(true); ?>";
	var URIBASE = "<?php echo JUri::base(true); ?>";
	var CKTOKEN = '<?php echo JFactory::getSession()->getFormToken() ?>';
	var CKCSSREPLACEMENT = new Object();
	<?php foreach (\Mobilemenuck\Helper::getCssReplacement() as $tag => $rep) { ?>
	CKCSSREPLACEMENT['<?php echo $tag ?>'] = '<?php echo $rep ?>';
	<?php } ?>

	jQuery(document).ready(function($){
		CKBox.initialize({});
		CKBox.assign($('a.ckmodal'), {
			parse: 'rel'
		});

		// manage the tabs
		function ckInitTabsStyles() {
			$('#styleswizard_options div.tab:not(.current)').hide();
			$('.menulink', $('#styleswizard_options')).each(function(i, tab) {
				$(tab).click(function() {
					$('#styleswizard_options div.tab[data-group="'+$(tab).attr('data-group')+'"]').hide();
					$('.menulink[data-group="'+$(tab).attr('data-group')+'"]', $('#styleswizard_options')).removeClass('current');
					if ($('#' + $(tab).attr('tab')).length)
						$('#' + $(tab).attr('tab')).show();
					$ck(this).addClass('current');
				});
			});
		}
		ckInitTabsStyles();

		// launch the preview when the user do a change
		$('#styleswizard_options input,#styleswizard_options select,#styleswizard_options textarea').change(function() {
			ckPreviewStylesparams();
		});
		// jQuery('.cktip').tooltip({"html": true,"container": "body"});
		CKApi.Tooltip('.cktip');
		ckApplyStylesparams();
		ckSetFloatingOnPreview();
		ckLoadGfontStylesheets();
	});

	var JoomlaCK = {};
	JoomlaCK.submitbutton = Joomla.submitbutton;
	Joomla.submitbutton = function(task) {
		ckSaveStylesparams();
	}
</script>
