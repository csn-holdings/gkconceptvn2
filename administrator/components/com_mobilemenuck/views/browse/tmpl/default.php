<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */

defined('_JEXEC') or die;

if (!defined('MOBILEMENUCK_MEDIA_URI'))
{
	define('MOBILEMENUCK_MEDIA_URI', JUri::root(true) . '/media/com_mobilemenuck');
}

$input = JFactory::getApplication()->input;
$imagespath = MOBILEMENUCK_MEDIA_URI .'/images/';
$type = $input->get('type', 'image', 'string');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
$doc->addStylesheet(MOBILEMENUCK_MEDIA_URI . '/assets/ckbrowse.css');
if ($type != 'folder') {
$doc->addScript(MOBILEMENUCK_MEDIA_URI . '/assets/ckbrowse.js');
}


$returnFunc = $input->get('func', 'ckSelectFile', 'cmd');
$returnField = $input->get('field', '', 'string');


switch ($type) {
	case 'video' :
		$fileicon = 'file_video.png';
		break;
	case 'audio' :
		$fileicon = 'file_audio.png';
		break;
	case 'image' :
	default :
		$fileicon = 'file_image.png';
		break;
}
?>
<div id="ckbrowse" class="clearfix">
<?php if ($type != 'folder') { ?>
<div id="ckfolderupload">
	<div class="inner">
		<div class="upload">
			<h2 class="uploadinstructions"><?php echo JText::_( 'Drop files here to upload' ); ?></h2>
			<p><?php echo JText::_( 'or Select Files' ); ?></p><input id="tck_file_upload" type="file" class="" />
		</div>
		<?php
		// $max_upload_size = wp_max_upload_size();
		// if ( ! $max_upload_size ) {
			// $max_upload_size = 0;
		// }
		?>

		<p class="max-upload-size"><?php
			// printf( __( 'Maximum upload file size: %s.' ), esc_html( size_format( $max_upload_size ) ) );
		?></p>
	</div>
</div>
<?php } ?>
<div id="ckfoldertreelist">
<?php if ($type != 'folder') { ?>
<p><?php echo JText::_('CK_BROWSE_INFOS') ?></p>
<?php } ?>
<?php
if ($type == 'folder') {
	$onclick = 'window.parent.ckGetFolder(\'' . $input->get('id', '', 'string') . '\',$ck(this).attr(\'data-path\'))';
} else {
	$onclick = 'ckShowFiles(this)';
}
$lastitem = 0;
foreach ($this->items as $i => $folder) {
	$submenustyle = '';
	$folderclass = '';
	if ($folder->level == 1) {
		$submenustyle = 'display: block;';
		$folderclass = 'ckcurrent';
	}
	?>
	<div class="ckfoldertree <?php echo $folderclass ?> <?php echo ($folder->deeper ? 'parent' : '') ?> <?php echo (count($folder->files) ? 'hasfiles' : '') ?>" data-level="<?php echo $folder->level ?>" data-path="<?php echo utf8_encode($folder->basepath) ?>">
		<?php if ($folder->level > 1) { ?><div class="ckfoldertreetoggler" onclick="ckToggleTreeSub(this)"></div><?php } ?>
		<div class="ckfoldertreename" data-path="<?php echo utf8_encode($folder->basepath) ?>" onclick="<?php echo $onclick  ?>"><img src="<?php echo $imagespath ?>folder.png" /><?php echo utf8_encode($folder->name); ?></div>
		<div class="ckfoldertreecount"><?php echo count($folder->files); ?></div>
		<?php if ($type != 'folder') { ?>
		<div class="ckfoldertreefiles">
		<?php foreach ($folder->files as $j => $file) { 
		?>
			<div class="ckfoldertreefile ckwait" data-type="<?php echo $type ?>" onclick="ckSelectFile(this)" data-path="<?php echo utf8_encode($folder->basepath) ?>" data-filename="<?php echo utf8_encode($file) ?>"><div class="ckfakeimage" data-src="<?php echo JUri::root(true) . '/' . utf8_encode($folder->basepath) . '/' . utf8_encode($file) ?>" title="<?php echo utf8_encode($file); ?>" ></div></div>
		<?php } ?>
		</div>
		<?php } ?>

	<?php
		if ($folder->deeper)
		{
			echo '<div class="cksubfolder" style="' . $submenustyle . '">';
		}
		elseif ($folder->shallower)
		{
			// The next item is shallower.
			echo '</div>'; // close ckfoldertree
			echo str_repeat('</div></div>', $folder->level_diff); // close cksubfolder + ckfoldertree
		} 
		else
		{
			// The next item is on the same level.
			echo '</div>'; // close ckfoldertree
		}
}

?>
</div>
<div id="ckfoldertreepreview">
	<div class="inner">
		<?php if ($type == 'image') { ?>
		<div id="ckfoldertreepreviewimage">
		</div>
		<?php } ?>
	</div>
</div>

</div>
<script>
var $ck = window.$ck || jQuery.noConflict();
var URIROOT = window.URIROOT || '<?php echo JUri::root(true) ?>';
var CKTOKEN = '<?php echo JSession::getFormToken() ?>';

function ckToggleTreeSub(btn) {
	var item = $ck(btn).parent();
	if (item.hasClass('ckopened')) {
		item.removeClass('ckopened');
	} else {
		item.addClass('ckopened')
		// item.find('> .cksubfolder, > .ckfoldertreefiles').css('opacity','0').animate({'opacity': '1'}, 300);
	}
}

function ckShowFiles(btn) {
	// show the image in place of divs
	var fakeImages = $ck(btn).find('~ .ckfoldertreefiles .ckfakeimage');
	if (fakeImages.length) {
		fakeImages.each(function() {
			$fakeImage = $ck(this);
			$fakeImage.after('<img src="' + $fakeImage.attr('data-src') + '" title="' + $fakeImage.attr('title') + '" />');
			$fakeImage.parent().removeClass('ckwait');
			$fakeImage.remove();
		});
	}
	// set the current state on the folder
	var item = $ck(btn).parent();
	$ck('.ckcurrent').not(btn).removeClass('ckcurrent');
	if (item.hasClass('ckcurrent')) {
		item.removeClass('ckcurrent');
	} else {
		item.addClass('ckcurrent')
	}
}

function ckSelectFile(btn) {
	try {
		if (typeof(window.parent.<?php echo $returnFunc ?>) != 'undefined') {
			window.parent.<?php echo $returnFunc ?>($ck(btn).attr('data-path') + '/' + $ck(btn).attr('data-filename'), '<?php echo $returnField ?>');
			if (typeof(window.parent.CKBox) != 'undefined') window.parent.CKBox.close();
		} else {
			alert('ERROR : The function <?php echo $returnFunc ?> is missing in the parent window. Please contact the developer');
		}
	}
	catch(err) {
		alert('ERROR : ' + err.message + '. Please contact the developper.');
	}
}

// display the images in the root folder
ckShowFiles($ck('.ckfoldertreename').first()[0]);
</script>
