<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2018. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */

// no direct access
defined('_JEXEC') or die;
// load the lightbox
MobilemenuckHelper::loadCkbox();

$doc = JFactory::getDocument();
$doc->addStylesheet(MOBILEMENUCK_MEDIA_URI . '/assets/ckframework.css');
// vars
$input	= JFactory::getApplication()->input;
$modal = $input->get('layout', '') == 'modal' ? true : false;

$user = JFactory::getUser();
$userId = $user->get('id');
// for ordering
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$isModal = $input->get('layout', '', 'string') == 'modal';
$function = $input->get('returnFunc', 'ckSelectStyle', 'string');
$appendUrl = $isModal ? '&layout=modal&tmpl=component' : '';
$style = $isModal ? 'style="padding:10px;"' : '';
?>
<div class="ckadminarea ckinterface" style="background:#fff; padding: 10px;">
<form action="<?php echo JRoute::_('index.php?option=com_mobilemenuck&view=styles'.$appendUrl); ?>" method="post" name="adminForm" id="adminForm" <?php echo $style ?>>
	<div id="filter-bar" class="ckbutton-toolbar">
		<?php if ($isModal) { ?>
			<a class="ckbutton ckbutton-small ckbutton-success" href="<?php echo JUri::root(true) ?>/administrator/index.php?option=com_mobilemenuck&view=style&layout=modal&tmpl=component&id=0&returnFunc=ckSelectStyle">
			<span class="icon-new icon-white"></span>
			<?php echo JText::_('JTOOLBAR_NEW'); ?>
			</a>
		<?php } ?>
		<div class="filter-search ckbutton-group pull-left">
			<label for="filter_search" class="element-invisible"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
			<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" class="hasTooltip" title="" />
		</div>
		<div class="ckbutton-group pull-left hidden-phone">
			<button type="submit" class="ckbutton hasTooltip" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
			<button type="button" class="ckbutton hasTooltip" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.getElementById('filter_search').value = '';
					this.form.submit();"><i class="icon-remove"></i></button>
		</div>
		<div class="ckbutton-group pull-right hidden-phone">
			<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
		<?php echo $this->pagination->getLimitBox(); ?>
		</div>
	</div>
	<table class="cktable cktable-striped cktable-single-bordered" id="itemsList">
		<thead>
			<tr>
				<?php if (! $isModal) { ?>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" value="" onclick="Joomla.checkAll(this)" />
				</th>
				<?php } ?>
				<th class='left'>
					<?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'a.name', $listDirn, $listOrder); ?>
				</th>
				<th width="1%" class="nowrap">
					<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="10">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php
			foreach ($this->items as $i => $item) :
				$link = 'index.php?option=com_mobilemenuck&view=style&layout=modal&tmpl=component&id=' . $item->id;
				$name = $item->name ? $item->name : 'style' . $item->id;
				?>
				<tr class="row<?php echo $i % 2; ?>">
					<?php if (! $isModal) { ?>
					<td class="center">
						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
					</td>
					<?php } ?>
					<td>
						<?php if ($modal) { ?>
						<a href="javascript:void(0)" onclick="window.parent.<?php echo $function ?>('<?php echo $item->id; ?>', '<?php echo $name; ?>')"><?php echo $name; ?></a>
						<?php /*<a href="<?php echo JUri::root(true) . '/administrator/' . $link ?>" class="ckbutton"><?php echo JText::_('CK_EDIT'); ?></a>*/ ?>
						<?php } else { ?>
						<a onclick="CKBox.open({handler:'iframe', fullscreen: true, url:'<?php echo JUri::root(true) . '/administrator/' . $link ?>'})" href="#"><?php echo $name; ?></a>
						<?php } ?>
					</td>
					<td class="center">
					<?php echo (int) $item->id; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
</div>