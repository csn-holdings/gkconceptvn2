<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// jimport('joomla.application.component.view');

/**
 * View class for a list of Maximenuck.
 */
class MobilemenuckViewStyles extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		// require_once JPATH_COMPONENT . '/helpers/mobilemenuck.php';

		// Load the left sidebar.
		MobilemenuckHelper::addSubmenu();

		// Load the title
		JToolBarHelper::title(JText::_('COM_MOBILEMENUCK') . ' - ' . JText::_('CK_STYLES_LIST'), 'logo_mobilemenuck_large.png');

		if (JFactory::getApplication()->isClient('administrator')) $this->addToolbar();
		parent::display($tpl);
	}

	
	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar() {
		MobilemenuckHelper::loadCkbox();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		$state = $this->get('State');
		$canDo = MobilemenuckHelper::getActions();

		//Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/style';
		if (file_exists($formPath)) {

			if ($canDo->get('core.create')) {
//				JToolBarHelper::addNew('style.add', 'JTOOLBAR_NEW');
				// Render the popup button
				$html = '<button class="btn btn-small btn-success" onclick="CKBox.open({handler:\'iframe\', fullscreen: true, url:\'' . JUri::root(true) . '/administrator/index.php?option=com_mobilemenuck&view=style&layout=modal&tmpl=component&id=0\'})">
						<span class="icon-new icon-white"></span>
						' . JText::_('JTOOLBAR_NEW') . '
						</button>';
				$bar->appendButton('Custom', $html);
			}

			if ($canDo->get('core.edit')) {
//				JToolBarHelper::editList('style.edit', 'JTOOLBAR_EDIT');
				JToolBarHelper::custom('style.copy', 'copy', 'copy', 'CK_COPY');
			}
		}

		if ($canDo->get('core.edit.state')) {

			if (isset($this->items[0]->state)) {
				JToolBarHelper::divider();
			} else {
				//If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::trash('styles.delete');
			}



			if (isset($this->items[0]->state)) {
				JToolBarHelper::divider();
			}
		}

		//Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state)) {
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
				JToolBarHelper::divider();
				JToolBarHelper::trash('styles.delete', 'JTOOLBAR_DELETE');
			} else if ($canDo->get('core.edit.state')) {
				JToolBarHelper::trash('styles.trash', 'JTOOLBAR_DELETE');
				JToolBarHelper::divider();
			}
		}

		if (file_exists(JPATH_ROOT . '/plugins/system/maximenuckmobile/maximenuckmobile.php')) {
			JToolBarHelper::custom('import.maximenuck', 'forward-2', 'forward-2', 'CK_IMPORT_FROM_MAXIMENUCK', false);
		}

		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_mobilemenuck');
		}
	}
}
