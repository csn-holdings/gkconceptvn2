<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * About View
 */
class MobilemenuckViewAbout extends JViewLegacy {

	/**
	 * About view display method
	 * @return void
	 * */
	function display($tpl = null) {
		// require_once JPATH_ADMINISTRATOR . '/components/com_mobilemenuck/helpers/mobilemenuck.php';

		$this->ckversion = MobilemenuckController::getCurrentVersion();

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the slider title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar() {
		// require_once JPATH_COMPONENT . '/helpers/mobilemenuck.php';

		// Load the left sidebar.
		MobilemenuckHelper::addSubmenu('about');

		JToolBarHelper::title(JText::_('COM_MOBILEMENUCK') . ' - ' . JText::_('CK_ABOUT') , 'mobilemenuck');
	}

}
