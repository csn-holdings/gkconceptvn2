<?php
/**
 * @name		Mobile Menu CK
 * @package		com_mobilemenuck
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */

// No direct access
defined('_JEXEC') or die;

if (!defined('MOBILEMENUCK_ADMIN_PATH'))
{
define('MOBILEMENUCK_ADMIN_PATH', JPATH_SITE . '/administrator/components/com_mobilemenuck');
}

if (!defined('MOBILEMENUCK_MEDIA_PATH'))
{
define('MOBILEMENUCK_MEDIA_PATH', JPATH_SITE . '/media/com_mobilemenuck');
}

if (!defined('MOBILEMENUCK_SITE_PATH'))
{
define('MOBILEMENUCK_SITE_PATH', JPATH_SITE . '/components/com_mobilemenuck');
}

if (!defined('MOBILEMENUCK_ADMIN_URI'))
{
define('MOBILEMENUCK_ADMIN_URI', JUri::root(true) . '/administrator/?option=com_mobilemenuck');
}

if (!defined('MOBILEMENUCK_MEDIA_URI'))
{
define('MOBILEMENUCK_MEDIA_URI', JUri::root(true) . '/media/com_mobilemenuck');
}
