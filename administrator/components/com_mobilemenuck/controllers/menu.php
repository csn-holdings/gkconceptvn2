<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */

// No direct access
defined('_JEXEC') or die;

require_once MOBILEMENUCK_ADMIN_PATH . '/controller.php';

/**
 * Style controller class.
 */
class MobilemenuckControllerMenu extends MobilemenuckController {

	function __construct() {
		parent::__construct();
	}

	public function edit() {
		$editIds = $this->input->get('cid', null, 'array');
		if (! empty($editIds)) {
			$editId = (int) $editIds[0];
		} else {
			$editId = (int) $this->input->get('id', null, 'int');
		}

		// Redirect to the edit screen.
		Mobilemenuck\CKFof::redirect(MOBILEMENUCK_ADMIN_URI . '&view=menu&layout=edit&id=' . $editId);
	}

	public function delete() {
		$app = JFactory::getApplication();
		$editIds = $this->input->get('cid', null, 'array');
		if (count($editIds)) {
			$id = (int) $editIds[0];
		} else {
			$id = (int) $this->input->get('id', null, 'int');
		}
		$model = $this->getModel('Menu', 'MobilemenuckModel');
		if ($model->delete($id)) {
			$app->enqueueMessage('Item deleted with success');
		} else {
			$app->enqueueMessage('Error : Item not deleted');
		}

		// Redirect to the edit screen.
		$this->setRedirect('index.php?option=com_mobilemenuck&view=items', false);
	}

	/**
	 * Method to save a user's profile data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public function save($key = null, $urlVar = null) {
		$app = JFactory::getApplication();
		if ($app->input->get('method','', 'cmd') == 'ajax') {
			// Check for request forgeries.
			JSession::checkToken('get') or jexit(JText::_('JINVALID_TOKEN'));
		} else {
			// Check for request forgeries.
			JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		}
		
		$task = $this->getTask();

		// Initialise variables.
		
		$model = $this->getModel('Menu', 'MobilemenuckModel');

		$appendToUrl = $app->input->get('tmpl') ? '&tmpl=' . $app->input->get('tmpl') : '';
		$layout = $app->input->get('layout') == 'modal' ? '&layout=modal' : '&layout=edit';

		// Get the user data.
		$data = $app->input->getArray($_POST);
		$data['params']['beforetext'] = $_POST['params']['beforetext'];
		$data['params']['aftertext'] = $_POST['params']['aftertext'];

		// Attempt to save the data.
		$return = $model->save($data);

		// Check for errors.
		if ($return === false) {
			// Save the data in the session.
			$app->setUserState('com_mobilemenuck.edit.menu.data', $data);

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_mobilemenuck.edit.menu.id');
			$app->enqueueMessage(JText::sprintf('Save failed', $model->getError()), 'warning');
			$this->setRedirect('index.php?option=com_mobilemenuck&view=menu&task=menu.edit'.$layout.'&id=' . $id . $appendToUrl, false);
			return false;
		}


		// Check in the profile.
		if ($return) {
			$model->checkin($return);
		}

		// Clear the profile id from the session.
		$app->setUserState('com_mobilemenuck.edit.menu.id', null);

		// Redirect to the list screen.
		$app->enqueueMessage(JText::_('Item saved successfully'));
		
		switch ($task)
		{
			case 'apply':
			default:
				// Set the record data in the session.
				// $this->holdEditId($context, $recordId);
				// $app->setUserState($context . '.data', null);
				$model->checkout($return);

				// Redirect back to the edit screen.
				$this->setRedirect('index.php?option=com_mobilemenuck&task==menu.edit&id=' . $return . $appendToUrl, false);
				break;
			case 'saveclose':
				// Clear the record id and data from the session.
				// $this->releaseEditId($context, $recordId);
				// $app->setUserState($context . '.data', null);

				// Redirect to the list screen.
				$this->setRedirect('index.php?option=com_mobilemenuck&view=items', false);
				break;
		}
		

		// Flush the data from the session.
		$app->setUserState('com_mobilemenuck.edit.page.data', null);
	}

	/**
	 * copy an existing page
	 * @return void
	 */
	function copy() {
		$model = $this->getModel('Menu', 'MobilemenuckModel');
		$input = JFactory::getApplication()->input;
		$cid = $input->get('cid', '', 'array');
//		$input->set('id', (int) $cid[0]);
		if (!$model->copy((int) $cid[0])) {
			$msg = JText::_('CK_COPY_ERROR');
			$type = 'error';
		} else {
			$msg = JText::_('CK_COPY_SUCCESS');
			$type = 'message';
		}

		$this->setRedirect('index.php?option=com_mobilemenuck&view=items', $msg, $type);
	}

	function cancel() {
		$this->setRedirect('index.php?option=com_mobilemenuck&view=items');
	}
}