<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */

// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.controllerform');

require_once JPATH_COMPONENT . '/controller.php';

class MobilemenuckControllerImport extends JControllerForm {

	public function maximenuck($key = null, $urlVar = null) {
		MobilemenuckHelper::checkToken();
		$app = JFactory::getApplication();

		// Create a new query object.
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select('params');
		$query->from('`#__extensions`');
		$query->where('`element` = \'maximenuckmobile\'');

		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$params = $db->loadResult();

		$params = new JRegistry($params);

// maximenumobile_customcss
// maximenumobile_menubarbuttoncontent
// maximenumobile_topbarbuttoncontent

		// Initialise variables.
		JLoader::register('MobilemenuckTableStyles', JPATH_COMPONENT_ADMINISTRATOR . '/tables/styles.php');
		$table = JTable::getInstance('Styles', 'MobilemenuckTable');

		$data = array();
		$data['params'] = str_replace('|qq|', '"', $params->get('maximenumobile_styles'));
		$data['name'] = 'Imported : Maximenu CK Mobile';

		if ($table->save($data) === true) {
			$message = JText::_('CK_IMPORT_SUCCESS');
			$type = 'message';
		} else {
			$message = JText::_('CK_IMPORT_ERROR');
			$type = 'error';
		}

		$app->redirect(MOBILEMENUCK_ADMIN_URI . '&view=styles', $message, $type);

	}
}