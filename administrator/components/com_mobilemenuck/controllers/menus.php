<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * Pages list controller class.
 */
class MobilemenuckControllerMenus extends JControllerAdmin {
	public function cancel() {
		// Redirect to the edit screen.
		Mobilemenuck\CKFof::redirect('index.php?option=com_mobilemenuck&view=items');
	}
}