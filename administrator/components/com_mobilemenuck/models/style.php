<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */

// No direct access.
defined('_JEXEC') or die;

use Joomla\Registry\Registry;
// jimport('joomla.application.component.modelform');
jimport('joomla.application.component.modeladmin');
jimport('joomla.event.dispatcher');
jimport('joomla.filesystem.folder');

class MobilemenuckModelStyle extends JModelAdmin {

	var $_item = null;

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState() {
		$app = JFactory::getApplication('com_mobilemenuck');

		// Load state from the request userState on edit or from the passed variable on default
		if ($app->input->get('layout', '', 'cmd') == 'edit') {
			$id = JFactory::getApplication()->getUserState('com_mobilemenuck.edit.style.id');
		} else {
			$id = $app->input->get('id', 0 , 'int');
			JFactory::getApplication()->setUserState('com_mobilemenuck.edit.style.id', $id);
		}
		$this->setState('style.id', $id);

		// Load the parameters.
		// $params = $app->getParams();
		// $this->setState('params', $params);
	}

	/**
	 * Method to get an ojbect.
	 *
	 * @param	integer	The id of the object to get.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function &getItem($id = null) {
		$app = JFactory::getApplication();
		if ($this->_item === null) {
			$this->_item = false;

			if (empty($id)) {
				$id = $app->input->get('id', 0, 'int');
			}

//			JTable::addIncludePath(MOBILEMENUCK_ADMIN_PATH . '/tables');
//			$row = JTable::getInstance('Styles', 'MobilemenuckTable');
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table->load((int)$id)) {
				// Check published state.
				if ($published = $this->getState('filter.published')) {
					if ($table->state != $published) {
						return $this->_item;
					}
				}

				// Convert the JTable to a clean JObject.
				$properties = $table->getProperties(1);
				$this->_item = \Mobilemenuck\CKFof::convertArrayToObject($properties, 'JObject');
			} elseif ($error = $table->getError()) {
				$this->setError($error);
			}
		}

		return $this->_item;
	}

	public function getTable($type = 'Styles', $prefix = 'MobilemenuckTable', $config = array()) {
		$this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_mobilemenuck.style', 'style', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData() {
		$data = $this->getData();

		return $data;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param	array		The form data.
	 * @return	mixed		The user id on success, false on failure.
	 * @since	1.6
	 */
	public function save($data) {
		$input = JFactory::getApplication()->input;
		$id = (!empty($data['id'])) ? $data['id'] : (int) $this->getState('style.id');
		$user = JFactory::getUser();
		// $data['htmlcode'] = JRequest::getVar('htmlcode', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$data['htmlcode'] = $data['htmlcode'] ? $data['htmlcode'] : $input->get('htmlcode', '', 'raw');
		$data['htmlcode'] = str_replace(JUri::root(true), "|URIROOT|", $data['htmlcode']);
		
		if (isset($data['options']) && is_array($data['options']))
		{
			$registry = new Registry;
			$registry->loadArray($data['options']);
			$data['params'] = (string) $registry;
		}

		if ($id) {
			//Check the user can edit this item
			$authorised = $user->authorise('core.edit', 'style.' . $id);
		} else {
			//Check the user can create new items in this section
			$authorised = $user->authorise('core.create', 'com_mobilemenuck');
		}

		if ($authorised !== true) {
			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}

		$table = $this->getTable();
		$table->load($data['id']);
		// if ($table->name !== $data['name']) {
			// $this->changeTemplateName($table->name, $data['name']);
		// }

		// make a backup before save
		PagebuilderckHelper::makeBackup($this->getData());

		if ($table->save($data) === true) {
			return $table->id;
		} else {
			return false;
		}
	}

	/**
	 * Method to copy a record
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function copy() {

		$row = $this->getTable();
		$cid = JFactory::getApplication()->input->get('id', '', 'array');
		$pk = isset($cid[0]) ? (int) $cid[0] : null;
		$data = $this->getItem($pk);
		$data->id = 0;

		// give the new name
		$data->name .= '(copy)';
		
		// Bind the form fields to the table
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Make sure the record is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Store the table to the database
		if (!$row->store()) {
			$this->setError($row->getErrorMsg());
			return false;
		}

		// $this->setId($row->id);

		return true;
	}

	

}