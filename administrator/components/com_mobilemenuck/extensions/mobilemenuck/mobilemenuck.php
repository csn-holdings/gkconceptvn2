<?php
/**
 * @name		Mobile Menu CK
 * @copyright	Copyright (C) 2018. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - https://www.template-creator.com - https://www.joomlack.fr
 */

defined('JPATH_BASE') or die;

/*
 * Module Chrome function specific to encapsulate the module
 */ 
// function modChrome_mobilemenuck($module, &$params, &$attribs)
// {
	
// }

/*
 * Class of the plugin
 */
class PlgSystemMobilemenuck extends JPlugin {

	private $styles = Array();

	private $modulesbar = Array();

	private $modulestop = Array();

	private $modulesbottom = Array();

	public function __construct(&$subject, $config = array()) {
		require_once(dirname(__FILE__) . '/defines.php');
		require_once(dirname(__FILE__) . '/helpers/helper.php');
		require_once(dirname(__FILE__) . '/helpers/menu.php');
		require_once(dirname(__FILE__) . '/helpers/function.php');

		parent::__construct($subject, $config);
	}

	public function onContentPrepareForm($form, $data) {
		if (
			($form->getName() != 'com_modules.module' && $form->getName() != 'com_advancedmodules.module' 
				|| ($form->getName() == 'com_modules.module' && $data && $data->module != 'mod_maximenuck' && $data->module != 'mod_accordeonck' && $data->module != 'mod_accordeonmenuck' && $data->module != 'mod_menu')
				)
			&& ($form->getName() != 'com_menus.item' && $form->getName() != 'com_menumanagerck.itemedition')
			)
			return;

		JForm::addFormPath(MOBILEMENUCK_PATH .'/params');
		JForm::addFieldPath(MOBILEMENUCK_PATH . '/elements');

		// get the language
		$this->loadLanguage();

		// load the additional options in the module
		if ($form->getName() == 'com_modules.module' || $form->getName() == 'com_advancedmodules.module') {
			$form->loadFile('mobilemenuck_params', false);
		}

		// menu item options
		if ($form->getName() == 'com_menus.item' || $form->getName() == 'com_menumanagerck.itemedition') {
			$form->loadFile('mobilemenuck_itemparams', false);
		}
	}

	/**
	 * Event launched when the module is rendered in the page
	 *
	 * @param       object   The module element.
	 * @param       array    The attributes to render the module.
	 * @return      void
	 */
	public function onRenderModule($module, &$attribs) {
		$app = JFactory::getApplication();
		$input = $app->input;

		if ($app->isClient('administrator')) {
			return;
		}

		// exit if we are in one of these cases
		if ($input->get('option', '', 'string') == 'com_modulesmanagerck' 
			|| $input->get('option', '', 'string') == 'com_media' 
			|| $input->get('format', '', 'string') == 'raw'
			|| $input->get('option', '', 'string') == 'com_config'
			|| $input->get('ck', '', 'string') === '1'
			) {
			return;
		}

		// $loadAssets = false;
		$moduleParams = new JRegistry($module->params);
		// find a module enabled for mobile
		if ($moduleParams->get('mobilemenuck_enable', 0, 'int')) {
			$attribs['style'] .= ' mobilemenuck';
			// $loadAssets = true;

			if (!empty ($module->content)) {
				require_once(dirname(__FILE__) . '/defines.php');
				require_once(dirname(__FILE__) . '/helpers/helper.php');
				require_once(dirname(__FILE__) . '/helpers/menu.php');
				require_once(dirname(__FILE__) . '/helpers/function.php');

				$id = \Mobilemenuck\Helper::createIdForModule($module);
				if ($module->module == 'mod_maximenuck') {
					$selector = '#' . $id;
					$menuselector = 'ul.maximenuck';
				} else if ($module->module == 'mod_accordeonmenuck') {
					$selector = '#' . $id . '-wrap';
					$html = '<div id="' . $id . '-wrap">
								' . $module->content . '
							</div>';
					$module->content = $html;
					$menuselector = 'ul.menu';
				} else {
					if (version_compare(JVERSION, '4') >= 0) {
						$selector = '#' . $id . '-wrap ul.nav';
					} else {
						$selector = '#' . $id . '-wrap ul.nav';
					}
					$html = '<div id="' . $id . '-wrap">
								' . $module->content . '
							</div>';

					$module->content = $html;
					$menuselector = 'ul';
				}
				$styleid = $moduleParams->get('mobilemenuck_styles');
				$menubarbuttoncontent = '&#x2261;';
				$topbarbuttoncontent = '×';
				if ($styleid) {
					$styleParams = json_decode(\Mobilemenuck\Helper::getStyleById($styleid, 'a.params'));

					$menubarbuttoncontent = \Mobilemenuck\Menu::getButtonContent($styleParams->menubarbuttoncontent, $styleParams);
					$topbarbuttoncontent = \Mobilemenuck\Menu::getButtonContent($styleParams->topbarbuttoncontent, $styleParams);
				}

				// loads the language files
				$lang	= JFactory::getLanguage();
				$lang->load('plg_system_mobilemenuck', JPATH_SITE . '/plugins/system/mobilemenuck', $lang->getTag(), false);
				$lang->load('plg_system_mobilemenuck', JPATH_ADMINISTRATOR, $lang->getTag(), false);

				$merge = $moduleParams->get('mobilemenuck_merge', '');
				$mergeorder = $moduleParams->get('mobilemenuck_mergeorder', '');
				if ($merge) {
					$mergemodule = \Mobilemenuck\Helper::getModuleById($merge);
					$merge = \Mobilemenuck\Helper::createIdForModule($mergemodule);
				}

				loadMobileMenuCK($selector, 
					array(
						'menuid' => $id
						,'menubarbuttoncontent' => $menubarbuttoncontent
						,'topbarbuttoncontent' => $topbarbuttoncontent
						,'showmobilemenutext' => $moduleParams->get('mobilemenuck_showmobilemenutext', 'default')
						,'mobilemenutext' => JText::_($moduleParams->get('mobilemenuck_mobilemenutext', 'PLG_MOBILEMENUCK_MENU'))
						,'container' => $moduleParams->get('mobilemenuck_container', 'body')
						,'detectiontype' => $moduleParams->get('mobilemenuck_detectiontype', 'resolution')
						,'resolution' => $moduleParams->get('mobilemenuck_resolution', '640')
						,'usemodules' => $moduleParams->get('mobilemenuck_usemodules', '0')
						,'useimages' => $moduleParams->get('mobilemenuck_useimages', '0')
						,'showlogo' => $moduleParams->get('mobilemenuck_showlogo', '1')
						,'showdesc' => $moduleParams->get('mobilemenuck_showdesc', '0')
						,'displaytype' => $moduleParams->get('mobilemenuck_displaytype', 'accordion')
						,'displayeffect' => $moduleParams->get('mobilemenuck_displayeffect', 'normal')
						,'menuwidth' => $moduleParams->get('mobilemenuck_menuwidth', '300')
						,'openedonactiveitem' => $moduleParams->get('mobilemenuck_openedonactiveitem', '0')
						,'mobilebackbuttontext' => JText::_($moduleParams->get('mobilemenuck_mobilebackbuttontext', 'PLG_MOBILEMENUCK_MOBILEBACKBUTTON'))
						,'menuselector' => $menuselector
						,'merge' => $merge
						,'beforetext' => addslashes($moduleParams->get('mobilemenuck_beforetext', ''))
						,'aftertext' => addslashes($moduleParams->get('mobilemenuck_aftertext', ''))
						,'mergeorder' => $mergeorder
						,'tooglebarevent' => $moduleParams->get('mobilemenuck_tooglebarevent', 'click')
						,'tooglebaron' => $moduleParams->get('mobilemenuck_tooglebaron', 'all')
					)
				);
			} else {
				return;
			}
		}
	}

	public function onAfterRender() {
		// exit if in admin
		if (JFactory::getApplication()->isClient('administrator')) return;

		// get the page body
		if (version_compare(JVERSION, '4') >= 0) {
			$body = JFactory::getApplication()->getBody(); 
		} else {
			$body = JResponse::getBody();
		}
		$html = '';

		if (! empty($this->modulesbar)) {
			$html .= '<div id="mobilemenuck-bar-module" style="display:none;">';
			foreach ($this->modulesbar as $module) {
				$html .= $module;
			}
			$html .= '</div>';
		}
		if (! empty($this->modulestop)) {
			$html .= '<div id="mobilemenuck-top-module" style="display:none;">';
			foreach ($this->modulestop as $module) {
				$html .= $module;
			}
			$html .= '</div>';
		}
		if (! empty($this->modulesbottom)) {
			$html .= '<div id="mobilemenuck-bottom-module" style="display:none;">';
			foreach ($this->modulesbottom as $module) {
				$html .= $module;
			}
			$html .= '</div>';
		}

		if ($html) {
			// add the html code
			$body = str_replace("</body>", $html . "\n</body>", $body);

			if (version_compare(JVERSION, '4') >= 0) {
				$body = JFactory::getApplication()->setBody($body); 
			} else {
				JResponse::setBody($body);
			}
		}
	}

	public function getModulesFromPosition($name) {
		$modules = JModuleHelper::getModules($name);

		return $modules;
	}
	/**
	 * Event launched when the module is rendered in the page
	 *
	 * @param       object   The module element.
	 * @param       array    The attributes to render the module.
	 * @return      void
	 */
	public function onAfterModuleList($modules) {
		$app = JFactory::getApplication();
		$input = $app->input;

		if ($app->isClient('administrator')) {
			return;
		}

		// exit if we are in one of these cases
		if ($input->get('option', '', 'string') == 'com_modulesmanagerck' 
			|| $input->get('option', '', 'string') == 'com_media' 
			|| $input->get('format', '', 'string') == 'raw'
			|| $input->get('option', '', 'string') == 'com_config'
			|| $input->get('ck', '', 'string') === '1'
			) {
			return;
		}

		$loadAssets = false;
		foreach ($modules as $module) {
			$moduleParams = new JRegistry($module->params);
			// find a module to use
			if ($moduleParams->get('mobilemenuck_enable', 0, 'int')) {
				$loadAssets = true;
				$this->loadAssets($module);
			}
		}
		if (! count($this->styles)) return;
		$styles = implode("\n", $this->styles);
		$doc = JFactory::getDocument();
		// css
		$doc->addStyleDeclaration($styles);
	}

	/**
	 * Add a word into a string
	 */
	private function addWord($string, $toggler) {
		$s = explode(' ', $string);
		if (!in_array($toggler, $s)) {
			array_push($s, $toggler);
		}

		$s = implode(' ', $s);
		return $s;
	}

	/**
	 * Remove a word from a string
	 */
	private function removeWord($string, $toggler) {
		$s = explode(' ', $string);
		if (in_array($toggler, $s)) {
			$s = array_diff($s, array($toggler));
		}

		$s = implode(' ', $s);
		return $s;
	}

	/**
	 * Load the scripts and styles
	 */
	protected function loadAssets($module) {
		// loads the helper in any case
		require_once('helpers/helper.php');
		$moduleParams = new JRegistry($module->params);
		$doc = JFactory::getDocument();
		// check if a style has been selected
		$styleid = $moduleParams->get('mobilemenuck_styles');

		if ($moduleParams->get('mobilemenuck_enable') == '1') {
			$id = \Mobilemenuck\Helper::createIdForModule($module);
			if ($styleid) {
				$styles = $this->getStylesCss($styleid);
				if ($styles->state == '1') {
					$module->mobilemenuck_params = new JRegistry($styles->params);
					$layoutcss = $styles->layoutcss;
					\Mobilemenuck\Helper::makeCssReplacement($layoutcss);

					// $fieldcss = str_replace('|ID|', '.mobilemenuck.' . $fieldClass, $fieldcss);
					$layoutcss = str_replace('|ID|', '[data-id="' . $id . '"]', $layoutcss);
					// $layoutcss = str_replace('|ID|', '', $layoutcss);
					// $fieldcss = str_replace('.fields-container', '.fields-container.' . $fieldClass, $fieldcss);
					// $fieldcss = str_replace('"field-entry"]', '"field-entry"].' . $fieldClass, $fieldcss);

					$this->styles[$id] = $layoutcss;

					global $ckcustomgooglefontslist;
					global $ckfontawesomeisloaded;
					// load the Font Awesome library
					if ($moduleParams->get('mobilemenuck_loadfa') == '1' && !$ckfontawesomeisloaded) {
						$doc->addStylesheet(JUri::root(true) . '/media/com_mobilemenuck/assets/font-awesome.min.css');
						$ckfontawesomeisloaded = true;
					}
					if ($module->mobilemenuck_params->get('texttextisgfont') && $module->mobilemenuck_params->get('texttextgfont')) {
						$ckcustomgooglefontslist[] = $module->mobilemenuck_params->get('texttextgfont');
		//				$doc->addStylesheet('https://fonts.googleapis.com/css?family=' . $field->mobilemenuck_params->get('texttextgfont'));
					}
					if ($module->mobilemenuck_params->get('titletextisgfont') && $module->mobilemenuck_params->get('titletextgfont')) {
						$ckcustomgooglefontslist[] = $module->mobilemenuck_params->get('titletextgfont');
		//				$doc->addStylesheet('https://fonts.googleapis.com/css?family=' . $field->mobilemenuck_params->get('titletextgfont'));
					}
				}
			} else {
				// $id = 'mobilemenuck-' . $module->id;
				$layoutcss = $this->getLayoutCss();
				\Mobilemenuck\Helper::makeCssReplacement($layoutcss);
				$layoutcss = str_replace('|ID|', '[data-id="' . $id . '"]', $layoutcss);
				$this->styles[$id] = $layoutcss;
			}
		}
	}

	/**
	 * Get the css rules from the styles
	 *
	 * @param int $id
	 * @return string
	 */
	protected function getStylesCss($id) {
		$db = JFactory::getDbo();
		$q = "SELECT params,layoutcss,state from #__mobilemenuck_styles WHERE id = " . (int)$id;
		$db->setQuery($q);
		$styles = $db->loadObject();

		return $styles;
	}

	/**
	 * Test if there is already a unit, else add the px
	 *
	 * @param string $value
	 * @return string
	 */
	public static function testUnit($value) {
		if ((stristr($value, 'px')) OR (stristr($value, 'em')) OR (stristr($value, '%')) OR (stristr($value, 'auto')) ) {
			return $value;
		}

		if ($value == '') {
			$value = 0;
		}

		return $value . 'px';
	}

	/**
	 * Check if we need to load the styles in the page
	 */
	public function onBeforeRender() {
		// si pas en frontend, on sort
		if (JFactory::getApplication()->isClient('administrator')) return;

		// load the custom menus  if pro version
		if (\Mobilemenuck\Helper::checkIsProVersion()) $this->loadMobileMenus();

//		if (! count($this->styles)) return;

		// try to load the module positions for the mobile menu
		$modulesBar = $this->getModulesFromPosition('mobilemenuck-bar');
		if (! empty($modulesBar)) {
			foreach ($modulesBar as $moduleBar) {
				$attribs = array();
				$attribs['style'] = 'none';

				$this->modulesbar[] = JModuleHelper::renderModule($moduleBar, $attribs);
			}
		}

		$modulesTop = $this->getModulesFromPosition('mobilemenuck-top');
		if (! empty($modulesTop)) {
			foreach ($modulesTop as $moduleTop) {
				$attribs = array();
				$attribs['style'] = 'none';

				$this->modulestop[] = JModuleHelper::renderModule($moduleTop, $attribs);
			}
		}

		$modulesBottom = $this->getModulesFromPosition('mobilemenuck-bottom');
		if (! empty($modulesBottom)) {
			foreach ($modulesBottom as $moduleBottom) {
				$attribs = array();
				$attribs['style'] = 'none';

				$this->modulesbottom[] = JModuleHelper::renderModule($moduleBottom, $attribs);
			}
		}
	}

	/**
	 * Load the fonts only if not already registered by another extension
	 */
	public function loadCustomGoogleFontsList() {
		global $ckcustomgooglefontslist;

		if (! empty($ckcustomgooglefontslist)) {
			$doc = JFactory::getDocument();
			foreach ($ckcustomgooglefontslist as $ckcustomgooglefont) {
				$doc->addStylesheet('//fonts.googleapis.com/css?family=' . $ckcustomgooglefont);
			}
		}
	}

	private function loadMobileMenus() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select('*')
			->from('#__mobilemenuck_menus')
			->where('state = 1');

		$db->setQuery($query);
		$menus = $db->loadObjectList();

		if (! empty($menus)) {
			require_once('helpers/helper.php');
			require_once('helpers/menu.php');
			require_once('helpers/function.php');
		}
		foreach ($menus as $menu) {
			if ($menu->params) {
				$params = new JRegistry(unserialize($menu->params));
			}

			// if no selector given, continue because the menu can not work 
			if (! $params->get('selector', '')) continue;

			// get the language
			$this->loadLanguage();

			$styleid = $menu->style;
			$id = $params->get('menuid', 'mobilemenuck-' . (int) (microtime(true) * 100));
			if ($styleid) {
				$styles = \Mobilemenuck\Helper::getStyleById($styleid, 'params,layoutcss,state', 'object');
				if ($styles->state == '1') {
//					$module->mobilemenuck_params = new JRegistry($styles->params);
					$layoutcss = $styles->layoutcss;
					\Mobilemenuck\Helper::makeCssReplacement($layoutcss);

					// $fieldcss = str_replace('|ID|', '.mobilemenuck.' . $fieldClass, $fieldcss);
					$layoutcss = str_replace('|ID|', '[data-id="' . $id . '"]', $layoutcss);
					// $layoutcss = str_replace('|ID|', '', $layoutcss);
					// $fieldcss = str_replace('.fields-container', '.fields-container.' . $fieldClass, $fieldcss);
					// $fieldcss = str_replace('"field-entry"]', '"field-entry"].' . $fieldClass, $fieldcss);

//					$this->styles[$id] = $layoutcss;
					require_once('helpers/' . MOBILEMENUCK_PLATFORM . '/loader.php');
					\Mobilemenuck\CKLoader::loadStyleDeclaration($layoutcss);
				}
			} else {
				$layoutcss = $this->getLayoutCss();
				\Mobilemenuck\Helper::makeCssReplacement($layoutcss);
				$layoutcss = str_replace('|ID|', '[data-id="' . $id . '"]', $layoutcss);
				// $this->styles[$id] = $layoutcss;
				require_once('helpers/' . MOBILEMENUCK_PLATFORM . '/loader.php');
				\Mobilemenuck\CKLoader::loadStyleDeclaration($layoutcss);
			}

			// create a unique ID for the menu
			// $menuid = 'mobilemenuck-' . (int) (microtime(true) * 100);
			$menubarbuttoncontent = '&#x2261;';
			$topbarbuttoncontent = '×';
			if ($styleid) {
				// $styleParams = json_decode(\Mobilemenuck\Helper::getStyleById($styleid, 'a.params'));

				// $menubarbuttoncontent = \Mobilemenuck\Menu::getButtonContent($styleParams->menubarbuttoncontent, $styleParams);
				// $topbarbuttoncontent = \Mobilemenuck\Menu::getButtonContent($styleParams->topbarbuttoncontent, $styleParams);
			}
			\Mobilemenuck\Menu::load($params->get('selector', ''), 
				array(
					'menuid' => $id
					,'menubarbuttoncontent' => $menubarbuttoncontent
					,'topbarbuttoncontent' => $topbarbuttoncontent
					 ,'showmobilemenutext' => $params->get('showmobilemenutext', 'default')
					 ,'mobilemenutext' => JText::_($params->get('mobilemenutext', 'PLG_MOBILEMENUCK_MENU'))
					 ,'container' => $params->get('container', 'body')
					 ,'detectiontype' => $params->get('detectiontype', 'resolution')
					 ,'resolution' => $params->get('resolution', '640')
					 ,'usemodules' => $params->get('usemodules', '0')
					 ,'useimages' => $params->get('useimages', '0')
					 ,'showlogo' => $params->get('showlogo', '1')
					 ,'showdesc' => $params->get('showdesc', '0')
					 ,'displaytype' => $params->get('displaytype', 'accordion')
					 ,'displayeffect' => $params->get('displayeffect', 'normal')
					 ,'menuwidth' => $params->get('menuwidth', '300')
					 ,'openedonactiveitem' => $params->get('openedonactiveitem', '0')
					 ,'mobilebackbuttontext' => JText::_($params->get('mobilebackbuttontext', 'PLG_MOBILEMENUCK_MOBILEBACKBUTTON'))
					 ,'menuselector' => $params->get('menuselector', 'ul')
					 ,'childselector' => $params->get('childselector', 'li')
					 ,'beforetext' => addslashes($params->get('beforetext', ''))
					 ,'aftertext' => addslashes($params->get('aftertext', ''))
//					 ,'merge' => '0'
//					 ,'mergeorder' => ''
				)
			);
		}
	}

	public static function getLayoutCss() {
		$doc = JFactory::getDocument();
		$overrideSrc = null;
		if (isset($doc->template)) {
			$overrideSrc = JPATH_ROOT . '/templates/' . $doc->template . '/css/mobilemenuck.css';
		}
		if (file_exists($overrideSrc)) {
			$layoutcss = file_get_contents($overrideSrc);
		} else {
			$layoutcss = file_get_contents(MOBILEMENUCK_PATH . '/default.txt');
		}

		return $layoutcss;
	}
}