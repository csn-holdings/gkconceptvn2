<?php

defined('_JEXEC') or die('Restricted access');
/*
	preflight which is executed before install and update
	install
	update
	uninstall
	postflight which is executed after install and update
	*/

class com_mobilemenuckInstallerScript {

	function install($parent) {
		
	}
	
	function update($parent) {
		$db = JFactory::getDbo();
		$sql = file_get_contents(dirname(__FILE__).'/administrator/sql/install.mysql.utf8.sql');
		$sqls = explode('/* --- */', $sql);
		foreach ($sqls as $s) {
			if ($s = trim($s)) {
				$db->setQuery($s);
				$db->execute();
			}
		}
	}
	
	function uninstall($parent) {
		jimport('joomla.installer.installer');
		$db = JFactory::getDbo();
		// Check first that the plugin exist
		$db->setQuery('SELECT `extension_id` FROM #__extensions WHERE `element` = "mobilemenuck" AND `type` = "plugin"');
		$id = $db->loadResult();

		if($id)
		{
			$installer = new JInstaller;
			$result = $installer->uninstall('plugin', $id);
		}
	}

	function preflight($type, $parent) {
		return true;
	}

	// run on install and update
	function postflight($type, $parent) {
		// install modules and plugins
		jimport('joomla.installer.installer');
		$db = JFactory::getDbo();
		$status = array();
		$src_ext = dirname(__FILE__).'/administrator/extensions';
		$installer = new JInstaller;

		// install the plugin
		$result = $installer->install($src_ext.'/mobilemenuck');
		$status[] = array('name'=>'Mobile Menu CK - Plugin','type'=>'plugin', 'result'=>$result);
		// auto enable the plugin
		$db->setQuery("UPDATE #__extensions SET enabled = '1' WHERE `element` = 'mobilemenuck' AND `type` = 'plugin'");
		$db->execute();

		foreach ($status as $statu) {
			if ($statu['result'] == true) {
				$alert = 'success';
				$icon = 'icon-ok';
				$text = 'Successful';
			} else {
				$alert = 'warning';
				$icon = 'icon-cancel';
				$text = 'Failed';
			}
			echo '<div class="alert alert-' . $alert . '"><i class="icon ' . $icon . '"></i>Installation and activation of the <b>' . $statu['type'] . ' ' . $statu['name'] . '</b> : ' . $text . '</div>';
		}

		return true;
	}
}
