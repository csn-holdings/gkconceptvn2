<?php
/**
 * @name		Mobile Menu CK
 * @package		com_mobilemenuck
 * @copyright	Copyright (C) 2017. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */


// no direct access
defined('_JEXEC') or die;
if (! file_exists(JPATH_ROOT . '/plugins/system/mobilemenuck/helpers/helper.php')) {
	echo 'Plugin Mobile Menu CK not found. Helper.php file missing, operation aborted.';
	return;
}

if (! JPluginHelper::isEnabled('system', 'mobilemenuck')) {
	echo 'Plugin Mobile Menu CK not enabled. Please enable the plugin. Operation aborted.';
	return;
}

define('CK_LOADED', 1);
require_once __DIR__ . '/helpers/defines.php';
require_once __DIR__ . '/helpers/ckinput.php';
require_once __DIR__ . '/helpers/cktext.php';
require_once __DIR__ . '/helpers/ckfof.php';
require_once __DIR__ . '/helpers/ckparams.php';
require_once(JPATH_ROOT . '/plugins/system/mobilemenuck/helpers/helper.php');
// load the needed libraries
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_mobilemenuck')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// loads the language files from the frontend
$lang	= JFactory::getLanguage();
$lang->load('com_mobilemenuck', MOBILEMENUCK_SITE_PATH, $lang->getTag(), false);

// loads the helper in any case
include_once MOBILEMENUCK_ADMIN_PATH . '/helpers/helper.php';

// Include dependancies
jimport('joomla.application.component.controller');

$controller	= JControllerLegacy::getInstance('Mobilemenuck');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
