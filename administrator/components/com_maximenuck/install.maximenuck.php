<?php
/**
 * @name		Maximenu CK params
 * @package		com_maximenuck
 * @copyright	Copyright (C) 2014. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */
 
defined('_JEXEC') or die('Restricted access');
/*
	preflight which is executed before install and update
	install
	update
	uninstall
	postflight which is executed after install and update
	*/

class com_maximenuckInstallerScript {

	function install($parent) {
		
	}

	function update($parent) {
		
	}

	function uninstall($parent) {

	}

	function preflight($type, $parent) {
		return true;
	}

	// run on install and update
	function postflight($type, $parent) {
		// install modules and plugins
		jimport('joomla.installer.installer');
		$db = JFactory::getDbo();
		$status = array();
		$src_ext = dirname(__FILE__).'/extensions';
		$installer = new JInstaller;

		// extensions to install
		// system plugin
		$result = $installer->install($src_ext.'/maximenuckparams');
		$status[] = array('name'=>'System - Maximenu CK Params','type'=>'plugin', 'result'=>$result);
		// system plugin must be enabled for user group limits and private areas
		$db->setQuery("UPDATE #__extensions SET enabled = '1' WHERE `element` = 'maximenuckparams' AND `type` = 'plugin'");
		$db->query();

		// additional plugins
		/*$plugins = array('articlesbydate', 'categories');
		foreach ($plugins as $plugin) {
			$result = $installer->install($src_ext . '/' . $plugin);
			$status[] = array('name'=>'Maximenu Menu CK - ' . $plugin,'type'=>'plugin', 'result'=>$result);
			// auto enable the plugin
			$db->setQuery("UPDATE #__extensions SET enabled = '1' WHERE `element` = '" . $plugin . "' AND `type` = 'plugin' AND `folder` = 'accordeonmenuck'");
			$db->query();
		}*/

		foreach ($status as $statu) {
			if ($statu['result'] == true) {
				$alert = 'success';
				$icon = 'icon-ok';
				$text = 'Successful';
			} else {
				$alert = 'warning';
				$icon = 'icon-cancel';
				$text = 'Failed';
			}
			echo '<div class="alert alert-' . $alert . '"><i class="icon ' . $icon . '"></i>Installation and activation of the <b>' . $statu['type'] . ' ' . $statu['name'] . '</b> : ' . $text . '</div>';
		}

		return true;
	}
}
