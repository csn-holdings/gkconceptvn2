<?php
/**
 * @name		Maximenu CK params
 * @package		com_maximenuck
 * @copyright	Copyright (C) 2014. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @author		Cedric Keiflin - http://www.template-creator.com - http://www.joomlack.fr
 */
defined('_JEXEC') or die;
//$path = '/administrator/components/com_maximenuck/presets/';
//$folder_path = JPATH_ROOT . '/administrator/components/com_maximenuck/presets/';
//$path = JPATH_ROOT . '/modules/mod_maximenuck/themes';

?>
<div id="themesselection">
	<div class="menulink current" data-level="2" tab="tab_themeshorizontal"><?php echo JText::_('CK_HORIZONTAL'); ?></div>
	<div class="menulink" data-level="2" tab="tab_themesvertical"><?php echo JText::_('CK_VERTICAL'); ?></div>
	<div class="clr"></div>
	<div class="tab current" id="tab_themeshorizontal" data-level="2">
		<div class="clearfix" style="min-height:35px;margin: 0 5px;">
			<?php
			$path = '/administrator/components/com_maximenuck/presets/horizontal/';
			$folder_path = JPATH_ROOT . '/administrator/components/com_maximenuck/presets/horizontal/';
			$folders = JFolder::folders($folder_path);
			natsort($folders);
			$i = 1;
			foreach ($folders as $folder) {
				$theme_title = "";
				if ( file_exists($folder_path . $folder. '/styles.json') ) {
					if ( file_exists($folder_path . '/' . $folder. '/preview.png') ) {
						$theme = JUri::root(true) . $path . $folder . '/preview.png';
					} else {
						$theme = Juri::root(true) . '/administrator/components/com_maximenuck/images/what.png" width="110" height="110';
						// $theme_title = JText::_('CK_THEME_PREVIEW_NOT_FOUND');
					}
				} else {
					// $theme = Juri::root(true) . '/administrator/components/com_maximenuck/images/warning.png" width="110" height="110';
					// $theme_title = JText::_('CK_THEME_CSS_NOT_COMPATIBLE');
					continue;
				}

				echo '<div class="themethumb" data-name="' . $folder . '" onclick="ckLoadPreset(\'' . $folder . '\', \'horizontal\')">'
					. '<div class="themethumbimg">'
					. '<img src="' . $theme . '" style="margin:0;padding:0;" title="' . $theme_title . '" class="hasTip" />'
					. '</div>'
					. '<div class="themename">' . $folder . '</div>'
					. '</div>';
				$i++;
			}
			?>
		</div>
	</div>
	<div class="tab" id="tab_themesvertical" data-level="2">
		<div class="clearfix" style="min-height:35px;margin: 0 5px;">
			<?php
			$path = '/administrator/components/com_maximenuck/presets/vertical/';
			$folder_path = JPATH_ROOT . '/administrator/components/com_maximenuck/presets/vertical/';
			$folders = JFolder::folders($folder_path);
			natsort($folders);
			$i = 1;
			foreach ($folders as $folder) {
				$theme_title = "";
				if ( file_exists($folder_path . $folder. '/styles.json') ) {
					if ( file_exists($folder_path . '/' . $folder. '/preview.png') ) {
						$theme = JUri::root(true) . $path . $folder . '/preview.png';
					} else {
						$theme = Juri::root(true) . '/administrator/components/com_maximenuck/images/what.png" width="110" height="110';
						// $theme_title = JText::_('CK_THEME_PREVIEW_NOT_FOUND');
					}
				} else {
					// $theme = Juri::root(true) . '/administrator/components/com_maximenuck/images/warning.png" width="110" height="110';
					// $theme_title = JText::_('CK_THEME_CSS_NOT_COMPATIBLE');
					continue;
				}

				echo '<div class="themethumb" data-name="' . $folder . '" onclick="ckLoadPreset(\'' . $folder . '\', \'vertical\')">'
					. '<div class="themethumbimg">'
					. '<img src="' . $theme . '" style="margin:0;padding:0;" title="' . $theme_title . '" class="hasTip" />'
					. '</div>'
					. '<div class="themename">' . $folder . '</div>'
					. '</div>';
				$i++;
			}
			?>
		</div>
	</div>
</div>