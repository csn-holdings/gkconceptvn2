<?php
/**
 * @package		VM2Tags
 * @subpackage	mod_vm2tags_cloud
 * @copyright	Copyright (C) 2010 - 2017 Nordmograph.com. All rights reserved.
 * @license		GNU General Public License version 3 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
class modVm2tagsCloudHelper
{		
	static function &getList(&$params )
	{
		$db 	= JFactory::getDBO();
		$app	= JFactory::getApplication();
		
		$langtag 			= JFactory::getLanguage()->get('tag');
		$dblangtag			= strtolower(str_replace("-","_",$langtag));
					
		$limit 				= $params->get( 'limit', '50');
		$catfilter			= $params->get( 'catfilter', 0);
		$cparams 			= JComponentHelper::getParams('com_vm2tags');
		$vmvendor_filter 	= $cparams->get('vmvendor_filter',0);
		$userid 			= $app->input->get('userid','','INT');
		$vendorid 			= $app->input->get('vendorid','','INT');
		$virtuemart_product_id 			= $app->input->get('virtuemart_product_id','','INT');
		
		
		$option = $app->input->get('option');
		$virtuemart_category_id = '';
		if($option=='com_virtuemart')
		{
			$virtuemart_category_id = $app->input->get('virtuemart_category_id','','INT'); 
		}
		if($option=='com_vmvendor' && !$userid)
			$userid = JFactory::getUser()->id;
		
		if($catfilter=='2')
		{ // get children cats from current cat.
			$children = array();
			function categoryChild($id)
			{
    			$db = JFactory::getDBO();
				$q ="SELECT `category_child_id` FROM `#__virtuemart_category_categories` 
				WHERE `category_parent_id` ='".$id."'  ";
				$db->setQuery($q);
				$cats = $db->loadObjectList();
				$children = array();
				$children[] =  $id   ;				
				$i = 0;
				if(count($cats) > 0)
				{
					foreach($cats as $cat)
					{
						$children[] = $cat->category_child_id ;
						$children[] = implode( ',' , categoryChild( $cat->category_child_id ) );						
						$i++;
						}
					}
				$children = implode("," ,$children);
				$children = explode("," ,$children);
				return array_unique($children);
			}
			$traverse = categoryChild( $virtuemart_category_id );
			$traverse = implode("," , $traverse);
		}
		
		
		
		
		
		
			if (!class_exists( 'VmConfig' ))
				require JPATH_ADMINISTRATOR .  '/components/com_virtuemart/helpers/config.php';
			VmConfig::loadConfig();
		
		
			$q ="SELECT vpl.`metakey` ,  vpprod.virtuemart_vendor_id 
			FROM `#__virtuemart_products_".$dblangtag."` vpl ";
			
			if($catfilter && $virtuemart_category_id!=''  && $virtuemart_category_id!='0')
			{
				$q .=" JOIN `#__virtuemart_product_categories` vpcat ON vpcat.`virtuemart_product_id` = vpl.`virtuemart_product_id` ";
			}
			$q .=" JOIN `#__virtuemart_products` vpprod ON vpprod.`virtuemart_product_id` = vpl.`virtuemart_product_id` ";
			if($vmvendor_filter)
			{
				if($userid!='')
					$q .= " JOIN #__virtuemart_vmusers vvmu ON vvmu.virtuemart_vendor_id = vpprod.virtuemart_vendor_id AND vvmu.virtuemart_user_id='".$userid."' ";
				elseif($vendorid!='')
					$q .= " JOIN #__virtuemart_vmusers vvmu ON vvmu.virtuemart_vendor_id = vpprod.virtuemart_vendor_id AND vpprod.virtuemart_vendor_id='".$vendorid."' ";
				
			}
			
			$q .="WHERE vpl.`metakey` !=' ' AND vpl.`metakey` !=''
			";
			$q .=" AND vpprod.published='1' "; 
			if($catfilter==1 && $virtuemart_category_id!='' && $virtuemart_category_id!='0')
			{
				$q .=" AND vpcat.`virtuemart_category_id` = '".$virtuemart_category_id."'  ";
			}
			if($catfilter==2 && $virtuemart_category_id!='' && $virtuemart_category_id!='0')
			{
				$q .=" AND vpcat.`virtuemart_category_id` IN($traverse)  ";
			}
			if($virtuemart_product_id)
				{
					$q .=" AND vpprod.virtuemart_vendor_id = (SELECT virtuemart_vendor_id FROM #__virtuemart_products WHERE virtuemart_product_id='".$virtuemart_product_id."' ) ";	
				}
			$q .="ORDER BY RAND() 
			LIMIT ".$limit;

		$db->setQuery($q);
		$rows = $db->loadObjectList();
		return $rows;
	}	
	
	static function getVM2tagsItemid()
	{
		$db 	= JFactory::getDBO();
		$lang 	= JFactory::getLanguage();
		$q = "SELECT id FROM #__menu
		WHERE link='index.php?option=com_vm2tags&view=productslist' 
		AND ( language ='".$lang->getTag()."' OR language='*') AND published='1' AND access='1' ";
		$db->setQuery($q);
		return $vm2tags_itemid = $db->loadResult();	
	}
}