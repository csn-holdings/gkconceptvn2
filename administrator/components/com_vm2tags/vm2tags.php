<?php
/**
 * @version     1.0.0
 * @package     com_vm2tags
 * @copyright   Copyright (C) Nordmograph 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Adrien Roussel <contact@nordmograph.com> - http://www.nordmograph.com
 */


// no direct access
defined('_JEXEC') or die;

//sessions
jimport( 'joomla.session.session' );

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_vm2tags')) 
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

JToolBarHelper::title(   JText::_( 'VM2Tags - The tagging solution for Virtuemart3' ), 'cpanel.png' );
JToolBarHelper::preferences('com_vm2tags');
// Include dependancies
jimport('joomla.application.component.controller');
//application
$app = JFactory::getApplication();
$task = $app->input->get('task');
 
// Require specific controller if requested
if($controller = $app->input->get('controller','legacyswitch')) {
  require_once (JPATH_COMPONENT.'/controllers/'.$controller.'.php');
}
 
// Create the controller
$classname  = 'Vm2tagsController'.$controller;
$controller = new $classname();
 JFactory::getApplication()->input->set('view', 'legacyswitch');
// Perform the Request task
$controller->execute($task);



echo '<div style="width:100%;text-align:center;"><a href="http://www.nordmograph.com/extensions/index.php?option=com_kunena&view=category&catid=86&Itemid=108" target="_blank">Support</a></div>';