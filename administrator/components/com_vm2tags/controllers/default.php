<?php
/**
 * @version     1.0.0
 * @package     com_vm2tags
 * @copyright   Copyright (C) Nordmograph 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Adrien Roussel <contact@nordmograph.com> - http://www.nordmograph.com/extensions
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');
// Load the controller framework
//jimport('joomla.application.component.controller');

/**
 * XXX_UCFIRST_INTERNAL_NAME_XXX controller class.
 */
class Vm2tagsControllerDefault extends JControllerLegacy
{	

}