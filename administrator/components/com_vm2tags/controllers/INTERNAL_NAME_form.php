<?php
/**
 * @version     1.0.0
 * @package     com_vm2tags
 * @copyright   Copyright (C) Nordmograph 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Adrien Roussel <contact@nordmograph.com> - http://www.nordmograph.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * XXX_UCFIRST_INTERNAL_NAME_XXX controller class.
 */
class Vm2tagsControllerXXX_UCFIRST_INTERNAL_NAME_XXX extends JControllerForm
{

    function __construct() {
        $this->view_list = 'XXX_INTERNAL_NAME_FORCE_LIST_XXX';
        parent::__construct();
    }

}