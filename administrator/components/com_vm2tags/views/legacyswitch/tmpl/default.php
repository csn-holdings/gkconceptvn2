<?php
/**
 * @version     1.0.0
 * @package     com_vm2tags
 * @copyright   Copyright (C) 2014. Adrien ROUSSEL Nordmograph.com All rights reserved.
 * @license     GNU General Public License version 3 or later; see LICENSE.txt
 * @author      Nordmograph <contact@nordmograph.com> - http://www.nordmograph.com./extensions
 */
// no direct access
defined('_JEXEC') or die;
if($this->counttobeconverted >0)
{
	?>
    <div class="well"><?php echo $this->counttobeconverted.' '.JText::_('COM_VM2TAGS_CONVERTOLDTAGS');  ?>
	<br /><br /><form  method="post" name="adminForm" id="adminForm" >
	<input type="submit" class="btn btn-primary" />
			<input type="hidden" name="task" value="convertTags" />
            <input type="hidden" name="controller" value="legacyswitch" />
			<input type="hidden" name="option" value="com_vm2tags" />
			<?php echo JHtml::_('form.token'); ?>
	</form>        </div>
	<?php
}