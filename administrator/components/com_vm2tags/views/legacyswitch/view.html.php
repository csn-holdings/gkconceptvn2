<?php
/**
 * @version     1.0.0
 * @package     com_vm2tags
 * @copyright   Copyright (C) 2014. Adrien ROUSSEL Nordmograph.com All rights reserved.
 * @license     GNU General Public License version 3 or later; see LICENSE.txt
 * @author      Nordmograph <contact@nordmograph.com> - http://www.nordmograph.com./extensions
 */
// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.view');
/**
 * View class for a list of VM2tags.
 */
class Vm2tagsViewLegacyswitch extends JViewLegacy
{
	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
        $app = JFactory::getApplication();
		$this->counttobeconverted = $this->get('countToboconverted') ;
		
		if($this->counttobeconverted<1)
			$app->enqueueMessage( '<i class="icon-ok"></i> '.JText::_('COM_VM2TAGS_NOOLDTAGSFOUND') );
		parent::display($tpl);
	}   
}