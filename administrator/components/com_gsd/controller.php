<?php

/**
 * @package         Google Structured Data
 * @version         4.8.1 Free
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2018 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

defined('_JEXEC') or die('Restricted access');

/**
 * Google Structured Data master display controller.
 */
class GSDController extends JControllerLegacy
{
}