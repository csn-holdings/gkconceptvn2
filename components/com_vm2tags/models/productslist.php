<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla modelitem library
jimport('joomla.application.component.modelitem');
class Vm2tagsModelproductslist extends JModelItem
{
	public function getPriceformat() 
	{
		$db = JFactory::getDBO();
		$q ="SELECT curr.*  
		FROM `#__virtuemart_currencies` AS curr 
		LEFT JOIN `#__virtuemart_vendors` AS vend ON vend.`vendor_currency` = curr.`virtuemart_currency_id`  
		WHERE vend.`virtuemart_vendor_id` = '1' ";
		$db->setQuery($q);
		$price_format 	= $db->loadRow();
		$this->_price_format = $price_format;
		return $this->_price_format;
	}
	
	static function applytaxes( $pricebefore, $catid ,  $vendor_id, $product_tax_id, $product_discount_id)
	{
		$db = JFactory::getDBO();
		$q ="SELECT DISTINCT(vc.`virtuemart_calc_id`) , vc.`calc_name` , vc.`calc_kind` , vc.`calc_value_mathop` , 
		vc.`calc_value` , vc.`calc_currency` ,  vc.`ordering` 
		FROM `#__virtuemart_calcs` vc 
		LEFT JOIN `#__virtuemart_calc_categories` vcc ON vcc.`virtuemart_calc_id` = vc.`virtuemart_calc_id`
		WHERE vc.`published`='1' 
		AND (vc.`shared` ='1' OR vc.`virtuemart_vendor_id` = '".$vendor_id."' )" ;
	
		$q .= "AND (vc.`publish_up`='0000-00-00 00:00:00' OR vc.`publish_up` <= NOW() ) ";
		$q .= "AND (vc.`publish_down`='0000-00-00 00:00:00' OR vc.`publish_down` >= NOW() ) 
		AND (vcc.`virtuemart_category_id` ='".$catid."' ";
		if($product_tax_id!='0')
			$q .= " OR vc.`virtuemart_calc_id` ='".$product_tax_id."' ";
		if($product_discount_id!='0')
			$q .= " OR vc.`virtuemart_calc_id` ='".$product_discount_id."' ";
		$q .= ") 
		GROUP BY vc.`virtuemart_calc_id` 
		ORDER BY vc.`ordering` ASC";
		$db->setQuery($q);
		$taxes = $db->loadObjectList();
		$price_withtax = $pricebefore;
		$i = 1;
		if(count($taxes)>0)
		{
			foreach($taxes as $tax)
			{
				$calc_value_mathop = $tax->calc_value_mathop;
				$calc_value = $tax->calc_value;
				switch ($calc_value_mathop)
				{
					case '+':
						$price_withtax = $price_withtax + $calc_value;
					break;
					case '-':
						$price_withtax = $price_withtax - $calc_value;
					break;
					case '+%':
						$price_withtax = $price_withtax + ( ( $price_withtax * $calc_value ) / 100 );
					break;
					case '-%':
						$price_withtax = $price_withtax - ( ( $price_withtax * $calc_value ) / 100 );
					break;
				}	
				//echo $i++ .'-';
			}
		}
	
		return $price_withtax;	
	}
		
	
	static function getVMItemid($catid)
	{
		$lang = JFactory::getLanguage();
		$db 	= JFactory::getDBO();
		$q = "SELECT `id` FROM `#__menu` 
		WHERE (
		`link` LIKE 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=".$catid."%' 
		OR `link` LIKE 'index.php?option=com_virtuemart&view=virtuemart&productsublayout=%' 
		OR `link` LIKE 'index.php?option=com_virtuemart&view=virtuemart%' 
		)
		AND `type`='component'  
		AND ( language ='".$lang->getTag()."' OR language='*') AND published='1'  AND client_id='0' ";
		$db->setQuery($q);
		return $vm_itemid = $db->loadResult();
	}
	
	
	
	public function getProducts() 
	{
		$db = JFactory::getDBO();
		if (!class_exists( 'VmConfig' ))
			require JPATH_ADMINISTRATOR .  '/components/com_virtuemart/helpers/config.php';
		VmConfig::loadConfig();
		
		$langtag 			= JFactory::getLanguage()->get('tag');
		$dblangtag			= strtolower(str_replace("-","_",$langtag));
		
		$cparams 		= JComponentHelper::getParams('com_vm2tags');
		$perpage	= $cparams->get('perpage','10');
		$perfeed	= $cparams->get('perfeed','10');	
		
		$app 		= JFactory::getApplication();
		// Get the pagination request variables
		$limit		= $app->input->getInt('limit');
		$format		= $app->input->get('format');
		if(!$limit )
			$limit = $perpage;
		if($format=='feed')
			$limit = $perfeed;
		$limitstart = $app->input->get('limitstart', 0, '', 'int');
		$tag		= $app->input->get('tag','','string');
		$tag		= str_replace('--','++',$tag);
		$tag		= str_replace('-',' ',$tag);
		$tag		= str_replace('++','-',$tag);
		$json_tag = json_encode($tag);
		$json_encoded = str_replace('"', '', $json_tag);
		$json_encoded = urlencode($json_encoded);	
		$json_encoded = str_replace('%5C', '%5C%5C%5C%5C' , $json_encoded);
		$json_encoded = urldecode($json_encoded);
		$json_encoded = explode(',' ,$json_encoded );
		
			$q = "SELECT DISTINCT(vpl.`virtuemart_product_id`) , vpl.`product_name` , vpl.`product_s_desc` , 
			vpl.`metakey`  AS accented_tags ,
			vp.`virtuemart_vendor_id` ,  vp.`product_special`, vp.`product_parent_id`,
			vcl.`category_name` ,  vcl.`virtuemart_category_id`  ,
			vm.`file_url` , vm.`file_url_thumb` ,
			
			vpp.`product_price`,vpp.`product_tax_id` ,vpp.`product_discount_id` ,vpp.`override`,vpp.`product_override_price`
			
			FROM `#__virtuemart_products_".$dblangtag."` vpl  
			JOIN `#__virtuemart_products` vp ON vpl.`virtuemart_product_id` = vp.`virtuemart_product_id` 
			JOIN `#__virtuemart_product_categories` vpcat ON vpcat.`virtuemart_product_id` = vp.`virtuemart_product_id` 
			JOIN `#__virtuemart_categories_".$dblangtag."` vcl ON vcl.`virtuemart_category_id` = vpcat.`virtuemart_category_id` 
			LEFT JOIN `#__virtuemart_product_medias` vpm ON vpl.`virtuemart_product_id` = vpm.`virtuemart_product_id` AND (vpm.ordering='0' OR vpm.ordering='1')
			LEFT JOIN `#__virtuemart_medias` vm ON vm.`virtuemart_media_id` = vpm.`virtuemart_media_id` AND SUBSTR( vm.file_mimetype , 1 ,6 ) = 'image/' AND vm.published='1'   
			LEFT JOIN #__virtuemart_product_prices vpp ON  vpl.virtuemart_product_id = vpp.virtuemart_product_id 
			WHERE vp.`published`='1'  
			GROUP BY vpl.`virtuemart_product_id` 
			 HAVING ( ";
			
			for($i=0;$i<count($json_encoded);$i++)
			{
				if($i>0)
					$q .= " AND (";
				else
					$q .= " (";
				$q .="accented_tags  LIKE LOWER('%,".$db->escape($tag).",%') 
				OR accented_tags  LIKE LOWER('".$db->escape($tag).",%') 
				OR accented_tags  LIKE LOWER('%,".$db->escape($tag)."') 
				OR accented_tags  LIKE LOWER('".$db->escape($tag)."') ";
				$q .= ")";
			}
			
			$q .=")
			ORDER BY vp.`product_special` DESC, vp.`virtuemart_product_id` DESC  ";

		$db->setQuery($q);
		$total = @$this->_getListCount($q);
		$products = $this->_getList($q, $limitstart, $limit);
		return array($products, $total, $limit, $limitstart);
	}
	
	static function getParentPrice( $pid )
	{
		$db = JFactory::getDBO();
		$q = "SELECT vpp.`product_price`,vpp.`product_tax_id` ,vpp.`product_discount_id` ,vpp.`override`,vpp.`product_override_price` 
		FROM #__virtuemart_product_prices vpp WHERE vpp.virtuemart_product_id = ".$db->quote( $pid ) ;
		$db->setQuery($q);
		return $db->loadObject();
			
	}
}
?>