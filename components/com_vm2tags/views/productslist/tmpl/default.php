<?php
/*
 * @component VM2tags
 * @copyright Copyright (C) 2010-2015 Adrien Roussel
 * @license : GNU/GPL
 * @Website : https://www.nordmograph.com/extensions
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$products 	= $this->products;
$juri 		= JURI::base();
$jconfig =JFactory::getConfig();
$app 		= JFactory::getApplication();
$tag		= $app->input->get('tag','','string');
$tag		= str_replace('--','++',$tag);
$tag		= str_replace('-',' ',$tag);
$tag		= str_replace('++','-',$tag);
$doc 		= JFactory::getDocument();
$doc->addStylesheet($juri.'components/com_vm2tags/assets/css/fontello.css');
$doc->addStylesheet($juri.'components/com_vm2tags/assets/css/style.css');

$doc->setTitle( sprintf( JText::_('COM_VM2TAGS_TAGGED_AS') ,  $tag ) );
$TmpDes = strlen($doc->getDescription()) ? $doc->getDescription(): $jconfig->get( 'sitename' );
$doc->setDescription( sprintf( JText::_('COM_VM2TAGS_TAGGED_AS') , $tag ).' '.$TmpDes );


JHtml::_('bootstrap.tooltip');
$cparams 					= JComponentHelper::getParams('com_vm2tags');
$vm_itemid 		= $cparams->get('vm_itemid');//default or masonry
			
$vm2tags_itemid 			= $cparams->get('vm2tags_itemid');
if(!$vm2tags_itemid)
{
	require_once JPATH_SITE.'/components/com_vm2tags/helpers/vm2tags.php';
	$vm2tags_itemid = Vm2tagsHelper::getVM2TItemid();
}

require_once JPATH_BASE.'/administrator/components/com_virtuemart/models/product.php';
$productModel = VmModel::getModel('product');
		
$show_price = $cparams->get('show_price','1');
$rss_button = $cparams->get('rss_button',1);
if($rss_button)
{
	$rss_tag = str_replace('-','--',$tag );
	$rss_tag = str_replace(' ','-',$rss_tag );
	$rss_url = JRoute::_('index.php?option=com_vm2tags&view=productslist&tag='.$rss_tag);
	if($jconfig->get('sef') ==0)
		$rss_url .= '&';
	else
		$rss_url .= '?';
	$rss_url .= 'format=feed';
	echo '<div id="vm2t_rssbuton"><a target="_blank" href="'.$rss_url.'" class="btn btn-small btn-default">
	<i class="vm2t-icon-rss"></i> RSS</a></div>';
}
echo '<h1>'. sprintf( JText::_('COM_VM2TAGS_TAGGED_AS') ,  $tag ) .'</h1>';



echo '<div class="pagination" >';
echo $this->pagination->getResultsCounter();
echo '</div>';

if (!class_exists( 'VmConfig' ))
	require(JPATH_ADMINISTRATOR . '/components/com_virtuemart/helpers/config.php');
VmConfig::loadConfig();
foreach($products as $product)
{
	if(!$vm_itemid)
		$vm_itemid = Vm2tagsModelproductslist::getVMItemid($product->virtuemart_category_id);
				
	$vmproducts 	= $productModel->getProducts ( array($product->virtuemart_product_id) );
	
				
	$product_url = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$product->virtuemart_product_id.'&virtuemart_category_id='.$product->virtuemart_category_id.'&Itemid='.$vm_itemid);
	echo '<div class="vm2t_product" >';
	echo '<div class="vm2t_product_thumb" >';
		$product_thumb =$product->file_url_thumb;
		if ($product_thumb =='')
		{
			$product_thumb = str_replace('/product/','/product/resized/',$product->file_url);
			
			$thum_side_width	=	VmConfig::get( 'img_width' );
			$thum_side_height	=	VmConfig::get( 'img_height' );
			$extension_pos = strrpos($product_thumb, "."); 
			$product_thumb = substr($product_thumb, 0, $extension_pos) . '_'.$thum_side_width.'x'.$thum_side_height . substr($product_thumb, $extension_pos);
		}
		if ($product->file_url =='')
			$product_thumb =  'components/com_virtuemart/assets/images/vmgeneral/'.VmConfig::get('no_image_set');
			
		echo '<a href="'.$product_url.'" ><img src="'.$juri.$product_thumb.'" height="90" alt="img" /></a>';
		if($show_price > 0 )
		{
			echo '<div class="vm2t_product_price" >';
			
			$price_format					= $this->price_format;
				$symbol 						= $price_format[7];
				$currency_positive_style 		= $price_format[11];
				$currency_id					= $price_format[0];
				$currency 						= $price_format[4];
				$currency_decimal_place 		= $price_format[8];
				
			$product_override_price = $product->product_override_price;	
			$product_price 			= $product->product_price;
				
			
			if($show_price=='1')  // without tax
			{
				if( $product->product_parent_id>0)
				{
					$parent = Vm2tagsModelproductslist::getParentPrice( $product->product_parent_id );
					$list_item__product_override_price = $parent->product_override_price;
					$list_item__product_price 			= $parent->product_price;
				}
				else
				{
					$list_item__product_override_price = $product_override_price;
					$list_item__product_price 			= $product_price;
				}
			}
			if($show_price=='2')
			{	// with tax(es)
				if( $product->product_parent_id>0)
				{
					$parent = Vm2tagsModelproductslist::getParentPrice( $product->product_parent_id );
					$product_override_price = $parent->product_override_price;
					$product_price 			= $parent->product_price;
				}
				/*$list_item__product_price = Vm2tagsModelproductslist::applytaxes( 
						$product_price   , 
						$product->virtuemart_category_id ,  
						$product->virtuemart_vendor_id,
						$product->product_tax_id,
						$product->product_discount_id
						)  ;*/
						
				$list_item__product_price = $vmproducts[0]->prices['salesPrice'];
						
				$list_item__product_override_price = Vm2tagsModelproductslist::applytaxes( 
						$product_override_price   , 
						$product->virtuemart_category_id ,  
						$product->virtuemart_vendor_id,
						$product->product_tax_id,
						$product->product_discount_id
						)  ;
			}
			
			$price_val =  number_format($list_item__product_price , $currency_decimal_place , '.' , ' ' );
			$print_price = str_replace('{number}' ,$price_val ,$currency_positive_style);
			$print_price = str_replace('{symbol}' ,$symbol ,$print_price);
			
			$price_override_val =  number_format($list_item__product_override_price , $currency_decimal_place , '.' , ' ' );
			$print_override_price = str_replace('{number}' ,$price_override_val ,$currency_positive_style);
			$print_override_price = str_replace('{symbol}' ,$symbol ,$print_override_price);
			if($product->product_override_price >0 && $product->override)
			{
				echo '<div><del>'.$print_price.'</del></div>' ;
				echo '<ins><a href="'.$product_url.'" >'.$print_override_price.'</a></ins>' ;
			}
			else
				echo '<div><a href="'.$product_url.'" >'.$print_price.'</a></div>' ;
				
			echo '</div>';
		}
		echo '</div>';
		if($product->product_special)
		{
			echo '<div class="badge badge-success vm2t-featured">'.JText::_('COM_VM2TAGS_FEATURED').'</div>';
		}
		echo '<div class="vm2t_product_name" >';
		echo '<a href="'.$product_url.'" >'.ucfirst($product->product_name).'</a>';
		echo '</div>';
		echo '<div class="vm2t_product_cat" >';
		
		echo '<a href="'.JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$product->virtuemart_category_id).'">';
		echo '<i class="vm2t-icon-folder" title="'.JText::_('COM_VM2TAGS_CATEGORY').'"  class="hasTooltip" ></i> ';
		echo  JText::_($product->category_name).'</a>';
		echo '</div>';
		echo '<div class="vm2t_product_s_desc" >';
		echo $product->product_s_desc;
		echo '</div>';
		echo '<div class="vm2t_product_tags" >';
		$sep_tags = explode(',',$product->accented_tags);
		echo '<i class="vm2t-icon-tags" title="'.JText::_('COM_VM2TAGS_TAGS').'"  class="hasTooltip" ></i> ';
		for($i=0; $i< count($sep_tags);$i++)
		{
			if($sep_tags[$i] !='' && $sep_tags[$i]!=' ')
			{
				$tag = trim(json_decode('"'.$sep_tags[$i].'"'));
				$sef_tag = str_replace('-','--', $tag);
				$sef_tag = str_replace(' ','-',$sef_tag );
				$sef_tag = mb_strtolower( $sef_tag );
				echo '<a class="btn btn-mini" 
				href="'.JRoute::_('index.php?option=com_vm2tags&view=productslist&tag='.$sef_tag.'&Itemid='.$vm2tags_itemid).'">'
				.$tag.'</a> ';
			}
		}
		
	echo '</div>';	
	echo '</div>';
	echo '<div style="clear:both;"></div>';
	echo '<hr />';
}
echo '<div style="clear:both;"></div>';
echo '<div class="pagination center" >';
echo '<div>'.$this->pagination->getResultsCounter().'</div>';
echo '<div>'.$this->pagination->getPagesLinks().'</div>';
echo '<div>'.$this->pagination->getPagesCounter().'</div>';
echo '</div>';
?>