<?php
/*
 * @component VM2tags
 * @copyright Copyright (C) 2008-2012 Adrien Roussel
 * @license : GNU/GPL
 * @Website : http://www.nordmograph.com
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla view library
jimport('joomla.application.component.view');
/**
 * HTML View class for the HelloWorld Component
 */
 jimport( 'joomla.html.pagination' );
 
class Vm2tagsViewProductslist extends JViewLegacy
{
	
	
	
	public function display($tpl = null)
	{
		$app            = JFactory::getApplication();
		$document       = JFactory::getDocument();
		
		$cparams 					= JComponentHelper::getParams('com_vm2tags');
		$vm_itemid 		= $cparams->get('vm_itemid');//default or masonry
		$perfeed 		= $cparams->get('perfeed','10');
		
		$this->productsarray		= $this->get('products');
		// Display the view
		$this->products	= $this->productsarray[0];
		$this->total	= $this->productsarray[1];
 		//$this->limit	= $this->productsarray[2];
		$this->limit	= $perfeed;
		$this->limitstart	= $this->productsarray[3];
		
		$this->price_format			= $this->get('priceformat');
		
		
		//$document->link = JRoute::_(TagsHelperRoute::getTagRoute($app->input->getInt('id')));
		$app->input->set('limit', $perfeed );
		
		$siteEmail        = $app->get('mailfrom');
		$fromName         = $app->get('fromname');
		$feedEmail        = $app->get('feed_email', 'author');
		$document->editor = $fromName;
		if ($feedEmail != "none")
		{
			$document->editorEmail = $siteEmail;
		}	
		
		
		
		// Get some data from the model
		$items    = $this->products;
		foreach ($items as $item)
		{
			if(!$vm_itemid)
				$vm_itemid = Vm2tagsModelproductslist::getVMItemid($item->virtuemart_category_id);
			
			
			// Strip HTML from feed item title
			$title = $this->escape($item->product_name);
			$title = html_entity_decode($title, ENT_COMPAT, 'UTF-8');
			
			$product_url = 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$item->virtuemart_product_id.'&virtuemart_category_id='.$item->virtuemart_category_id.'&Itemid='.$vm_itemid;
			// URL link to tagged item
			// Change to new routing once it is merged
			$link = JRoute::_($product_url);
			// Strip HTML from feed item description text
			$description = $item->core_body;
			$author      = $item->core_created_by_alias ? $item->core_created_by_alias : $item->author;
			$date        = ($item->displayDate ? date('r', strtotime($item->displayDate)) : '');
			// Load individual item creator class
			$feeditem              = new JFeedItem;
			$feeditem->title       = $title;
			$feeditem->link        = $link;
			$feeditem->description = $item->product_s_desc;
			$feeditem->date        = $date;
			$feeditem->category    = $item->category_name;
			if ($feedEmail == 'site')
			{
				$item->authorEmail = $siteEmail;
			}
			elseif ($feedEmail === 'author')
			{
				$item->authorEmail = $item->author_email;
			}
			// Loads item info into RSS array
			$document->addItem($feeditem);
		}
	}
	
	
	
	
	
	
	
	
	function get_string_between($string, $start, $end){
		$string = " ".$string;
		$ini = strpos($string,$start);
		if ($ini == 0) return "";
		$ini += strlen($start);
		$len = strpos($string,$end,$ini) - $ini;
		return substr($string,$ini,$len);
	}
}