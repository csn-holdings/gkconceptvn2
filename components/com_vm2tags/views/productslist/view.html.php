<?php
/*
 * @component VM2tags
 * @copyright Copyright (C) 2008-2012 Adrien Roussel
 * @license : GNU/GPL
 * @Website : http://www.nordmograph.com
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla view library
jimport( 'joomla.application.component.view');
jimport( 'joomla.html.pagination' );
class Vm2tagsViewProductslist extends JViewLegacy
{
	function display($tpl = null) 
	{
		$app 	= JFactory::getApplication();
		$doc 	= JFactory::getDocument();
		$tag	= $app->input->get('tag','','string');
		$doc->setMetaData( 'keywords', sprintf( JText::_('COM_VM2TAGS_METAKEYWORDS') ,  $tag ) );
		
		$cparams 	= JComponentHelper::getParams('com_vm2tags');
		$perpage 	= $cparams->get('perpage','10');
		$layout 	= $cparams->get('layout','default');//default or masonry
		
		$this->productsarray		= $this->get('products');
		// Display the view
		$this->products	= $this->productsarray[0];
		$this->total	= $this->productsarray[1];
		$this->limit	= $perpage;
		$this->limitstart	= $this->productsarray[3];
		
		$this->price_format			= $this->get('priceformat');
		
		$pagination = new JPagination( $this->total, $this->limitstart, $this->limit );		
		$this->assignRef('pagination', $pagination );
		if($layout !='default') 
			$tpl = $layout;
		parent::display($tpl);
	}	
	function get_string_between($string, $start, $end)
	{
		$string = " ".$string;
		$ini = strpos($string,$start);
		if ($ini == 0) return "";
		$ini += strlen($start);
		$len = strpos($string,$end,$ini) - $ini;
		return substr($string,$ini,$len);
	}
}