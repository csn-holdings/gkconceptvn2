<?php
/**
 * @version     1.0.0
 * @package     com_vm2tags
 * @copyright   Copyright (C) Nordmograph 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Adrien Roussel <contact@nordmograph.com> - http://www.nordmograph.com
 */

defined('_JEXEC') or die;

abstract class Vm2tagsHelper
{
	static function getVM2TItemid()
	{
		$lang = JFactory::getLanguage();
		$db 	= JFactory::getDBO();
		$q = "SELECT `id` FROM `#__menu` 
		WHERE `link` = 'index.php?option=com_vm2tags&view=productslist' 
		AND `type`='component'  
		AND ( language ='".$lang->getTag()."' OR language='*')
		AND published='1'  AND client_id='0' ";
		$db->setQuery($q);
		return $vm2t_itemid = $db->loadResult();
	}

}

