<?php
/**
 * @version     1.0.0
 * @package     com_vm2tags
 * @copyright   Copyright (C) Nordmograph 2015. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE.txt
 * @author      Adrien Roussel <contact@nordmograph.com> - https://www.nordmograph.com/extensions
 */
// No direct access
defined('_JEXEC') or die;
/**
 * Routing class from com_installatron
 *
 * @since  3.3
 */
class Vm2tagsRouter extends JComponentRouterBase
{
	public function build(&$query)
	{
		$cparams 		= JComponentHelper::getParams('com_vm2tags');
		$sef_view		= $cparams->get('sef_view','productslist');
		$sef_view		= JFilterOutput::stringURLSafe( JText::_($sef_view));
		
		$segments = array();
		if(isset($query['view']))
		{
			if(empty($query['Itemid'])) {
			//if(isset($query['Itemid'])) {
				$segments[] = $query['Itemid'];
			}
			if($query['view'] == 'productslist' )
			{
				$segments[] = $sef_view;
			}
			unset($query['view']);
		}
		if(isset($query['tag']))
		{			
		//echo  $query['tag'];
			$segments[] = $query['tag'];
			unset($query['tag']);
		}
		if(isset($query['format']))
		{			
		//	$segments[] = $query['format'];
		//	unset($query['format']);
		}
		return $segments;
	}
	public function parse(&$segments)
	{
		$cparams 		= JComponentHelper::getParams('com_vm2tags');
		$sef_view		= $cparams->get('sef_view','productslist');
		$sef_view		= JFilterOutput::stringURLSafe( JText::_($sef_view));
		$vars = array();
		// view is always the first element of the array
		$count = count($segments);
		if ($count)
		{
			if($segments[0] == $sef_view)
			{
				$vars['view'] = 'productslist';
			//	$vars['tag'] = str_replace(':','-', $segments[1]);	
				$vars['tag'] =  $segments[1];	
			//	if(@$segments[2])
			//		$vars['format'] = 'feed' ;
			}
			
		}
		return $vars;
	}
}