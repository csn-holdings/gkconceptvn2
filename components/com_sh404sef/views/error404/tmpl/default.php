<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author       Yannick Gaultier
 * @copyright    (c) Yannick Gaultier - Weeblr llc - 2020
 * @package      sh404SEF
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version      4.22.1.4233
 * @date        2020-12-03
 *
 */

/**
 * NOTE: this file and this folder, though not used to display anything, is required
 * so that Joomla! presents the user with a "404 error page" menu type
 * if they want to specify a dedicated menu item for that error page
 */
defined('_JEXEC') or die;

echo $this->content;
