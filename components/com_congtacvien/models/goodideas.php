<?php
defined('_JEXEC') or die;

use Joomla\Registry\Registry;
use Symfony\Component\Yaml\Yaml;
use Joomla\CMS\Response\JsonResponse;
use Joomla\CMS\User\UserHelper;
use Joomla\CMS\Application\ApplicationHelper;
use Joomla\Image\Image;
use Psr\Log\NullLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareInterface;

if (!class_exists( 'VmConfig' ))
	require(JPATH_ROOT .'/administrator/components/com_virtuemart/helpers/config.php');

VmConfig::loadConfig();
vmLanguage::loadJLang('com_virtuemart', true);
vmLanguage::loadJLang('com_virtuemart_orders', true);
vmLanguage::loadJLang('com_virtuemart_shoppers', true);


jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

JLoader::register('CTVHelperRoute', JPATH_COMPONENT . '/helpers/route.php');
JLoader::register('CTVHelper', JPATH_COMPONENT . '/helpers/helper.php');

class CtvModelGoodIdeas extends JModelLegacy
{
	
	protected $input = null;
	protected $sessionId = null;
	protected $app = null;
	protected $config = null;
	protected $user = null;
	protected $vendor = null;
	protected $authorise;

	public function __construct()
	{
		$this->input = JFactory::getApplication()->input;
		$session = JFactory::getSession();
		$this->sessionId = $session->getId();
		// Get configuration
		$this->app    = JFactory::getApplication();
		$this->config = JFactory::getConfig();
		$this->user = JFactory::getUser();
		
		parent::__construct();
		
	}
	
	// Good Ideas
	public function getProductGoodIdeas()
	{
		$result = new \JResponseJson();
		
		$vmProductModel = new VirtueMartModelProduct();
		$vmProductModel->setPaginationLimits(3000);
		$product = $vmProductModel->getProductListing(false, 100000000, TRUE, FALSE, FALSE, FALSE, 0, FALSE, 0, 0);
		$vmProductModel->addImages($product);
		$result->data = $product;
		
		return $result;
	}
	
	public function getProductDetailGoodIdeasModel($id)
	{
		if (!class_exists('VmConfig')) {
			require JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers' . DS . 'config.php';
		}
		VmConfig::loadConfig();
		if (!class_exists('VmHTML')) {
			require JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'html.php';
		}
		
		$pModel = VmModel::getModel('product');
		$product = $pModel->getProduct($id, true, true, false);
		$pModel->addImages($product);
		
		return $product;
	}
	
	public function getAllProductByCategoryGoodIdeasModel($id)
	{
		if (!class_exists('VmConfig')) {
			require JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers' . DS . 'config.php';
		}
		VmConfig::loadConfig();
		if (!class_exists('VmHTML')) {
			require JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'html.php';
		}
		
		$productModel = VmModel::getModel('Product');
		$productsChildren = $productModel->getProductListing(false, false, true, true, false, true, $id, false, 0);
		$productModel->addImages($productsChildren);
		
		return $productsChildren;
	}
}
