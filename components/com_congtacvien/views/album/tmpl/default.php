<?php
	defined('_JEXEC') || die('=;)');
	$dataGoodIdeas = $this->dataGoodIdeas;
	$dataGoodIdeas = array();
	
	foreach ($this->dataGoodIdeas->data as $product) {
		$temp = new stdClass();
		$temp->file_url = $product->file_url;
		foreach ($product->categoryItem as $category) {
			$data = array();
			$data[] = $category->virtuemart_category_id;
		}
		$temp->category_id = $data;
		$temp->virtuemart_product_id = $product->virtuemart_product_id;
		$temp->product_sku = $product->product_sku;
		$temp->product_sku = $product->product_sku;
		$temp->product_name = $product->product_name;
		$temp->slug = $product->slug;
		$temp->product_s_desc = $product->product_s_desc;
		$temp->product_in_stock = $product->product_in_stock;
		$temp->link = $product->link;
		$temp->file_url_thumb = $product->file_url_thumb;
		$dataGoodIdeas[$product->product_sku] = $temp;
		
	}
	
	
	if (!class_exists('VmConfig')) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart' . DS . 'helpers' . DS . 'config.php');
	
	$categoryModel = VmModel::getModel('category');
	$tempCategory = $categoryModel->getCategoryTreeAllGkconcept();
	
	foreach ($tempCategory as $cate) {
		if ($cate->published == 1 && $cate->category_parent_id == 0) {
			$tem = new stdClass();
			$tem->virtuemart_category_id = $cate->virtuemart_category_id;
			$tem->category_name = $cate->category_name;
			
			foreach ($tempCategory as $child) {
				if ($child->category_parent_id == $tem->virtuemart_category_id) {
					$temchild = new stdClass();
					$temchild->virtuemart_category_id = $child->virtuemart_category_id;
					$temchild->category_name = $child->category_name;
					
					$tem->child[] = $temchild;
				}
			}
			
			$categoryData[$tem->virtuemart_category_id] = $tem;
		}
	}
	
	
	if (
		isset($_POST['email']) && $_POST['email'] &&
		isset($_POST['name']) && $_POST['name'] &&
		isset($_POST['sku']) && $_POST['sku'] &&
		isset($_POST['product_name']) && $_POST['product_name'] &&
		isset($_POST['yeucaukhachhang']) && $_POST['yeucaukhachhang']
	) {
		$myUser = new stdClass();
		$myUser->email = $_POST['email'];
		$myUser->name = $_POST['email'];
		$myUser->confirmed = 1;
		$customFields = [];
		$customFields['5'] = $_POST['name'];
		$customFields['4'] = $_POST['sku'];
		$customFields['6'] = $_POST['product_name'];
		$customFields['7'] = $_POST['yeucaukhachhang'];
		$userClass = acym_get('class.user');
		$userClass->sendConf = true;
		$userId = $userClass->save($myUser, $customFields);
		
	}
	
	$menu = JFactory::getApplication()->getMenu();
	$items = $menu->getMenu();
	$bosuutap = array();

	foreach ($items as $ite)
    {
        if(
                $ite->query['option'] = 'com_congtacvien' &&
                $ite->query['view'] = 'album' &&
                isset($ite->query['virtuemart_category_id']) &&
	            $ite->component == 'com_congtacvien'
        )
        {
            $ite->images = $ite->params->get('menu_image');
	        $bosuutap[] = $ite;
        }
    }


?>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
<main role="main" id="appgoodideasalbum">
	<template>
		<nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
			<a class="navbar-brand font-weight-bolder mr-3" href="https://gkconcept.vn">
				Đến : <img src="images/stories/virtuemart/product/resized/logoGKconcept-01.webp"
							style="width: 100px;height: 50px">
			</a>


			<button class="navbar-light navbar-toggler"
					type="button"
					data-toggle="collapse"
					data-target="#navbarsDefault"
					aria-controls="navbarsDefault"
					aria-expanded="false"
					aria-label="Toggle navigation"
			>
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsDefault">
				<ul class="navbar-nav mr-auto align-items-center">
					<form class="bd-search hidden-sm-down">
						<input type="text" class="form-control bg-graylight border-0 font-weight-bold" id="search"
							   v-model="search"
							   placeholder="Tìm ý tưởng cho bạn ..." autocomplete="off">
						<div class="dropdown-menu bd-search-results" id="search-results">
							asdkslaksdj
						</div>
					</form>
				</ul>
			</div>

		</nav>
		<section class="bg-gray200">
			<div class="row">
				<div class="col-md-2 d-none d-lg-block">
					<div class="sidebar-panel ">
						<!--  user -->
						<div class="scroll-nav ps ps--active-y" data-perfect-scrollbar="data-perfect-scrollbar"
							 data-suppress-scroll-x="true">
							<div class="side-nav">
								<div class="main-menu">
									<ul class="metismenu" id="menu">
										<li class="Ul_li--hover" v-for="(cate, index) in category">
											<a class="has-arrow" href="#"
											   v-on:click="getCategoryFilter(cate.virtuemart_category_id)">
												<i class="i-Bar-Chart text-20 mr-2 text-muted">{{ cate.category_name
													}}</i>
												<span class="item-name text-15 text-muted"></span>
											</a>
											<ul class="mm-collapse">
												<li class="item-name" v-for="(child, index) in cate.child">
													<a href="#"
													   v-on:click="getCategoryFilter(child.virtuemart_category_id)">
														<i class="i-Circular-Point mr-2 text-muted"></i>
														<span class="text-muted">{{ child.category_name }}</span>
													</a>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--  side-nav-close -->
					</div>
				</div>
				<div class="col-md-10 col-xs-12 col-sm-12">
					<template v-if="if_child_show">
						<div class="container-fluid">
							<div class="row justify-content-center">
								<div class="col-md-6">
									<article class="card">
										<img class="card-img-top" :src="this.product.file_url" @error="setAltImg"
											 alt="Card image">
									</article>

									<form action="#" method="POST">
										<div class="form-group">
											<label for="ten">Tên:</label>
											<input name="name" type="text" class="form-control" required id="name"
												   placeholder="Tên: ">
										</div>
										<div class="form-group">
											<label for="email">Email</label>
											<input name="email" type="email" class="form-control" required id="email"
												   aria-describedby="emailHelp" placeholder="nhập email">
										</div>
										<div class="form-group">
											<label for="sodienthoai">Số Điện Thoại:</label>
											<input type="text" class="form-control" id="phone"
												   placeholder="Số Điện Thoại: ">
										</div>
										<div class="form-group">
											<label for="sku">Yêu cầu:</label>
											<textarea style="width: 100%" name="yeucaukhachhang" id="yeucaukhachhang"
													  rows="5"></textarea>
										</div>
										<div class="form-group">
											<label for="sku">Mã SKU</label>
											<input name="sku" type="text" v-bind:value="this.product.product_sku"
												   class="form-control" id="product_sku">
										</div>
										<div class="form-group">
											<label for="sku">Tên Sản Phẩm</label>
											<input name="product_name" type="text"
												   v-bind:value="this.product.product_name" class="form-control"
												   id="product_name">
										</div>
										<button type="submit" class="btn btn-primary">Gởi</button>
									</form>

								</div>
								<div class="col-md-6">
									<article class="card">
										<div class="card-body">
											<h1 class="card-title display-4">{{ this.product.product_name }}</h1>
											<p class="card-title title">
												{{ this.product.product_s_desc }}
											</p>
											<small class="d-block">
												<a class="btn btn-sm btn-gray200" href="https://gkconcept.vn">
													<i class="fa fa-external-link"></i> đến Gkconcept.vn
												</a>
											</small>


										</div>
									</article>
									<div class="clear"></div>
									<div class="card" style="margin: 20px 0px;">
										<div class="card-body">
											<p class="card-title title">
												Bạn đã tìm được mẫu thiết kế phù hợp với không gian và gu thẩm mỹ riêng
												của mình? Nhưng làm sao để tìm được đơn vị có thể nhận sản xuất, thực
												hiện trọn vẹn ý tưởng của mình với chi phí và thời gian cho phép có phải
												là điều mà bạn đang thực sự quan tâm?<br>
												<br>Hãy liên hệ GKConcept thực hiện trọn vẹn ý tưởng bằng dịch vụ thiết
												kế theo mẫu yêu cầu với chất lượng, chi phí và thời gian làm hài lòng
												những khách hàng khó tính nhất<br>
												<br>
												GKConcept với kho thư viện ý tưởng với trên 10,000 mẫu mã cho bạn tự do
												sáng tạo đi kèm với dịch vụ sản xuất yêu cầu theo mẫu có trong thư viện
												với chi phí và thời gian giao hàng rút ngắn đến 24h, cho bạn thỏa mãn
												đam mê kiến tạo không gian sống, làm việc và tận hưởng, khẳng định gu
												thẩm mỹ cá nhân không thể nhầm lẫn.
												Chính sách bán hàng, hậu mãi độc đáo:
												<br>
												cam kết gỗ thật 100% bồi thường đến 300%, bao đổi trả trong 7 ngày,
												chính sách
												bảo hành 5 năm đến trọn đời, dịch vụ xài cũ bán lại, dịch vụ cho thuê -
												sở hữu duy nhất tại Việt Nam
												<br>
												Đến Với GKconcept, bạn sẽ được thỏa mãn đam mê trong tôn tạo không gian,
												khi đó, dù cuộc sống bộn bề, bạn sẽ tận hưởng sự trọn vẹn, sẽ là chính
												mình khi trở về với không gian do chính mình tôn tạo, và đây là không
												gian mà ta luôn muốn quay về.
												<br>
												GKconcept- ĐƯỢC LÀ CHÍNH MÌNH, THỎA SỨC ĐAM MÊ TRONG TÔN TẠO KHÔNG GIAN
												ĐĂNG KÝ

											</p>
										</div>
									</div>
									<article class="card">
										<img class="card-img-top" :src="this.image_step" @error="setAltImg"
											 alt="Card image">
									</article>
								</div>

							</div>
						</div>
					</template>
					<div class="container-fluid mt-5">
						<div class="row">
							<section>
								<div class="container-fluid">
									<div class="row">
										<div class="card-columns">
											<div class="card card-pin" v-for="item in filteredBlogs">
												<a href="#" v-on:click="productdetail(item.virtuemart_product_id)">
													<img class="card-img" :src="item.file_url" @error="setAltImg"
														 alt="cart">
													<div class="overlay">
														<h5 class="card-title title">{{ item.product_name }}</h5>
													</div>
												</a>
											</div>
										</div>
									</div>
								</div>
							</section>


						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12">
					<div class="container-fluid mt-5">
                        <div class="row">
                            <template v-if="if_album_show">
                                <section class="mt-4 mb-5">
                                    <div class="container-fluid">
                                        <div class="row">

                                            <div class="mainheading">
                                                <h1 class="sitetitle">Chi Tiết Bộ Sưu Tập</h1>
                                                <p class="lead">
                                                    Chi Tiết Bộ Sưu Tập!!
                                                </p>
                                            </div>

                                            <div class="container-fluid">
                                                <div class="row">

                                                    <!-- Begin Post -->
                                                    <div class="col-md-12 col-xs-12">
                                                        <div class="mainheading">

                                                            <h1 class="posttitle">
                                                                {{ this.album.title}}
                                                            </h1>

                                                        </div>

                                                        <!-- Begin Featured Image -->
                                                        <img class="featured-image img-fluid"
                                                             :src="this.album.images"
                                                             alt="">
                                                        <!-- End Featured Image -->

                                                        <!-- Begin Post Content -->
                                                        <div class="article-post">
                                                            <p>
                                                              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate dignissimos eveniet laudantium reiciendis rem suscipit voluptatibus? Adipisci aperiam consectetur natus nisi quos. Cum dignissimos excepturi ipsam similique! Asperiores, iste, similique.
                                                            </p>
                                                        </div>
                                                        <!-- End Post Content -->

                                                        <!-- Begin Tags -->
                                                        <div class="after-post-tags">
                                                            <ul class="tags">
                                                                <li><a href="#">ke go</a></li>
                                                                <li><a href="#">tu go</a></li>
                                                                <li><a href="#">vach ngan</a></li>
                                                                <li><a href="#">ban hoc sinh</a></li>
                                                                <li><a href="#">giuong ngu</a></li>
                                                            </ul>
                                                        </div>
                                                        <!-- End Tags -->

                                                    </div>
                                                    <!-- End Post -->

                                                </div>
                                            </div>
                                            
                                        </div>
                                </section>
                            </template>
                        </div>
						<div class="row">
							<section class="mt-4 mb-5">
								<div class="container-fluid">
									<div class="row">
										<div class="mainheading">
											<h1 class="sitetitle">Bộ Sưu Tập</h1>
											<p class="lead">
												Album của Gkconcept.vn dành cho bạn !!
											</p>
										</div>
                                        <div class="row">
                                            <div class="card-columns">
                                                <div class="card card-pin" v-for="suutap in this.bosuutap">
                                                    <a v-on:click="albumdetail(suutap.link)">
                                                        <img class="img-fluid"
                                                             :src="suutap.images"
                                                             :alt="suutap.title"
                                                             @error="setAltImg"
                                                            >
                                                        <div class="overlay">
                                                            <h5 class="card-title title text-center">{{ suutap.title }}</h5>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        

									</div>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>

		</section>
	</template>
</main>
<style>


</style>

<script>


	const blogs = [<?php
		$i = 0;
		foreach ($this->dataGoodIdeas->data as $product) {
			if ($i > 30) {
				break;
			}
			
			$temp = new stdClass();
			foreach ($product->categoryItem as $category) {
				$data = array();
				$data[] = $category->virtuemart_category_id;
			}
			$temp->category_id = $data;
			$temp->file_url = $product->file_url;
			$temp->product_name = $product->product_name;
			$temp->virtuemart_product_id = $product->virtuemart_product_id;
			$temp->product_alias = $product->product_alias;
			$temp->slug = $product->slug;
			$i++;
			echo "{ category_id: '" . $temp->category_id . "',file_url: '" . $temp->file_url . "',virtuemart_product_id: '" . $temp->virtuemart_product_id . "',product_name : ' " . $temp->product_name . "', product_alias : ' " . $temp->product_name . "',slug : ' " . $temp->slug . "'},";
		}
		?>]


	var vm = new Vue( {
		el: '#appgoodideasalbum',
		data() {
			return {
				clubs: blogs,
				items: blogs,
				search: '',
				product: null,
				album: null,
				image_step: 'images/stories/virtuemart/product/resized/step.webp',
				show: false,
                link_images:'https://gkconcept.vn/images/stories/virtuemart/product/resized/khay-go-dung-do-an(1).webp',
				if_child: false,
                if_album: false,
				category: <?php echo $categoryData ? json_encode($categoryData) : '';  ?>,
                bosuutap: <?php echo $bosuutap ? json_encode($bosuutap) : '';  ?>,
			};
		},
		methods: {
			getCategoryFilter(id) {
				let url = 'index.php?option=com_congtacvien&task=goodideas.getAllProductByCategoryGoodIdeas&=virtuemart_category_id=' + id;
				var product = this;
				jQuery.ajax( {
					url: url,
					type: 'POST',
					dataType: 'JSON',
					data: {
						id
					},
					success: function (info) {
						product.clubs = info;
					},
				} );

				return true;
			},
			imgError() {
				let defaultImage = "https://gkconcept.vn/images/stories/virtuemart/product/resized/khay-go-dung-do-an(1).webp";
				this.src = this.defaultImg;
			},
            albumdetail(link){
			    let link_album = link;
                let url = 'index.php?option=com_congtacvien&task=album.getAlbum';
                var album = this;
                jQuery.ajax( {
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        link
                    },
                    success: function (info) {
                        console.log(info);
                        console.log(album.if_album);
                        album.if_album = true;
                        console.log(album.if_album);
                        album.album = info;
                    },
                } );

                return true;
            },
			productdetail(id) {
				let url = 'index.php?option=com_congtacvien&task=goodideas.getProductDetailGoodIdeas&=virtuemart_product_id=' + id;
				var product = this;
				jQuery.ajax( {
					url: url,
					type: 'POST',
					dataType: 'JSON',
					data: {
						id
					},
					success: function (info) {
						product.if_child = true;
						product.product = info;
					},
				} );

				return true;
			},
			setAltImg(event) {
				event.target.src = "https://gkconcept.vn/images/stories/virtuemart/product/resized/khay-go-dung-do-an(1).webp"
			},
		},
		computed: {
			if_child_show() {
				if (this.if_child == true) {
					return true;
				}
				return false;
			},
            if_album_show()
            {
                if (this.if_album == true)
                {
                    return true;
                }
                
                return false;
            },
			filteredBlogs() {
				if (this.search !== '') {
					return this.clubs.filter( (blog) => {
						return blog.product_alias.match( this.search )
					} );
				}

				return this.clubs;
			}


		}

	} )
</script>
