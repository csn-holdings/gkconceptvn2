<?php
defined('_JEXEC') || die('=;)');

class CTVViewGoodIdeas extends JViewLegacy
{

    protected $params;
    protected $form;
    protected $item;
    protected $return_page;
    protected $app;
    protected $user;
    protected $input;
    protected $authorise;
    protected $vendor;
    protected $product;
    protected $categories;

    function __construct($config = array())
    {
        $this->input = JFactory::getApplication()->input;
        $this->params = JComponentHelper::getParams('com_congtacvien');
        $this->app = JFactory::getApplication();
        $this->user		= JFactory::getUser();
        $this->document = JFactory::getDocument();

        $asset		= 'com_congtacvien';
        $this->authorise['admin']  = $this->user->authorise('inventory.admin', $asset);
        $this->authorise['edit']  = $this->user->authorise('inventory.edit', $asset);
        $this->authorise['editown']  = $this->user->authorise('inventory.editown', $asset);
        $this->authorise['view'] = $this->user->authorise('inventory.view', $asset);
        $this->authorise['statistic'] = $this->user->authorise('inventory.statistic', $asset);
        
        parent::__construct($config);
    }
    
    public function display($tpl = null)
    {
        $layout = $this->getLayout();

        switch ($layout) {
            case 'config':
                self::_layoutConfig($tpl);
                return;
            default:
                self::_layoutDefault($tpl);
                return;
        }

        parent::display($tpl);
    }

    private function _layoutDefault($tpl)
    {
        $asset		= 'com_congtacvien';
        $this->authorise['admin']  = $this->user->authorise('shop.admin', $asset);
        $this->authorise['edit']  = $this->user->authorise('shop.edit', $asset);
        $this->authorise['editown']  = $this->user->authorise('shop.editown', $asset);
        $this->authorise['view'] = $this->user->authorise('shop.view', $asset);
        $this->authorise['statistic'] = $this->user->authorise('shop.statistic', $asset);
        
        JHtml::_('jquery.framework');

        $this->document->addStyleSheet("//fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons", array('relative' => 'stylesheet'), array('type'=>'text/css'));
        $this->document->addStyleSheet("//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css", array('relative' => 'stylesheet'));
        $this->document->addStyleSheet(JURI::root(true)."/templates/sj_shoppystore/asset/bootstrap/css/bootstrap.min.css", array('relative' => 'stylesheet'));
        $this->document->addStyleSheet("//cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css", array('relative' => 'stylesheet'));
        $this->document->addStyleSheet("//cdn.jsdelivr.net/npm/placeholder-loading/dist/css/placeholder-loading.min.css", array('relative' => 'stylesheet'));
        
	    JHtml::_('jquery.framework');
	    
	    $this->document->addScript("//ajax.googleapis.com/ajax/libs/angularjs/1.7.9/angular.min.js");
	    $this->document->addScript("//code.angularjs.org/1.4.2/angular-animate.min.js");
	    $this->document->addScript("//cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/2.1.0/toaster.js");
        $this->document->addScript(JURI::root(true)."/media/com_congtacvien/js/popper.min.js");
	    $this->document->addScript(JURI::root(true)."/media/com_congtacvien/js/ui-bootstrap-tpls-3.0.6.min.js");
	    
        $model = $this->getModel();
        $this->dataGoodIdeas = $model->getProductGoodIdeas();

        parent::display($tpl);
    }

}