<?php

// no direct access
	defined( '_JEXEC' ) or die( 'Restricted access' );
	
	use Joomla\CMS\Response\JsonResponse;
	
	jimport('joomla.application.component.controller');
	
	class CTVControllerAlbum extends JControllerLegacy
	{
		protected $user;
		protected $authorise;
		protected $input;
		
		function __construct($config = array())
		{
			$this->input = JFactory::getApplication()->input;
			$this->user = JFactory::getUser();
			
			parent::__construct($config);
		}
		
		
		public function getAlbum()
		{
			if(empty($_POST['link']) && $_POST['link'])
			{
				return;
			}
			
			$link = $_POST['link'];
			$query = parse_url($link, PHP_URL_QUERY);
			parse_str($query, $queryArray);
			
			$virtuemart_category_id = $queryArray['virtuemart_category_id'];
			$menuitem = $queryArray['menuitem'];
			
			$menu = JFactory::getApplication()->getMenu();
			$items = $menu->getMenu();
			$bosuutap = array();
			
			foreach ($items as $ite)
			{
				if(
				$ite->query['option'] = 'com_congtacvien' &&
					$ite->query['view'] = 'album' &&
						$ite->query['virtuemart_category_id'] == $virtuemart_category_id &&
						$ite->component == 'com_congtacvien' &&
						$ite->id == $menuitem
				)
				{
					$ite->images = $ite->params->get('menu_image');
					$bosuutap[] = $ite;
				}
			}
			
			echo json_encode($bosuutap[0]);
			JFactory::getApplication()->close();
			
		}
		
	}