<?php

// no direct access
	defined( '_JEXEC' ) or die( 'Restricted access' );
	
	use Joomla\CMS\Response\JsonResponse;
	
	jimport('joomla.application.component.controller');
	
	class CTVControllerGoodIdeas extends JControllerLegacy
	{
		protected $user;
		protected $authorise;
		protected $input;
		
		function __construct($config = array())
		{
			$this->input = JFactory::getApplication()->input;
			$this->user = JFactory::getUser();
			
			parent::__construct($config);
		}
		
		public function getProductDetailGoodIdeas()
		{
			if(empty($_POST['id']) && $_POST['id'])
			{
				return;
			}
			
			$id= $_POST['id'];
			$model = $this->getModel('GoodIdeas');
			$result = $model->getProductDetailGoodIdeasModel($id);
			echo json_encode($result);
			JFactory::getApplication()->close();
			
		}
		
		public function getAllProductByCategoryGoodIdeas()
		{
			if(empty($_POST['id']) && $_POST['id'])
			{
				return;
			}
			
			$id= $_POST['id'];
			$model = $this->getModel('GoodIdeas');
			$result = $model->getAllProductByCategoryGoodIdeasModel($id);
			echo json_encode($result);
			JFactory::getApplication()->close();
			
		}
		
	}