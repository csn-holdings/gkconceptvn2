<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$config = JFactory::getConfig();
$app = JFactory::getApplication();
$db = JFactory::getDBO();
$time=time();

$component_setting = PX::getComponentSetting();

$component=new plgComponentVirtuemart();

?>
<script>
window.addEvent('domready', function(){
	$$('textarea').addEvent('keydown', function(event){
		if (event.code==9 && !event.shift) {
			var start = this.selectionStart;
			var end = this.selectionEnd;
			this.value = this.value.substring(0, start)
				  + "\t"
				  + this.value.substring(end);
			this.selectionStart = this.selectionEnd = start + 1;				            	
			if(event.preventDefault) {
				event.preventDefault();
			}			
			return false;
		}
	});
});
</script>
<style>
.export-type-button {
	width: 20px !important;
	text-align: center;
}
.export-type-radio-label {
	font-weight: normal !important;
}
</style>

	<?php
	echo JHtml::_('bootstrap.startTabSet', 'config-pane'.$time, array('active' => 'tab1'));
	echo JHtml::_('bootstrap.addTab', 'config-pane'.$time, 'tab1', JText :: _('Basic setting'));
	?>
	<fieldset class="adminform">
		<legend><?php echo JText::_('Setting for VirtueMart'); ?></legend>

		<table class="admintable" width="100%">

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Export published products only'); ?>:
			</td>
			<td>
				<fieldset id="jform_published_only" class="radio">
					<input type="radio" id="jform_published_only0" name="jform[published_only]" value="1" <?php echo $component_setting->get('published_only')?'CHECKED':''; ?> />
					<label for="jform_published_only0"><?php echo JText::_( 'JYES' ); ?></label>
					<input type="radio" id="jform_published_only1" name="jform[published_only]" value="0" <?php echo $component_setting->get('published_only')?'':'CHECKED'; ?> />
					<label for="jform_published_only1"><?php echo JText::_( 'JNO' ); ?></label>
				</fieldset>

			</td>
			<td width="50%">
				<?php echo JText::_('If YES, component exports published products only'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Export only products in stock'); ?>:
			</td>
			<td>
				<fieldset id="jform_stock_only" class="radio">
					<input type="radio" id="jform_stock_only0" name="jform[stock_only]" value="1" <?php echo $component_setting->get('stock_only')?'CHECKED':''; ?> />
					<label for="jform_stock_only0"><?php echo JText::_( 'JYES' ); ?></label>
					<input type="radio" id="jform_stock_only1" name="jform[stock_only]" value="0" <?php echo $component_setting->get('stock_only')?'':'CHECKED'; ?> />
					<label for="jform_stock_only1"><?php echo JText::_( 'JNO' ); ?></label>
				</fieldset>
			</td>
			<td width="50%">
				<?php echo JText::_('If YES, component exports only products in stock'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Export only products with price higher then 0'); ?>:
			</td>
			<td>
				<fieldset id="jform_plus_price_only" class="radio">
					<input type="radio" id="jform_plus_price_only0" name="jform[plus_price_only]" value="1" <?php echo $component_setting->get('plus_price_only')?'CHECKED':''; ?> />
					<label for="jform_plus_price_only0"><?php echo JText::_( 'JYES' ); ?></label>
					<input type="radio" id="jform_plus_price_only1" name="jform[plus_price_only]" value="0" <?php echo $component_setting->get('plus_price_only')?'':'CHECKED'; ?> />
					<label for="jform_plus_price_only1"><?php echo JText::_( 'JNO' ); ?></label>
				</fieldset>
			</td>
			<td width="50%">
				<?php echo JText::_('If YES, component exports only products with price > 0'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Export price from certain shoppergroup'); ?>:
			</td>
			<td>
				<?php
					$db = JFactory::getDBO();
					$query = "SELECT DISTINCT pp.virtuemart_shoppergroup_id AS value, sg.shopper_group_name AS text
						FROM `#__virtuemart_product_prices` AS pp
						LEFT JOIN `#__virtuemart_shoppergroups` AS sg ON pp.virtuemart_shoppergroup_id=sg.virtuemart_shoppergroup_id
						";
					$db->setQuery($query);
					$sg=$db->loadObjectList('value');
					if ($sg[0]) {
						if (!$sg[0]->text) {
							$sg[0]->text=JText::_('Without group');
						}
					}
					$not_used=new stdClass();
					$not_used->value='';
					$not_used->text=JText::_('--without specification--');
					array_unshift($sg, $not_used);
					echo JHTML::_('select.genericlist',   $sg, 'jform[shoppergroup_prices]', 'class="inputbox" ', 'value', 'text', $component_setting->get('shoppergroup_prices') );

				?>
			</td>
			<td width="50%">
				<?php echo JText::_('If you need to export prices of certain shopper group, choose here the requested shoppergroup'); ?>
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_("More categories export:"); ?>
			</td>
			<td>
				<select name="jform[category_list]">
					<option value="longest" <?php echo $component_setting->get('category_list')=='longest'?'selected':'';?>><?php echo JText::_("Longest"); ?></option>
					<option value="shortest" <?php echo $component_setting->get('category_list')=='shortest'?'selected':'';?>><?php echo JText::_("Shortest"); ?></option>
				</select>
			</td>
			<td width="50%">
				<?php echo JText::_("If more categories are assigned to the product, choose, which one is exported"); ?>
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_("Children:"); ?>
			</td>
			<td>
				<select name="jform[children]">
					<option value="only_parents" <?php echo $component_setting->get('children')=='only_parents'?'selected':'';?>><?php echo JText::_("Export only parent products /no children/"); ?></option>
					<option value="parent_and_children" <?php echo $component_setting->get('children')=='parent_and_children'?'selected':'';?>><?php echo JText::_("Export all products /both parents and children/"); ?></option>
				</select>
			</td>
			<td width="50%">
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Remove HTML tags from short description'); ?>:
			</td>
			<td>
				<fieldset id="jform_strip_tags_sdesc" class="radio">
					<input type="radio" id="jform_strip_tags_sdesc0" name="jform[strip_tags_sdesc]" value="1" <?php echo $component_setting->get('strip_tags_sdesc')?'CHECKED':''; ?> />
					<label for="jform_strip_tags_sdesc0"><?php echo JText::_( 'JYES' ); ?></label>
					<input type="radio" id="jform_strip_tags_sdesc1" name="jform[strip_tags_sdesc]" value="0" <?php echo $component_setting->get('strip_tags_sdesc')?'':'CHECKED'; ?> />
					<label for="jform_strip_tags_sdesc1"><?php echo JText::_( 'JNO' ); ?></label>
				</fieldset>
			</td>
			<td width="50%">
				<?php echo JText::_('If YES, HTML tags will be removed from the field SHORT DESCRIPTION. If not, all the tags will be converted to entities.'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Export long description'); ?>:
			</td>
			<td>
				<select name="jform[long_desc]">
					<option value="no" <?php echo $component_setting->get('long_desc')=='no'?'selected':'';?>><?php echo JText::_("Do not export"); ?></option>
					<option value="yes_strip" <?php echo $component_setting->get('long_desc')=='yes_strip'?'selected':'';?>><?php echo JText::_("Yes, export and strip html tags"); ?></option>
					<option value="yes_raw" <?php echo $component_setting->get('long_desc')=='yes_raw'?'selected':'';?>><?php echo JText::_("Yes, export without any changes"); ?></option>
				</select>
			</td>
			<td width="50%">
				<?php echo JText::_('If YES, long description is going to export /HTML tags will be removed automaticaly/. <br />If NOT, long description is not exported /RECOMMENDED, because it saves the memory of the server/.'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Use language code in product URL'); ?>:
			</td>
			<td>
				<fieldset id="language_code_in_url" class="radio">
					<input type="radio" id="jform_language_code_in_url0" name="jform[language_code_in_url]" value="1" <?php echo $component_setting->get('language_code_in_url')?'CHECKED':''; ?> />
					<label for="jform_language_code_in_url0"><?php echo JText::_( 'JYES' ); ?></label>
					<input type="radio" id="jform_language_code_in_url1" name="jform[language_code_in_url]" value="0" <?php echo $component_setting->get('language_code_in_url')?'':'CHECKED'; ?> />
					<label for="jform_language_code_in_url1"><?php echo JText::_( 'JNO' ); ?></label>
				</fieldset>
			</td>
		</tr>
		
	</table>
	</fieldset>
	<?php
	echo JHtml::_('bootstrap.endTab');
	echo JHtml::_('bootstrap.addTab', 'config-pane'.$time, 'tab2', JText :: _('Categories'));
	?>
	<fieldset class="adminform">
		<legend><?php echo JText::_('Categories setting'); ?></legend>

		<table class="admintable" width="100%" border="0">

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('Map categories to alternative categories'); ?>:
			</td>
			<td>
				<textarea cols="80" rows="10" name="map_categories" style="width: 700px;"><?php echo htmlspecialchars(base64_decode($component_setting->get('map_categories'))); ?></textarea>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td colspan="2">
				<?php echo JText::_('Map categories - description'); ?>	
			</td>
		</tr>

		</table>
	</fieldset>
	<?php
	echo JHtml::_('bootstrap.endTab');
	echo JHtml::_('bootstrap.addTab', 'config-pane'.$time, 'tab3', JText :: _('Advanced options'));
	?>
	<fieldset class="adminform">
		<legend><?php echo JText::_('Advanced options'); ?></legend>

		<table class="admintable" width="100%" border="0">

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('PHP code to run during product preparing'); ?>:
			</td>
			<td>
				<textarea cols="80" rows="10" name="prepare_code" style="width: 700px;"><?php echo htmlspecialchars(base64_decode($component_setting->get('prepare_code'))); ?></textarea>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td colspan="2">
				<?php echo JText::_('You can modify object of product named product anyhow you want. Usually used for setting of your delivery date/availability. There is a more info in the manual.'); ?>	
			</td>
		</tr>

		<tr>
			<td width="100" align="right" class="key">
				<?php echo JText::_('SQL limit'); ?>:
			</td>
			<td>
				<input type="text" name="jform[limit]" id="limit" value="<?php echo $component_setting->get('limit'); ?>">
			</td>
			<td>
			</td>
		</tr>
		</table>
	</fieldset>
<?php
	echo JHtml::_('bootstrap.endTab');
?>
<input type="hidden" name="name" value="virtuemart" />
<input type="hidden" name="type" value="component" />

