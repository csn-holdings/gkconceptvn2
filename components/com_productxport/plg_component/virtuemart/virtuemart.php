<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

DEFINE('_PRODUCTXPORT_DEBUG', 0);
DEFINE('_PRODUCTXPORT_MAXLEGTH', 600);

class plgComponentVirtuemart extends JObject {

	var $parent;
	var $type;
	var $title;
	var $code;
	var $setting;
	var $valid_inst;
	var $vm_config;
	var $vm_lang;

	var $discounts_before_tax;
	var $discounts_after_tax;
	var $discounts_categories_before_tax;
	var $discounts_categories_after_tax;
	var $discounts_manufacturers_before_tax;
	var $discounts_manufacturers_after_tax;
	var $_data;
	var $db_resource;

	var $categories_full;
	var $map_categories;
	var $tollfree;
	var $tax_rates;
	var $urlItemid;

	var $memory_limit;
	var $memory_leak;
	var $max_execution_time;

	var $max_memory_used;

	var $debug_mode;

	function __construct($parent=null) {

		if (JFile::exists(JPATH_ADMINISTRATOR.'/components/com_virtuemart/helpers/config.php')) {
			$this->valid_inst = true;
		} else {
			$this->valid_inst = false;
		}

		if ($this->valid_inst) {
			if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR.'/components/com_virtuemart/helpers/config.php');
			$config= VmConfig::loadConfig();

			$app = JFactory::getApplication();

			$this->vm_lang=VmConfig::$vmlang;

			$this->parent=$parent;
			$this->type='component';
			$this->title='VirtueMart';
			$this->code='virtuemart';
			$this->directory='virtuemart';
			$this->shopName = $app->getCfg('sitename');

			@$this->shopUrl = JURI::root();

			$this->setting	= new JRegistry();
			if (JFile::exists(JPATH_ROOT.'/components/com_productxport/plg_component/'.$this->directory.'/setting.ini')) {
				$this->setting->loadFile(JPATH_ROOT.'/components/com_productxport/plg_component/'.$this->directory.'/setting.ini', 'INI');
				//$this->setting->set('prepare_code',base64_decode($this->setting->get('prepare_code')));
			}

			$this->categories_full = array();
			$this->tollfree = array();

			if ($this->setting->get('map_categories')) {
				$map1=explode("\n",base64_decode($this->setting->get('map_categories')));
				foreach ($map1 as $map2) {
					list($cid,$cmap)=explode('=', trim($map2));
					$this->map_categories[$cid]=$cmap;
				}
			}

			$this->max_execution_time = ini_get('max_execution_time');
			$memory_limit = ini_get('memory_limit');
			if ($memory_limit) {
				if (preg_match("/([0-9]*)/", $memory_limit, $regs)) {
					$this->memory_limit=$regs[1]*1024*1024;
				} else {
					$this->memory_limit = false;
				}

			} else {
				$this->memory_limit = false;
			}
		}
	}

	/**
	* This function prepares specific product for export
	*/
	function prepareProduct(&$product) {
		$db	= JFactory::getDBO();

		# SET BY SQL - id of product
		#$product->id;
		$plgComponentSetting = PX::getComponentSetting();

		if ($this->memory_limit) {
			if (memory_get_usage() + $this->max_memory_used >= $this->memory_limit) {
				echo JText::_("MEMORY LIMIT EXCEEDED currently used:")." ".memory_get_usage()."\n";
				echo JText::_("Ask your provider to increase memory_limit for your website or upgrade to productXport proffesional");

				die();
			}
		}
		if ($this->max_execution_time) {
			if (round(PX::getExecTime()) >= $this->max_execution_time) {
				echo JText::_("MAXIMAL EXECUTION TIME EXCEEDED:")." ".round(PX::getExecTime())."s\n";
				echo JText::_("Ask your provider to increase max_execution_time for your website or upgrade to productXport proffesional");

				die();
			}
		}
		$current_memory_used = memory_get_usage();

		$product->name = $product->name;

		//$product->category_id = $product->category_id?$product->category_id:0;
		if ($product->category_id) {
			if (is_numeric($product->category_id)) {
				list($product->category_id)=explode(',',$product->category_id);
				$product->category = $this->categories_full[$product->category_id];
				$product->category_ids = array($product->category_id);
			} else {
				$product->category_ids = explode(',', $product->category_id);
				$product->category_id = $this->getCategoryId($product->category_ids, $this->setting->get('category_list'));
				if ($product->category_id) {
					$product->category = $this->categories_full[$product->category_id];
				}
			}
		}

		if ($this->map_categories) {
			if ($product->category_id) {
				if (isset($this->map_categories[$product->category_id])) {
					$product->category_alternative=$this->map_categories[$product->category_id];
				}
			}
		}

//		$product->sef_url = $this->JRouteProductXport("index.php?option=com_virtuemart&page=shop.product_details&category_id=$product->category_id"
//			.$flypage."&product_id=" . $product->id. $urlItemid, false, 2, true);
//		$product->nonsef_url = $this->JRouteProductXport("index.php?option=com_virtuemart&page=shop.product_details&category_id=$product->category_id"
//			.$flypage."&product_id=" . $product->id. $urlItemid, false, 2, false);
		$product->sef_url = $this->JRouteProductXport("index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=".$product->id."&virtuemart_category_id=".$product->category_id.$this->urlItemid, false, 2, true);
		$product->nonsef_url = $this->JRouteProductXport("index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=".$product->id."&virtuemart_category_id=".$product->category_id.$this->urlItemid, false, 2, false);

		if ($this->setting->get('strip_tags_sdesc')) {
			$product->short_description = strip_tags($product->short_description);
		}
		$product->short_description = trim($product->short_description);

		switch ($this->setting->get('long_desc')) {
			case '1':
			case 'yes_strip':
				@$product->long_description = trim(strip_tags($product->long_description));
				break;
		}
		@$product->long_description = str_replace('&nbsp;', ' ',$product->long_description);


		# change image filename to full URL of image
		$product->image = $this->image_url($product->id, $product->image);

		### SHOULD BE SET BY "Advanced options" (PHP code to run during product preparing)
		//$product->availability = "0";
		//$product->availability_date = "0";
		//$product->availability_in = "0";

		# SET BY SQL
		// $product->stock = (int)$product->product_in_stock;

		if ($product->product_tax_id > 0) {
			$product->vat = $this->tax_rates[$product->product_tax_id]->tax;
		} else {
			foreach ($this->tax_rates as $tax_rate) {
				$product->vat = $tax_rate->tax;
				break;
			}
		}
		$product->orig_price=$product->price;
		$product->orig_price_vat=$product->orig_price*(1+$product->vat);

		# APPLY GENERAl RULES
		if ($product->product_discount_id == 0) {
			$product->product_discounts=array();
			# go through all discount and apply general
			foreach ($this->discounts_before_tax as $key=>$v) {
				if (!$this->discounts_before_tax[$key]->category_ids && !$this->discounts_before_tax[$key]->manufacturer_ids) {
					$product->product_discounts[]=$key;
				}
			}
			# go through categories discounts and apply them
			if (is_array($product->category_ids)) {
				foreach ($product->category_ids as $cid) {
					if (isset($this->discounts_categories_before_tax[$cid])) {
						foreach ($this->discounts_categories_before_tax[$cid] as $d) {
							if (!in_array($d, $product->product_discounts)) {
								$product->product_discounts[]=$d;
							}
						}
					}
				}
			}
			# apply manufacturer discount if exists
			if (isset($this->discounts_manufacturers_before_tax[$product->manufacturer_id])) {
				foreach ($this->discounts_manufacturers_before_tax[$product->manufacturer_id] as $d) {
					if (!in_array($d, $product->product_discounts)) {
						$product->product_discounts[]=$d;
					}
				}
			}

			foreach ($product->product_discounts as $d) {
				if (isset($this->discounts_before_tax[$d])) {
					switch ($this->discounts_before_tax[$d]->calc_value_mathop) {
						case '+':
							$product->price+=$this->discounts_before_tax[$d]->calc_value;
							break;
						case '-':
							$product->price-=$this->discounts_before_tax[$d]->calc_value;
							break;
						case '+%':
							$product->price+=$product->price*($this->discounts_before_tax[$d]->calc_value/100);
							break;
						case '-%':
							$product->price-=$product->price*($this->discounts_before_tax[$d]->calc_value/100);
							break;
					}
				}
			}
		}
		# APPLY SPECIFIC RULE
		if ($product->product_discount_id > 0) {
			if (isset($this->discounts_before_tax[$product->product_discount_id])) {
				switch ($this->discounts_before_tax[$product->product_discount_id]->calc_value_mathop) {
					case '+':
						$product->price+=$this->discounts_before_tax[$product->product_discount_id]->calc_value;
						break;
					case '-':
						$product->price-=$this->discounts_before_tax[$product->product_discount_id]->calc_value;
						break;
					case '+%':
						$product->price+=$product->price*($this->discounts_before_tax[$product->product_discount_id]->calc_value/100);
						break;
					case '-%':
						$product->price-=$product->price*($this->discounts_before_tax[$product->product_discount_id]->calc_value/100);
						break;
				}
			}
		}

		$product->price_vat = $product->price*($product->vat+1);
		$product->price = $product->price;

		# APPLY GENERAl RULES
		if ($product->product_discount_id == 0) {
			$product->product_discounts=array();
			# go through all discount and apply general
			foreach ($this->discounts_after_tax as $key=>$v) {
				if (!$this->discounts_after_tax[$key]->category_ids && !$this->discounts_after_tax[$key]->manufacturer_ids) {
					$product->product_discounts[]=$key;
				}
			}
			# go through categories discounts and apply them
			if (is_array($product->category_ids)) {
				foreach ($product->category_ids as $cid) {
					if (isset($this->discounts_categories_after_tax[$cid])) {
						foreach ($this->discounts_categories_after_tax[$cid] as $d) {
							if (!in_array($d, $product->product_discounts)) {
								$product->product_discounts[]=$d;
							}
						}
					}
				}
			}
			# apply manufacturer discount if exists
			if (isset($this->discounts_manufacturers_after_tax[$product->manufacturer_id])) {
				foreach ($this->discounts_manufacturers_after_tax[$product->manufacturer_id] as $d) {
					if (!in_array($d, $product->product_discounts)) {
						$product->product_discounts[]=$d;
					}
				}
			}
			foreach ($product->product_discounts as $d) {
				if (isset($this->discounts_after_tax[$d])) {
					switch ($this->discounts_after_tax[$d]->calc_value_mathop) {
						case '+':
							$product->price_vat+=$this->discounts_after_tax[$d]->calc_value;
							$product->price = $product->price_vat/($product->vat+1);
							break;
						case '-':
							$product->price_vat-=$this->discounts_after_tax[$d]->calc_value;
							$product->price = $product->price_vat/($product->vat+1);
							break;
						case '+%':
							$product->price+=$product->price*($this->discounts_after_tax[$d]->calc_value/100);
							$product->price_vat+=$product->price_vat*($this->discounts_after_tax[$d]->calc_value/100);
							break;
						case '-%':
							$product->price-=$product->price*($this->discounts_after_tax[$d]->calc_value/100);
							$product->price_vat-=$product->price_vat*($this->discounts_after_tax[$d]->calc_value/100);
							break;
					}
				}
			}
		}
		# APPLY SPECIFIC RULE
		if ($product->product_discount_id>0) {
			if (isset($this->discounts_after_tax[$product->product_discount_id])) {
				switch ($this->discounts_after_tax[$product->product_discount_id]->calc_value_mathop) {
					case '+':
						$product->price_vat+=$this->discounts_after_tax[$product->product_discount_id]->calc_value;
						$product->price = $product->price_vat/($product->vat+1);
						break;
					case '-':
						$product->price_vat-=$this->discounts_after_tax[$product->product_discount_id]->calc_value;
						$product->price = $product->price_vat/($product->vat+1);
						break;
					case '+%':
						$product->price+=$product->price*($this->discounts_after_tax[$product->product_discount_id]->calc_value/100);
						$product->price_vat+=$product->price_vat*($this->discounts_after_tax[$product->product_discount_id]->calc_value/100);
						break;
					case '-%':
						$product->price-=$product->price*($this->discounts_after_tax[$product->product_discount_id]->calc_value/100);
						$product->price_vat-=$product->price_vat*($this->discounts_after_tax[$product->product_discount_id]->calc_value/100);
						break;
				}
//			} else {
//				$product->product_discount_id = 0;
			}
		}

		if (($product->override==1) && ($product->product_override_price>0)) {
			$product->price_vat = $product->product_override_price;
			$product->price = $product->product_override_price/($product->vat+1);
		} else if (($product->override==-1) && ($product->product_override_price>0)) {
			$product->price = $product->product_override_price;
			$product->price_vat = $product->price*($product->vat+1);
		}

		if ($plgComponentSetting->get('prepare_code')) {
			$c=base64_decode($this->setting->get('prepare_code'));
			if ($c) eval($c);
		}

		if ($this->max_memory_used < (memory_get_usage()-$current_memory_used)) {
			$this->max_memory_used = memory_get_usage()-$current_memory_used;
		}
		eval(base64_decode(PX::pcode));
		return $product;
	}

	/**
	* This function returns real image of product
	*/

	function prepareData() {

		if (!$this->valid_inst) {
			return;
		}
		
		$app = JFactory::getApplication();
		$uri	= JFactory::getURI();
		$db	= JFactory::getDBO();
		$input = $app->input;

		# load category list
		$this->getCategoryTreeArray();

		# load discount list
		$this->generate_discounts();

		# some db server needs this query
		$db->setQuery('SET SQL_BIG_SELECTS=1');
		$db->query();

		# prepare main images
		$db->setQuery("ALTER TABLE `#__virtuemart_product_medias` ORDER BY `virtuemart_product_id`,`ordering`");
		$db->query();
		$db->setQuery("TRUNCATE TABLE `#__px_vm_images`");
		$db->query();
		$db->setQuery("INSERT INTO #__px_vm_images (virtuemart_product_id,virtuemart_media_id,ordering)"
			."\n SELECT virtuemart_product_id, virtuemart_media_id, ordering"
			."\n FROM `#__virtuemart_product_medias` "
			."\n WHERE virtuemart_product_id>0 "
			."\n GROUP BY virtuemart_product_id");
		$db->query();

		# build main SQL query
		$query = $this->_buildQuery();
		if ($input->get('debug', false)) {
			echo "\n".$db->replacePrefix($query)."\n\n";
		}

		$db->setQuery( $query );

		// set to the user defined error handler
		$this->_data = $db->loadObjectList();
		if($db->getErrorNum()) {
			JError::raiseError( 500, $db->stderr());
		}
	}

	function prepareExport() {
		if (!$this->valid_inst) {
			return;
		}
		$this->prepareData();
	}

	function getRecord() {
		if (!$this->valid_inst) {
			return;
		}
		// Load Products
		if ($record_array = each($this->_data)) {
			$record =&$record_array[1];
			$this->prepareProduct($record);
			return $record;
		} else {
			return false;
		}
	}

	function getTotal() {
		if (!$this->valid_inst) {
			return;
		}
		$db	= JFactory::getDBO();
		$query = "SELECT COUNT(product_id) FROM `#__vm_product`";
		$db->setQuery( $query );
		return ($db->loadResult());
	}

	function getCategoryId($ids, $category_type) {
		if (!$this->valid_inst) {
			return;
		}
		$longest=0;
		$shortest=0;
		$max=0;
		$min=1000;
		foreach ($ids as $id) {
			@$c=substr_count($this->categories_full[$id], "\t")+1;
			if ($c > $max) {
				$longest = $id;
				$max = $c;
			}
			if ($c < $min) {
				$shortest = $id;
				$min = $c;
			}
		}
		switch ($category_type) {
			case 'shortest':
				return $shortest;
				break;
			case 'longest':
			default:
				return $longest;
				break;
		}
		return false;
	}

    function adminGetForm() {
		if (!$this->valid_inst) {
			echo "<b>".JText::_("VirtueMart is not installed")."</b>";
			return;
		}

		$ret_string="";

		$db	= JFactory::getDBO();

		// start capturing output into a buffer
		ob_start();
		// include the requested template filename in the local scope
		// (this will execute the view logic).
		include ('virtuemart.html.php');

		// done with the requested template; get the buffer and
		// clear it.
		$ret_string = ob_get_contents();
		ob_end_clean();

		return $ret_string;
	}


	/*
	 * Build standart array structure to save in config DB
	 */
	function adminBuildConfig() {
		$input = JFactory::getApplication()->input;
		$setting=array();
		$form = $input->get('jform', array(), 'array');

		if (is_array($form)) {
			foreach ($form as $key => $value) {
				$setting[$key]=$value;
			}
		}
		$setting['prepare_code'] = base64_encode(JRequest::getVar('prepare_code', null, 'post', 'string', JREQUEST_ALLOWRAW));
		$setting['map_categories'] = base64_encode(JRequest::getVar('map_categories', null, 'post', 'string', JREQUEST_ALLOWRAW));
		$setting['export_type'] = 'direct_export';

		return $setting;
	}

	function image_url($product_id, $image, $image_alternative= '') {
		if (!$image)
			$image=$image_alternative;
		if ($image != "") {
			if( substr( $image, 0, 4) == "http" ) {
				// URL
				$url = $image;
			} else {
				$url=JURI::root().$image;
			}
		} else {
			$url = '';
		}

		$url =	str_replace(':80/', '/', $url);
		return $url;
	}

	/**
	* This function is repsonsible for returning an array containing category information
	* @param boolean Show only published products?
	* @param string the keyword to filter categories
	*/
	function getCategoryTreeArray( $only_published=true, $keyword = "" ) {

			$db	= JFactory::getDBO();
			// Get only published categories

			$query = "SELECT c.virtuemart_category_id AS category_id, lang.category_name,"
			."\n cc.category_child_id as cid, cc.category_parent_id as parent,"
			."\n c.ordering AS list_order, c.published AS category_publish"
			."\n FROM #__virtuemart_category_categories AS cc"
			."\n LEFT JOIN #__virtuemart_categories AS c ON c.virtuemart_category_id=cc.category_child_id"
			."\n LEFT JOIN #__virtuemart_categories_".$this->vm_lang." AS lang ON c.virtuemart_category_id=lang.virtuemart_category_id"
			."\n WHERE c.virtuemart_category_id IS NOT NULL AND c.published=1"
			."\n ORDER BY c.ordering ASC, lang.category_name ASC"
			;


			// initialise the query in the $database connector
			// this translates the '#__' prefix into the real database prefix
			$db->setQuery( $query );
			$category_list = $db->loadAssocList('cid');

			// Transfer the Result into a searchable Array

			// establish the hierarchy of the menu
			$children = array();
			// first pass - collect children
			foreach ($category_list as $v ) {
				$v['category_name'] = stripslashes( $v['category_name'] );
				$pt = $v['parent'];
				$list = @$children[$pt] ? $children[$pt] : array();
				array_push( $list, $v );
				$children[$pt] = $list;
			}

			$this->categories_full=$this->buildTree($children);
			//print_r($seznam);
//			print_r($children);
//			print_r($this->categories_full);
//			print_r($this->categories_simple);

			//return $categories;
	}

	function buildTree(&$fields, $index=0) {
		$list=array();
		if (isset($fields[$index])) {
			if (is_array($fields[$index])) {
				foreach ($fields[$index] as $key => $value) {
					$list[$value['cid']]=$value['category_name'];
					$children=$this->buildTree($fields, $value['cid']);
					foreach ($children as $key_child => $value_child) {
						$list[$key_child]=$value['category_name']." \t ".$value_child;
					}
				}
			}
		}
		return $list;
	}

	function generate_discounts() {
		$db	= JFactory::getDBO();
		$plgComponentSetting = PX::getComponentSetting();

		# check manufaturers table (older version of VM does not have this table)
		$query="SELECT 1 FROM `#__virtuemart_calc_manufacturers` LIMIT 1";
		$db->setQuery($query);
		$manufacturers_table=$db->loadResult();

		# if shoppergroup is specified, prepare part of query
		if ($plgComponentSetting->get('shoppergroup_prices') != '') {
			$shoppergroup_limit=" AND csg.virtuemart_shoppergroup_id='".$plgComponentSetting->get('shoppergroup_prices')."'";
		} else {
			$shoppergroup_limit=" AND ((csg.virtuemart_shoppergroup_id IS NULL) OR (csg.virtuemart_shoppergroup_id=1) OR (csg.virtuemart_shoppergroup_id=2)) ";
		}

		if (empty($this->discounts_categories_before_tax)) {
			$this->discounts_categories_before_tax = array();
		}
		if (empty($this->discounts_categories_after_tax)) {
			$this->discounts_categories_after_tax = array();
		}
		if (empty($this->discounts_manufacturers_before_tax)) {
			$this->discounts_manufacturers_before_tax = array();
		}
		if (empty($this->discounts_manufacturers_after_tax)) {
			$this->discounts_manufacturers_after_tax = array();
		}
		if (empty($this->discounts_before_tax)) {
			#get discounts_before_tax 
			$starttime = time();
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			// get the beginning time of today
			$endofday = mktime(0, 0, 0, $month, $day, $year) - 1440;
			$query="SELECT c.virtuemart_calc_id, calc_value_mathop, calc_value, calc_currency,"
					. "\n GROUP_CONCAT(DISTINCT cc.virtuemart_category_id) AS category_ids"
					.($manufacturers_table?" \n, GROUP_CONCAT(DISTINCT cm.virtuemart_manufacturer_id) AS manufacturer_ids":'')
					. "\n FROM #__virtuemart_calcs AS c"
					. "\n LEFT JOIN #__virtuemart_calc_categories AS cc ON c.virtuemart_calc_id=cc.virtuemart_calc_id"
					. "\n LEFT JOIN #__virtuemart_calc_shoppergroups AS csg ON c.virtuemart_calc_id=csg.virtuemart_calc_id"
					.($manufacturers_table?"\n LEFT JOIN #__virtuemart_calc_manufacturers AS cm ON c.virtuemart_calc_id=cm.virtuemart_calc_id":'')
					. "\n WHERE (publish_up<=NOW() OR publish_up='0000-00-00 00:00:00') AND (publish_down>=NOW() OR publish_down='0000-00-00 00:00:00') "
					//. "\n AND calc_shopper_published=1 "
					. "\n AND calc_kind='DBTax' "
					. "\n AND published=1 "
					. "\n $shoppergroup_limit "
					. "\n GROUP BY c.virtuemart_calc_id "
					;
			$db->setQuery( $query );
			$this->discounts_before_tax = $db->loadObjectList('virtuemart_calc_id');
			foreach ($this->discounts_before_tax as $key=>$v) {
				if ($this->discounts_before_tax[$key]->category_ids) {
					$this->discounts_before_tax[$key]->category_ids=explode(',',$this->discounts_before_tax[$key]->category_ids);
					foreach ($this->discounts_before_tax[$key]->category_ids as $cid) {
						if (!isset($this->discounts_categories_before_tax[$cid])) {
							$this->discounts_categories_before_tax[$cid]=array();
						}
						$this->discounts_categories_before_tax[$cid][]=$key;
					}
				}
				if ($this->discounts_before_tax[$key]->manufacturer_ids) {
					$this->discounts_before_tax[$key]->manufacturer_ids=explode(',',$this->discounts_before_tax[$key]->manufacturer_ids);
					foreach ($this->discounts_before_tax[$key]->manufacturer_ids as $cid) {
						if (!isset($this->discounts_manufacturers_before_tax[$cid])) {
							$this->discounts_manufacturers_before_tax[$cid]=array();
						}
						$this->discounts_manufacturers_before_tax[$cid][]=$key;
					}
				}
			}

		}
		if (empty($this->discounts_after_tax)) {
			#get discounts_after_tax 
			$starttime = time();
			$year = date('Y');
			$month = date('n');
			$day = date('j');
			// get the beginning time of today
			$endofday = mktime(0, 0, 0, $month, $day, $year) - 1440;
			$query="SELECT c.virtuemart_calc_id, calc_value_mathop, calc_value, calc_currency, "
					. "\n GROUP_CONCAT(DISTINCT cc.virtuemart_category_id) AS category_ids"
					.($manufacturers_table?" \n, GROUP_CONCAT(DISTINCT cm.virtuemart_manufacturer_id) AS manufacturer_ids":'')
					. "\n FROM #__virtuemart_calcs AS c"
					. "\n LEFT JOIN #__virtuemart_calc_categories AS cc ON c.virtuemart_calc_id=cc.virtuemart_calc_id"
					. "\n LEFT JOIN #__virtuemart_calc_shoppergroups AS csg ON c.virtuemart_calc_id=csg.virtuemart_calc_id"
					.($manufacturers_table?"\n LEFT JOIN #__virtuemart_calc_manufacturers AS cm ON c.virtuemart_calc_id=cm.virtuemart_calc_id":'')
					. "\n WHERE (publish_up<=NOW() OR publish_up='0000-00-00 00:00:00') AND (publish_down>=NOW() OR publish_down='0000-00-00 00:00:00') "
					//. "\n AND calc_shopper_published=1 "
					. "\n AND calc_kind='DATax' "
					. "\n AND published=1 "
					. "\n $shoppergroup_limit "
					. "\n GROUP BY c.virtuemart_calc_id "
					;
			$db->setQuery( $query );
			$this->discounts_after_tax = $db->loadObjectList('virtuemart_calc_id');
			foreach ($this->discounts_after_tax as $key=>$v) {
				if ($this->discounts_after_tax[$key]->category_ids) {
					$this->discounts_after_tax[$key]->category_ids=explode(',',$this->discounts_after_tax[$key]->category_ids);
					foreach ($this->discounts_after_tax[$key]->category_ids as $cid) {
						if (!isset($this->discounts_categories_after_tax[$cid])) {
							$this->discounts_categories_after_tax[$cid]=array();
						}
						$this->discounts_categories_after_tax[$cid][]=$key;
					}
				}
				if ($this->discounts_after_tax[$key]->manufacturer_ids) {
					$this->discounts_after_tax[$key]->manufacturer_ids=explode(',',$this->discounts_after_tax[$key]->manufacturer_ids);
					foreach ($this->discounts_after_tax[$key]->manufacturer_ids as $cid) {
						if (!isset($this->discounts_manufacturers_after_tax[$cid])) {
							$this->discounts_manufacturers_after_tax[$cid]=array();
						}
						$this->discounts_manufacturers_after_tax[$cid][]=$key;
					}
				}
			}

		}
	}

	function JRouteProductXport($url, $xhtml = true, $ssl = null, $sef = true) {
		// Get the router
		$app	= JFactory::getApplication();
		$router = $app->getRouter();

		// Make sure that we have our router
		if (! $router) {
			return null;
		}

		if ( (strpos($url, '&') !== 0 ) && (strpos($url, 'index.php') !== 0) ) {
            return $url;
 		}
		if (!$sef) {
			if($xhtml) {
				$url = str_replace( '&', '&amp;', $url );
			}
			$url = JURI::root().$url;
			return $url;
		}

		// Build route
		$uri = $router->build($url);
		//$uri->_path = str_replace('/components/com_productxport/plg_component/'.$this->directory.'/simple_cron', '', $uri->_path);
		//$uri->_path = str_replace(REL_PATH_SCRIPT, '', $uri->_path);
		$url = $uri->toString(array('path', 'query', 'fragment'));

		// Replace spaces
		$url = preg_replace('/\s/', '%20', $url);

		if($xhtml) {
			$url = str_replace( '&', '&amp;', $url );
		}
		$url =	str_replace(':80', '', $url);

		if (strpos($url, '/') === 0) {
			$url = substr($url, 1);
		}
		$url = JURI::root().$url;
		return $url;
	}

	function _buildQuery() {
		$db	= JFactory::getDBO();

		$plgComponentSetting = PX::getComponentSetting();
		$export_type = $plgComponentSetting->get('export_type');
		$optional_fields = '';
		$shoppergroup_prices='';

		$left_join = '';
		switch ($this->setting->get('long_desc')) {
			case '1':
			case 'yes_strip':
			case 'yes_raw':
				$optional_fields .= ', product_desc AS long_description';
				break;
			case '0':
			case 'NoNo':
			case 'no':
			default:
				break;
		}

		# CUSTOM FIELDS
		if ($this->setting->get('custom_fields')) {
		}


		# GET Itemid for products
		$query="SELECT id FROM #__menu WHERE link='index.php?option=com_virtuemart' AND menutype!='menutype' ";
		$db->setQuery( $query );
		$Itemid = $db->loadResult();
		if ($Itemid) {
			$this->urlItemid="&Itemid=$Itemid";
		} else {
			$this->urlItemid="";
		}

		// set limit for SQL query - depends of XML feed version
		$sql_limit = '';
		if ($plgComponentSetting->get('limit')) {
			$sql_limit = " LIMIT ".$plgComponentSetting->get('limit');
		}

		# if shoppergroup is specified, prepare part of query
		if ($plgComponentSetting->get('shoppergroup_prices') != '') {
			$shoppergroup_prices=" AND pp.virtuemart_shoppergroup_id='".$plgComponentSetting->get('shoppergroup_prices')."'";
		}

		$pathinfo = pathinfo(__FILE__);

		# get tax rates
		$query = "SELECT virtuemart_calc_id,calc_value/100 AS tax FROM `#__virtuemart_calcs` WHERE calc_kind IN ('Tax', 'VatTax') AND published=1";
		$db->setQuery($query);
		$this->tax_rates = $db->loadObjectList('virtuemart_calc_id');

		// Load Products
		$query = "SELECT p.virtuemart_product_id as id, product_sku as code, lang.product_name as name, lang.product_s_desc as short_description, p.product_parent_id, m.file_url AS image,"
			."\n pp.product_price AS price,pp.override,pp.product_override_price,pp.product_discount_id, pp.product_tax_id, ppc.currency_code_3 AS currency, product_available_date, product_availability AS availability,"
			."\n GROUP_CONCAT(DISTINCT pc.virtuemart_category_id) AS category_id, pman.virtuemart_manufacturer_id AS manufacturer_id, mf_name AS manufacturer, p.product_in_stock AS stock, product_desc AS long_description, product_weight AS weight, product_gtin, product_mpn $optional_fields"
			."\n FROM #__virtuemart_products AS p"
			."\n LEFT JOIN #__virtuemart_products_".$this->vm_lang." AS lang ON p.virtuemart_product_id=lang.virtuemart_product_id"
//			."\n LEFT JOIN #__vm_tax_rate ON product_tax_id=tax_rate_id"
			."\n LEFT JOIN #__virtuemart_product_prices AS pp ON p.virtuemart_product_id=pp.virtuemart_product_id $shoppergroup_prices"
			."\n LEFT JOIN #__virtuemart_currencies AS ppc ON pp.product_currency=ppc.virtuemart_currency_id"
			."\n LEFT JOIN #__px_vm_images AS pm ON p.virtuemart_product_id=pm.virtuemart_product_id"
			."\n LEFT JOIN #__virtuemart_medias AS m ON pm.virtuemart_media_id=m.virtuemart_media_id"
//			."\n LEFT JOIN #__vm_product_votes AS v ON p.product_id=v.product_id"
			."\n LEFT JOIN #__virtuemart_product_categories AS pc ON p.virtuemart_product_id=pc.virtuemart_product_id"
//			."\n LEFT JOIN #__vm_category AS cc ON c.category_id = cc.category_id"
			."\n LEFT JOIN #__virtuemart_product_manufacturers AS pman ON p.virtuemart_product_id=pman.virtuemart_product_id"
			."\n LEFT JOIN #__virtuemart_manufacturers_".$this->vm_lang." AS man ON pman.virtuemart_manufacturer_id = man.virtuemart_manufacturer_id"
			."\n WHERE 1=1 "
			;
		$query .= "\n AND lang.product_name!='' ";
		if ($plgComponentSetting->get('children') == 'only_parents') {
			$query .= "\n AND p.product_parent_id=0 ";
		}
		if ($plgComponentSetting->get('published_only')) {
			$query .= "\n AND p.published = '1' ";
		}
		if ($plgComponentSetting->get('stock_only')) {
			$query .= "\n AND p.product_in_stock > 0 ";
		}
		if ($plgComponentSetting->get('plus_price_only')) {
			$query .= "\n AND pp.product_price > 0 ";
		}
//		//$query .= $higher_then;
		$query .= "\n GROUP BY p.virtuemart_product_id";
//		$query .= "\n ORDER BY p.product_id";
		if ($sql_limit) {
			$query .= $sql_limit;
		}
		return $query;
	}

}

