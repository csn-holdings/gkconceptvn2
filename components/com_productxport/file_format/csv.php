<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class fileFormatCsv {
	/**
	* serverFeeder xml creator function
	*/ 
	function buildOutput( &$component ) {
		$app = JFactory::getApplication();
		$input = $app->input;
		$document = JFactory::getDocument();
		$setting=PX::getServerSetting();

		if ($input->get('debug', false)) {
			$setting['debug_mode']=true;
		}
		if (@$setting['debug_mode']) {
			header('Content-Type: text/plain; charset=utf8');
		} else {
			header('Content-Type: text/csv');
			header("Content-Disposition: attachment; filename=".$setting['code'].".csv");
		}

		ob_implicit_flush(true);
		ob_end_flush();

		//print_r($setting);
		if (@$setting['debug_mode']) {
			echo "Memory using before component product processing: ".memory_get_usage() . "\n";
			echo "Time before component product processing: ".PX::getExecTime()."\n";
		}

		$component->prepareExport();

		$header_line = trim($setting['format_params']['header_line']);
		$header_line = str_replace('\t', "\t", $header_line);
		$header_line = str_replace('\r', "\r", $header_line);
		$header_line = str_replace('\n', "\n", $header_line);
		$line_format = trim($setting['format_params']['line_format']);
		$line_format = str_replace('\t', "\t", $line_format);
		$line_format = str_replace('\r', "\r", $line_format);
		$line_format = str_replace('\n', "\n", $line_format);
		$line_delimiter = $setting['format_params']['line_delimiter'];
		$line_delimiter = str_replace('\t', "\t", $line_delimiter);
		$line_delimiter = str_replace('\r', "\r", $line_delimiter);
		$line_delimiter = str_replace('\n', "\n", $line_delimiter);
		$replacements1 = $setting['format_params']['replacements'];
		$replace_from = array();
		$replace_to = array();
		if ($replacements1) {
			$replacements2 = explode("\n", $replacements1);
			if (is_array($replacements2)) {
				foreach ($replacements2 as $replacements3) {
					if (trim($replacements3)) {
						list($from, $to) = explode("=", trim($replacements3));
						$replace_from[] = $from;
						$replace_to[] = $to;
					}
				}
			}
		}

		if ($header_line) {
			echo $header_line.$line_delimiter;
		}

		$i=0;
		while ($product=$component->getRecord()) {
//			print_r($product);
			$line_export = $line_format;
			$fields_array=array();
			foreach ($setting['fields'] as $key=>$field) {
				$export=true;
				$value='';
				switch ($key) {
					case 'product_id':
						$value=$product->id;
						break;
					case 'product_sku':
						$value=$product->code;
						break;
					case 'url':
						switch ($field['url_type']) {
							case 'sef':
								$value=$product->sef_url;
								break;
							case 'nonsef':
								$value=$product->nonsef_url;
								break;
						}
						break;
					case 'name':
						$value=$product->name;
						break;
					case 'short_description':
						if ($field['strip_tags']) {
							$value=strip_tags($product->short_description);
						} else {
							$value=$product->short_description;
						}
						break;
					case 'long_description':
						if ($field['max_length']) {
							$value=mb_substr($product->long_description, 0, $field['max_length'], 'UTF-8');
						} else {
							$value=$product->long_description;
						}
						break;
					case 'image':
						$value=$product->image;
						break;
					case 'stock':
						$value=$product->stock;
						break;
					case 'currency':
						$value=$field['currency_value'];
						break;
					case 'category':
						if ($product->category) {
							$value=str_replace("\t", $field['delimiter'], $product->category);
						} else {
							if (is_numeric($product->category_id)) {
								$value=str_replace("\t", $field['delimiter'], $product->category);
							} else {
								if ($product->category_id) {
									$value=str_replace("\t", $field['delimiter'], $component->getCategoryName($product->category_id, $field['category_list']));
								}
							}
						}
						//$value=htmlentities($value);
						break;
					case 'category_alternative':
						$value=$product->category_alternative;
						break;
					case 'delivery_date':
						$value=$product->availability;
						break;
					case 'delivery_date_in':
						if (is_numeric($product->availability)) {
							switch ($field['time_format']) {
								case 'days':
									$value=(string)round($product->availability_in/24);
									break;
								case 'hours':
								default:
									$value=$product->availability_in;
									break;
							}
						} else {
							$value=$product->availability_in;
						}
						break;
					case 'delivery_date_date':
						$value=$product->availability_date;
						break;
					case 'vat':
						switch ($field['format']) {
							case 'percent':
								$value=$product->vat*100;
								break;
							case 'decimal':
								$value=$product->vat;
								break;
							case 'decimalext':
								$value=$product->vat+1;
								break;
						}
						break;
					case 'price':
						if (empty($field['decimal_symbol'])) $field['decimal_symbol']='.';
						if ($field['decimal_symbol']=='.')
							$value = round($product->price, $field['decimals']);
						else
							$value = number_format(round($product->price, $field['decimals']),$field['decimals'], $field['decimal_symbol'], '');
						break;
					case 'price_vat':
						if (empty($field['decimal_symbol'])) $field['decimal_symbol']='.';
						if ($field['decimal_symbol']=='.')
							$value = round($product->price_vat, $field['decimals']);
						else
							$value = number_format(round($product->price_vat, $field['decimals']),$field['decimals'], $field['decimal_symbol'], '');
						break;
					case 'orig_price':
						if (empty($field['decimal_symbol'])) $field['decimal_symbol']='.';
						if ($product->price != $product->orig_price) {
							if ($field['decimal_symbol']=='.')
								$value = round($product->orig_price, $field['decimals']);
							else
								$value = number_format(round($product->orig_price, $field['decimals']),$field['decimals'], $field['decimal_symbol'], '');
						}
						break;
					case 'orig_price_vat':
						if (empty($field['decimal_symbol'])) $field['decimal_symbol']='.';
						if ($product->price != $product->orig_price) {
							if ($field['decimal_symbol']=='.')
								$value = round($product->orig_price_vat, $field['decimals']);
							else
								$value = number_format(round($product->orig_price_vat, $field['decimals']),$field['decimals'], $field['decimal_symbol'], '');
						}
						break;
					case 'weight':
						$value=$product->weight;
						break;
					case 'manufacturer':
						$value=$product->manufacturer;
						break;
					case 'ean':
						$value=$product->product_gtin;
						break;
					case 'mpn':
						$value=$product->product_mpn;
						break;
					case 'extended1':
					case 'extended2':
					case 'extended3':
					case 'extended4':
					case 'extended5':
					case 'extended6':
						$value=$field['extended_value'];
						break;
				}
				if ($export) {
					$value = str_replace($replace_from, $replace_to, $value);
					$line_export = str_replace('{'.$field['fieldname'].'}', $value, $line_export);
				}
			}
			echo $line_export.$line_delimiter;
			//echo implode($field_delimiter,$fields_array).$line_delimiter;
			$i++;
		}

		if (@$setting['debug_mode']) {
			echo "\n";
			echo "Memory using after component product processing: ".memory_get_usage() . "\n";
			echo "Time after component product processing: ".PX::getExecTime()."\n";
		}

		PX::log($component, $setting['code'], $i);
	}
}

