<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class fileFormatXml {
	/**
	* serverFeeder xml creator function
	*/ 
	function buildOutput( &$component ) {
		$app = JFactory::getApplication();
		$input = $app->input;
		$document = JFactory::getDocument();
		$setting=PX::getServerSetting();

		if ($input->get('debug', false)) {
			$setting['debug_mode']=true;
		}
		if (@$setting['debug_mode']) {
			header('Content-Type: text/plain; charset=utf8');
		} else {
			header('Content-Type: text/xml; charset=utf8');
		}
		if ($input->get('task')=='download') {
			header("Content-Disposition: attachment; filename=".$setting['code'].".xml");
		}

		ob_implicit_flush(true);
		ob_end_flush();

		//print_r($setting);
		if (@$setting['debug_mode']) {
			echo "Memory using before component product processing: ".memory_get_usage() . "\n";
			echo "Time before component product processing: ".PX::getExecTime()."\n";
		}

		$component->prepareExport();
		$this->xml_start='';
		$this->xml_start .= $setting['format_params']['xml_header']."\n";
		if ($setting['format_params']['xml_root_element']) {
			$this->xml_start .= "<".$setting['format_params']['xml_root_element'].">\n";
		}

		$this->xml_start = str_replace('{SITENAME}', $app->getCfg('sitename'), $this->xml_start);
		$this->xml_start = str_replace('{SITEURL}', JURI::root(), $this->xml_start);

		echo $this->xml_start;

		$i=0;
		while ($product=$component->getRecord()) {
//			print_r($product);
			$this->xml_product = '';
			$this->xml_product .= "\t<".$setting['format_params']['xml_product_element'].">\n";
			foreach ($setting['fields'] as $key=>$field) {
				$export=true;
				$value='';
				switch ($key) {
					case 'product_id':
						$value=$product->id;
						break;
					case 'product_sku':
						$value=$product->code;
						break;
					case 'url':
						switch ($field['url_type']) {
							case 'sef':
								$value=$product->sef_url;
								break;
							case 'nonsef':
								$value=$product->nonsef_url;
								break;
						}
						break;
					case 'name':
						$value=$product->name;
						break;
					case 'short_description':
						if ($field['strip_tags']) {
							$value=strip_tags($product->short_description);
						} else {
							$value=$product->short_description;
						}
						break;
					case 'long_description':
						if ($field['max_length']) {
							$value=mb_substr($product->long_description, 0, $field['max_length'], 'UTF-8');
						} else {
							$value=$product->long_description;
						}
						break;
					case 'image':
						$value=$product->image;
						$value = str_replace(' ','%20',$value);
						break;
					case 'stock':
						$value=$product->stock;
						break;
					case 'currency':
						$value=$field['currency_value'];
						break;
					case 'category':
						if ($product->category) {
							$value=str_replace("\t", $field['delimiter'], $product->category);
//						} else {
//							if (is_numeric($product->category_id)) {
//								$value=str_replace("\t", $field['delimiter'], $product->category);
//							} else {
//								if ($product->category_id) {
//									$value=str_replace("\t", $field['delimiter'], $component->getCategoryName($product->category_id, $field['category_list']));
//								}
//							}
						}
						break;
					case 'category_alternative':
						$value=$product->category_alternative;
						break;
					case 'delivery_date':
						$value=$product->availability;
						break;
					case 'vat':
						switch ($field['format']) {
							case 'percent':
								$value=$product->vat*100;
								break;
							case 'decimal':
								$value=$product->vat;
								break;
							case 'decimalext':
								$value=$product->vat+1;
								break;
						}
						break;
					case 'price':
						if (empty($field['decimal_symbol'])) $field['decimal_symbol']='.';
						if ($field['decimal_symbol']=='.')
							$value = round($product->price, $field['decimals']);
						else
							$value = number_format(round($product->price, $field['decimals']),$field['decimals'], $field['decimal_symbol'], '');
						if (@$field['currency']==1) $value.=' '.$product->currency;
						break;
					case 'price_vat':
						if (empty($field['decimal_symbol'])) $field['decimal_symbol']='.';
						if ($field['decimal_symbol']=='.')
							$value = round($product->price_vat, $field['decimals']);
						else
							$value = number_format(round($product->price_vat, $field['decimals']),$field['decimals'], $field['decimal_symbol'], '');
						if (@$field['currency']==1) $value.=' '.$product->currency;
						break;
					case 'orig_price':
						if (empty($field['decimal_symbol'])) $field['decimal_symbol']='.';
						if ($product->price != $product->orig_price) {
							if (@$field['decimal_symbol']=='.')
								$value = round($product->orig_price, $field['decimals']);
							else
								$value = number_format(round($product->orig_price, $field['decimals']),$field['decimals'], $field['decimal_symbol'], '');
							if (@$field['currency']==1) $value.=' '.$product->currency;
						}
						break;
					case 'orig_price_vat':
						if (empty($field['decimal_symbol'])) $field['decimal_symbol']='.';
						if ($product->price != $product->orig_price) {
							if (@$field['decimal_symbol']=='.')
								$value = round($product->orig_price_vat, $field['decimals']);
							else
								$value = number_format(round($product->orig_price_vat, $field['decimals']),$field['decimals'], $field['decimal_symbol'], '');
							if (@$field['currency']==1) $value.=' '.$product->currency;
						}
						break;
					case 'weight':
						$value=$product->weight;
						break;
					case 'manufacturer':
						$value=$product->manufacturer;
						break;
					case 'ean':
						$value=$product->product_gtin;
						break;
					case 'mpn':
						$value=$product->product_mpn;
						break;
					case 'extended1':
					case 'extended2':
					case 'extended3':
					case 'extended4':
					case 'extended5':
					case 'extended6':
						$value=$field['extended_value'];
						break;
				}
				if ($export && ($value!='' || $value===0)) {
					if (@$field['cdata']==1) {
						$value='<![CDATA['.$value.']]>';
					} else {
						$value=htmlspecialchars($value);
					}

					$this->xml_product .= "\t\t".'<'.$field['fieldname'].'>'.$value.'</'.$field['fieldname'].'>'."\n";
				}
			}
			$this->xml_product .= "\t</".$setting['format_params']['xml_product_element'].">\n";
			echo $this->xml_product;
			$i++;
		}
		if ($setting['format_params']['xml_root_element']) {
			list($xml_end)=explode(' ', $setting['format_params']['xml_root_element']);
			$this->xml_end = "</".$xml_end.">\n";
		}
		$this->xml_end .= $setting['format_params']['xml_footer']."\n";
		echo $this->xml_end;

		if (@$setting['debug_mode']) {
			echo "\n";
			echo "Memory using after component product processing: ".memory_get_usage() . "\n";
			echo "Time after component product processing: ".PX::getExecTime()."\n";
		}

		PX::log($component, $setting['code'], $i);
	}
}

