<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

function ProductxportBuildRoute(&$query)
{
   $segments = array();
   
   return $segments;
}

function ProductxportParseRoute($segments)
{
	global $mainframe;

	$vars	= array();

	switch (count($segments)) {
		case 0:
			break;
		default:
			$real_segments = explode('/',$_SERVER["REQUEST_URI"]);
			//array_shift($real_segments);
			//array_shift($real_segments);
			//array_shift($real_segments);
			//print_r($real_segments);
			$server_index = array_search('server', $real_segments);
			$code_index = $server_index + 1;
			$real_code = $real_segments[$code_index];

			$vars['view'] = $segments[0];
			switch ($vars['view']) {
				case 'server':
					$vars['code']=$segments[1];
					if ((strpos($real_code, $segments[1]) === 0) && ($real_code!=$segments[1])) {
						$vars['code']=$real_code;
					}
					break;
				case 'component':
					$vars['cron']=$segments[1];
					break;
				case 'test':
					break;
			}
			$vars['tmpl']="component";
			break;
	}

	return $vars;
}
