<?php
/*------------------------------------------------------------------------
# com_productxport - component which export data from your eshop to product search aggregators
# ------------------------------------------------------------------------
# author    David Zirhut
# copyright Copyright (c) 2011 David Zirhut. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://productxport.linelab.org
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if (!JRequest::getVar('view', null)) {
	die('<div style="color: red; font-weight: bold; text-align: center;">URL ERROR<br />view is not defined!!!<br />Go to: administration of productXport :: setting for E-shop :: VirtueMart 1.1 <br />and save the current setting again</div>');
}

$app=JFactory::getApplication();
$jinput = JFactory::getApplication()->input;

// Require the base controller
require_once (JPATH_COMPONENT_ADMINISTRATOR.'/classes/common.php');
PX::init();

$componentPluginName = PX::getGlobalSetting()->get('COMPONENT_PLUGIN');
$componentPluginCode = PX::name2code($componentPluginName);

PX::setComponentSetting(JPATH_ROOT.'/components/com_productxport/plg_component/'.$componentPluginName.'/setting.ini', 'INI');

switch ($jinput->get('view', null, 'string')) {
	case 'server':
		if ($jinput->get('code', null, 'string')) {
			define('EXPORT_CODE', $jinput->get('code', null, 'string'));
		}

		if (defined('EXPORT_CODE')) {

			if (file_exists(JPATH_ROOT.'/components/com_productxport/plg_component/'.$componentPluginName.'/'.$componentPluginName.'.php')) {
				require_once(JPATH_ROOT.'/components/com_productxport/plg_component/'.$componentPluginName.'/'.$componentPluginName.'.php');

				// Create the plugin object
				$classname	= 'plgComponent'.ucfirst($componentPluginCode);
				$component = new $classname( );
			} else {
				die('Export named: '.$componentPluginName.' is not available!');
			}

			PX::setServer(EXPORT_CODE);

			$serverSetting = PX::getServerSetting();
			$formatClassName='fileFormat'.ucfirst($serverSetting['format']);
			$export = new $formatClassName;

			# MAIN METHOD TO EXPORT DATA
			$export->buildOutput($component);
			exit;

		} else {
			die("Server code must be defined in the URL!");
		}
		break;
	case 'test':
		header('Content-Type: text/plain');
		echo "[OK]";
		exit;
		break;
	case 'check_includes':
		header('Content-Type: text/plain; charset=utf8');
		PX::checkIncludes();
		exit;
	case 'log':
		header('Content-Type: text/plain');
		$limit = $jinput->get('limit', 0);
		echo $componentPluginName.':'.PX::getVersion().':'.ini_get('memory_limit').':'.ini_get('max_execution_time').":".PX::enc(base64_encode($app->getCfg('mailfrom')))."\n";
		echo PX::getLogs($limit);
		exit;
		break;
}

